
/* c206.c **********************************************************}
{* Téma: Dvousměrně vázaný lineární seznam
**
**                   Návrh a referenční implementace: Bohuslav Křena, říjen 2001
**                            Přepracované do jazyka C: Martin Tuček, říjen 2004
**                                            Úpravy: Bohuslav Křena, říjen 2016
**
** Implementujte abstraktní datový typ dvousměrně vázaný lineární seznam.
** Užitečným obsahem prvku seznamu je hodnota typu int.
** Seznam bude jako datová abstrakce reprezentován proměnnou
** typu tDLList (DL znamená Double-Linked a slouží pro odlišení
** jmen konstant, typů a funkcí od jmen u jednosměrně vázaného lineárního
** seznamu). Definici konstant a typů naleznete v hlavičkovém souboru c206.h.
**
** Vaším úkolem je implementovat následující operace, které spolu
** s výše uvedenou datovou částí abstrakce tvoří abstraktní datový typ
** obousměrně vázaný lineární seznam:
**
**      DLInitList ...... inicializace seznamu před prvním použitím,
**      DLDisposeList ... zrušení všech prvků seznamu,
**      DLInsertFirst ... vložení prvku na začátek seznamu,
**      DLInsertLast .... vložení prvku na konec seznamu, 
**      DLFirst ......... nastavení aktivity na první prvek,
**      DLLast .......... nastavení aktivity na poslední prvek, 
**      DLCopyFirst ..... vrací hodnotu prvního prvku,
**      DLCopyLast ...... vrací hodnotu posledního prvku, 
**      DLDeleteFirst ... zruší první prvek seznamu,
**      DLDeleteLast .... zruší poslední prvek seznamu, 
**      DLPostDelete .... ruší prvek za aktivním prvkem,
**      DLPreDelete ..... ruší prvek před aktivním prvkem, 
**      DLPostInsert .... vloží nový prvek za aktivní prvek seznamu,
**      DLPreInsert ..... vloží nový prvek před aktivní prvek seznamu,
**      DLCopy .......... vrací hodnotu aktivního prvku,
**      DLActualize ..... přepíše obsah aktivního prvku novou hodnotou,
**      DLSucc .......... posune aktivitu na další prvek seznamu,
**      DLPred .......... posune aktivitu na předchozí prvek seznamu, 
**      DLActive ........ zjišťuje aktivitu seznamu.
**
** Při implementaci jednotlivých funkcí nevolejte žádnou z funkcí
** implementovaných v rámci tohoto příkladu, není-li u funkce
** explicitně uvedeno něco jiného.
**
** Nemusíte ošetřovat situaci, kdy místo legálního ukazatele na seznam 
** předá někdo jako parametr hodnotu NULL.
**
** Svou implementaci vhodně komentujte!
**
** Terminologická poznámka: Jazyk C nepoužívá pojem procedura.
** Proto zde používáme pojem funkce i pro operace, které by byly
** v algoritmickém jazyce Pascalovského typu implemenovány jako
** procedury (v jazyce C procedurám odpovídají funkce vracející typ void).
**/

#include "c206.h"

int solved;
int errflg;

void DLError() {
/*
** Vytiskne upozornění na to, že došlo k chybě.
** Tato funkce bude volána z některých dále implementovaných operací.
**/
    printf("*ERROR* The program has performed an illegal operation.\n");
    errflg = TRUE;             /* globální proměnná -- příznak ošetření chyby */
    return;
}

void DLInitList(tDLList* L) {
/*
** Provede inicializaci seznamu L před jeho prvním použitím (tzn. žádná
** z následujících funkcí nebude volána nad neinicializovaným seznamem).
** Tato inicializace se nikdy nebude provádět nad již inicializovaným
** seznamem, a proto tuto možnost neošetřujte. Vždy předpokládejte,
** že neinicializované proměnné mají nedefinovanou hodnotu.
**/
    //nově inicializovaný seznam má všechny tři ukazatele nastavené na hodnotu NULL
    L->Act = NULL;
    L->First = NULL;
    L->Last = NULL;
}

void DLDisposeList(tDLList* L) {
/*
** Zruší všechny prvky seznamu L a uvede seznam do stavu, v jakém
** se nacházel po inicializaci. Rušené prvky seznamu budou korektně
** uvolněny voláním operace free. 
**/
    //mazat budeme od posledního prvku
    tDLElemPtr act = L->Last; //uloží si adresu posledného prvku
    tDLElemPtr help;

    do {
        help = act->lptr; //uloží si prvek předcházející prvek mazaný
        free(act); //uvolníme aktuálně poslední prvek
        act = help; //jako aktuální si označíme předposlední prvek (resp. poslední)
    } while(act != NULL);

    L->First = NULL;
    L->Last = NULL;
    L->Act = NULL;
}

void DLInsertFirst(tDLList* L, int val) {
/*
** Vloží nový prvek na začátek seznamu L.
** V případě, že není dostatek paměti pro nový prvek při operaci malloc,
** volá funkci DLError().
**/
    tDLElemPtr new = malloc(sizeof(struct tDLElem)); //pokusíme se v paměti vytvořit místo pro nový prvek

    if(new == NULL){ //pokud se vytvoření nepovedlo, volá DLError()
        DLError();
    } else {
        if(L->First == NULL){ //pokud ještě seznam žádný prvek neobsahuje, bude nový prvek i posledním
            L->Last = new;
        } else { //pokud už obsahuje, bude nový prvek předchůdce původního prvního prvku
            L->First->lptr = new;
        }

        //naplní nový prvek
        new->data = val;
        new->lptr = NULL;
        new->rptr = L->First;

        L->First = new; //jako první si označí nový prvek
    }
}

void DLInsertLast(tDLList* L, int val) {
/*
** Vloží nový prvek na konec seznamu L (symetrická operace k DLInsertFirst).
** V případě, že není dostatek paměti pro nový prvek při operaci malloc,
** volá funkci DLError().
**/
    tDLElemPtr new = malloc(sizeof(struct tDLElem)); //pokusíme se v paměti vytvořit místo pro nový prvek

    if(new == NULL){ //pokud se vytvoření nepovede, volá DLError()
        DLError();
    } else {
        if(L->First == NULL){ //pokud ještě seznam žádný prvek neobsahuje, bude nový prvek i prvkem prvním
            L->First = new;
        } else { //pokud už obsahujem, bude nový prvek následník původního posledního prvku
            L->Last->rptr = new;
        }

        //naplní nový prvek
        new->data = val;
        new->lptr = L->Last;
        new->rptr = NULL;

        L->Last = new; //jako poslední si označí nový prvek
    }
}

void DLFirst(tDLList* L) {
/*
** Nastaví aktivitu na první prvek seznamu L.
** Funkci implementujte jako jediný příkaz (nepočítáme-li return),
** aniž byste testovali, zda je seznam L prázdný.
**/
    L->Act = L->First;
}

void DLLast(tDLList* L) {
/*
** Nastaví aktivitu na poslední prvek seznamu L.
** Funkci implementujte jako jediný příkaz (nepočítáme-li return),
** aniž byste testovali, zda je seznam L prázdný.
**/
    L->Act = L->Last;
}

void DLCopyFirst(tDLList* L, int* val) {
/*
** Prostřednictvím parametru val vrátí hodnotu prvního prvku seznamu L.
** Pokud je seznam L prázdný, volá funkci DLError().
**/
    if(L->First == NULL){ //pokud je seznam prázdný, volá DLError()
        DLError();
    } else { //pokud obsahuje nějaký prvek, vrátí hodnotu prvního
        *val = L->First->data;
    }
}

void DLCopyLast(tDLList* L, int* val) {
/*
** Prostřednictvím parametru val vrátí hodnotu posledního prvku seznamu L.
** Pokud je seznam L prázdný, volá funkci DLError().
**/
    if(L->Last == NULL){ //pokud je seznam prázdný, volá DLError()
        DLError();
    } else { //pokud obsahuje nějaký prvek, vrátí hodnotu posledního
        *val = L->Last->data;
    }
}

void DLDeleteFirst(tDLList* L) {
/*
** Zruší první prvek seznamu L. Pokud byl první prvek aktivní, aktivita 
** se ztrácí. Pokud byl seznam L prázdný, nic se neděje.
**/
    tDLElemPtr deleted = L->First;

    if(deleted != NULL){ //seznam obsahuje alespoň jeden prvek
        if(deleted == L->Last){ //pokud byl první i poslední -> jediný prvek seznamu
            L->Last = NULL;
        } else { //pokud nebyl jediný, následuje za ním další, který se stává prvním -> jeho lptr bude NULL
            deleted->rptr->lptr = NULL;
        }

        if(deleted == L->Act){ //pokud je mazaný prvek i aktivní, ukazatel Act je vynulován
            L->Act = NULL;
        }

        L->First = deleted->rptr; //nastav první prvek seznamu na následníka mazaného (může být i NULL)

        free(deleted);
    }
}

void DLDeleteLast(tDLList* L) {
/*
** Zruší poslední prvek seznamu L. Pokud byl poslední prvek aktivní,
** aktivita seznamu se ztrácí. Pokud byl seznam L prázdný, nic se neděje.
**/
    tDLElemPtr deleted = L->Last;

    if(deleted != NULL){ //seznam obsahuje alespoň jeden prvek
        if(deleted == L->First){ //pokud se jedná o jediný prvek seznamu
            L->First = NULL;
        } else { //pokud nebyl jediný, je před ním další, který se stává posledním -> jeho rptr bude NULL
            deleted->lptr->rptr = NULL;
        }

        if(deleted == L->Act){ //pokud je mazaný prvek i aktivní, ukazatel Act je vynulován
            L->Act = NULL;
        }

        L->Last = deleted->lptr; //nastav poslední prvek seznamu na předchůdce mazaného (může být i NULL)

        free(deleted);
    }
}

void DLPostDelete(tDLList* L) {
/*
** Zruší prvek seznamu L za aktivním prvkem.
** Pokud je seznam L neaktivní nebo pokud je aktivní prvek
** posledním prvkem seznamu, nic se neděje.
**/
    if(L->Act != NULL && L->Act != L->Last){ //pokud je nějaký prvek aktivní a není to poslední prvek
        tDLElemPtr deleted = L->Act->rptr;

        //předchůdce mazaného
        L->Act->rptr = deleted->rptr;

        //následovník mazaného
        if(deleted->rptr != NULL){ //pokud není mazaný prvek poslední
            deleted->rptr->lptr = L->Act;
        }

        if(deleted == L->Last){ //pokud je mazaný prvek poslední, nový poslední prvek je prvek aktivní
            L->Last = L->Act;
        }

        free(deleted);
    }
}

void DLPreDelete(tDLList* L) {
/*
** Zruší prvek před aktivním prvkem seznamu L .
** Pokud je seznam L neaktivní nebo pokud je aktivní prvek
** prvním prvkem seznamu, nic se neděje.
**/
    if(L->Act != NULL && L->Act != L->First){
        tDLElemPtr deleted = L->Act->lptr;

        //předchůdce mazaného
        if(deleted->lptr != NULL){ //pokud není mazaný prvek první
            deleted->lptr->rptr = L->Act;
        }

        //následovník mazaného
        L->Act->lptr = deleted->lptr;

        if(deleted == L->First){ //pokud je mazaný prvek první, nový první prvek je prvek aktivní
            L->First = L->Act;
        }

        free(deleted);
    }
}

void DLPostInsert(tDLList* L, int val) {
/*
** Vloží prvek za aktivní prvek seznamu L.
** Pokud nebyl seznam L aktivní, nic se neděje.
** V případě, že není dostatek paměti pro nový prvek při operaci malloc,
** volá funkci DLError().
**/
    if(L->Act != NULL){ //pokud je seznam aktivní
        tDLElemPtr new = malloc(sizeof(struct tDLElem)); //pokusí se vytvořit místo v paměti pro nový prvek

        if(new == NULL){ //pokud se vytvoření nepovedlo
            DLError();
        } else {
            //uloží si adresy prvků před a za přidávaným
            tDLElemPtr before, after;
            before = L->Act;
            after = L->Act->rptr;

            //vloží prvek mezi aktivní prvek a jeho následníka
            before->rptr = new;
            if(after == NULL){ //přidáváme na konec seznamu
                L->Last = new;
            } else {
                after->lptr = new;
            }

            //nastaví ukazatele nového prvku a naplní jej daty
            new->data = val;
            new->lptr = before;
            new->rptr = after;
        }
    }
}

void DLPreInsert(tDLList* L, int val) {
/*
** Vloží prvek před aktivní prvek seznamu L.
** Pokud nebyl seznam L aktivní, nic se neděje.
** V případě, že není dostatek paměti pro nový prvek při operaci malloc,
** volá funkci DLError().
**/
    if(L->Act != NULL){ //pokud je seznam aktivní
        tDLElemPtr new = malloc(sizeof(struct tDLElem)); //pokusí se vytvořit místo v paměti pro nový prvek

        if(new == NULL){ //pokud se vytvoření nepovedlo
            DLError();
        } else {
            //uloží si adresy prvků před a za přidávaným
            tDLElemPtr before, after;
            before = L->Act->lptr;
            after = L->Act;

            //vloží prvek mezi aktivní prvek a jeho předchůdce
            if(before == NULL){ //přidáváme na začátek seznamu
                L->First = new;
            } else {
                before->rptr = new;
            }
            after->lptr = new;

            //nastaví ukazatele nového prvku a naplní jej daty
            new->data = val;
            new->lptr = before;
            new->rptr = after;
        }
    }
}

void DLCopy(tDLList* L, int* val) {
/*
** Prostřednictvím parametru val vrátí hodnotu aktivního prvku seznamu L.
** Pokud seznam L není aktivní, volá funkci DLError ().
**/
    if(L->Act == NULL){ //pokud seznam není aktivní, volá DLError()
        DLError();
    } else {
        *val = L->Act->data;
    }
}

void DLActualize(tDLList* L, int val) {
/*
** Přepíše obsah aktivního prvku seznamu L.
** Pokud seznam L není aktivní, nedělá nic.
**/
    if(L->Act != NULL){ //pokud je seznam aktivní, nastaví hodnotu aktivního prvku
        L->Act->data = val;
    }
}

void DLSucc(tDLList* L) {
/*
** Posune aktivitu na následující prvek seznamu L.
** Není-li seznam aktivní, nedělá nic.
** Všimněte si, že při aktivitě na posledním prvku se seznam stane neaktivním.
**/
    if(L->Act != NULL){ //pokud je seznam aktivní, posune aktivitu na prvek následující po aktivním
        L->Act = L->Act->rptr;
    }
}


void DLPred(tDLList* L) {
/*
** Posune aktivitu na předchozí prvek seznamu L.
** Není-li seznam aktivní, nedělá nic.
** Všimněte si, že při aktivitě na prvním prvku se seznam stane neaktivním.
**/
    if(L->Act != NULL){ //pokud je seznam aktivní, posune aktivitu na prvek předchzející před aktivním
        L->Act = L->Act->lptr;
    }
}

int DLActive(tDLList* L) {
/*
** Je-li seznam L aktivní, vrací nenulovou hodnotu, jinak vrací 0.
** Funkci je vhodné implementovat jedním příkazem return.
**/
    return L->Act != NULL;
}

/* Konec c206.c*/
