/*
*Student login: xvodaz01
*/
#include <cstdio>

#ifdef DLLDIR_EX
#define DLLDIR  __declspec(dllexport) 
#else
#define DLLDIR  __declspec(dllimport) 
#endif 

#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL.h>
#include <SDL_image.h>

float part1Top[][2] = {
	{ 0.0f, 0.8f },
	{ 0.07f, 0.7f },
	{ 0.1f, 0.6f },
	{ -0.1f, 0.6f },
	{ -0.07f, 0.7f }
};

float part2Top[][2] = {
	{ -0.6f, 0.6f },
	{ 0.6f, 0.6f },
	{ 0.4f, 0.4f },
	{ -0.4f, 0.4f }
};

float part3Top[][2] = {
	{ 0.1f, 0.4f },
	{ 0.07f, 0.1f },
	{ -0.07f, 0.1f },
	{ -0.1f, 0.4f }
};

float part4Top[][2] = {
	{ 0.07f, 0.1f },
	{ 0.2f, 0.0f },
	{ -0.2f, 0.0f },
	{ -0.07f, 0.1f }
};

float part1Bottom[][2] = {
	{ 0.0f, 0.75f },
	{ 0.05f, 0.7f },
	{ 0.08f, 0.55f },
	{ -0.08f, 0.55f },
	{ -0.05f, 0.7f }
};

float part2Bottom[][2] = {
	{ -0.55f, 0.55f },
	{ 0.55f, 0.55f },
	{ 0.35f, 0.45f },
	{ -0.35f, 0.45f }
};

float part3Bottom[][2] = {
	{ 0.08f, 0.45f },
	{ 0.06f, 0.1f },
	{ -0.06f, 0.1f },
	{ -0.08f, 0.45f }
};

float part4Bottom[][2] = {
	{ 0.07f, 0.1f },
	{ 0.2f, 0.0f },
	{ -0.2f, 0.0f },
	{ -0.07f, 0.1f }
};

float xoff = 0.0f;
float yoff = 0.4f;

void displayPolygon(float data[][2], float z, int noOfPoints) {
	glBegin(GL_POLYGON);
	glColor3f(1.0f, 1.0f, 1.0f);
	for (int i = 0; i < noOfPoints; i++) {
		glVertex3f(data[i][0], data[i][1] - yoff, z);
	}
	glEnd();

	glBegin(GL_LINE_LOOP);
	glColor3f(0.0f, 0.0f, 0.0f);
	for (int i = 0; i < noOfPoints; i++) {
		glVertex3f(data[i][0], data[i][1] - yoff, z);
	}
	glEnd();
}

void joinPolygons(float polyA[][2], float zA, float polyB[][2], float zB, int noOfPoints) {
	glBegin(GL_POLYGON);
	glColor3f(1.0f, 1.0f, 1.0f);
	for (int i = 0; i < noOfPoints; i++) {
		int mod = (i + 1) % noOfPoints;

		glVertex3f(polyA[i][0], polyA[i][1] - yoff, zA);
		glVertex3f(polyB[i][0], polyB[i][1] - yoff, zB);
		glVertex3f(polyB[mod][0], polyB[mod][1] - yoff, zB);
		glVertex3f(polyA[mod][0], polyA[mod][1] - yoff, zA);
	}
	glEnd();

	glBegin(GL_LINE_LOOP);
	glColor3f(0.0f, 0.0f, 0.0f);
	for (int i = 0; i < noOfPoints; i++) {
		int mod = (i + 1) % noOfPoints;

		glVertex3f(polyA[i][0], polyA[i][1] - yoff, zA);
		glVertex3f(polyB[i][0], polyB[i][1] - yoff, zB);
		glVertex3f(polyB[mod][0], polyB[mod][1] - yoff, zB);
		glVertex3f(polyA[mod][0], polyA[mod][1] - yoff, zA);
	}
	glEnd();
}

float topZ = 0.07f;
float bottomZ = 0.0f;

extern "C" DLLDIR void  printplane() {
	
	
	displayPolygon(part1Top, topZ, 5);
	displayPolygon(part2Top, topZ, 4);
	displayPolygon(part3Top, topZ, 4);
	displayPolygon(part4Top, topZ, 4);

	displayPolygon(part1Bottom, bottomZ, 5);
	displayPolygon(part2Bottom, bottomZ, 4);
	displayPolygon(part3Bottom, bottomZ, 4);
	displayPolygon(part4Bottom, bottomZ, 4);

	joinPolygons(part1Top, topZ, part1Bottom, bottomZ, 5);
	joinPolygons(part2Top, topZ, part2Bottom, bottomZ, 4);
	joinPolygons(part3Top, topZ, part3Bottom, bottomZ, 4);
	joinPolygons(part4Top, topZ, part4Bottom, bottomZ, 4);
}

extern "C" DLLDIR void  printcube() {
	glBegin(GL_QUADS);                // Begin drawing the color cube with 6 quads
									  // Top face (y = 1.0f)
									  // Define vertices in counter-clockwise (CCW) order with normal pointing out
	glColor3f(0.0f, 1.0f, 0.0f);     // Green
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	// Bottom face (y = -1.0f)
	glColor3f(1.0f, 0.5f, 0.0f);     // Orange
	glVertex3f(1.0f, -1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	// Front face  (z = 1.0f)
	glColor3f(1.0f, 0.0f, 0.0f);     // Red
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	// Back face (z = -1.0f)
	glColor3f(1.0f, 1.0f, 0.0f);     // Yellow
	glVertex3f(1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	// Left face (x = -1.0f)
	glColor3f(0.0f, 0.0f, 1.0f);     // Blue
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	// Right face (x = 1.0f)
	glColor3f(1.0f, 0.0f, 1.0f);     // Magenta
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glEnd();  // End of drawing color-cube
}

