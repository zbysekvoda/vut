﻿/*Template for IPA project, 3D game
*Author: Tomas Goldmann, igoldmann@fit.vutbr.cz, most source code from Anton Claes (https://www.youtube.com/watch?v=a3MACw5hB2Q)
*
*STUDENT LOGIN: xvodaz01
*/

#include "sdl_game.h"

using namespace std;

SDL_Window *screen;

typedef struct {
	double x;
	double y;
	double z;
} vect3d;

typedef void(*Ipa_algorithm)();
typedef void(*Printcube)();
typedef void(*Printplane)();

Printcube printcube;
Printplane printplane;
HINSTANCE hInstLibrary;

char flying = 0;

// STATE VARIABLES
double alpha = 0;
double beta = 0;
double gamma = 0;

double plane_v_scalar = 50.0f;
vect3d plane_v = { 0.0f, plane_v_scalar, 0.0f};
vect3d plane_pos = {
	150.0f,
	150.0f,
	20.0f
};

// PARAMETERS
double plane_m = 500;
double angle_delta = 2;
double F_T_scalar = 20000.0f; // thrust in Newtons

// CONSTANTS
double g = 9.81;
double ro = 0.0015;
#define PI 3.14159265359f
//Fg
vect3d F_G = { 0, 0, -plane_m * g };

vect3d multiplyVect3dByScalar(vect3d a, double s) {
	return {
		a.x * s,
		a.y * s,
		a.z * s
	};
}

vect3d asm_nextPos(vect3d plane_pos, double alpha, double beta) {
	double C2 = 2.0;
	double C8 = 8.0;
	double C90 = 90.0;
	double C100 = 100.0;
	double C180 = 180.0;
	double C200 = 200.0;
	double alphaRad;
	double betaRad;
	double sinAlpha;
	double sinAlphaPlusPi;
	double sinBeta;
	double cosAlpha;
	double cosBeta;
	double abs;
	double F_L_scalar;
	double F_D_scalar;
	double tr;

	vect3d F;
	vect3d F_T;
	vect3d F_L;
	vect3d F_D;
	vect3d n_L_n;
	vect3d n_D_n;
	vect3d n_v_n;
	vect3d v_delta;
	vect3d pos_delta;

	__asm {
		// alphaRad = alpha * PI / 180.0f;
			fld alpha
			fldpi
			fmul
			fdiv C180
			fstp alphaRad
		
		// betaRad = beta * PI / 90.0f;
			fld beta
			fldpi
			fmul
			fdiv C90
			fstp betaRad
		
		// sin(alphaRad)
			fld alphaRad
			fsin
			fstp sinAlpha
		
		// sin(betaRad)
			fld betaRad
			fsin
			fstp sinBeta
		
		// cos(alphaRad)
			fld alphaRad
			fcos
			fstp cosAlpha
		
		// cos(betaRad)
			fld betaRad
			fcos
			fstp cosBeta
		
		// sin(alphaRad + PI)
			fld alphaRad
			fldpi
			fadd 
			fsin
			fstp sinAlphaPlusPi
		
		////////// F_T ///////////
		// F_T_scalar * sin(alphaRad) * sin(betaRad)
			fld F_T_scalar
			fmul sinAlpha
			fmul sinBeta
			fstp F_T.x
		
		// F_T_scalar * cos(alphaRad)
			fld F_T_scalar
			fmul cosAlpha
			fstp F_T.y
		
		// F_T_scalar * sin(alphaRad) * cos(betaRad)
			fld F_T_scalar
			fmul sinAlpha
			fmul cosBeta
			fstp F_T.z
		
		////////// F_L ///////////
		// n_L_n
		// crossProductVect3d(F_T, { 1, 0, 0 })
			fldz
			fstp n_L_n.x

			fld F_T.z
			fstp n_L_n.y

			fld F_T.y
			fchs
			fstp n_L_n.z

		// normalizeVect3d(crossProductVect3d(F_T, { 1, 0, 0 }))
			fldz
			fld n_L_n.y
			fmul n_L_n.y
			fld n_L_n.z
			fmul n_L_n.z
			fadd
			fadd
			fsqrt
			fstp abs

			fld n_L_n.x
			fdiv abs
			fld n_L_n.y
			fdiv abs
			fld n_L_n.z
			fdiv abs
			fstp n_L_n.z
			fstp n_L_n.y
			fstp n_L_n.x

		// F_L_scalar = 8 * ro * absVect3d(plane_v)^2 * sin(alphaRad + PI);
			fld plane_v.x
			fmul plane_v.x
			fld plane_v.y
			fmul plane_v.y
			fld plane_v.z
			fmul plane_v.z
			fadd
			fadd
			fsqrt
			fst tr
			fmul tr
			fmul ro
			fld C8
			fld sinAlphaPlusPi
			fmul
			fmul
			fstp F_L_scalar
		
		// F_L = multiplyVect3dByScalar(n_L_n, F_L_scalar)
			fld n_L_n.x
			fmul F_L_scalar
			fld n_L_n.y
			fmul F_L_scalar
			fld n_L_n.z
			fmul F_L_scalar
			fstp F_L.z
			fstp F_L.y
			fstp F_L.x
		
		////////// F_D ///////////
		// n_D_n
		// multiplyVect3dByScalar(F_T,-1)
			fld F_T.x
			fchs
			fld F_T.y
			fchs
			fld F_T.z
			fchs
			fstp n_D_n.z
			fstp n_D_n.y
			fstp n_D_n.x
		
		// normalizeVect3d(multiplyVect3dByScalar(F_T,-1))
			fld n_D_n.x
			fmul n_D_n.x
			fld n_D_n.y
			fmul n_D_n.y
			fld n_D_n.z
			fmul n_D_n.z
			fadd
			fadd
			fsqrt
			fstp abs

			fld n_D_n.x
			fdiv abs
			fld n_D_n.y
			fdiv abs
			fld n_D_n.z
			fdiv abs
			fstp n_D_n.z
			fstp n_D_n.y
			fstp n_D_n.x
		
		// F_D_scalar = ro * absVect3d(plane_v)^2 * cos(alphaRad)
			fld plane_v.x
			fmul plane_v.x
			fld plane_v.y
			fmul plane_v.y
			fld plane_v.z
			fmul plane_v.z
			fadd
			fadd
			fsqrt
			fst tr
			fmul tr
			fmul ro
			fld cosAlpha
			fmul
			fstp F_D_scalar
		
		// F_D = multiplyVect3dByScalar(n_D_n, F_D_scalar)
			fld n_D_n.x
			fmul F_D_scalar
			fld n_D_n.y
			fmul F_D_scalar
			fld n_D_n.z
			fmul F_D_scalar
			fstp F_D.z
			fstp F_D.y
			fstp F_D.x
		
		////////// F ///////////
		// F = sumVect3d(F_T, F_L)
		// F = sumVect3d(F, F_D)
		// F = sumVect3d(F, F_G)
			fld F_T.x
			fadd F_L.x
			fadd F_D.x
			fadd F_G.x
			fstp F.x

			fld F_T.y
			fadd F_L.y
			fadd F_D.y
			fadd F_G.y
			fstp F.y

			fld F_T.z
			fadd F_L.z
			fadd F_D.z
			fadd F_G.z
			fstp F.z

		////////// v_delta ///////////
		// v_delta = divideVect3dByScalar(F, plane_m)
			fld F.x
			fdiv plane_m
			fld F.y
			fdiv plane_m
			fld F.z
			fdiv plane_m
			fstp v_delta.z
			fstp v_delta.y
			fstp v_delta.x

		// plane_v = sumVect3d(plane_v, v_delta)
			fld plane_v.x
			fadd v_delta.x
			fstp plane_v.x

			fld plane_v.y
			fadd v_delta.y
			fstp plane_v.y

			fld plane_v.z
			fadd v_delta.z
			fstp plane_v.z

		// speed trim
		// abs = absVect3d(plane_v);
			fld plane_v.x
			fmul plane_v.x	
			fld plane_v.y
			fmul plane_v.y
			fld plane_v.z
			fmul plane_v.z
			fadd
			fadd
			fsqrt
			fstp abs

		// if (abs > 200.0)
			fld abs
			fld C200
			fcomi st(0), st(1)
			fstp tr
			fstp tr

			ja dontcrop

		// normalizeVect3d(plane_v)
			fld plane_v.x
			fld abs
			fdiv
			fld plane_v.y
			fld abs
			fdiv
			fld plane_v.z
			fld abs
			fdiv
			fstp plane_v.z
			fstp plane_v.y
			fstp plane_v.x

		// plane_v = multiplyVect3dByScalar(normalizeVect3d(plane_v), 200.0);
			fld plane_v.x
			fmul C200
			fld plane_v.y
			fmul C200
			fld plane_v.z
			fmul C200
			fstp plane_v.z
			fstp plane_v.y
			fstp plane_v.x

		dontcrop:
			fld plane_v.x
			fdiv C100
			fld plane_v.y
			fdiv C100
			fld plane_v.z
			fdiv C100
			fstp pos_delta.z
			fstp pos_delta.y
			fstp pos_delta.x

		// plane_pos = sumVect3d(plane_pos, pos_delta)
			fld plane_pos.x
			fld pos_delta.x
			fadd
			fld plane_pos.y
			fld pos_delta.y
			fadd
			fld plane_pos.z
			fld pos_delta.z
			fadd
			fstp plane_pos.z
			fstp plane_pos.y
			fstp plane_pos.x
	}

	return plane_pos;
}

//Funkce, ktera se vola v danem intervalu (muzete zde provadet vykreslovani)
Uint32 timer_callbackfunc(Uint32 interval, void *param)
{
	if (!flying) return interval;

	plane_pos = asm_nextPos(plane_pos, alpha, beta);

	return interval;
}

int main(int argc, char** argv)
{
	SDL_Event event;
	bool continuer = true;

	SDL_Init(SDL_INIT_VIDEO);
	atexit(SDL_Quit);

	// TODO remove
	screen = SDL_CreateWindow("IPA - game 2017", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 900, 500, SDL_WINDOW_OPENGL);
	SDL_GL_CreateContext(screen);
	init_ogl();

	//IPA-TODO
	//V pripade, ze budete vyuzivat SDL timer pro casovani, tak si tady nastavte delay
	Uint32 delay = 40;
	SDL_TimerID my_timer_id = SDL_AddTimer(delay, timer_callbackfunc, "parametr");

	HINSTANCE hInstLibrary = LoadLibrary("Student_DLL.dll");

	if (!hInstLibrary)
	{
		std::cout << "DLL Failed To Load!" << std::endl;
		return EXIT_FAILURE;
	}
	else
	{
		//IPA-TODO
		//Zde nacitejte ukazatele na funkce z DLL knihovny
		printplane = (Ipa_algorithm)GetProcAddress(hInstLibrary, "printplane");
	}

	while (continuer)
	{
		if (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_QUIT:
				continuer = false;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym) {
				case SDLK_ESCAPE:
					continuer = false;
					break;
				default:
					break;
				}

				break;
			}
		}
		proceed(event);
	}

	quit_audio();
	FreeLibrary(hInstLibrary);
	return 0;
}

void init_ogl()
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(80, (double)SCREEN_WIDTH / SCREEN_HEIGHT, .1, 1000);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_NORMALIZE);
	glClearColor(0, 0, 0, 0);

	glEnable(GL_LIGHT0);
	GLfloat ambient[] = { 5,5,5,1 };
	GLfloat diffuse[] = { 1,3,3,5 };
	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, ambient);

	int MatSpec[4] = { 1,1,1,1 };
	glMaterialiv(GL_FRONT_AND_BACK, GL_SPECULAR, MatSpec);
	glMateriali(GL_FRONT_AND_BACK, GL_SHININESS, 100);

	SDL_WarpMouseGlobal(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
	SDL_SetRelativeMouseMode(SDL_TRUE);
}

void proceed(SDL_Event event)
{
	static objet terrain;
	static objet fling;
	static objet grass;
	static objet skybox;
	static bool loaded = false;
	Vector camera;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if (!loaded)
	{
		fling.load("fling.obj");
		painter(fling);
		play_shot();

		gest_terrain(LOAD, (char *)"terrain.obj");

		skybox.load("sky.obj");
		loaded = true;
	}

	camera = place_cam(event);
	gest_terrain(RETURN).disp();

	if (printplane)
	{
		double terrain = get_height(plane_pos.x, plane_pos.y, true);
		if (plane_pos.z < terrain) {
			plane_pos.z = terrain;
			plane_v = multiplyVect3dByScalar(plane_v, 0.8);
		}
		
		glPushMatrix();
		glEnable(GL_COLOR_MATERIAL);
		glTranslated(plane_pos.x, plane_pos.y, plane_pos.z);
		glRotated(alpha, 1, 0, 0);
		glRotated(beta, 0, 1, 0);
		glRotated(gamma, 0, 0, 1);
		glScaled(20, 20, 20);
		printplane();
		glPopMatrix();
	}

	glPushMatrix();
	disp_fling(fling, camera);
	glPopMatrix();

	glPushMatrix();
	skybox.disp();
	glPopMatrix();

	//gest_robot(0, camera.origine);
	glFlush();
	SDL_GL_SwapWindow(screen);
}

double get_height(double x, double y, bool interpolated)
{
	static SDL_Surface *heightmap = NULL;
	static bool loaded = false;
	static double tailleZ, tailleX, tailleY;
	double hauteur = 0;
	double minimum = 0;

	int ix = 0, iy = 0;
	double dx = 0, dy = 0;

	Uint32 pxColor1 = 0, pxColor2 = 0, pxColor3 = 0, pxColor4;

	if (!loaded)
	{
		heightmap = IMG_Load("heightmap.png");
		tailleZ = gest_terrain(RETURN).relBoundingBox.coordMax.z - gest_terrain(RETURN).relBoundingBox.coordMin.z;
		/* On part du principe que le terrain commence a 0;0 , donc que la taille X et Y sont max X et max Y*/
		tailleX = gest_terrain(RETURN).relBoundingBox.coordMax.x;
		tailleY = gest_terrain(RETURN).relBoundingBox.coordMax.y;

		loaded = true;
	}

	else//chargement deja fait
	{

		if (x >= 0 && x <= tailleX && y >= 0 && y <= tailleY)//position camera dans le terrain
		{
			x = (x / tailleX) * heightmap->w;           // on cherche la position relative par rapport au terrain, que l'on multiplie par la taille de
			y = (1 - (y / tailleY)) * heightmap->h;     // la heightmap ensuite
														// (1 - .. ) permet de faire une inversion verticale de la heightmap car blender l'inverse

			if (interpolated)//s'il faut interpoler la hauteur en fct des autres sommets, ce qui n'est pas toujours le cas
			{
				/* Pour l'interpolation bilineaire qui permet de connaitre la hauteur sans avoir de pas dus a la heightmap, on procede comme suit :
				-on recupere les quatres pixels qui encadrent la position, et leur valeur en hauteur
				-on recupere les coordonnees entieres de la position, et leurs restes decimaux
				-on applique la formule d'interpolation bilineaire :
				hauteur = ((1-dx) * pxColor1 + dx * pxColor2) * (1-dy) + ((1-dx) * pxColor3 + dx * pxColor4) * dy*/

				ix = (int)x;//position entiere
				iy = (int)y;
				dx = x - ix;//position decimale relative
				dy = y - iy;


				SDL_LockSurface(heightmap);

				/* Pour l'interpolation lineaire de la position, on recupere la position des quatres pixels autours de notre position
				on travaille ac un img en nb, donc les 4 couleurs ont la meme valeur, donc on ne prend qu'une d'entres elles, soit le 1er octet*/
				pxColor1 = getpixel(heightmap, ix, iy);//recup des couleurs
				pxColor2 = getpixel(heightmap, ix + 1, iy);
				pxColor3 = getpixel(heightmap, ix, iy + 1);
				pxColor4 = getpixel(heightmap, ix + 1, iy + 1);
				//recup du 1er octet
				pxColor1 = (*(unsigned char *)(&pxColor1));
				pxColor2 = (*(unsigned char *)(&pxColor2));
				pxColor3 = (*(unsigned char *)(&pxColor3));
				pxColor4 = (*(unsigned char *)(&pxColor4));

				/* On empeche l'interpolation lineaire de nous faire des siennes, c'est a dire de faire monter en trop haut. Pour cela, on definit
				une limite de hauteur, et si elle est depassee par la hauteur d'un des 4 pts, on retourne la valeur minimale des autres.
				Cela pourra eventuellement amener a certains bugs graphiques, a regler plus tard si besoin. */
				//on recupere le minimum
				minimum = pxColor1*tailleZ / 255;
				if (pxColor2*tailleZ / 255 < minimum)//peu etre un peu basic comme methode, mais simple et fonctionnelle
					minimum = pxColor2*tailleZ / 255;
				if (pxColor3*tailleZ / 255 < minimum)
					minimum = pxColor3*tailleZ / 255;
				if (pxColor4*tailleZ / 255 < minimum)
					minimum = pxColor4*tailleZ / 255;
				//maintenant, on verifie que la hauteur max depasse pas la limite
				if (pxColor1*tailleZ / 255 > minimum + .5 ||
					pxColor2*tailleZ / 255 > minimum + .5 ||
					pxColor3*tailleZ / 255 > minimum + .5 ||
					pxColor4*tailleZ / 255 > minimum + .5)
				{
					hauteur = minimum*tailleZ / 255;
				}
				else//ok pr l'interpolation lineaire, les pts ne sont pas trop ecartes en hauteur
				{
					hauteur = ((1 - dx) * pxColor1 + dx * pxColor2) * (1 - dy) + ((1 - dx) * pxColor3 + dx * pxColor4) * dy;//calcul de la hauteur interpolee
				}
			}
			else
			{
				/* Il est possible qu'il ne faille pas interpoler, par exemple pour tester la hauteur pour voir si un mvt est possible ou pas. */
				pxColor1 = getpixel(heightmap, (int)x, (int)y);
				pxColor1 = (*(unsigned char *)(&pxColor1));//recup du 1er octet
				hauteur = (double)pxColor1;
			}

			hauteur *= tailleZ / 255;//on adapte la hauteur retournee a la hauteur de la map

			return hauteur + gest_terrain(RETURN).relBoundingBox.coordMin.z;
		}

	}
	return 0;
}

Vector place_cam(SDL_Event event)
{
	//IPA-TODO - v teot casti se resi ovladani postavy
	static Point position = { 0,0,0 };
	Point positionVoulue = { 60,62,0 };
	static Point oldPosition = { 150, 100, 0 }; //sauvgarde temporaire de l'ancienne position au cas d'un mvt refuse(hauteur trop haute)
	static Point aimAt = { 0,0,0 };
	static int mouseX = 0, mouseY = 0;
	static int oldMouseX = 0, oldMouseY = 0, newMouseX = 0, newMouseY = 0;
	static double angleY = 0, angleZ = 15;//on ne n'oriente la camera que sur l'axe X et Y (par rapport a la cam.) sinon, cela inclinerait l'img

	int tpsEcoule = 0;//temps ecoule depuis le dernier appel de la fonction

	SDL_GetMouseState(&newMouseX, &newMouseY);

	const Uint8 *keystates = SDL_GetKeyboardState(NULL);
	positionVoulue = oldPosition;

	SDL_GetRelativeMouseState(&newMouseX, &newMouseY);
	angleY += -newMouseY;
	angleZ += -newMouseX;

	if (angleY >= 80)
		angleY = 80;
	if (angleY <= -80)
		angleY = -80;


	if (keystates[SDL_SCANCODE_W])
	{
		positionVoulue.x += cos(angleZ*M_PI / 180) / SPEED_COEF;
		positionVoulue.y += sin(angleZ*M_PI / 180) / SPEED_COEF;
	}
	if (keystates[SDL_SCANCODE_S])
	{
		positionVoulue.x -= cos(angleZ*M_PI / 180) / SPEED_COEF;
		positionVoulue.y -= sin(angleZ*M_PI / 180) / SPEED_COEF;
	}
	if (keystates[SDL_SCANCODE_A])
	{
		positionVoulue.x += cos((angleZ + 90)*M_PI / 180) / SPEED_COEF;
		positionVoulue.y += sin((angleZ + 90)*M_PI / 180) / SPEED_COEF;
	}
	if (keystates[SDL_SCANCODE_D])
	{
		positionVoulue.x += cos((angleZ - 90)*M_PI / 180) / SPEED_COEF;
		positionVoulue.y += sin((angleZ - 90)*M_PI / 180) / SPEED_COEF;
	}

	Point min, max;
	min = gest_terrain(RETURN).relBoundingBox.coordMin;
	max = gest_terrain(RETURN).relBoundingBox.coordMax;

	if (positionVoulue.x>min.x + .5&& positionVoulue.y>min.y + .5&& positionVoulue.y<max.y - .5 && positionVoulue.x<max.x - .5)//dans le terrain
		position = positionVoulue;

	// exit game on ESC
	if (keystates[SDL_SCANCODE_ESCAPE])
		ExitProcess(0);

	position.z = get_height(position.x, position.y, false);      // hauteur correspondante
	aimAt.x = position.x + cos(angleZ*M_PI / 180)*cos(angleY*M_PI / 180);
	aimAt.y = position.y + sin(angleZ*M_PI / 180)*cos(angleY*M_PI / 180);
	aimAt.z = position.z + sin(angleY*M_PI / 180);

	position.z += EYE_HEIGHT;
	aimAt.z += EYE_HEIGHT;


	gluLookAt(position.x, position.y, position.z, aimAt.x, aimAt.y, aimAt.z, 0, 0, 1);

	oldMouseX = newMouseX;
	oldMouseY = newMouseY;
	oldPosition = position;

	Vector camera; //vecteur associe a la camera
	camera.origine = position;
	camera.angleY = angleY;
	camera.angleZ = angleZ;
	return camera;
}

objet gest_terrain(Act_Terrain action, char filename[])
{
	static objet objdefaut;
	if (action == LOAD)
		objdefaut.load(filename);

	return objdefaut;
}

void disp_fling(objet fling, Vector camera)
{
	const Uint8 *keystates = SDL_GetKeyboardState(NULL);

	/* Petite animation du fling : mvt de droite a gauche */
	static double decalageY;    //decalage du fling par rapport a la camera
	static double recul = 0;
	static bool croissant = true;      // si le decalage augmente ou diminue

	if (keystates[SDL_SCANCODE_W] ||
		keystates[SDL_SCANCODE_S] ||
		keystates[SDL_SCANCODE_A] ||
		keystates[SDL_SCANCODE_D])
	{
		if (croissant)
		{
			decalageY += .002;
			if (decalageY >= .07)
				croissant = false;
		}
		else
		{
			decalageY -= .002;
			if (decalageY <= 0)
				croissant = true;
		}
	}
	else
	{
		if (decalageY >= 0)
			decalageY -= .002;
	}

	if (keystates[SDL_SCANCODE_UP]) {
		alpha -= angle_delta / 2;
	}
	else if (keystates[SDL_SCANCODE_DOWN]) {
		alpha += angle_delta / 2;
	}

	if (keystates[SDL_SCANCODE_LEFT]) {
		beta -= angle_delta;
	}
	else if (keystates[SDL_SCANCODE_RIGHT]) {
		beta += angle_delta;
	}

	if (keystates[SDL_SCANCODE_SPACE]) {
		flying = 1;
	}

	if (keystates[SDL_SCANCODE_R]) {
		plane_pos = { 150.0, 150,20.0 };
		plane_v = { 0, 0, 5 };
		flying = 0;
	}


	if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(1) && recul <= 0)// click gauche de souris
	{
		recul = .08;
		//shot(camera);
	}
	else
	{
		if (recul >= 0)
			recul -= .004;
	}

	glPushMatrix();
	glTranslated(camera.origine.x, camera.origine.y, camera.origine.z);
	glRotated(camera.angleZ, 0, 0, 1);
	glRotated(-camera.angleY, 0, 1, 0);
	glTranslated(.4 - recul, -.16 + decalageY, -.25 + (-16 * decalageY*decalageY) + .6*decalageY);

	glScaled(.1, .1, .1);
	fling.disp();
	glPopMatrix();
}

robot *gest_robot(int r, Point posPlayer)
{
	static robot robots[ROBOTS];
	static bool loaded = false;
	if (!loaded)
	{
		loaded = true;
		robots[0].shape.load("robot.obj");
		for (int i = 0; i<ROBOTS; i++)
		{
			robots[i].shape = robots[0].shape;
			robots[i].position = randSpawn();
		}
	}

	Point pos;
	bool collision;
	double dist;
	if (posPlayer.x != -1)//si la position du joueur a été donnée en arguments, x != -1
	{
		for (int i = 0; i<ROBOTS; i++)
		{
			collision = false;
			pos = robots[i].position;
			pos.x += -((pos.x - posPlayer.x) / (fabs(pos.x - posPlayer.x) + 1)) / 30;
			pos.y += -((pos.y - posPlayer.y) / (fabs(pos.y - posPlayer.y) + 1)) / 30;

			for (int a = 0; a<i; a++)//detection de collision entre robots
			{

				dist = sqrt((robots[a].position.x - robots[i].position.x)*
					(robots[a].position.x - robots[i].position.x) +
					(robots[a].position.y - robots[i].position.y)*
					(robots[a].position.y - robots[i].position.y));
				if (dist<3)
					collision = true;

			}
			if (collision != true)
				robots[i].position = pos;
		}

		for (int i = 0; i<ROBOTS; i++)
		{
			robots[i].position.z = get_height(robots[i].position.x, robots[i].position.y);
			glPushMatrix();
			glTranslated(robots[i].position.x, robots[i].position.y, robots[i].position.z + 10);
			glRotated(robots[i].z, 0, 0, 1);
			robots[i].z++;
			robots[i].shape.disp();

			glPopMatrix();
		}
	}

	return &(robots[r]);
}

void shot(Vector camera)
{
	/* Pour savoir si la balle a touché un des robots, on effectue un test point par point. Il serait
	possible de le faire en direct, en calculant la trajectoire de la balle, mais dans ce cas, on ne pourrait
	pas savoir si la balle traverse le sol préalablement. Pour eviter de calculer deux points qui pourraient etre de part et d'autre
	d'un robot, on prend des petits intervales. */

	Point test; // point en cours de test
	test = camera.origine;
	robot *bot;
	double dist;


	for (int i = 0; i<500; i++)    //500 points
	{
		test.x += cos(camera.angleZ * (M_PI / 180)) * cos(camera.angleY * (M_PI / 180)) * PITCH;
		test.y += sin(camera.angleZ * (M_PI / 180)) * cos(camera.angleY * (M_PI / 180)) * PITCH;
		test.z += sin(camera.angleY * (M_PI / 180)) * PITCH;


		if (test.z<get_height(test.x, test.y, true))//balle dans le sol
			i = 500;//on arrete la boucle, si la balle passe dans le sol, elle s'arrete, donc la boucle aussi

					/* Maintenant, la boucle qui permet de tester si le point est dans un robot */
		for (int j = 0; j<5; j++)
		{
			bot = gest_robot(j);
			dist = sqrt((bot->position.x - test.x)*(bot->position.x - test.x) +
				(bot->position.y - test.y)*(bot->position.y - test.y));

			if (dist<1 && test.z>(bot->shape.relBoundingBox.coordMin.z + bot->position.z) && test.z<(bot->shape.relBoundingBox.coordMax.z + bot->position.z))//le point est dans le robot
			{
				gest_robot(j)->kill();
				j = 5;
				i = 500;
			}
		}
	}
	play_shot();
}

void painter(objet fling)
{
	gluLookAt(0, 0, 0, 1, 0, 0, 0, 0, 1);
	glPushMatrix();
	glTranslated(1, 0, 0);
	glScaled(.15, .15, .15);
	glRotated(120, 0, 0, 1);
	glRotated(20, -1, 1, 0);
	fling.disp();
	glPopMatrix();
	glFlush();
	SDL_GL_SwapWindow(screen);
}
