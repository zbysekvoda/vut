--vytvorenie pristupovych prav pre druheho clena
GRANT ALL ON Zakaznik TO xdubaj00;
GRANT ALL ON Pobyt TO xdubaj00;
GRANT ALL ON Pokoj TO xdubaj00;
GRANT ALL ON Sluzba TO xdubaj00;
GRANT ALL ON Platba TO xdubaj00;

--materializovany pohlad na vyuzivane sluzby jednotlivych pokojov
CREATE MATERIALIZED VIEW pokoje_s_vyuzivanymi_sluzbami
BUILD IMMEDIATE 
REFRESH COMPLETE
ON DEMAND
AS
    SELECT pobyt_id, pob.od pobyt_od, pob.do pobyt_do, pokoj_id, pok.typ pokoj_typ, sluzba_id, s.od sluzba_od, s.do sluzba_do, s.typ sluzba_typ
    FROM Pokoj pok, Pobyt pob, Sluzba s
    WHERE s.pokoj = pok.pokoj_id AND pok.pobyt = pob.pobyt_id AND s.od <= TO_DATE(CURRENT_DATE, 'dd.mm.yy') AND s.do >= TO_DATE(CURRENT_DATE, 'dd.mm.yy');

GRANT ALL ON pokoje_s_vyuzivanymi_sluzbami TO xdubaj00;

SELECT * FROM pokoje_s_vyuzivanymi_sluzbami;

INSERT INTO Sluzba (pokoj, typ, od, do, cena) VALUES ('3', 'masaz', TO_DATE('2017/01/08', 'yyyy/mm/dd'), TO_DATE('2017/01/09', 'yyyy/mm/dd'), '300');

SELECT * FROM pokoje_s_vyuzivanymi_sluzbami;