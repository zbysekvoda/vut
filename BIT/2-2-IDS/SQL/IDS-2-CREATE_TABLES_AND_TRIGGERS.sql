CREATE TABLE Zakaznik(
    zakaznik_id SMALLINT GENERATED ALWAYS AS IDENTITY,
    jmeno VARCHAR(30) NOT NULL,
    prijmeni VARCHAR(30) NOT NULL,
    datum_narozeni DATE NOT NULL,
    pohlavi VARCHAR(1),
    bydliste VARCHAR(200) NOT NULL,
    rodne_cislo SMALLINT NOT NULL,

    CONSTRAINT zak_pk PRIMARY KEY (zakaznik_id),
    CONSTRAINT rod_ci CHECK (MOD(rodne_cislo, 11) = 0 AND rodne_cislo >= 1000000000 AND rodne_cislo <= 9999999999)
);

CREATE TABLE Zamestnanec(
    zamestnanec_id SMALLINT GENERATED ALWAYS AS IDENTITY,
    jmeno VARCHAR(30) NOT NULL,
    prijmeni VARCHAR(30) NOT NULL,
    funkce VARCHAR(20) NOT NULL,

    CONSTRAINT zam_pk PRIMARY KEY (zamestnanec_id)
);

CREATE TABLE Pobyt (
    pobyt_id SMALLINT,
    zakaznik SMALLINT NOT NULL,
    zamestnanec SMALLINT NOT NULL,
    datum_rezervace DATE,
    od DATE NOT NULL,
    do DATE NOT NULL,

    CONSTRAINT pobyt_p_id PRIMARY KEY (pobyt_id),
    CONSTRAINT zk_fk FOREIGN KEY (zakaznik) REFERENCES Zakaznik(zakaznik_id),
    CONSTRAINT zm_fk FOREIGN KEY (zamestnanec) REFERENCES Zamestnanec(zamestnanec_id)
);

CREATE TABLE Pokoj (
    pokoj_id SMALLINT GENERATED ALWAYS AS IDENTITY,
    pobyt SMALLINT NOT NULL,
    pocet_luzek SMALLINT NOT NULL,
    typ VARCHAR(50) NOT NULL,
    cena SMALLINT NOT NULL,

    CONSTRAINT pokoj_p_id PRIMARY KEY (pokoj_id),
    CONSTRAINT po_fk FOREIGN KEY (pobyt) REFERENCES Pobyt(pobyt_id)
);

CREATE TABLE Sluzba (
    sluzba_id SMALLINT GENERATED ALWAYS AS IDENTITY,
    pokoj SMALLINT NOT NULL,
    typ VARCHAR(50) NOT NULL,
    od DATE NOT NULL,
    do DATE NOT NULL,
    cena SMALLINT NOT NULL,

    CONSTRAINT sluzba_p_id PRIMARY KEY (sluzba_id),
    CONSTRAINT pok_fk FOREIGN KEY (pokoj) REFERENCES Pokoj(pokoj_id)
);

CREATE TABLE Platba (
    platba_id SMALLINT GENERATED ALWAYS AS IDENTITY,
    zamestnanec SMALLINT NOT NULL,
    pobyt SMALLINT NOT NULL,
    datum DATE NOT NULL,
    suma SMALLINT NOT NULL,
    cislo_karty SMALLINT,
    mena VARCHAR(3),

    CONSTRAINT platba_p_id PRIMARY KEY (platba_id),
    CONSTRAINT zam_fk FOREIGN KEY (zamestnanec) REFERENCES Zamestnanec(zamestnanec_id),
    CONSTRAINT pob_fk FOREIGN KEY (pobyt) REFERENCES Pobyt(pobyt_id),

    CONSTRAINT typ_platby CHECK (((CASE WHEN cislo_karty IS NULL THEN 1 ELSE 0 END) + (CASE WHEN mena IS NULL THEN 1 ELSE 0 END)) = 1),
    CONSTRAINT ci_ka CHECK (cislo_karty >= 1000000000000000 AND cislo_karty <= 9999999999999999)
);



/* Zákazníci, kteří v hotelu zůstanou déle, než 6 dní mají TV zdarma */
CREATE OR REPLACE TRIGGER tv_zdarma
    BEFORE INSERT
        ON Sluzba
        FOR EACH ROW
    DECLARE
        delka SMALLINT;
    BEGIN
        SELECT pob.do - pob.od INTO delka
        FROM Pobyt pob, Pokoj pok
        WHERE pob.pobyt_id = pok.pobyt AND pob.pobyt_id = :new.pokoj;

        IF :new.typ = 'tv' AND delka >= 7 THEN
            :new.cena := 0;
        END IF;
    END tv_zdarma;
/

-- SELECT * FROM Sluzba s, Pokoj pok, Pobyt pob WHERE s.pokoj = pok.pokoj_id AND pok.pobyt = pob.pobyt_id;

CREATE SEQUENCE pobyt_seq
    START WITH 1 
    INCREMENT BY 1;

CREATE OR REPLACE TRIGGER pobyt_seq_trigger
    BEFORE INSERT 
        ON Pobyt
        FOR EACH ROW
    BEGIN
      IF(:NEW.pobyt_id IS NULL)
      THEN
        :NEW.pobyt_id := pobyt_seq.nextval;
      END IF;
    END pobyt_seq_trigger;
/