/*Najde zamestnance, ktery evidoval platbu za pobyt s ID 1*/
SELECT z.jmeno, z.prijmeni, z.funkce
FROM Platba p 
JOIN Zamestnanec z ON z.zamestnanec_id = p.zamestnanec
WHERE p.pobyt = 1;

/*Najde pobyty, ve kterych kdy byl zakaznik Karel Novak */
SELECT p.pobyt_id, p.datum_rezervace, p.od, p.do
FROM Zakaznik z
JOIN Pobyt p ON z.zakaznik_id = p.zakaznik
WHERE z.jmeno = 'Karel' AND z.prijmeni = 'Novak';

/*Najde pokoje, ve kterych byl zakaznik Pavel Sedlak*/
SELECT pok.typ, pok.pocet_luzek, pok.cena
FROM Zakaznik zak
JOIN Pobyt pob ON pob.zakaznik = zak.zakaznik_id
JOIN Pokoj pok ON pok.pobyt = pob.pobyt_id
WHERE zak.jmeno = 'Pavel' AND zak.prijmeni = 'Sedlak';

/*Zjistí, kolikrát byly využité které pokoje*/
SELECT typ, COUNT(pokoj_id) 
FROM Pokoj
GROUP BY typ;

/*Zjistí, kolik plateb bylo uskutečněno danou platební kartou, popřípadě hotově*/
SELECT cislo_karty, COUNT(*)
FROM Platba
GROUP BY cislo_karty;

/*Zjistí, jestli existují zákazníci, kteří ještě nemají evidovaný žádný pobyt*/
SELECT z.jmeno, z.prijmeni
FROM Zakaznik z
WHERE NOT EXISTS (
SELECT * FROM Pobyt p WHERE p.zakaznik = z.zakaznik_id 
);

/*Zjistí jména zákazníků, kteří jsou aktuálně ubytovaní*/
SELECT jmeno, prijmeni
FROM Zakaznik
WHERE zakaznik_id IN
(SELECT zakaznik
FROM Pobyt p
WHERE do >= TO_DATE(CURRENT_DATE, 'dd.mm.yy') AND od < TO_DATE(CURRENT_DATE, 'dd.mm.yy')
);


SELECT * FROM Pobyt;
SELECT * FROM Zakaznik;
SELECT * FROM Pokoj;
SELECT * FROM Sluzba;
SELECT * FROM Zamestnanec;
SELECT * FROM Platba;
