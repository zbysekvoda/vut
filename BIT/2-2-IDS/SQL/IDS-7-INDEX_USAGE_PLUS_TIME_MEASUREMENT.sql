SET SERVEROUTPUT ON;

CREATE OR REPLACE PROCEDURE cas (p_opakovani NUMBER)
AS
    tmp Zakaznik.jmeno%TYPE;
    counter SMALLINT;
    l_start NUMBER;
    l_end NUMBER;
    l_diff DOUBLE PRECISION;
BEGIN
    counter := p_opakovani;
    l_start := dbms_utility.get_time;
    LOOP
        EXIT WHEN counter = 1;
        counter := counter - 1;
        SELECT jmeno INTO tmp FROM Zakaznik WHERE rodne_cislo = 9506244132;
    END LOOP;
    
    l_end := dbms_utility.get_time;
    l_diff := (l_end - l_start) / p_opakovani;
    dbms_output.put_line('Prumerny cas: '|| l_diff);
END cas;
/

-- 100000x dotaz na zákazníka s daným rodným číslem, bez indexu na RČ
EXECUTE cas(100000);

EXPLAIN PLAN FOR
SELECT jmeno, prijmeni, COUNT(*)
FROM Zakaznik z, Pobyt p
WHERE z.zakaznik_id = p.zakaznik AND rodne_cislo = 9506244154
GROUP BY z.rodne_cislo, z.prijmeni, z.jmeno;
SELECT * FROM TABLE(DBMS_XPLAN.display);

CREATE UNIQUE INDEX rodne_cislo_zakaznika
ON Zakaznik (rodne_cislo);

-- 100000x dotaz na zákazníka s daným rodným číslem, s indexem na RČ
EXECUTE cas(100000);

EXPLAIN PLAN FOR
SELECT jmeno, prijmeni, COUNT(*)
FROM Zakaznik z, Pobyt p
WHERE z.zakaznik_id = p.zakaznik AND rodne_cislo = 9506244154
GROUP BY z.rodne_cislo, z.prijmeni, z.jmeno;
SELECT * FROM TABLE(DBMS_XPLAN.display);