INSERT INTO Zakaznik (jmeno, prijmeni, datum_narozeni, pohlavi, bydliste, rodne_cislo) VALUES ('Karel', 'Novak', TO_DATE('1950/07/09', 'yyyy/mm/dd'), 'M', 'Kamenice 123', '9506244187');
INSERT INTO Zakaznik (jmeno, prijmeni, datum_narozeni, pohlavi, bydliste, rodne_cislo) VALUES ('Emil', 'Novy', TO_DATE('1960/07/09', 'yyyy/mm/dd'), 'M', 'Brno 123', '9506244176');
INSERT INTO Zakaznik (jmeno, prijmeni, datum_narozeni, pohlavi, bydliste, rodne_cislo) VALUES ('Bedrich', 'ABC', TO_DATE('1970/07/09', 'yyyy/mm/dd'), 'M', 'Praha 123', '9506244165');
INSERT INTO Zakaznik (jmeno, prijmeni, datum_narozeni, pohlavi, bydliste, rodne_cislo) VALUES ('Pavel', 'Sedlak', TO_DATE('1980/07/09', 'yyyy/mm/dd'), 'M', 'Jihlava 123', '9506244154');
INSERT INTO Zakaznik (jmeno, prijmeni, datum_narozeni, pohlavi, bydliste, rodne_cislo) VALUES ('Karel', 'Dubaj', TO_DATE('1990/07/09', 'yyyy/mm/dd'), 'M', 'Olomouc 123', '9506244143');
INSERT INTO Zakaznik (jmeno, prijmeni, datum_narozeni, pohlavi, bydliste, rodne_cislo) VALUES ('Petr', 'Dubaj', TO_DATE('1991/07/09', 'yyyy/mm/dd'), 'M', 'Krnov 123', '9506244132');
INSERT INTO Zakaznik (jmeno, prijmeni, datum_narozeni, pohlavi, bydliste, rodne_cislo) VALUES ('Pavel', 'Dubaj', TO_DATE('1992/07/09', 'yyyy/mm/dd'), 'M', 'Krnov 123', '9506244121');

INSERT INTO Zamestnanec (jmeno, prijmeni, funkce) VALUES ('Bedrich', 'Smetana', 'Vedouci');
INSERT INTO Zamestnanec (jmeno, prijmeni, funkce) VALUES ('Igor', 'Smetana', 'Recepcni');
INSERT INTO Zamestnanec (jmeno, prijmeni, funkce) VALUES ('Ondrej', 'Smetana', 'Uklizec');
INSERT INTO Zamestnanec (jmeno, prijmeni, funkce) VALUES ('Jakub', 'Smetana', 'Uklizec');
INSERT INTO Zamestnanec (jmeno, prijmeni, funkce) VALUES ('Vladislav', 'Smetana', 'Kuchar');
INSERT INTO Zamestnanec (jmeno, prijmeni, funkce) VALUES ('Jindrich', 'Smetana', 'Spravce');

INSERT INTO Pobyt (zakaznik, zamestnanec, datum_rezervace, od, do) VALUES ('1', '1', TO_DATE('2017/01/01', 'yyyy/mm/dd'), TO_DATE('2017/01/07', 'yyyy/mm/dd'), TO_DATE('2017/01/11', 'yyyy/mm/dd'));
INSERT INTO Pobyt (zakaznik, zamestnanec, datum_rezervace, od, do) VALUES ('2', '2', TO_DATE('2017/01/08', 'yyyy/mm/dd'), TO_DATE('2017/01/10', 'yyyy/mm/dd'), TO_DATE('2017/01/20', 'yyyy/mm/dd'));
INSERT INTO Pobyt (zakaznik, zamestnanec, datum_rezervace, od, do) VALUES ('3', '1', TO_DATE('2017/01/05', 'yyyy/mm/dd'), TO_DATE('2017/01/14', 'yyyy/mm/dd'), TO_DATE('2017/02/01', 'yyyy/mm/dd'));
INSERT INTO Pobyt (zakaznik, zamestnanec, datum_rezervace, od, do) VALUES ('4', '2', TO_DATE('2017/01/13', 'yyyy/mm/dd'), TO_DATE('2017/01/27', 'yyyy/mm/dd'), TO_DATE('2017/02/09', 'yyyy/mm/dd'));
INSERT INTO Pobyt (zakaznik, zamestnanec, datum_rezervace, od, do) VALUES ('5', '1', TO_DATE('2017/01/06', 'yyyy/mm/dd'), TO_DATE('2017/01/25', 'yyyy/mm/dd'), TO_DATE('2017/02/12', 'yyyy/mm/dd'));
INSERT INTO Pobyt (zakaznik, zamestnanec, datum_rezervace, od, do) VALUES ('1', '2', TO_DATE('2017/01/22', 'yyyy/mm/dd'), TO_DATE('2017/01/30', 'yyyy/mm/dd'), TO_DATE('2017/05/10', 'yyyy/mm/dd'));
INSERT INTO Pobyt (zakaznik, zamestnanec, datum_rezervace, od, do) VALUES ('4', '2', TO_DATE('2017/01/24', 'yyyy/mm/dd'), TO_DATE('2017/01/30', 'yyyy/mm/dd'), TO_DATE('2017/05/09', 'yyyy/mm/dd'));

INSERT INTO Pokoj (pobyt, pocet_luzek, typ, cena) VALUES ('1', '6', 'apartma 1', '2500');
INSERT INTO Pokoj (pobyt, pocet_luzek, typ, cena) VALUES ('2', '3', 'pokoj 1', '1500');
INSERT INTO Pokoj (pobyt, pocet_luzek, typ, cena) VALUES ('3', '3', 'pokoj 1', '1500');
INSERT INTO Pokoj (pobyt, pocet_luzek, typ, cena) VALUES ('4', '2', 'pokoj 2', '1000');
INSERT INTO Pokoj (pobyt, pocet_luzek, typ, cena) VALUES ('5', '3', 'pokoj 3', '1500');
INSERT INTO Pokoj (pobyt, pocet_luzek, typ, cena) VALUES ('6', '3', 'pokoj 1', '1500');
INSERT INTO Pokoj (pobyt, pocet_luzek, typ, cena) VALUES ('7', '6', 'apartma 1', '2500');

INSERT INTO Sluzba (pokoj, typ, od, do, cena) VALUES ('1', 'masaz', TO_DATE('2017/01/08', 'yyyy/mm/dd'), TO_DATE('2017/01/09', 'yyyy/mm/dd'), '300');
INSERT INTO Sluzba (pokoj, typ, od, do, cena) VALUES ('1', 'tv', TO_DATE('2017/01/10', 'yyyy/mm/dd'), TO_DATE('2017/01/20', 'yyyy/mm/dd'), '400');
INSERT INTO Sluzba (pokoj, typ, od, do, cena) VALUES ('7', 'tv', TO_DATE('2017/01/10', 'yyyy/mm/dd'), TO_DATE('2017/01/20', 'yyyy/mm/dd'), '400');
INSERT INTO Sluzba (pokoj, typ, od, do, cena) VALUES ('3', 'snidane', TO_DATE('2017/01/14', 'yyyy/mm/dd'), TO_DATE('2017/02/01', 'yyyy/mm/dd'), '500');
INSERT INTO Sluzba (pokoj, typ, od, do, cena) VALUES ('4', 'bazen', TO_DATE('2017/01/30', 'yyyy/mm/dd'), TO_DATE('2017/01/31', 'yyyy/mm/dd'), '200');
INSERT INTO Sluzba (pokoj, typ, od, do, cena) VALUES ('3', 'golf', TO_DATE('2017/01/20', 'yyyy/mm/dd'), TO_DATE('2017/01/21', 'yyyy/mm/dd'), '600');
INSERT INTO Sluzba (pokoj, typ, od, do, cena) VALUES ('4', 'wifi', TO_DATE('2017/01/31', 'yyyy/mm/dd'), TO_DATE('2017/02/01', 'yyyy/mm/dd'), '100');
INSERT INTO Sluzba (pokoj, typ, od, do, cena) VALUES ('7', 'tenis', TO_DATE('2017/02/03', 'yyyy/mm/dd'), TO_DATE('2017/02/04', 'yyyy/mm/dd'), '300');
INSERT INTO Sluzba (pokoj, typ, od, do, cena) VALUES ('4', 'masaz', TO_DATE('2017/02/02', 'yyyy/mm/dd'), TO_DATE('2017/02/03', 'yyyy/mm/dd'), '300');
INSERT INTO Sluzba (pokoj, typ, od, do, cena) VALUES ('4', 'masaz', TO_DATE(CURRENT_DATE, 'dd.mm.yy'), TO_DATE(CURRENT_DATE, 'dd.mm.yy') + 3, '300');

INSERT INTO Platba (zamestnanec, pobyt, datum, suma, cislo_karty, mena) VALUES ('1', '1', TO_DATE('2017/01/10', 'yyyy/mm/dd'), '2800', NULL, 'CZK');
INSERT INTO Platba (zamestnanec, pobyt, datum, suma, cislo_karty, mena) VALUES ('2', '2', TO_DATE('2017/01/15', 'yyyy/mm/dd'), '3000', '1234567897791204', NULL);
INSERT INTO Platba (zamestnanec, pobyt, datum, suma, cislo_karty, mena) VALUES ('2', '3', TO_DATE('2017/02/01', 'yyyy/mm/dd'), '10000', NULL, 'CZK');
INSERT INTO Platba (zamestnanec, pobyt, datum, suma, cislo_karty, mena) VALUES ('2', '4', TO_DATE('2017/02/09', 'yyyy/mm/dd'), '5000', '1234567897791204', NULL);
INSERT INTO Platba (zamestnanec, pobyt, datum, suma, cislo_karty, mena) VALUES ('1', '5', TO_DATE('2017/01/25', 'yyyy/mm/dd'), '3200', NULL, 'CZK');
INSERT INTO Platba (zamestnanec, pobyt, datum, suma, cislo_karty, mena) VALUES ('2', '6', TO_DATE('2017/02/01', 'yyyy/mm/dd'), '1400', '1234567897791204', NULL);
INSERT INTO Platba (zamestnanec, pobyt, datum, suma, cislo_karty, mena) VALUES ('1', '7', TO_DATE('2017/02/09', 'yyyy/mm/dd'), '6000', '1234567897791204', NULL);

INSERT INTO Pobyt (zakaznik, zamestnanec, datum_rezervace, od, do) VALUES ('4', '2', TO_DATE('2017/01/24', 'yyyy/mm/dd'), TO_DATE(CURRENT_DATE, 'dd.mm.yy'), TO_DATE(CURRENT_DATE, 'dd.mm.yy') + 4);
INSERT INTO Pokoj (pobyt, pocet_luzek, typ, cena) VALUES ('8', '6', 'apartma 1', '2500');
INSERT INTO Sluzba (pokoj, typ, od, do, cena) VALUES ('8', 'masaz', TO_DATE(CURRENT_DATE, 'dd.mm.yy'), TO_DATE(CURRENT_DATE, 'dd.mm.yy') + 10, '300');