/*
Projekt část č. 4 - Konceptuálni model
Zadání č. 27 - Hotel
Řešitelé: 	Ondrej Dubaj	xdubaj00
		Zbyšek Voda	xvodaz01
*/
SET SERVEROUTPUT ON;

/*
DROP SEQUENCE pobyt_seq;
DROP TABLE Platba;
DROP TABLE Sluzba;
DROP TABLE Pokoj;
DROP TABLE Pobyt;
DROP TABLE Zamestnanec;
DROP TABLE Zakaznik;
DROP MATERIALIZED VIEW pokoje_s_vyuzivanymi_sluzbami;
*/

CREATE TABLE Zakaznik(
    zakaznik_id SMALLINT GENERATED ALWAYS AS IDENTITY,
    jmeno VARCHAR(30) NOT NULL,
    prijmeni VARCHAR(30) NOT NULL,
    datum_narozeni DATE NOT NULL,
    pohlavi VARCHAR(1),
    bydliste VARCHAR(200) NOT NULL,
    rodne_cislo SMALLINT NOT NULL,

    CONSTRAINT zak_pk PRIMARY KEY (zakaznik_id),
    CONSTRAINT rod_ci CHECK (MOD(rodne_cislo, 11) = 0 AND rodne_cislo >= 1000000000 AND rodne_cislo <= 9999999999)
);

CREATE TABLE Zamestnanec(
    zamestnanec_id SMALLINT GENERATED ALWAYS AS IDENTITY,
    jmeno VARCHAR(30) NOT NULL,
    prijmeni VARCHAR(30) NOT NULL,
    funkce VARCHAR(20) NOT NULL,

    CONSTRAINT zam_pk PRIMARY KEY (zamestnanec_id)
);

CREATE TABLE Pobyt (
    pobyt_id SMALLINT,
    zakaznik SMALLINT NOT NULL,
    zamestnanec SMALLINT NOT NULL,
    datum_rezervace DATE,
    od DATE NOT NULL,
    do DATE NOT NULL,

    CONSTRAINT pobyt_p_id PRIMARY KEY (pobyt_id),
    CONSTRAINT zk_fk FOREIGN KEY (zakaznik) REFERENCES Zakaznik(zakaznik_id),
    CONSTRAINT zm_fk FOREIGN KEY (zamestnanec) REFERENCES Zamestnanec(zamestnanec_id)
);

CREATE TABLE Pokoj (
    pokoj_id SMALLINT GENERATED ALWAYS AS IDENTITY,
    pobyt SMALLINT NOT NULL,
    pocet_luzek SMALLINT NOT NULL,
    typ VARCHAR(50) NOT NULL,
    cena SMALLINT NOT NULL,

    CONSTRAINT pokoj_p_id PRIMARY KEY (pokoj_id),
    CONSTRAINT po_fk FOREIGN KEY (pobyt) REFERENCES Pobyt(pobyt_id)
);

CREATE TABLE Sluzba (
    sluzba_id SMALLINT GENERATED ALWAYS AS IDENTITY,
    pokoj SMALLINT NOT NULL,
    typ VARCHAR(50) NOT NULL,
    od DATE NOT NULL,
    do DATE NOT NULL,
    cena SMALLINT NOT NULL,

    CONSTRAINT sluzba_p_id PRIMARY KEY (sluzba_id),
    CONSTRAINT pok_fk FOREIGN KEY (pokoj) REFERENCES Pokoj(pokoj_id)
);

CREATE TABLE Platba (
    platba_id SMALLINT GENERATED ALWAYS AS IDENTITY,
    zamestnanec SMALLINT NOT NULL,
    pobyt SMALLINT NOT NULL,
    datum DATE NOT NULL,
    suma SMALLINT NOT NULL,
    cislo_karty SMALLINT,
    mena VARCHAR(3),

    CONSTRAINT platba_p_id PRIMARY KEY (platba_id),
    CONSTRAINT zam_fk FOREIGN KEY (zamestnanec) REFERENCES Zamestnanec(zamestnanec_id),
    CONSTRAINT pob_fk FOREIGN KEY (pobyt) REFERENCES Pobyt(pobyt_id),

    CONSTRAINT typ_platby CHECK (((CASE WHEN cislo_karty IS NULL THEN 1 ELSE 0 END) + (CASE WHEN mena IS NULL THEN 1 ELSE 0 END)) = 1),
    CONSTRAINT ci_ka CHECK (cislo_karty >= 1000000000000000 AND cislo_karty <= 9999999999999999)
);






/* Zákazníci, kteří v hotelu zůstanou déle, než 6 dní mají TV zdarma */
CREATE OR REPLACE TRIGGER tv_zdarma
    BEFORE INSERT
        ON Sluzba
        FOR EACH ROW
    DECLARE
        delka SMALLINT;
    BEGIN
        SELECT pob.do - pob.od INTO delka
        FROM Pobyt pob, Pokoj pok
        WHERE pob.pobyt_id = pok.pobyt AND pob.pobyt_id = :new.pokoj;

        IF :new.typ = 'tv' AND delka >= 7 THEN
            :new.cena := 0;
        END IF;
    END tv_zdarma;
/

-- SELECT * FROM Sluzba s, Pokoj pok, Pobyt pob WHERE s.pokoj = pok.pokoj_id AND pok.pobyt = pob.pobyt_id;

CREATE SEQUENCE pobyt_seq
    START WITH 1 
    INCREMENT BY 1;

CREATE OR REPLACE TRIGGER pobyt_seq_trigger
    BEFORE INSERT 
        ON Pobyt
        FOR EACH ROW
    BEGIN
      IF(:NEW.pobyt_id IS NULL)
      THEN
        :NEW.pobyt_id := pobyt_seq.nextval;
      END IF;
    END pobyt_seq_trigger;
/

/* Spočítá cenu pobytu zadaného ID a zapíše platbu */
/* Parametr exchange_rate udává kurz měny ku CZK*/
/* vysledna_cena = cena / exchange_rate */
CREATE OR REPLACE PROCEDURE zaznamenej_platbu (
    p_zamestnanec IN Platba.zamestnanec%TYPE, 
    p_pobyt IN Platba.pobyt%TYPE, 
    p_datum IN Platba.datum%TYPE,
    p_cislo_karty IN Platba.cislo_karty%TYPE, 
    p_mena IN Platba.mena%TYPE,
    p_exchange_rate IN SMALLINT)
AS
    var_pokoj_id Pokoj.pokoj_id%TYPE;
    var_cena_celkem Pokoj.cena%TYPE;
    var_cena_pokoje Pokoj.cena%TYPE;
    var_cena_sluzby Sluzba.cena%TYPE;
    
    CURSOR k_pokoje IS
        SELECT pokoj_id, cena 
        FROM Pokoj
        WHERE pobyt = p_pobyt;
        
    CURSOR k_sluzby (p_pokoj IN Sluzba.pokoj%TYPE) IS
        SELECT cena
        FROM Sluzba
        WHERE pokoj = p_pokoj;
BEGIN
    var_cena_celkem := 0;
    
    OPEN k_pokoje;
    LOOP
        FETCH k_pokoje INTO var_pokoj_id, var_cena_pokoje;
        EXIT WHEN k_pokoje%NOTFOUND; 
        
        OPEN k_sluzby(var_pokoj_id);
        LOOP
            FETCH k_sluzby INTO var_cena_sluzby;
            EXIT WHEN k_sluzby%NOTFOUND;
            
            var_cena_celkem := var_cena_celkem + var_cena_sluzby;
        END LOOP;
        CLOSE k_sluzby;
        
        var_cena_celkem := var_cena_celkem + var_cena_pokoje;   
    END LOOP;
    CLOSE k_pokoje;
    
    INSERT INTO Platba (zamestnanec, pobyt, datum, suma, cislo_karty, mena) VALUES (p_zamestnanec, p_pobyt, p_datum, var_cena_celkem / p_exchange_rate, p_cislo_karty, p_mena);
EXCEPTION
    WHEN ZERO_DIVIDE THEN
        INSERT INTO Platba (zamestnanec, pobyt, datum, suma, cislo_karty, mena) VALUES (p_zamestnanec, p_pobyt, p_datum, var_cena_celkem, p_cislo_karty, 'CZK');
END zaznamenej_platbu;
/

--EXECUTE zaznamenej_platbu(1, 1, TO_DATE('2017/01/10', 'yyyy/mm/dd'), NULL, 'EUR', 30);
--EXECUTE zaznamenej_platbu(1, 1, TO_DATE('2017/01/10', 'yyyy/mm/dd'), NULL, 'EUR', 0);

/* Smaže veškeré záznamy o osobě daného ID v DB */
CREATE OR REPLACE PROCEDURE smaz_zaznam_zakaznika (p_zakaznik IN Zakaznik.zakaznik_id%TYPE)
AS
    var_pobyt Pobyt.pobyt_id%TYPE;
    var_pokoj Pokoj.pokoj_id%TYPE;
    
    CURSOR k_pobyty IS
        SELECT pobyt_id 
        FROM Pobyt
        WHERE zakaznik = p_zakaznik;
    
    CURSOR k_pokoje (p_pobyt IN Pokoj.pobyt%TYPE) IS
        SELECT pokoj_id
        FROM Pokoj
        WHERE pobyt = p_pobyt;
    
BEGIN
    OPEN k_pobyty;
    LOOP
        FETCH k_pobyty INTO var_pobyt;
        EXIT WHEN k_pobyty%NOTFOUND;
        
        OPEN k_pokoje(var_pobyt);
        LOOP
            FETCH k_pokoje INTO var_pokoj;
            EXIT WHEN k_pokoje%NOTFOUND;
            
            DELETE FROM Sluzba  WHERE pokoj = var_pokoj;
        END LOOP;
        CLOSE k_pokoje;
        
        DELETE FROM Pokoj WHERE pobyt = var_pobyt;
        DELETE FROM Platba WHERE pobyt = var_pobyt;
    END LOOP;
    CLOSE k_pobyty;
    
    DELETE FROM Pobyt WHERE zakaznik = p_zakaznik;
    DELETE FROM Zakaznik WHERE zakaznik_id = p_zakaznik;
END smaz_zaznam_zakaznika;
/

--EXECUTE smaz_zaznam_zakaznika(1);
-- SELECT * FROM Sluzba s, Pokoj pok, Platba pl, Pobyt pob, Zakaznik z WHERE s.pokoj = pok.pokoj_id AND pok.pobyt = pob.pobyt_id AND pob.zakaznik = z.zakaznik_id AND pl.pobyt = pob.pobyt_id;




INSERT INTO Zakaznik (jmeno, prijmeni, datum_narozeni, pohlavi, bydliste, rodne_cislo) VALUES ('Karel', 'Novak', TO_DATE('1950/07/09', 'yyyy/mm/dd'), 'M', 'Kamenice 123', '9506244187');
INSERT INTO Zakaznik (jmeno, prijmeni, datum_narozeni, pohlavi, bydliste, rodne_cislo) VALUES ('Emil', 'Novy', TO_DATE('1960/07/09', 'yyyy/mm/dd'), 'M', 'Brno 123', '9506244176');
INSERT INTO Zakaznik (jmeno, prijmeni, datum_narozeni, pohlavi, bydliste, rodne_cislo) VALUES ('Bedrich', 'ABC', TO_DATE('1970/07/09', 'yyyy/mm/dd'), 'M', 'Praha 123', '9506244165');
INSERT INTO Zakaznik (jmeno, prijmeni, datum_narozeni, pohlavi, bydliste, rodne_cislo) VALUES ('Pavel', 'Sedlak', TO_DATE('1980/07/09', 'yyyy/mm/dd'), 'M', 'Jihlava 123', '9506244154');
INSERT INTO Zakaznik (jmeno, prijmeni, datum_narozeni, pohlavi, bydliste, rodne_cislo) VALUES ('Karel', 'Dubaj', TO_DATE('1990/07/09', 'yyyy/mm/dd'), 'M', 'Olomouc 123', '9506244143');
INSERT INTO Zakaznik (jmeno, prijmeni, datum_narozeni, pohlavi, bydliste, rodne_cislo) VALUES ('Petr', 'Dubaj', TO_DATE('1991/07/09', 'yyyy/mm/dd'), 'M', 'Krnov 123', '9506244132');
INSERT INTO Zakaznik (jmeno, prijmeni, datum_narozeni, pohlavi, bydliste, rodne_cislo) VALUES ('Pavel', 'Dubaj', TO_DATE('1992/07/09', 'yyyy/mm/dd'), 'M', 'Krnov 123', '9506244121');

INSERT INTO Zamestnanec (jmeno, prijmeni, funkce) VALUES ('Bedrich', 'Smetana', 'Vedouci');
INSERT INTO Zamestnanec (jmeno, prijmeni, funkce) VALUES ('Igor', 'Smetana', 'Recepcni');
INSERT INTO Zamestnanec (jmeno, prijmeni, funkce) VALUES ('Ondrej', 'Smetana', 'Uklizec');
INSERT INTO Zamestnanec (jmeno, prijmeni, funkce) VALUES ('Jakub', 'Smetana', 'Uklizec');
INSERT INTO Zamestnanec (jmeno, prijmeni, funkce) VALUES ('Vladislav', 'Smetana', 'Kuchar');
INSERT INTO Zamestnanec (jmeno, prijmeni, funkce) VALUES ('Jindrich', 'Smetana', 'Spravce');

INSERT INTO Pobyt (zakaznik, zamestnanec, datum_rezervace, od, do) VALUES ('1', '1', TO_DATE('2017/01/01', 'yyyy/mm/dd'), TO_DATE('2017/01/07', 'yyyy/mm/dd'), TO_DATE('2017/01/11', 'yyyy/mm/dd'));
INSERT INTO Pobyt (zakaznik, zamestnanec, datum_rezervace, od, do) VALUES ('2', '2', TO_DATE('2017/01/08', 'yyyy/mm/dd'), TO_DATE('2017/01/10', 'yyyy/mm/dd'), TO_DATE('2017/01/20', 'yyyy/mm/dd'));
INSERT INTO Pobyt (zakaznik, zamestnanec, datum_rezervace, od, do) VALUES ('3', '1', TO_DATE('2017/01/05', 'yyyy/mm/dd'), TO_DATE('2017/01/14', 'yyyy/mm/dd'), TO_DATE('2017/02/01', 'yyyy/mm/dd'));
INSERT INTO Pobyt (zakaznik, zamestnanec, datum_rezervace, od, do) VALUES ('4', '2', TO_DATE('2017/01/13', 'yyyy/mm/dd'), TO_DATE('2017/01/27', 'yyyy/mm/dd'), TO_DATE('2017/02/09', 'yyyy/mm/dd'));
INSERT INTO Pobyt (zakaznik, zamestnanec, datum_rezervace, od, do) VALUES ('5', '1', TO_DATE('2017/01/06', 'yyyy/mm/dd'), TO_DATE('2017/01/25', 'yyyy/mm/dd'), TO_DATE('2017/02/12', 'yyyy/mm/dd'));
INSERT INTO Pobyt (zakaznik, zamestnanec, datum_rezervace, od, do) VALUES ('1', '2', TO_DATE('2017/01/22', 'yyyy/mm/dd'), TO_DATE('2017/01/30', 'yyyy/mm/dd'), TO_DATE('2017/05/10', 'yyyy/mm/dd'));
INSERT INTO Pobyt (zakaznik, zamestnanec, datum_rezervace, od, do) VALUES ('4', '2', TO_DATE('2017/01/24', 'yyyy/mm/dd'), TO_DATE('2017/01/30', 'yyyy/mm/dd'), TO_DATE('2017/05/09', 'yyyy/mm/dd'));

INSERT INTO Pokoj (pobyt, pocet_luzek, typ, cena) VALUES ('1', '6', 'apartma 1', '2500');
INSERT INTO Pokoj (pobyt, pocet_luzek, typ, cena) VALUES ('2', '3', 'pokoj 1', '1500');
INSERT INTO Pokoj (pobyt, pocet_luzek, typ, cena) VALUES ('3', '3', 'pokoj 1', '1500');
INSERT INTO Pokoj (pobyt, pocet_luzek, typ, cena) VALUES ('4', '2', 'pokoj 2', '1000');
INSERT INTO Pokoj (pobyt, pocet_luzek, typ, cena) VALUES ('5', '3', 'pokoj 3', '1500');
INSERT INTO Pokoj (pobyt, pocet_luzek, typ, cena) VALUES ('6', '3', 'pokoj 1', '1500');
INSERT INTO Pokoj (pobyt, pocet_luzek, typ, cena) VALUES ('7', '6', 'apartma 1', '2500');

INSERT INTO Sluzba (pokoj, typ, od, do, cena) VALUES ('1', 'masaz', TO_DATE('2017/01/08', 'yyyy/mm/dd'), TO_DATE('2017/01/09', 'yyyy/mm/dd'), '300');
INSERT INTO Sluzba (pokoj, typ, od, do, cena) VALUES ('1', 'tv', TO_DATE('2017/01/10', 'yyyy/mm/dd'), TO_DATE('2017/01/20', 'yyyy/mm/dd'), '400');
INSERT INTO Sluzba (pokoj, typ, od, do, cena) VALUES ('7', 'tv', TO_DATE('2017/01/10', 'yyyy/mm/dd'), TO_DATE('2017/01/20', 'yyyy/mm/dd'), '400');
INSERT INTO Sluzba (pokoj, typ, od, do, cena) VALUES ('3', 'snidane', TO_DATE('2017/01/14', 'yyyy/mm/dd'), TO_DATE('2017/02/01', 'yyyy/mm/dd'), '500');
INSERT INTO Sluzba (pokoj, typ, od, do, cena) VALUES ('4', 'bazen', TO_DATE('2017/01/30', 'yyyy/mm/dd'), TO_DATE('2017/01/31', 'yyyy/mm/dd'), '200');
INSERT INTO Sluzba (pokoj, typ, od, do, cena) VALUES ('3', 'golf', TO_DATE('2017/01/20', 'yyyy/mm/dd'), TO_DATE('2017/01/21', 'yyyy/mm/dd'), '600');
INSERT INTO Sluzba (pokoj, typ, od, do, cena) VALUES ('4', 'wifi', TO_DATE('2017/01/31', 'yyyy/mm/dd'), TO_DATE('2017/02/01', 'yyyy/mm/dd'), '100');
INSERT INTO Sluzba (pokoj, typ, od, do, cena) VALUES ('7', 'tenis', TO_DATE('2017/02/03', 'yyyy/mm/dd'), TO_DATE('2017/02/04', 'yyyy/mm/dd'), '300');
INSERT INTO Sluzba (pokoj, typ, od, do, cena) VALUES ('4', 'masaz', TO_DATE('2017/02/02', 'yyyy/mm/dd'), TO_DATE('2017/02/03', 'yyyy/mm/dd'), '300');
INSERT INTO Sluzba (pokoj, typ, od, do, cena) VALUES ('4', 'masaz', TO_DATE(CURRENT_DATE, 'dd.mm.yy'), TO_DATE(CURRENT_DATE, 'dd.mm.yy') + 3, '300');

INSERT INTO Platba (zamestnanec, pobyt, datum, suma, cislo_karty, mena) VALUES ('1', '1', TO_DATE('2017/01/10', 'yyyy/mm/dd'), '2800', NULL, 'CZK');
INSERT INTO Platba (zamestnanec, pobyt, datum, suma, cislo_karty, mena) VALUES ('2', '2', TO_DATE('2017/01/15', 'yyyy/mm/dd'), '3000', '1234567897791204', NULL);
INSERT INTO Platba (zamestnanec, pobyt, datum, suma, cislo_karty, mena) VALUES ('2', '3', TO_DATE('2017/02/01', 'yyyy/mm/dd'), '10000', NULL, 'CZK');
INSERT INTO Platba (zamestnanec, pobyt, datum, suma, cislo_karty, mena) VALUES ('2', '4', TO_DATE('2017/02/09', 'yyyy/mm/dd'), '5000', '1234567897791204', NULL);
INSERT INTO Platba (zamestnanec, pobyt, datum, suma, cislo_karty, mena) VALUES ('1', '5', TO_DATE('2017/01/25', 'yyyy/mm/dd'), '3200', NULL, 'CZK');
INSERT INTO Platba (zamestnanec, pobyt, datum, suma, cislo_karty, mena) VALUES ('2', '6', TO_DATE('2017/02/01', 'yyyy/mm/dd'), '1400', '1234567897791204', NULL);
INSERT INTO Platba (zamestnanec, pobyt, datum, suma, cislo_karty, mena) VALUES ('1', '7', TO_DATE('2017/02/09', 'yyyy/mm/dd'), '6000', '1234567897791204', NULL);

INSERT INTO Pobyt (zakaznik, zamestnanec, datum_rezervace, od, do) VALUES ('4', '2', TO_DATE('2017/01/24', 'yyyy/mm/dd'), TO_DATE(CURRENT_DATE, 'dd.mm.yy'), TO_DATE(CURRENT_DATE, 'dd.mm.yy') + 4);
INSERT INTO Pokoj (pobyt, pocet_luzek, typ, cena) VALUES ('8', '6', 'apartma 1', '2500');
INSERT INTO Sluzba (pokoj, typ, od, do, cena) VALUES ('8', 'masaz', TO_DATE(CURRENT_DATE, 'dd.mm.yy'), TO_DATE(CURRENT_DATE, 'dd.mm.yy') + 10, '300');

/*Najde zamestnance, ktery evidoval platbu za pobyt s ID 1*/
SELECT z.jmeno, z.prijmeni, z.funkce
FROM Platba p 
JOIN Zamestnanec z ON z.zamestnanec_id = p.zamestnanec
WHERE p.pobyt = 1;

/*Najde pobyty, ve kterych kdy byl zakaznik Karel Novak */
SELECT p.pobyt_id, p.datum_rezervace, p.od, p.do
FROM Zakaznik z
JOIN Pobyt p ON z.zakaznik_id = p.zakaznik
WHERE z.jmeno = 'Karel' AND z.prijmeni = 'Novak';

/*Najde pokoje, ve kterych byl zakaznik Pavel Sedlak*/
SELECT pok.typ, pok.pocet_luzek, pok.cena
FROM Zakaznik zak
JOIN Pobyt pob ON pob.zakaznik = zak.zakaznik_id
JOIN Pokoj pok ON pok.pobyt = pob.pobyt_id
WHERE zak.jmeno = 'Pavel' AND zak.prijmeni = 'Sedlak';

/*Zjistí, kolikrát byly využité které pokoje*/
SELECT typ, COUNT(pokoj_id) 
FROM Pokoj
GROUP BY typ;

/*Zjistí, kolik plateb bylo uskutečněno danou platební kartou, popřípadě hotově*/
SELECT cislo_karty, COUNT(*)
FROM Platba
GROUP BY cislo_karty;

/*Zjistí, jestli existují zákazníci, kteří ještě nemají evidovaný žádný pobyt*/
SELECT z.jmeno, z.prijmeni
FROM Zakaznik z
WHERE NOT EXISTS (
SELECT * FROM Pobyt p WHERE p.zakaznik = z.zakaznik_id 
);

/*Zjistí jména zákazníků, kteří jsou aktuálně ubytovaní*/
SELECT jmeno, prijmeni
FROM Zakaznik
WHERE zakaznik_id IN
(SELECT zakaznik
FROM Pobyt p
WHERE do >= TO_DATE(CURRENT_DATE, 'dd.mm.yy') AND od < TO_DATE(CURRENT_DATE, 'dd.mm.yy')
);


--vytvorenie pristupovych prav pre druheho clena
GRANT ALL ON Zakaznik TO xdubaj00;
GRANT ALL ON Pobyt TO xdubaj00;
GRANT ALL ON Pokoj TO xdubaj00;
GRANT ALL ON Sluzba TO xdubaj00;
GRANT ALL ON Platba TO xdubaj00;

--materializovany pohlad na vyuzivane sluzby jednotlivych pokojov
CREATE MATERIALIZED VIEW pokoje_s_vyuzivanymi_sluzbami
BUILD IMMEDIATE 
REFRESH COMPLETE
ON DEMAND
AS
    SELECT pobyt_id, pob.od pobyt_od, pob.do pobyt_do, pokoj_id, pok.typ pokoj_typ, sluzba_id, s.od sluzba_od, s.do sluzba_do, s.typ sluzba_typ
    FROM Pokoj pok, Pobyt pob, Sluzba s
    WHERE s.pokoj = pok.pokoj_id AND pok.pobyt = pob.pobyt_id AND s.od <= TO_DATE(CURRENT_DATE, 'dd.mm.yy') AND s.do >= TO_DATE(CURRENT_DATE, 'dd.mm.yy');

GRANT ALL ON pokoje_s_vyuzivanymi_sluzbami TO xdubaj00;

SELECT * FROM pokoje_s_vyuzivanymi_sluzbami;

INSERT INTO Sluzba (pokoj, typ, od, do, cena) VALUES ('3', 'masaz', TO_DATE('2017/01/08', 'yyyy/mm/dd'), TO_DATE('2017/01/09', 'yyyy/mm/dd'), '300');

SELECT * FROM pokoje_s_vyuzivanymi_sluzbami;


CREATE OR REPLACE PROCEDURE cas (p_opakovani NUMBER)
AS
    tmp Zakaznik.jmeno%TYPE;
    counter SMALLINT;
    l_start NUMBER;
    l_end NUMBER;
    l_diff DOUBLE PRECISION;
BEGIN
    counter := p_opakovani;
    l_start := dbms_utility.get_time;
    LOOP
        EXIT WHEN counter = 1;
        counter := counter - 1;
        SELECT jmeno INTO tmp FROM Zakaznik WHERE rodne_cislo = 9506244132;
    END LOOP;
    
    l_end := dbms_utility.get_time;
    l_diff := (l_end - l_start) / p_opakovani;
    dbms_output.put_line('Prumerny cas: '|| l_diff);
END cas;
/

-- 100000x dotaz na zákazníka s daným rodným číslem, bez indexu na RČ
EXECUTE cas(100000);

EXPLAIN PLAN FOR
SELECT jmeno, prijmeni, COUNT(*)
FROM Zakaznik z, Pobyt p
WHERE z.zakaznik_id = p.zakaznik AND rodne_cislo = 9506244154
GROUP BY z.rodne_cislo, z.prijmeni, z.jmeno;
SELECT * FROM TABLE(DBMS_XPLAN.display);

CREATE UNIQUE INDEX rodne_cislo_zakaznika
ON Zakaznik (rodne_cislo);

-- 100000x dotaz na zákazníka s daným rodným číslem, s indexem na RČ
EXECUTE cas(100000);

EXPLAIN PLAN FOR
SELECT jmeno, prijmeni, COUNT(*)
FROM Zakaznik z, Pobyt p
WHERE z.zakaznik_id = p.zakaznik AND rodne_cislo = 9506244154
GROUP BY z.rodne_cislo, z.prijmeni, z.jmeno;
SELECT * FROM TABLE(DBMS_XPLAN.display);

