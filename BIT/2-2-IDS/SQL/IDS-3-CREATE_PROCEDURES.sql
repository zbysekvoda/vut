/* Spočítá cenu pobytu zadaného ID a zapíše platbu */
/* Parametr exchange_rate udává kurz měny ku CZK*/
/* vysledna_cena = cena / exchange_rate */
CREATE OR REPLACE PROCEDURE zaznamenej_platbu (
    p_zamestnanec IN Platba.zamestnanec%TYPE, 
    p_pobyt IN Platba.pobyt%TYPE, 
    p_datum IN Platba.datum%TYPE,
    p_cislo_karty IN Platba.cislo_karty%TYPE, 
    p_mena IN Platba.mena%TYPE,
    p_exchange_rate IN SMALLINT)
AS
    var_pokoj_id Pokoj.pokoj_id%TYPE;
    var_cena_celkem Pokoj.cena%TYPE;
    var_cena_pokoje Pokoj.cena%TYPE;
    var_cena_sluzby Sluzba.cena%TYPE;
    
    CURSOR k_pokoje IS
        SELECT pokoj_id, cena 
        FROM Pokoj
        WHERE pobyt = p_pobyt;
        
    CURSOR k_sluzby (p_pokoj IN Sluzba.pokoj%TYPE) IS
        SELECT cena
        FROM Sluzba
        WHERE pokoj = p_pokoj;
BEGIN
    var_cena_celkem := 0;
    
    OPEN k_pokoje;
    LOOP
        FETCH k_pokoje INTO var_pokoj_id, var_cena_pokoje;
        EXIT WHEN k_pokoje%NOTFOUND; 
        
        OPEN k_sluzby(var_pokoj_id);
        LOOP
            FETCH k_sluzby INTO var_cena_sluzby;
            EXIT WHEN k_sluzby%NOTFOUND;
            
            var_cena_celkem := var_cena_celkem + var_cena_sluzby;
        END LOOP;
        CLOSE k_sluzby;
        
        var_cena_celkem := var_cena_celkem + var_cena_pokoje;   
    END LOOP;
    CLOSE k_pokoje;
    
    INSERT INTO Platba (zamestnanec, pobyt, datum, suma, cislo_karty, mena) VALUES (p_zamestnanec, p_pobyt, p_datum, var_cena_celkem / p_exchange_rate, p_cislo_karty, p_mena);
EXCEPTION
    WHEN ZERO_DIVIDE THEN
        INSERT INTO Platba (zamestnanec, pobyt, datum, suma, cislo_karty, mena) VALUES (p_zamestnanec, p_pobyt, p_datum, var_cena_celkem, p_cislo_karty, 'CZK');
END zaznamenej_platbu;
/

--EXECUTE zaznamenej_platbu(1, 1, TO_DATE('2017/01/10', 'yyyy/mm/dd'), NULL, 'EUR', 30);
--EXECUTE zaznamenej_platbu(1, 1, TO_DATE('2017/01/10', 'yyyy/mm/dd'), NULL, 'EUR', 0);

/* Smaže veškeré záznamy o osobě daného ID v DB */
CREATE OR REPLACE PROCEDURE smaz_zaznam_zakaznika (p_zakaznik IN Zakaznik.zakaznik_id%TYPE)
AS
    var_pobyt Pobyt.pobyt_id%TYPE;
    var_pokoj Pokoj.pokoj_id%TYPE;
    
    CURSOR k_pobyty IS
        SELECT pobyt_id 
        FROM Pobyt
        WHERE zakaznik = p_zakaznik;
    
    CURSOR k_pokoje (p_pobyt IN Pokoj.pobyt%TYPE) IS
        SELECT pokoj_id
        FROM Pokoj
        WHERE pobyt = p_pobyt;
    
BEGIN
    OPEN k_pobyty;
    LOOP
        FETCH k_pobyty INTO var_pobyt;
        EXIT WHEN k_pobyty%NOTFOUND;
        
        OPEN k_pokoje(var_pobyt);
        LOOP
            FETCH k_pokoje INTO var_pokoj;
            EXIT WHEN k_pokoje%NOTFOUND;
            
            DELETE FROM Sluzba  WHERE pokoj = var_pokoj;
        END LOOP;
        CLOSE k_pokoje;
        
        DELETE FROM Pokoj WHERE pobyt = var_pobyt;
        DELETE FROM Platba WHERE pobyt = var_pobyt;
    END LOOP;
    CLOSE k_pobyty;
    
    DELETE FROM Pobyt WHERE zakaznik = p_zakaznik;
    DELETE FROM Zakaznik WHERE zakaznik_id = p_zakaznik;
END smaz_zaznam_zakaznika;
/

--EXECUTE smaz_zaznam_zakaznika(1);
-- SELECT * FROM Sluzba s, Pokoj pok, Platba pl, Pobyt pob, Zakaznik z WHERE s.pokoj = pok.pokoj_id AND pok.pobyt = pob.pobyt_id AND pob.zakaznik = z.zakaznik_id AND pl.pobyt = pob.pobyt_id;
