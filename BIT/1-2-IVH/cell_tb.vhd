-- TestBench Template 

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.math_pack.ALL;

ENTITY cell_tb IS
END cell_tb;

ARCHITECTURE behavior OF cell_tb IS 
	COMPONENT cell
		GENERIC (
			MASK 			: mask_t
		);
		PORT(
			NEIGHBOUR   : IN   STD_LOGIC_VECTOR (3 downto 0);
			CLICK       : IN   STD_LOGIC;
			STATE       : IN   STD_LOGIC;
			LIGHT       : OUT  STD_LOGIC
		);
	END COMPONENT;
	
	signal NEIGHBOUR	 : STD_LOGIC_VECTOR (3 downto 0) := (others => '0');
	signal CLICK       : STD_LOGIC := '0';
	signal STATE       : STD_LOGIC := '0';
	signal LIGHT       : STD_LOGIC_VECTOR(8 downto 0);
	
	constant IDX_TOP     : NATURAL := 0; -- index sousedni bunky nachazejici se nahore v signalu NEIGHBOUR
	constant IDX_LEFT   	: NATURAL := 1; -- index sousedni bunky nachazejici se vlevo  v signalu NEIGHBOUR
	constant IDX_RIGHT  	: NATURAL := 2; -- index sousedni bunky nachazejici se vpravo v signalu NEIGHBOUR
	constant IDX_BOTTOM 	: NATURAL := 3; -- index sousedni bunky nachazejici se dole   v signalu NEIGHBOUR
BEGIN
	cols: for X in 0 to 2 generate
		rows: for Y in 0 to 2 generate
			cl: cell
				GENERIC MAP(
					MASK => getmask(X,Y,3,3)
				)
				PORT MAP (
					NEIGHBOUR => NEIGHBOUR,
					CLICK => CLICK, 
					STATE => STATE,
					LIGHT => LIGHT(3*Y+X)
				);
		end generate rows;
		
		-- 0 1 2
		-- 3 4 5
		-- 6 7 8
	end generate cols;
	
	NEIGHBOUR(3) 	<= not NEIGHBOUR(3) 	after 1ns;
	NEIGHBOUR(2) 	<= not NEIGHBOUR(2) 	after 2ns;
	NEIGHBOUR(1) 	<= not NEIGHBOUR(1) 	after 4ns;
	NEIGHBOUR(0) 	<= not NEIGHBOUR(0) 	after 8ns;
	CLICK	 			<= not CLICK 			after 16ns;	
	STATE 			<= not STATE 			after 32ns;
	
	process
	begin
		--teoreticky sta�� otestovat jen p��pady, kdy je v NEIGHBOUR jen jedna jedni�ka, prakticky rad�i v�echny
		
		wait until NEIGHBOUR(IDX_TOP) = '0' AND NEIGHBOUR(IDX_RIGHT) = '0' AND NEIGHBOUR(IDX_BOTTOM) = '0' AND NEIGHBOUR(IDX_LEFT) = '0' AND STATE = '1' AND CLICK = '1';
		wait for 1 ps;
		assert LIGHT = "000000000" report "Bu�ky si neuchov�vaj� stav" severity error;
		
		wait until NEIGHBOUR(IDX_TOP) = '0' AND NEIGHBOUR(IDX_RIGHT) = '0' AND NEIGHBOUR(IDX_BOTTOM) = '0' AND NEIGHBOUR(IDX_LEFT) = '0' AND STATE = '1' AND CLICK = '0';
		wait for 1 ps;
		assert LIGHT = "111111111" report "Bu�ky si neuchov�vaj� stav" severity error;
		
		-------------------------------------------------------------------
		
		wait until NEIGHBOUR(IDX_TOP) = '1' AND NEIGHBOUR(IDX_RIGHT) = '0' AND NEIGHBOUR(IDX_BOTTOM) = '0' AND NEIGHBOUR(IDX_LEFT) = '0' AND STATE = '1' AND CLICK = '1';
		wait for 1 ps;
		assert LIGHT = "111111000" report "Bu�ky nereaguj� dob�e na kliknut� souseda nad sebou" severity error;
		
		wait until NEIGHBOUR(IDX_TOP) = '1' AND NEIGHBOUR(IDX_RIGHT) = '0' AND NEIGHBOUR(IDX_BOTTOM) = '0' AND NEIGHBOUR(IDX_LEFT) = '0' AND STATE = '1' AND CLICK = '0';
		wait for 1 ps;
		assert LIGHT = "000000111" report "Bu�ky nereaguj� dob�e na nekliknut� na sebe" severity error;
		
		-------------------------------------------------------------------		
		
		wait until NEIGHBOUR(IDX_TOP) = '0' AND NEIGHBOUR(IDX_RIGHT) = '1' AND NEIGHBOUR(IDX_BOTTOM) = '0' AND NEIGHBOUR(IDX_LEFT) = '0' AND STATE = '1' AND CLICK = '1';
		wait for 1 ps;
		assert LIGHT = "011011011" report "Bu�ky nereaguj� dob�e na kliknut� souseda vpravo" severity error;
		
		wait until NEIGHBOUR(IDX_TOP) = '0' AND NEIGHBOUR(IDX_RIGHT) = '1' AND NEIGHBOUR(IDX_BOTTOM) = '0' AND NEIGHBOUR(IDX_LEFT) = '0' AND STATE = '1' AND CLICK = '0';
		wait for 1 ps;
		assert LIGHT = "100100100" report "Bu�ky nereaguj� dob�e na nekliknut� souseda vpravo" severity error;
		
		-------------------------------------------------------------------	
		
		wait until NEIGHBOUR(IDX_TOP) = '1' AND NEIGHBOUR(IDX_RIGHT) = '1' AND NEIGHBOUR(IDX_BOTTOM) = '0' AND NEIGHBOUR(IDX_LEFT) = '0' AND STATE = '1' AND CLICK = '1';
		wait for 1 ps;
		assert LIGHT = "100100011" report "Bu�ky nereaguj� dob�e na kliknut� souseda nad sebou a vpravo" severity error;
		
		wait until NEIGHBOUR(IDX_TOP) = '1' AND NEIGHBOUR(IDX_RIGHT) = '1' AND NEIGHBOUR(IDX_BOTTOM) = '0' AND NEIGHBOUR(IDX_LEFT) = '0' AND STATE = '1' AND CLICK = '0';
		wait for 1 ps;
		assert LIGHT = "011011100" report "Bu�ky nereaguj� dob�e na nekliknut� souseda nad sebou a vpravo" severity error;
		
		-------------------------------------------------------------------	
		
		wait until NEIGHBOUR(IDX_TOP) = '0' AND NEIGHBOUR(IDX_RIGHT) = '0' AND NEIGHBOUR(IDX_BOTTOM) = '1' AND NEIGHBOUR(IDX_LEFT) = '0' AND STATE = '1' AND CLICK = '1';
		wait for 1 ps;
		assert LIGHT = "000111111" report "Bu�ky nereaguj� dob�e na kliknut� souseda pod sebou" severity error;
		
		wait until NEIGHBOUR(IDX_TOP) = '0' AND NEIGHBOUR(IDX_RIGHT) = '0' AND NEIGHBOUR(IDX_BOTTOM) = '1' AND NEIGHBOUR(IDX_LEFT) = '0' AND STATE = '1' AND CLICK = '0';
		wait for 1 ps;
		assert LIGHT = "111000000" report "Bu�ky nereaguj� dob�e na nekliknut� souseda pod sebou" severity error;
		
		-------------------------------------------------------------------	
		
		wait until NEIGHBOUR(IDX_TOP) = '1' AND NEIGHBOUR(IDX_RIGHT) = '0' AND NEIGHBOUR(IDX_BOTTOM) = '1' AND NEIGHBOUR(IDX_LEFT) = '0' AND STATE = '1' AND CLICK = '1';
		wait for 1 ps;
		assert LIGHT = "111000111" report "Bu�ky nereaguj� dob�e na kliknut� souseda nad sebou a pod sebou" severity error;
		
		wait until NEIGHBOUR(IDX_TOP) = '1' AND NEIGHBOUR(IDX_RIGHT) = '0' AND NEIGHBOUR(IDX_BOTTOM) = '1' AND NEIGHBOUR(IDX_LEFT) = '0' AND STATE = '1' AND CLICK = '0';
		wait for 1 ps;
		assert LIGHT = "000111000" report "Bu�ky nereaguj� dob�e na nekliknut� souseda nad sebou a pod sebou" severity error;
		
		-------------------------------------------------------------------	
		
		wait until NEIGHBOUR(IDX_TOP) = '0' AND NEIGHBOUR(IDX_RIGHT) = '1' AND NEIGHBOUR(IDX_BOTTOM) = '1' AND NEIGHBOUR(IDX_LEFT) = '0' AND STATE = '1' AND CLICK = '1';
		wait for 1 ps;
		assert LIGHT = "011100100" report "Bu�ky nereaguj� dob�e na kliknut� souseda pod sebou a vpravo" severity error;
		
		wait until NEIGHBOUR(IDX_TOP) = '0' AND NEIGHBOUR(IDX_RIGHT) = '1' AND NEIGHBOUR(IDX_BOTTOM) = '1' AND NEIGHBOUR(IDX_LEFT) = '0' AND STATE = '1' AND CLICK = '0';
		wait for 1 ps;
		assert LIGHT = "100011011" report "Bu�ky nereaguj� dob�e na nekliknut� souseda pod sebou a vpravo" severity error;
		
		-------------------------------------------------------------------	
		
		wait until NEIGHBOUR(IDX_TOP) = '1' AND NEIGHBOUR(IDX_RIGHT) = '1' AND NEIGHBOUR(IDX_BOTTOM) = '1' AND NEIGHBOUR(IDX_LEFT) = '0' AND STATE = '1' AND CLICK = '1';
		wait for 1 ps;
		assert LIGHT = "100011100" report "Bu�ky nereaguj� dob�e na kliknut� souseda nad sebou, pod sebou a vpravo" severity error;
		
		wait until NEIGHBOUR(IDX_TOP) = '1' AND NEIGHBOUR(IDX_RIGHT) = '1' AND NEIGHBOUR(IDX_BOTTOM) = '1' AND NEIGHBOUR(IDX_LEFT) = '0' AND STATE = '1' AND CLICK = '0';
		wait for 1 ps;
		assert LIGHT = "011100011" report "Bu�ky nereaguj� dob�e na nekliknut� souseda pod sebou a vpravo" severity error;
		
		-------------------------------------------------------------------	
		
		wait until NEIGHBOUR(IDX_TOP) = '0' AND NEIGHBOUR(IDX_RIGHT) = '0' AND NEIGHBOUR(IDX_BOTTOM) = '0' AND NEIGHBOUR(IDX_LEFT) = '1' AND STATE = '1' AND CLICK = '1';
		wait for 1 ps;
		assert LIGHT = "110110110" report "Bu�ky nereaguj� dob�e na kliknut� souseda vlevo" severity error;
		
		wait until NEIGHBOUR(IDX_TOP) = '0' AND NEIGHBOUR(IDX_RIGHT) = '0' AND NEIGHBOUR(IDX_BOTTOM) = '0' AND NEIGHBOUR(IDX_LEFT) = '1' AND STATE = '1' AND CLICK = '0';
		wait for 1 ps;
		assert LIGHT = "001001001" report "Bu�ky nereaguj� dob�e na nekliknut� souseda vlevo" severity error;
		
		-------------------------------------------------------------------	
		
		wait until NEIGHBOUR(IDX_TOP) = '1' AND NEIGHBOUR(IDX_RIGHT) = '0' AND NEIGHBOUR(IDX_BOTTOM) = '0' AND NEIGHBOUR(IDX_LEFT) = '1' AND STATE = '1' AND CLICK = '1';
		wait for 1 ps;
		assert LIGHT = "001001110" report "Bu�ky nereaguj� dob�e na kliknut� souseda naho�e a vlevo" severity error;
		
		wait until NEIGHBOUR(IDX_TOP) = '1' AND NEIGHBOUR(IDX_RIGHT) = '0' AND NEIGHBOUR(IDX_BOTTOM) = '0' AND NEIGHBOUR(IDX_LEFT) = '1' AND STATE = '1' AND CLICK = '0';
		wait for 1 ps;
		assert LIGHT = "110110001" report "Bu�ky nereaguj� dob�e na nekliknut� souseda naho�e a vlevo" severity error;
		
		-------------------------------------------------------------------	
		
		wait until NEIGHBOUR(IDX_TOP) = '0' AND NEIGHBOUR(IDX_RIGHT) = '1' AND NEIGHBOUR(IDX_BOTTOM) = '0' AND NEIGHBOUR(IDX_LEFT) = '1' AND STATE = '1' AND CLICK = '1';
		wait for 1 ps;
		assert LIGHT = "101101101" report "Bu�ky nereaguj� dob�e na kliknut� souseda vpravo a vlevo" severity error;
		
		wait until NEIGHBOUR(IDX_TOP) = '0' AND NEIGHBOUR(IDX_RIGHT) = '1' AND NEIGHBOUR(IDX_BOTTOM) = '0' AND NEIGHBOUR(IDX_LEFT) = '1' AND STATE = '1' AND CLICK = '0';
		wait for 1 ps;
		assert LIGHT = "010010010" report "Bu�ky nereaguj� dob�e na nekliknut� souseda vpravo a vlevo" severity error;
		
		-------------------------------------------------------------------	
		
		wait until NEIGHBOUR(IDX_TOP) = '1' AND NEIGHBOUR(IDX_RIGHT) = '1' AND NEIGHBOUR(IDX_BOTTOM) = '0' AND NEIGHBOUR(IDX_LEFT) = '1' AND STATE = '1' AND CLICK = '1';
		wait for 1 ps;
		assert LIGHT = "010010101" report "Bu�ky nereaguj� dob�e na kliknut� souseda vpravo, vlevo a naho�e" severity error;
		
		wait until NEIGHBOUR(IDX_TOP) = '1' AND NEIGHBOUR(IDX_RIGHT) = '1' AND NEIGHBOUR(IDX_BOTTOM) = '0' AND NEIGHBOUR(IDX_LEFT) = '1' AND STATE = '1' AND CLICK = '0';
		wait for 1 ps;
		assert LIGHT = "101101010" report "Bu�ky nereaguj� dob�e na nekliknut� souseda vpravo, vlevo a naho�e" severity error;
		
		-------------------------------------------------------------------	
		
		wait until NEIGHBOUR(IDX_TOP) = '0' AND NEIGHBOUR(IDX_RIGHT) = '0' AND NEIGHBOUR(IDX_BOTTOM) = '1' AND NEIGHBOUR(IDX_LEFT) = '1' AND STATE = '1' AND CLICK = '1';
		wait for 1 ps;
		assert LIGHT = "110001001" report "Bu�ky nereaguj� dob�e na kliknut� souseda vlevo a pod sebou" severity error;
		
		wait until NEIGHBOUR(IDX_TOP) = '0' AND NEIGHBOUR(IDX_RIGHT) = '0' AND NEIGHBOUR(IDX_BOTTOM) = '1' AND NEIGHBOUR(IDX_LEFT) = '1' AND STATE = '1' AND CLICK = '0';
		wait for 1 ps;
		assert LIGHT = "001110110" report "Bu�ky nereaguj� dob�e na nekliknut� souseda vlevo a pod sebou" severity error;
		
		-------------------------------------------------------------------	
		
		wait until NEIGHBOUR(IDX_TOP) = '1' AND NEIGHBOUR(IDX_RIGHT) = '0' AND NEIGHBOUR(IDX_BOTTOM) = '1' AND NEIGHBOUR(IDX_LEFT) = '1' AND STATE = '1' AND CLICK = '1';
		wait for 1 ps;
		assert LIGHT = "001110001" report "Bu�ky nereaguj� dob�e na kliknut� souseda vlevo, pod sebou a nad sebou" severity error;
		
		wait until NEIGHBOUR(IDX_TOP) = '1' AND NEIGHBOUR(IDX_RIGHT) = '0' AND NEIGHBOUR(IDX_BOTTOM) = '1' AND NEIGHBOUR(IDX_LEFT) = '1' AND STATE = '1' AND CLICK = '0';
		wait for 1 ps;
		assert LIGHT = "110001110" report "Bu�ky nereaguj� dob�e na nekliknut� souseda vlevo, pod sebou a nad sebou" severity error;
		
		-------------------------------------------------------------------	
		
		wait until NEIGHBOUR(IDX_TOP) = '0' AND NEIGHBOUR(IDX_RIGHT) = '1' AND NEIGHBOUR(IDX_BOTTOM) = '1' AND NEIGHBOUR(IDX_LEFT) = '1' AND STATE = '1' AND CLICK = '1';
		wait for 1 ps;
		assert LIGHT = "101010010" report "Bu�ky nereaguj� dob�e na kliknut� souseda vlevo, vpravo a pod sebou" severity error;
		
		wait until NEIGHBOUR(IDX_TOP) = '0' AND NEIGHBOUR(IDX_RIGHT) = '1' AND NEIGHBOUR(IDX_BOTTOM) = '1' AND NEIGHBOUR(IDX_LEFT) = '1' AND STATE = '1' AND CLICK = '0';
		wait for 1 ps;
		assert LIGHT = "010101101" report "Bu�ky nereaguj� dob�e na nekliknut� souseda vlevo, vpravo a pod sebou" severity error;
		
		-------------------------------------------------------------------

		wait until NEIGHBOUR(IDX_TOP) = '1' AND NEIGHBOUR(IDX_RIGHT) = '1' AND NEIGHBOUR(IDX_BOTTOM) = '1' AND NEIGHBOUR(IDX_LEFT) = '1' AND STATE = '1' AND CLICK = '1';
		wait for 1 ps;
		assert LIGHT = "010101010" report "Bu�ky nereaguj� dob�e na kliknut� souseda vsude" severity error;
		
		wait until NEIGHBOUR(IDX_TOP) = '1' AND NEIGHBOUR(IDX_RIGHT) = '1' AND NEIGHBOUR(IDX_BOTTOM) = '1' AND NEIGHBOUR(IDX_LEFT) = '1' AND STATE = '1' AND CLICK = '0';
		wait for 1 ps;
		assert LIGHT = "101010101" report "Bu�ky nereaguj� dob�e na nekliknut� souseda vsude" severity error;		
	end process;
END;
