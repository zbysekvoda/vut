library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;

entity cntr is
	Generic(
		INITIAL_VALUE : STD_LOGIC_VECTOR (24 downto 0) := std_logic_vector(to_unsigned(2**25-100, 25))
	);
	Port (
		CLK 		: in  STD_LOGIC;
		RESET 	: in  STD_LOGIC;
		ENABLE 	: in  STD_LOGIC;
		VALUE 	: buffer  STD_LOGIC_VECTOR (24 downto 0) := INITIAL_VALUE;
		DONE 		: buffer  STD_LOGIC := '0'
	);
end cntr;

architecture Behavioral of cntr is
begin
	process (CLK, RESET)
	begin
		if RESET = '1' then
				VALUE <= INITIAL_VALUE;
				DONE <= '0';
		elsif rising_edge(CLK) then
			if VALUE = std_logic_vector(to_unsigned(2**25-1, 25)) then
				DONE <= '1';
			elsif (ENABLE = '1') and (DONE = '0') then
				VALUE <= VALUE + 1;
			end if;
		end if;
	end process;
end Behavioral;
