LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;
 
ENTITY cntr_tb IS
END cntr_tb;
 
ARCHITECTURE behavior OF cntr_tb IS 
COMPONENT cntr
	PORT(
		CLK : IN  std_logic;
		RESET : IN  std_logic;
		ENABLE : IN  std_logic;
		VALUE : BUFFER std_logic_vector(24 downto 0);
		DONE : BUFFER  std_logic
	);
END COMPONENT;
    
signal CLK : std_logic := '0';
signal RESET : std_logic := '0';
signal ENABLE : std_logic := '0';

signal VALUE : std_logic_vector(24 downto 0);
signal DONE : std_logic;

constant CLK_period : time := 100 ps;
signal OLD_VAL : std_logic_vector(24 downto 0);
 
BEGIN
	uut: cntr PORT MAP (
		CLK => CLK,
		RESET => RESET,
      ENABLE => ENABLE,
      VALUE => VALUE,
      DONE => DONE
	);
	
   process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
		
		OLD_VAL <= VALUE;
   end process;
	
	process
	begin
		ENABLE <= '0', '1' after 100 ns, '0' after 105 ns, '1' after 115 ns;
		wait until ENABLE = '0';
		wait for CLK_period;
		assert OLD_VAL = VALUE report "ENABLE nefunguje. P�i '0' se nezachov�v� hodnota." severity error;
	
		wait until DONE = '1';
		assert VALUE = std_logic_vector(to_unsigned(2**25-1, 25)) report "VALUE po nastaven� DONE nejsou sam� jendi�ky" severity error;
		
		RESET <= '0', '1' after 500 ns, '0' after 501 ns;
		wait until RESET = '1';
		wait for CLK_period;
		assert VALUE = std_logic_vector(to_unsigned(2**25-100, 25)) report "VALUE po nastaven� RESET nen� nastavena na v�choz� hodnotu" severity error;
	end process;
END;
