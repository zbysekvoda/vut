library IEEE;
use IEEE.STD_LOGIC_1164.all;

package math_pack is
	type mask_t is
	record
		top: std_logic;
		right: std_logic;
		bottom: std_logic;
		left: std_logic;
	end record;	
	
	function getmask(x,y: natural; COLUMNS, ROWS: natural) return mask_t;
end math_pack;

package body math_pack is
	function getmask(x,y: natural; COLUMNS, ROWS: natural) return mask_t is 
		variable ret: mask_t := (others => '1');
	begin
		if y = 0 then
			ret.top := '0';
		elsif y = (ROWS-1) then
			ret.bottom := '0';
		end if;
		
		if x = 0 then
			ret.left := '0';
		elsif x = (COLUMNS-1) then
			ret.right := '0';
		end if;
				
		return ret;
	end;
end math_pack;
