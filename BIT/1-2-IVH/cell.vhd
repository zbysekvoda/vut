----------------------------------------------------------------------------------
-- Engineer: 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.math_pack.ALL; -- reseni prvniho ukolu

entity cell is
	GENERIC (
		MASK			: mask_t := (others => '1') 				-- mask_t definovano v baliku math_pack
   );
   PORT ( 
      NEIGHBOUR   : IN   STD_LOGIC_VECTOR (3 downto 0); 	-- '1' na urcite pozici urcuje, ze bylo kliknuto na urcitou sousedni bunku
      CLICK       : IN   STD_LOGIC; 							-- '1' urcuje, ze bylo kliknuto na tuto bunku
      STATE       : IN   STD_LOGIC; 							-- aktualni stav bunky
      LIGHT       : OUT  STD_LOGIC								-- novy stav bunky
   );
end cell;

architecture Behavioral of cell is
	constant IDX_TOP     : NATURAL := 0; -- index sousedni bunky nachazejici se nahore v signalu NEIGHBOUR
	constant IDX_LEFT   	: NATURAL := 1; -- index sousedni bunky nachazejici se vlevo  v signalu NEIGHBOUR
	constant IDX_RIGHT  	: NATURAL := 2; -- index sousedni bunky nachazejici se vpravo v signalu NEIGHBOUR
	constant IDX_BOTTOM 	: NATURAL := 3; -- index sousedni bunky nachazejici se dole   v signalu NEIGHBOUR 
	signal 	masked		: STD_LOGIC_VECTOR (3 downto 0);
begin
	--   Pozadavky na funkci (pouze kombinacni logika, nesmi byt zadny latch!)
	--   pri urceni vystupni hodnoty LIGHT je nutne zohlednit aktualni stav bunky (signal STATE) a dale zda-li bylo kliknuto na bunku (CLICKED) a pripadne na nejake bunky z okoli (NEIGHBOUR)
	--   pocet kliknuti (tzn. lichy nebo sudy) timpadem urci novy stav bunky (signal LIGHT)   
	--   hodnota bunky z okoli vstupuje do vypoctu jen pokud maska je na prislusne pozici v '1'
	masked(IDX_TOP) <= MASK.top AND NEIGHBOUR(IDX_TOP); 
	masked(IDX_LEFT) <= MASK.left AND NEIGHBOUR(IDX_LEFT); 
	masked(IDX_RIGHT) <= MASK.right AND NEIGHBOUR(IDX_RIGHT); 
	masked(IDX_BOTTOM) <= MASK.bottom AND NEIGHBOUR(IDX_BOTTOM); 
	
	LIGHT <= masked(IDX_TOP) XOR masked(IDX_LEFT) XOR masked(IDX_RIGHT) XOR masked(IDX_BOTTOM) XOR STATE XOR CLICK; 
end Behavioral;

