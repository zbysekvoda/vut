#!/usr/local/bin/python3

class Polynomial(object):
	def __init__(self, *args, **kwargs):
		"""Initialises new polynom. 
		Polynom can be initialised usign list, keyword args (x0 - xn) or positional args in order from x0 to xn."""
		self.polynom = []

		if len(kwargs) > 0:
			for key in kwargs:
				self.setXn(key,kwargs[key])
		if len(args) > 0:
			if type(args[0]) is list:
				self.polynom = args[0]
			else:
				self.polynom = list(args)

	def setXn(self, n, k):
		"""Sets multiplicator of member x of index n to k. 
		Technically setXn() sets element in list polynom with index n.
		Index can be set using string 'xn', where n is integer."""
		if type(n) == str:
			n = n[1:]
			n = int(n)			

		if len(self.polynom) < n+1:
			for i in range(n-len(self.polynom)+1):
				self.polynom.append(0)
		self.polynom[n] = k	

	def getMem(self, n):
		"""Gets formatted member with x^n for print."""
		mn = self.polynom[n]	#member multiplier
		ma = abs(mn)			#member multiplier abs val

		ma = "" if ma == 1 and n != 0 else str(ma)

		sign = " + " if mn >= 0 else " - "

		if mn == 0:
			return ""

		if n == 0:
			mem = ma
		elif n == 1:
			mem = ma+"x"
		else:
			mem = ma+"x^"+str(n)		
		return sign+mem

	def __str__(self):
		"""Returns actual polynom as string"""
		ret = []

		for i in reversed(range(0, len(self.polynom))):
			ret.append(self.getMem(i));
		res = "".join(ret)

		if len(res) == 0:
			return "0"
		elif len(res) > 3:
			if res[1] == "+":
				return res[3:]
			elif res[1] == "-":
				return "-"+res[3:]
		return res

	def makeLen(self, newLen):
		"""Stretches out list polynom with zeroes if needed"""
		while len(self.polynom) < newLen:
			self.polynom.append(0)

	def __add__(self, other):
		"""Overloads adittion operator + for adding two polynoms."""
		maxLen = max(len(self.polynom), len(other.polynom))
		self.makeLen(maxLen)
		other.makeLen(maxLen)
		return Polynomial([x + y for x, y in zip(self.polynom, other.polynom)])

	def __pow__(self, power):
		"""Overloads power operator ** for polynom and integers greater or equal zero."""
		poly = self
		if power == 0:
			poly = 0
		else:
			for i in range (0, power-1):
				poly *= self
		return poly

	def __mul__(self, other):
		"""Overloads multiplication operator * for two polynoms."""
		maxLen = max(len(self.polynom), len(other.polynom))
		self.makeLen(maxLen)
		other.makeLen(maxLen)
		res = [0]*(len(self.polynom)+len(other.polynom)-1)
		for o1,i1 in enumerate(self.polynom):
			for o2,i2 in enumerate(other.polynom):
				res[o1+o2] += i1*i2
		return Polynomial(res)

	def __eq__(self, other):
		"""Return true, if self and other are equal, else returns false. Based on polynom list comparsion."""
		maxLen = max(len(self.polynom), len(other.polynom))
		self.makeLen(maxLen)
		other.makeLen(maxLen)

		return self.polynom == other.polynom
		
	def derivative(self):
		"""Returns derivative of actual polynom. Inner list polynom stays unchanged."""
		ret = []
		for n,val in enumerate(self.polynom):
			if n == 0: 
				continue

			ret.append(val*n)
		return Polynomial(ret)	

	def numberify(self, x):
		"""Returns value of actual polynom for given x"""
		suma = 0
		for n,val in enumerate(self.polynom):
			suma += val*(x**n)
		return suma

	def at_value(self, *args):
		"""Returns value of actual polynom for given x or difference of polynom values (numberify(2nd_param) - numberify(1st_param))"""
		if len(args) == 1:
			return self.numberify(args[0])
		elif len(args) == 2:
			return self.numberify(args[1]) - self.numberify(args[0])
		else:
			return None
			
def test():
	assert str(Polynomial(0,1,0,-1,4,-2,0,1,3,0)) == "3x^8 + x^7 - 2x^5 + 4x^4 - x^3 + x"
	assert str(Polynomial([-5,1,0,-1,4,-2,0,1,3,0])) == "3x^8 + x^7 - 2x^5 + 4x^4 - x^3 + x - 5"
	assert str(Polynomial(x7=1, x4=4, x8=3, x9=0, x0=0, x5=-2, x3= -1, x1=1)) == "3x^8 + x^7 - 2x^5 + 4x^4 - x^3 + x"
	assert str(Polynomial(x2=0)) == "0"
	assert str(Polynomial(x0=0)) == "0"
	assert Polynomial(x0=2, x1=0, x3=0, x2=3) == Polynomial(2,0,3)
	assert Polynomial(x2=0) == Polynomial(x0=0)
	assert str(Polynomial(x0=1)+Polynomial(x1=1)) == "x + 1"
	assert str(Polynomial([-1,1,1,0])+Polynomial(1,-1,1)) == "2x^2"
	pol1 = Polynomial(x2=3, x0=1)
	pol2 = Polynomial(x1=1, x3=0)
	assert str(pol1+pol2) == "3x^2 + x + 1"
	assert str(pol1+pol2) == "3x^2 + x + 1"
	assert str(Polynomial(x0=-1,x1=1)**1) == "x - 1"
	assert str(Polynomial(x0=-1,x1=1)**2) == "x^2 - 2x + 1"
	pol3 = Polynomial(x0=-1,x1=1)
	assert str(pol3**4) == "x^4 - 4x^3 + 6x^2 - 4x + 1"
	assert str(pol3**4) == "x^4 - 4x^3 + 6x^2 - 4x + 1"
	assert str(Polynomial(x0=2).derivative()) == "0"
	assert str(Polynomial(x3=2,x1=3,x0=2).derivative()) == "6x^2 + 3"
	assert str(Polynomial(x3=2,x1=3,x0=2).derivative().derivative()) == "12x"
	pol4 = Polynomial(x3=2,x1=3,x0=2)
	assert str(pol4.derivative()) == "6x^2 + 3"
	assert str(pol4.derivative()) == "6x^2 + 3"
	assert Polynomial(-2,3,4,-5).at_value(0) == -2
	assert Polynomial(x2=3, x0=-1, x1=-2).at_value(3) == 20
	assert Polynomial(x2=3, x0=-1, x1=-2).at_value(3,5) == 44
	pol5 = Polynomial([1,0,-2])
	assert pol5.at_value(-2.4) == -10.52
	assert pol5.at_value(-2.4) == -10.52
	assert pol5.at_value(-1,3.6) == -23.92
	assert pol5.at_value(-1,3.6) == -23.92

if __name__ == '__main__':
	test()