#!/usr/bin/env python3

import fileinput
import multiprocessing

words = {}

for line in fileinput.input():
	word = line.rstrip()
	words[word[::-1]] = word

palindroms = [w for w in words if w == w[::-1]]

result = [w for w in words if w not in palindroms and w[::-1] in words]

print(sorted(result))
