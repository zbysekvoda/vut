#!/usr/bin/env python3

import multiprocessing

try:
    cpus = multiprocessing.cpu_count()
except NotImplementedError:
    cpus = 2

def count(n):
    while n > 0:
        n -= 1

pool = multiprocessing.Pool(processes=cpus)
pool.map(count, [10**8, 10**8]) 



