#!/usr/bin/env python3

from __future__ import division, print_function
import re
import itertools

### REPLACEMENT PART ###
def replacement():
    in_f = open("xkcd1313.ipynb","r")
    out_f = open("xvodaz01.ipynb", "a");

    reg_ex1 = re.compile("bu.*ls")
    reg_ex2 = re.compile("xkcd")
    replacement1 = "[gikuj]..n|a.[alt]|[pivo].l|i..o|[jocy]e|sh|di|oo"
    replacement2 = "xvodaz01"
    for line in in_f:
        if line.find("xkcd") != -1:
            line = reg_ex1.sub(replacement1, line)
        if line.find('=') != -1 and line.find('[') != -1 and line.find('!') == -1:
            line = reg_ex2.sub(replacement2, line)
        out_f.write(line)
    out_f.close()
    in_f.close()

replacement()

### MISTAKES PART ###
def words(text): return set(text.split())

winners = words('''washington adams jefferson jefferson madison madison monroe
    monroe adams jackson jackson van-buren harrison polk taylor pierce buchanan
    lincoln lincoln grant grapartnt hayes garfield cleveland harrison cleveland mckinley
    mckinley roosevelt taft wilson wilson harding coolidge hoover roosevelt
    roosevelt roosevelt roosevelt truman eisenhower eisenhower kennedy johnson nixon
    nixon carter reagan reagan bush clinton clinton bush bush obama obama''')

losers = words('''clinton jefferson adams pinckney pinckney clinton king adams
    jackson adams clay van-buren van-buren clay cass scott fremont breckinridge
    mcclellan seymour greeley tilden hancock blaine cleveland harrison bryan bryan
    parker bryan roosevelt hughes cox davis smith hoover landon willkie dewey dewey
    stevenson stevenson nixon goldwater humphrey mcgovern ford carter mondale
    dukakis bush dole gore kerry mccain romney''')

losers = losers - winners

def mistakes(regex, winners, losers):
    "The set of mistakes made by this regex in classifying winners and losers."
    return ({"Should have matched: " + W
             for W in winners if not re.search(regex, W)} |
            {"Should not have matched: " + L
             for L in losers if re.search(regex, L)})

def verify(regex, winners, losers):
    assert not mistakes(regex, winners, losers)
    return True

xvodaz01 = "[gikuj]..n|a.[alt]|[pivo].l|i..o|[jocy]e|sh|di|oo"

print(mistakes(xvodaz01, winners, losers))