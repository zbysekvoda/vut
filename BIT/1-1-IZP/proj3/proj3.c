/**
 * Kostra programu pro 3. projekt IZP 2015/16
 *
 * Jednoducha shlukova analyza: 2D nejblizsi soused.
 * Single linkage
 * http://is.muni.cz/th/172767/fi_b/5739129/web/web/slsrov.html
 */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h> // sqrtf
#include <limits.h> // INT_MAX

/*****************************************************************
 * Ladici makra. Vypnout jejich efekt lze definici makra
 * NDEBUG, napr.:
 *   a) pri prekladu argumentem prekladaci -DNDEBUG
 *   b) v souboru (na radek pred #include <assert.h>
 *      #define NDEBUG
 */
#ifdef NDEBUG
#define debug(s)
#define dfmt(s, ...)
#define dint(i)
#define dfloat(f)
#else

// vypise ladici retezec
#define debug(s) printf("- %s\n", s)

// vypise formatovany ladici vystup - pouziti podobne jako printf
#define dfmt(s, ...) printf(" - "__FILE__":%u: "s"\n",__LINE__,__VA_ARGS__)

// vypise ladici informaci o promenne - pouziti dint(identifikator_promenne)
#define dint(i) printf(" - " __FILE__ ":%u: " #i " = %d\n", __LINE__, i)

// vypise ladici informaci o promenne typu float - pouziti
// dfloat(identifikator_promenne)
#define dfloat(f) printf(" - " __FILE__ ":%u: " #f " = %g\n", __LINE__, f)

#endif

/*****************************************************************
 * Deklarace potrebnych datovych typu:
 *
 * TYTO DEKLARACE NEMENTE
 *
 *   struct obj_t - struktura objektu: identifikator a souradnice
 *   struct cluster_t - shluk objektu:
 *      pocet objektu ve shluku,
 *      kapacita shluku (pocet objektu, pro ktere je rezervovano
 *          misto v poli),
 *      ukazatel na pole shluku.
 */

struct obj_t {
    int id;
    float x;
    float y;
};

struct cluster_t {
    int size;
    int capacity;
    struct obj_t *obj;
};

/*****************************************************************
 * Deklarace potrebnych funkci.
 *
 * PROTOTYPY FUNKCI NEMENTE
 *
 * IMPLEMENTUJTE POUZE FUNKCE NA MISTECH OZNACENYCH 'TODO'
 *
 */

/*
 Inicializace shluku 'c'. Alokuje pamet pro cap objektu (kapacitu).
 Ukazatel NULL u pole objektu znamena kapacitu 0.
*/
void init_cluster(struct cluster_t *c, int cap)
{
    assert(c != NULL);
    assert(cap >= 0);

	c->size = 0;
	c->capacity = cap;
	
	if(cap == 0){
		c -> obj = NULL; 		
	}
	else{
		c->obj = malloc(cap * sizeof(struct obj_t));

		if(c->obj == NULL){
			printf("Nepovedlo se alokovat pamÄĹĽ\n");
			return;
		}
	}
}

/*
 Odstraneni vsech objektu shluku a inicializace na prazdny shluk.
 */
void clear_cluster(struct cluster_t *c)
{
	free(c->obj);

	init_cluster(c, 0);
}

/// Chunk of cluster objects. Value recommended for reallocation.
const int CLUSTER_CHUNK = 10;

/*
 Zmena kapacity shluku 'c' na kapacitu 'new_cap'.
 */ 
struct cluster_t *resize_cluster(struct cluster_t *c, int new_cap)
{
    // TUTO FUNKCI NEMENTE
    assert(c);
    assert(c->capacity >= 0);
    assert(new_cap >= 0);

    if (c->capacity >= new_cap)
        return c;

    size_t size = sizeof(struct obj_t) * new_cap;

    void *arr = realloc(c->obj, size);
    if (arr == NULL)
        return NULL;

    c->obj = arr;
    c->capacity = new_cap;
    return c;
}

/*
 Prida objekt 'obj' na konec shluku 'c'. Rozsiri shluk, pokud se do nej objekt
 nevejde.
 */
void append_cluster(struct cluster_t *c, struct obj_t obj)
{
	if((long)c->size + CLUSTER_CHUNK > INT_MAX){
		printf("MaximĂĄlnĂ­ kapacita Shluku byla pĹekroÄena\n");
		return;
	}

	if(c->size + CLUSTER_CHUNK >= c->capacity){
		c->obj = realloc(c->obj, (c->capacity + CLUSTER_CHUNK) * sizeof(struct obj_t));
	}

	c->obj[c->size].id = obj.id;
	c->obj[c->size].x = obj.x;
	c->obj[c->size].y = obj.y;
	c->size++;	
}

/*
 Seradi objekty ve shluku 'c' vzestupne podle jejich identifikacniho cisla.
 */
void sort_cluster(struct cluster_t *c);

/*
 Do shluku 'c1' prida objekty 'c2'. Shluk 'c1' bude v pripade nutnosti rozsiren.
 Objekty ve shluku 'c1' budou serazny vzestupne podle identifikacniho cisla.
 Shluk 'c2' bude nezmenen.
 */
void merge_clusters(struct cluster_t *c1, struct cluster_t *c2)
{
    assert(c1 != NULL);
    assert(c2 != NULL);

	if((long)c1->size + c2->size >= INT_MAX){
		printf("MaximĂĄlnĂ­ kapacita Shluku byla pĹekroÄena\n");
		return;
	}
	
	if(c1->capacity < (c1->size + c2->size)){
		c1 = resize_cluster(c1, c1->size + c2->size);
		if(c1 == NULL){
			printf("ZmÄna velikosti Clusteru se nepovedla\n");
			return;
		}
	}
	
	for(int i = 0; i < c2->size; i++){
		append_cluster(c1, c2->obj[i]);
	}

	sort_cluster(c1);
}

/**********************************************************************/
/* Prace s polem shluku */

/*
 Odstrani shluk z pole shluku 'carr'. Pole shluku obsahuje 'narr' polozek
 (shluku). Shluk pro odstraneni se nachazi na indexu 'idx'. Funkce vraci novy
 pocet shluku v poli.
*/
int remove_cluster(struct cluster_t *carr, int narr, int idx)
{
    assert(idx < narr);
    assert(narr > 0);

	clear_cluster(&(carr[idx])); //uvolnĂ­ pamÄĹĽ zabĂ­ranou mazanĂ˝m clusterem
	
	for(int i = idx; i < narr-1; i++){ //posune clustery na mĂ­sto pĹŻvodnĂ­h
		carr[i] = carr[i+1];
	}

	return narr - 1;
}

/*
 Pocita Euklidovskou vzdalenost mezi dvema objekty.
 */
float obj_distance(struct obj_t *o1, struct obj_t *o2)
{
    assert(o1 != NULL);
    assert(o2 != NULL);

	return sqrtf(pow((o1->x - o2->x),2) + pow((o1->y - o2->y),2));
}

/*
 Pocita vzdalenost dvou shluku. Vzdalenost je vypoctena na zaklade nejblizsiho
 souseda.
*/
float cluster_distance(struct cluster_t *c1, struct cluster_t *c2)
{
    assert(c1 != NULL);
    assert(c1->size > 0);
    assert(c2 != NULL);
    assert(c2->size > 0);

	float min_dist = obj_distance(&c1->obj[0],&c2->obj[0]);

	float dist = 0.0;
	
	for(int i = 0; i < c1->size; i++){
		for(int j = 0; j < c2->size; j++){
			dist = obj_distance(&c1->obj[i],&c2->obj[j]);
			if(dist < min_dist){
				min_dist = dist;
			}
		}  
	}
	
	return min_dist;
}

/*
 Funkce najde dva nejblizsi shluky. V poli shluku 'carr' o velikosti 'narr'
 hleda dva nejblizsi shluky (podle nejblizsiho souseda). Nalezene shluky
 identifikuje jejich indexy v poli 'carr'. Funkce nalezene shluky (indexy do
 pole 'carr') uklada do pameti na adresu 'c1' resp. 'c2'.
*/
void find_neighbours(struct cluster_t *carr, int narr, int *c1, int *c2)
{
    assert(narr > 0);

	int min_c1_index = 1;
	int min_c2_index = 0;
	float min_dist = cluster_distance(&carr[0],&carr[1]);
	float dist = 0.0;
	
	for(int i = 0; i < narr; i++){
		for(int j = 0; j < narr; j++){
			if(j == i){
				continue;
			}
			dist = cluster_distance(&carr[i],&carr[j]);
			if(dist < min_dist){
				min_dist = dist;
				min_c1_index = i; 
				min_c2_index = j;
			}
		}  
	}

	*c1 = min_c1_index;
	*c2 = min_c2_index;
}

// pomocna funkce pro razeni shluku
static int obj_sort_compar(const void *a, const void *b)
{
    // TUTO FUNKCI NEMENTE
    const struct obj_t *o1 = a;
    const struct obj_t *o2 = b;
    if (o1->id < o2->id) return -1;
    if (o1->id > o2->id) return 1;
    return 0;
}

/*
 Razeni objektu ve shluku vzestupne podle jejich identifikatoru.
*/
void sort_cluster(struct cluster_t *c)
{
    // TUTO FUNKCI NEMENTE
    qsort(c->obj, c->size, sizeof(struct obj_t), &obj_sort_compar);
}

/*
 Tisk shluku 'c' na stdout.
*/
void print_cluster(struct cluster_t *c)
{
    // TUTO FUNKCI NEMENTE
    for (int i = 0; i < c->size; i++)
    {
        if (i) putchar(' ');
        printf("%d[%g,%g]", c->obj[i].id, c->obj[i].x, c->obj[i].y);
    }
    putchar('\n');
}

/*
 Ze souboru 'filename' nacte objekty. Pro kazdy objekt vytvori shluk a ulozi
 jej do pole shluku. Alokuje prostor pro pole vsech shluku a ukazatel na prvni
 polozku pole (ukalazatel na prvni shluk v alokovanem poli) ulozi do pameti,
 kam se odkazuje parametr 'arr'. Funkce vraci pocet nactenych objektu (shluku).
 V pripade nejake chyby uklada do pameti, kam se odkazuje 'arr', hodnotu NULL.
*/
int load_clusters(char *filename, struct cluster_t **arr)
{
    assert(arr != NULL);
	
	FILE *fp = fopen(filename, "r");
	if (fp == NULL){
		return 0;
	}
	
	const int maxLineLength = 256;
	char line[maxLineLength];
	int counter = 0;
	
	for(int i = 0; i < maxLineLength; i++){
		line[i] = '\0';
	}

	do{
		line[counter] = fgetc(fp);
		counter++;
	} while(counter <= maxLineLength && line[counter-1] != '\n' && line[counter-1] != EOF);

	if(counter == 1){
		return 0;
	}

	int numberOfObjects = 0;
	sscanf(line,"count=%d",&numberOfObjects);
	
	//r"[+,-]?[0-9]+"
	if(numberOfObjects <= 0){
		printf("PĹĂ­liĹĄ mĂĄlo objektĹŻ, nebo nesprĂĄvnÄ uvedenĂ˝ poÄet clusterĹŻ\n");
		return 0;
	}

	*arr = malloc(numberOfObjects * sizeof(struct cluster_t));
	if(*arr == NULL){
		return 0;
	}

	char *end;
	for(int i = 0; i < numberOfObjects; i++){
		init_cluster(&(*arr)[i], 1);
		
		counter = 0;
	
		for(int i = 0; i < maxLineLength; i++){
			line[i] = '\0';
		}

		do{
			line[counter] = fgetc(fp);
			counter++;
		} while(counter <= maxLineLength && line[counter-1] != '\n' && line[counter-1] != EOF);

		if(counter == 1){
			printf("Soubor neobsahuje zadanĂ˝ poÄet objektĹŻ \n");
			return 0;
		}
 
		int id = strtol(line, &end, 10);
		float x = strtof(end, &end);
		float y = strtof(end, &end);
		
		if(x >= 0 && x<=1000 && y >= 0 && y <=1000){			
			(*arr)[i].obj[0].id = id;
			(*arr)[i].obj[0].x = x;
			(*arr)[i].obj[0].y = y;

			(*arr)[i].size++;
		}
		else{
			printf("SouĹadnice objektu jsou mimo platnĂ˝ rozsah \n");
			return 0;
		}
	}
	
	fclose(fp);
	
	return numberOfObjects;
}

/*
 Tisk pole shluku. Parametr 'carr' je ukazatel na prvni polozku (shluk).
 Tiskne se prvnich 'narr' shluku.
*/
void print_clusters(struct cluster_t *carr, int narr)
{
    printf("Clusters:\n");
    for (int i = 0; i < narr; i++)
    {
        printf("cluster %d: ", i);
        print_cluster(&carr[i]);
    }
}

int main(int argc, char *argv[])
{
	if(argc < 2){
		printf("Moc mĂĄlo argumentĹŻ \n");
	}
	else if(argc > 3){
		printf("PĹĂ­liĹĄ mnoho argumentĹŻ \n");
	}
	else{
		char filename[64];
		sscanf(argv[1], "%s", filename);

		int endNumberOfClusters = 1;
		if(argc == 3){
			sscanf(argv[2], "%d", &endNumberOfClusters);
		}
	
		struct cluster_t *clusters;

		int numberOfClusters = load_clusters(filename, &clusters);
		if(numberOfClusters == 0){
			printf("Nepovedlo se otevĹĂ­t soubor, nebo soubor neobsahuje platnĂŠ definice objektĹŻ\n");
		}
		else{	
			if(numberOfClusters < endNumberOfClusters){
				printf("PoĹžadovanĂ˝ koneÄnĂ˝ poÄet clusterĹŻ je vÄtĹĄĂ­ neĹž poÄet objektĹŻ na vstupu. PokraÄuji s upravenĂ˝m rozsahem (%d -> %d). \n", endNumberOfClusters, numberOfClusters);
			}
			
			int c1 = 0; 
			int c2 = 0;

			while(numberOfClusters > endNumberOfClusters){
				find_neighbours(clusters, numberOfClusters, &c1, &c2);

				merge_clusters(&clusters[c1], &clusters[c2]);
			
				numberOfClusters = remove_cluster(clusters, numberOfClusters, c2);
			}

			print_clusters(clusters, numberOfClusters);

			do{
				numberOfClusters = remove_cluster(clusters, numberOfClusters, numberOfClusters-1); //uvolnĂ­ pamÄĹĽ zabĂ­ranou clustery
			} while(numberOfClusters > 0);
			
			free(clusters); //uvolnĂ­ pole clusterĹŻ
		}
	}

	return 0;
}