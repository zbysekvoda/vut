/*
 * project: IZP - Project 1
 * author: Zbysek Voda
 * login: xvodaz01
*/

#include <stdio.h>
#include <time.h>
#include <limits.h>


// vrati delku retezce predaneho v parametru
int getStringLength(char *s){
	int i = 0;
	while(s[i] != '\0'){
		i++;
	}
	return i;
}

// overi, zda je predany char cislice
int isDigit(char c){
	if(c >= '0' && c <= '9'){
		return 1;
	}

	return 0;
}

// overi, zda je predany retezece cislo
int isNumber(char *s){
	int length = getStringLength(s);

	for(int  i = 0; i < length; i++){ // testuje, jestli jsou to vsechno opravdu jen cislice
		if(!isDigit(s[i])){
			return 0;
		}
	}

	return 1;
}


// vrati zaklad umocneny na exponent
long int power(int base, int exp){
	long int val = 1;
	for(int i = 0; i < exp-1; i++){
		val *= base;
	}
	return val;
}


//funkce podobnĂĄ strtol, avsak vracĂ­ -1 pro cisla vetsi INT_MAX
long int myStrtol(char *s){
	long int val = 0;
	int len = getStringLength(s);

	int handle = 0;
	while(s[handle] == '0'){
		handle++;
	}

	for(int i = handle; i < len; i++){
		val += (s[i] - '0') * power(10, len-i);

		if(val > INT_MAX){
			return -1;
		}
	}

	return val;
}

// pokusi se ziskat kladne cislo z predaneho retezce
// pokud se povede, vrati jeho hodnotu
// pokud se nepovede, vrati -1
long int getNumberFromString(char *s){
	if(!isNumber(s)){
		return -1;
	}

	//long int num = strtol(s, NULL, 10);
	long int num = myStrtol(s);

	
	if(num >= 0){
		return num;
	}
	else{
		return -1;
	}
}

// vrati 1, pokud je predane cislo prvocislo
// jinak vrati 0
int isPrime(long int num){
	unsigned int countDividers = 0;

	if(num > INT_MAX){
		return 0;
	}
	
	if(num > 1){
		for(unsigned int i = 2; i*i <= num; i++){
			if(num % i == 0){
				countDividers++;
			}
		}
	}
	else{
        return 0;
    }

	if(countDividers > 0){
		return 0;
	}
	else{
		return 1;
	}
}


// vrati datum, pokud je predany retezec datum ve formatu yyyy-mm-dd
// jinak vrati 0
time_t getDate(char *s){
	if(getStringLength(s) > 10){
		return 0;
	}

	if(s[4] != '-' || s[7] != '-'){
		return 0;
	}

	int badChars = 0;
	for(int i = 0; i < 10; i++){
		if(!isDigit(s[i])){
			badChars++;
		}
	}
	if(badChars != 2){ //v celem datu mohou byt 2 znaky jine nez cislice (-)
		return 0;
	}

	int year = strtol(&s[0], NULL, 10) - 1900;
	int month = strtol(&s[5], NULL, 10) - 1;
	int day = strtol(&s[8], NULL, 10);

	if(month >= 0 && month <= 11 && day >= 1 && day <= 31){
		struct tm date;
		date.tm_year = year;
		date.tm_mon = month;
		date.tm_mday = day;
		date.tm_hour = 0;
		date.tm_min = 0;
		date.tm_sec = 0;

		return mktime(&date);
	}

	return 0;
}

// vrati 1, pokud je predany retezec palindrom
// jinak vrati 0
int isPalindrome(char *s){
	int length = getStringLength(s);

	int countUnequalities = 0;

	for(int i = 0; i <= length/2; i++){
		if(s[i] != s[length-i-1]){
			countUnequalities++;
		}
	}

	if(countUnequalities == 0){
		return 1;
	}
	else{
		return 0;
	}


}

int main(int argv, char *argc[]){
	(void)argv;
	(void)argc;

	char testedString[101];

	if(argv > 1){
		printf("I don't want your arguments! \n");
		printf("This program expects stdio input. \n");
		printf("This program is simple text analyser made by Zbysek Voda \n");

		return 1;
	}
	else{
		int scan = scanf("%100s", testedString);
        while(scan == 1){
			long int num = getNumberFromString(testedString); //-1 pokud neni cislo
            time_t date = getDate(testedString); //0 pokud neni datum

            if(date){
                char dateString[16];
                strftime(dateString, 15, "%a %Y-%m-%d", localtime(&date));
                printf("date: %s", dateString);
            }
            else{
				if(isNumber(testedString)){
					if(num != -1){ // pokud se povedlo z retezce ziskat cislo
						printf("number: %d", num); //kdyby tu byl jen vypis prijateho retezce, 0123 by se neprevedlo na 123
					}
					else{
						printf("number: %s", testedString); //je cislo, ale moc velke
					}
                    
                    if(isPrime(num)){
                        printf(" (prime)");
                    }
                }
                else{
                    printf("word: %s", testedString);
                    if(isPalindrome(testedString)){
                        printf(" (palindrome)");
                    }
                }
            }
            printf("\n");

			int scan = scanf("%100s", testedString);
			if(scan == EOF){ //pokud se neco pokazi, skonci program s chybou
				return -1;
			}
        }
	}
	
	//program se neukonÄuje?
	return 0;
}