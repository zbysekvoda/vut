/* 
 * IZP - Project 2
 * Author: Zbysek Voda, xvodaz01
*/

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <math.h>
#include <limits.h>

// returns absolute value of given number
double my_abs(double a){
	return a >= 0 ? a : -a;
}

// function for handling different errors + error messages
void handleInputError(int errCode, int message){
	if(message == 0){
		printf("Bad number of arguments or their type \nRun me with: \n --log X N \n  for log in N iterations or \n --iter MIN MAX EPS \n  for number of iterations \n");	
	}
	else if(message == 1){
		printf("Numeric arguments must be bigger than 0 \n");
	}
	else if(message == 2){
		printf("EPS is too small for exact result \n");
	}
	else if(message == 3){
		printf("N must be at least 1 \n");
	}
	
	errno = errCode;
}

//function for taylor logarithm
double taylor_log(double x, unsigned int n){
	if(x < 0){
		handleInputError(EINVAL, 0);
		return NAN;
	}
	else if(x == 0){
		handleInputError(EINVAL, 0);
		return -INFINITY;
	}
	else if(x == INFINITY){
		return INFINITY;
	}

	if(n <= 0){
		handleInputError(EINVAL, 3);
		return NAN;
	}
	
	double log = 0.0;
	double member = 0.0;
	double sum = 0.0;

	if(x <= 1){
		x = 1-x;
		member = -x;

		//(n+1) ty clen rady je (n/(n+1)) nasobkem predchoziho
		for(unsigned int i = 1; i < n; i++){
			member = member * ((double)i / (i+1)) * x;
			sum += member;
		}
		
		log = -x + sum;
	}
	else{
		double z = (x-1)/x; //substituce
		member = z;

		//(n+1) ty clen rady je (n/(n+1)) nasobkem predchoziho
		for(unsigned int i = 1; i < n; i++){
			member = member * ((double)i / (i+1)) * z;
			sum += member;
		}
		
		log = z + sum;
	}

	return log;
}

//counts iterations for taylor log.
unsigned int countTaylorIterations(double val, double eps){
	unsigned int iterations = 0;

	double result = 0.0;

	double reference = log(val);
	
	do{
		iterations++;

		result = taylor_log(val, iterations);

		if(iterations == UINT_MAX){
			handleInputError(EINVAL, 2);
			break;
		}
	} while(my_abs(result - reference) >= eps); 
	
	return iterations;
}

//function for chain fractions logarithm
double cfrac_log(double x, unsigned int n){
	if(x < 0){
		handleInputError(EINVAL, 0);
		return NAN;
	}
	else if(x == 0){
		handleInputError(EINVAL, 0);
		return -INFINITY;
	}
	else if(x == INFINITY){
		return INFINITY;
	}

	if(n <= 0){
		handleInputError(EINVAL, 3);
		return NAN;
	}

	n = n-1;
	
	double z = (x-1)/(x+1);
	double log = 2*z;
	
	double pom = (2*n + 1);
	
	for(int i = n; i > 0; i--){
		pom = (2*i - 1) - ((long double)i*i * (long double)z*z) / pom;
	}
	
	log /= pom;

	return log;
}

//counts iterations for chain fractions
unsigned int countCFragIterations(double val, double eps){
	unsigned int iterations = 1;
	
	double result= 0.0;

	double reference = log(val);

	do{
		iterations++;

		result = cfrac_log(val, iterations);

		if(iterations == UINT_MAX){
			handleInputError(EINVAL, 2);
			break;
		}
	} while(my_abs(result-reference) >= eps);

	return iterations;
}

//returns bigger number
unsigned int returnBigger(unsigned int a, unsigned int b){
	return a > b ? a : b;
}

//finds desired string in argc on 1st index, ix string is not found, sets error state
int testSpecificationSettings(char *argc[], char *specif){
	if(strcmp(argc[1], specif) == 0){
		return 1;
	}
	else{
		handleInputError(EINVAL, 0);
		return 0;
	}	
}

//loads x and n from argc
int loadXN(char *argc[], double *x, unsigned int *n){
	if(sscanf(argc[2], "%lf", x) != 1){
		handleInputError(ECANCELED, 0);
		return 0;
	}
	if(sscanf(argc[3], "%u", n) != 1){
		handleInputError(ECANCELED, 0);
		return 0;
	}

	return 1;
}

//loads min, max and eps from argc
int loadMinMaxEps(char *argc[], double *min, double *max, double *eps){
	if(sscanf(argc[2], "%lf", min) != 1){
		handleInputError(ECANCELED, 0);
		return 0;
	}
	if(sscanf(argc[3], "%lf", max) != 1){
		handleInputError(ECANCELED, 0);
		return 0;
	}
	if(sscanf(argc[4], "%lf", eps) != 1){
		handleInputError(ECANCELED, 0);
		return 0;
	}

	return 1;
}

//tests if x and n are bigger than 0
int xNInRange(double x, unsigned int n){
	if(x > 0.0 && n > 0){
		return 1;
	}
	else{
		handleInputError(ERANGE, 1);
		return 0;
	}
}

//tests if min, max and eps are bigger than 0
int minMaxEpsInRange(double min, double max, double eps){
	if(min > 0.0 && max > 0 && eps > 0){
		return 1;
	}
	else{
		handleInputError(ERANGE, 1);
		return 0;
	}
}

void countBisectionLimits(double val, double eps, unsigned int *min, unsigned int *max, int type){
	unsigned int upper = 1;

	double result = 0.0;

	result = cfrac_log(val, upper);

	double reference = log(val);

	while(my_abs(result - reference) >= eps){
		upper *= 2;

		result = type == 1 ? taylor_log(val, upper) : cfrac_log(val, upper);
	}

	*max = upper;
	*min = upper / 2;
}

//counts taylor iterations using bisection
unsigned int countBisection(double val, double eps, int type){
	unsigned int min = 0; 
	unsigned int max = 0;
	countBisectionLimits(val, eps, &min, &max, type);
	
	unsigned int middle = 0.0;

	double result = 0.0;
	double reference = log(val);

	do{
		middle = (min + max) / 2;
		result = taylor_log(val, middle);

		if(my_abs(result - reference) >= eps){
			min = middle+1;
		}
		else{
			max = middle-1;
		}
	}while(max >= min && max - min > 1);
	
	return middle;
}


int main(int argv, char *argc[]){
	errno = 0;

	if(argv == 4 && testSpecificationSettings(argc, "--log")){
		double x = 0.0;
		unsigned int n = 0;

		loadXN(argc, &x,&n);
		if(xNInRange(x,n)){
			printf("       log(%.5g) = %.12g \n", x, log(x));
			printf("    cf_log(%.5g) = %.12g \n", x, cfrac_log(x,n));
			printf("taylor_log(%.5g) = %.12g \n", x, taylor_log(x,n)); 
		}		
	}
	else if(argv == 5 && testSpecificationSettings(argc, "--iter")){
		//./proj2 --iter MIN MAX EPS
		double min = 0.0;
		double max = 0.0;
		double eps = 0.0;

		if(min > max){
			double pom = min;
			min = max;
			max = pom;
		}	

		loadMinMaxEps(argc, &min, &max, &eps);
		if(minMaxEpsInRange(min, max, eps)){
			printf("       log(%.12g) = %.12g \n", min, log(min));
			printf("       log(%.12g) = %.12g \n", max, log(max));

			unsigned int cfrac_iter = returnBigger(countCFragIterations(min, eps), countCFragIterations(max, eps));
			printf("continued fraction iterations = %d \n", cfrac_iter);
			printf("    cf_log(%.12g) = %.12g \n", min, cfrac_log(min, cfrac_iter));
			printf("    cf_log(%.12g) = %.12g \n", max, cfrac_log(max, cfrac_iter));
			
			unsigned int taylor_iter = returnBigger(countTaylorIterations(min, eps), countTaylorIterations(max, eps));
			printf("taylor polynomial iterations = %d \n", taylor_iter);
			printf("taylor_log(%.12g) = %.12g \n", min, taylor_log(min, taylor_iter));
			printf("taylor_log(%.12g) = %.12g \n", max, taylor_log(max, taylor_iter));
		}
	}
	else{
		handleInputError(EINVAL, 0);
	}
	
	if(errno){
		perror("Error");
	}
	return errno;
}