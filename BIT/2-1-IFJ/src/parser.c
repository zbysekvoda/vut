//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj

#ifndef PARSER_C
#define PARSER_C

#include "parser.h"
#include "lexical_analysis.c"
#include "list.c"
#include "embedded_functions.c"
#include "memory_functions.c"

//deklaracia potrebnych globalnych premennych
SToken c, tmp, pom_token;
char *class_store = NULL;
char *func_store;
char *id_called_func;
char *id_called_class;
struct hash_table *T;
struct hash_table_local *T_loc;
int arg_counter;
tDLList TokenList;
tStack ParamStack;
int argument_index;
int return_bool;
void *assign_value;
char *assign_type;
int relationOperation = 0;
int isReturnExp = 0;
int isVoid = 0;



SToken returned;
int if_level = 0;
int while_level = 0;
ConditionalAdressStack if_adress_stack;
ConditionalAdressStack while_adress_stack;

//pomocna funkcia pre zistenie rovnosti 2 tokenov
int TokenTypeEqual(SToken s1, TokenType s2)
{
	if (s1.type == s2)
		return 1;
	else
		return -1;
}
//funckia pre zistenie ci sa v lass Main nachadya funkcia run
int CheckMain()
{
	hash_item *item;

	if((item = hashTab_search(T, "run", type_func, "Main")) == NULL)
		return ERR_VAR;
	else
		return ERR_OK;
}

//funkcia pre rozbitie plne kvalifikovaneho identifikatoru pre ziskanie prvej a druhej casti
int breakIdentifier(char * FQID, char **first_part, char **second_part)
{
	char dot[] = ".";

	int position = find(FQID, dot);
	int lenght = strlen(FQID);

	lenght = lenght - position;
	position++;

	*first_part = my_malloc(sizeof(char)*position);
	if(*first_part == NULL)
		return ERR_INTER;

	*second_part = my_malloc(sizeof(char)*lenght +1);
	if(*second_part == NULL)
		return ERR_INTER;

	*second_part = substr(FQID, position, lenght);
	*first_part = substr(FQID,0, position-1);

	return ERR_OK;
}
//funckia pre cistanie dalsieho tokenu z listu, ktory je za aktualnym vstupom a nevymaze ho
SToken readNextToken()
{
	SToken g;

	DLLast(&TokenList);
	DLCopy(&TokenList, &g);

	return g;
}
//pomocna funkcia pre nacitanie posledneho tokenu v liste
SToken readToken()
{
	SToken r;

	DLCopyLast(&TokenList, &r);

	return r;
}

//funkcia pre nacitanie tokenu z listu vyuzivana v 2. prechode
SToken getTokenList()
{
	SToken r;

	DLCopyLast(&TokenList, &r);
	DLDeleteLast (&TokenList);

	return r;
}
//funckia pre nacitanie tokenov s preskakovanim komentarov
SToken getRealToken()
{
	SToken s;

	while((TokenTypeEqual((s = getToken()), comment)) == 1)
		continue;

	return s;
}
//prvy prechod analyzy, naplnenie statickej tabulky symbolov, overenie konstrukcii statickych deklaracii, plni sa tokenlist
int Parse()
{
	initConditionalAdressStack(&if_adress_stack);
	initConditionalAdressStack(&while_adress_stack);

	//printf("som v parse\n");
	int retV, ret;
	FArg arguments[256];
	char *id_store;
	FArg polozka;
	char *datatype;

	while((TokenTypeEqual((tmp = getRealToken()), end)) == -1)
	{
		if((TokenTypeEqual(tmp, error)) == 1)
			return ERR_LEX;
		else if((TokenTypeEqual(tmp, error99)) == 1)
			return ERR_INTER;

		DLInsertFirst (&TokenList, &tmp);

		if((tmp.type != keyword_normal) && (tmp.type != keyword_data_type))
			continue;

		if(keywords[*(unsigned char*)tmp.data] == keywords[7])
		{
			//printf("som v class\n");
			tmp = getRealToken();
			if((TokenTypeEqual(tmp, error)) == 1)
				return ERR_LEX;
			else if((TokenTypeEqual(tmp, error99)) == 1)
				return ERR_INTER;
			//printf("davam do zoznamu v class\n");
			DLInsertFirst (&TokenList, &tmp);

			if(tmp.type != simple_identifier)
				return ERR_SYN;

			if((strcmp(tmp.data, "ifj16")) == 0)
				return ERR_SEM_UNDEF;

			class_store = tmp.data;

			//printf("koncim nacitanie mena class\n");
		}

		if(keywords[*(unsigned char*)tmp.data] == keywords[5])
		{
			//printf("som v static########\n");
			tmp = getRealToken();
			if((TokenTypeEqual(tmp, error)) == 1)
				return ERR_LEX;
			else if((TokenTypeEqual(tmp, error99)) == 1)
				return ERR_INTER;

			if(class_store == NULL)
				return ERR_SYN;

			DLInsertFirst (&TokenList, &tmp);

			if(tmp.type != keyword_data_type)
				return ERR_SYN;

			datatype = keywords[*(unsigned char*)tmp.data];
			hash_item *item;

			if(keywords[*(unsigned char*)tmp.data] == keywords[4])
			{
				tmp = getRealToken();
				if((TokenTypeEqual(tmp, error)) == 1)
					return ERR_LEX;
				else if((TokenTypeEqual(tmp, error99)) == 1)
					return ERR_INTER;

				DLInsertFirst (&TokenList, &tmp);

				if(tmp.type != simple_identifier)
					return ERR_SYN;

				id_store = (char*)tmp.data;

				tmp = getRealToken();
				if((TokenTypeEqual(tmp, error)) == 1)
					return ERR_LEX;
				else if((TokenTypeEqual(tmp, error99)) == 1)
					return ERR_INTER;

				DLInsertFirst (&TokenList, &tmp);

				int retV = StateDeclarationFunction();
				if(retV == ERR_SYN)
					return ERR_SYN;
				else if(retV == ERR_LEX)
					return ERR_LEX;
				else if(retV == ERR_INTER)
					return ERR_INTER;
				else if(retV == ERR_SEM_UNDEF)
					return ERR_SEM_UNDEF;
				else if(retV == ERR_SEM_TYPE)
					return ERR_SEM_TYPE;
				else if(retV == ERR_SEM)
					return ERR_SEM;
				else if(retV == ERR_VAR)
					return ERR_VAR;
				else if(retV == ERR_OTHER)
					return ERR_OTHER;
	//			printf("vratil som sa po nacitani bez chyby\n");

				FSymbolTable *data_in = my_malloc(sizeof(FSymbolTable) + sizeof(FArg)*arg_counter);
				if(data_in == NULL)
					return ERR_INTER;

				data_in->return_type = datatype;
	//			printf("return type funkcie %s\n",data_in->return_type );
				data_in->arg_num = arg_counter;
	//			printf("%dargnum\n",data_in->arg_num );

				for(int i = arg_counter-1; i >= 0; i--)
				{
					stackTop_param (&ParamStack, &polozka);
	//				printf("preslo top %d\n",i);
					stackPop_param(&ParamStack);
	//				printf("preslo pop %d\n",i );
					data_in->arguments[i].type = polozka.type;
					data_in->arguments[i].id_name = polozka.id_name;
	//				printf("preslo koniec\n");
	//				printf("%s\n",data_in->arguments[i].type);
	//				printf("%s\n",data_in->arguments[i].id_name);
				}
	//			printf("vypisuje funkciu meno a class\n");
	//			printf("%s\n",id_store );
	//			printf("%s\n",class_store );

				if((item = hashTab_search (T, id_store, type_func, class_store)) == NULL)
				{
					if((item = hashTab_search (T, id_store, type_id, class_store)) == NULL)
					{
						ret = hashTab_insert(T,id_store,NULL,data_in,type_func, class_store);
						if(ret == ERR_INTER)
							return ERR_INTER;
					}
					else
						return ERR_SEM_UNDEF;
				}
				else
				{
					return ERR_SEM_UNDEF;
				}
	//			printf("dokoncilo declaration void funkcie\n");

			}
			else
			{
				tmp = getRealToken();
				if((TokenTypeEqual(tmp, error)) == 1)
					return ERR_LEX;
				else if((TokenTypeEqual(tmp, error99)) == 1)
					return ERR_INTER;
	//			printf("preplo ma do druhej vetvy\n");
				DLInsertFirst (&TokenList, &tmp);

				if(tmp.type != simple_identifier)
					return ERR_SYN;

				id_store = (char*)tmp.data;

				tmp = getRealToken();
				if((TokenTypeEqual(tmp, error)) == 1)
					return ERR_LEX;
				else if((TokenTypeEqual(tmp, error99)) == 1)
					return ERR_INTER;

				DLInsertFirst (&TokenList, &tmp);

				if(tmp.type == bracket_left)
				{
					int retV = StateDeclarationFunction();
					if(retV == ERR_SYN)
						return ERR_SYN;
					else if(retV == ERR_LEX)
						return ERR_LEX;
					else if(retV == ERR_INTER)
						return ERR_INTER;
					else if(retV == ERR_SEM_UNDEF)
						return ERR_SEM_UNDEF;
					else if(retV == ERR_SEM_TYPE)
						return ERR_SEM_TYPE;
					else if(retV == ERR_SEM)
						return ERR_SEM;
					else if(retV == ERR_VAR)
						return ERR_VAR;
					else if(retV == ERR_OTHER)
						return ERR_OTHER;

					FSymbolTable *data_in = my_malloc(sizeof(FSymbolTable) + sizeof(FArg)*arg_counter);
					if(data_in == NULL)
						return ERR_INTER;

					data_in->return_type = datatype;
				//	printf("return type funkcie %s\n",data_in->return_type );
					data_in->arg_num = arg_counter;
				//	printf("%dargnum\n",data_in->arg_num );

				//	printf("vypisuje funkciu meno a class\n");
				//	printf("%s\n",id_store );
				//	printf("%s\n",class_store );

					for(int i = arg_counter-1; i >= 0; i--)
					{
						stackTop_param (&ParamStack, &polozka);
				//		printf("preslo top %d\n",i);
						stackPop_param(&ParamStack);
				//		printf("preslo pop %d\n",i );
						data_in->arguments[i].type = polozka.type;
						data_in->arguments[i].id_name = polozka.id_name;
				//		printf("preslo koniec\n");
				//		printf("%s\n",data_in->arguments[i].type);
				//		printf("%s\n",data_in->arguments[i].id_name);
					}

					if((item = hashTab_search (T, id_store, type_func, class_store)) == NULL)
					{
						if((item = hashTab_search (T, id_store, type_id, class_store)) == NULL)
						{
							ret = hashTab_insert(T,id_store,NULL,data_in,type_func, class_store);
							if(ret == ERR_INTER)
								return ERR_INTER;
						}
						else
							return ERR_SEM_UNDEF;
					}
					else
					{
						return ERR_SEM_UNDEF;
					}
				//	printf("dokoncilo declaration inej ako void funkcie\n");
				}
				else
				{
					if (tmp.type == operator_relation)
					{
						tmp = getRealToken();
						if((TokenTypeEqual(tmp, error)) == 1)
							return ERR_LEX;
						else if((TokenTypeEqual(tmp, error99)) == 1)
							return ERR_INTER;

						DLInsertFirst (&TokenList, &tmp);

						if 	((tmp.type == num_d) ||
							   (tmp.type == num_i) ||
							   (tmp.type == string) ||
							   (tmp.type == keyword_true) ||
							   (tmp.type == simple_identifier) ||
							   (tmp.type == fully_qualified_identifier) ||
							   (tmp.type == keyword_false))

						{
							IDSymbolTable *data_in = my_malloc(sizeof(IDSymbolTable));
							if(data_in == NULL)
								return ERR_INTER;

							if((tmp.type == num_d) && (datatype == "double"))
							{
								data_in->value = (double*)tmp.data;
				//				printf("data %f\n",*(double*)data_in->value );
							}
							else if((tmp.type == num_i) && (datatype == "double"))
							{
								data_in->value = mallocDouble(*(int*)tmp.data);
				//				printf("data %f\n",*(double*)data_in->value );
							}
							else if((tmp.type == num_i) && (datatype == "int"))
							{
				//				printf("som v int vetve\n");
								data_in->value = (int*)tmp.data;
				//				printf("data %d\n",*(int*)data_in->value );
							}
							else if((tmp.type == string) && (datatype == "string"))
							{
								data_in->value = mallocString((char*)tmp.data);
				//				printf("data %s\n",(char*)data_in->value );
							}
							else if((tmp.type == keyword_false) && (tmp.type == keyword_true) && (datatype == "boolean"))
							{
								data_in->value = keywords[*(unsigned char*)tmp.data];
				//				printf("data %s\n",(char*)data_in->value );
							}
							else if(tmp.type == simple_identifier)
							{
								hash_item *id_item;
								if((id_item = hashTab_search (T, tmp.data, type_id, class_store)) != NULL)
								{
									if((strcmp(id_item->struct_type.itemID->type, datatype)) == 0)
									{
										data_in->value = id_item->struct_type.itemID->value;
									}
								}
								else
									return ERR_SEM_UNDEF;
							}
							else if(tmp.type == fully_qualified_identifier)
							{
								hash_item *id_item;
								char *first_p;
								char *second_p;
								int pom = breakIdentifier(tmp.data, &first_p, &second_p);
								if(pom == ERR_INTER)
									return ERR_INTER;

								if((id_item = hashTab_search (T, second_p, type_id, first_p)) != NULL)
								{
									if((strcmp(id_item->struct_type.itemID->type, datatype)) == 0)
									{
										data_in->value = id_item->struct_type.itemID->value;
									}
								}
								else
									return ERR_OTHER;
							}
							else
								return ERR_SEM;

							data_in->type = datatype;
						//	printf("%s datatype\n",data_in->type );
							data_in->initialized = 1;
						//	printf("%d init\n",data_in->initialized );

						//	printf("vypisuje identifikator meno a class\n");
						//	printf("%s\n",id_store );
						//	printf("%s\n",class_store );

							if((item = hashTab_search (T, id_store, type_id, class_store)) == NULL)
							{
								if((item = hashTab_search (T, id_store, type_func, class_store)) == NULL)
								{
									ret = hashTab_insert(T,id_store,data_in,NULL,type_id, class_store);
									if(ret == ERR_INTER)
										return ERR_INTER;
								}
								else
									return ERR_SEM_UNDEF;

						//		printf("preslo static declaration\n");
							}
							else
							{
								return ERR_SEM;
							}

							tmp = getRealToken();
							if((TokenTypeEqual(tmp, error)) == 1)
								return ERR_LEX;
							else if((TokenTypeEqual(tmp, error99)) == 1)
								return ERR_INTER;

							DLInsertFirst (&TokenList, &tmp);

							if (tmp.type == semicolon)
							{
						//		printf("dokoncilo declaration identifikatoru s priradenim \n");
							}
							else
								return ERR_SYN;
						}
						else
							return ERR_SYN;
					}
					else if (tmp.type == semicolon)
					{
						//printf("identifikator so semicolonom\n");
						IDSymbolTable *data_in = my_malloc(sizeof(IDSymbolTable));
						if(data_in == NULL)
							return ERR_INTER;

						 if((strcmp(datatype, "int")) == 0)
				        {				             
				                data_in->value = mallocInt(0);				            
				        }
				        else if((strcmp(datatype, "double")) == 0)
				        {
				                data_in->value = mallocDouble(0);			            
				        }
				        else if((strcmp(datatype, "String")) == 0)
				        {    
				                data_in->value = mallocString(" ");				           
				        }

						data_in->type = datatype;
						//printf("%s datatype\n",data_in->type );
						data_in->initialized = 0;
						//printf("%d init\n",data_in->initialized );

						//printf("vypisuje identifikator meno a class\n");
						//printf("%s\n",id_store );
						//printf("%s\n",class_store );

						if((item = hashTab_search (T, id_store, type_id, class_store)) == NULL)
						{
							if((item = hashTab_search (T, id_store, type_func, class_store)) == NULL)
							{
								ret = hashTab_insert(T,id_store,data_in,NULL,type_id, class_store);
								if(ret == ERR_INTER)
									return ERR_INTER;
							}
							else
								return ERR_SEM_UNDEF;
						}
						else
							return ERR_SEM_UNDEF;

						//printf("dokoncilo declaration v id so semicolon \n");
					}
					else
						return ERR_SYN;
				}

			}
		}
	}

	DLInsertFirst (&TokenList, &tmp);

	int mainv = CheckMain();
	if(mainv != 0)
		return mainv;
//zaciaok 2. prechodu, prebieha nacitavanie z listu tokenov
	int retValue = Start();
	if(retValue == ERR_LEX)
		return ERR_LEX;
	else if(retValue == ERR_SYN)
		return ERR_SYN;
	else if(retValue == ERR_SEM_UNDEF)
		return ERR_SEM_UNDEF;
	else if(retValue == ERR_SEM_TYPE)
		return ERR_SEM_TYPE;
	else if(retValue == ERR_SEM)
		return ERR_SEM;
	else if(retValue == ERR_READ)
		return ERR_READ;
	else if(retValue == ERR_VAR)
		return ERR_VAR;
	else if(retValue == ERR_ZERO)
		return ERR_ZERO;
	else if(retValue == ERR_OTHER)
		return ERR_OTHER;
	else if(retValue == ERR_INTER)
		return ERR_INTER;
	else
		return ERR_OK;
}

//mnozina first pre vyrazy, volanie non void funkcii ....id = .....
int StateID()
{
	int retV;
	char * func_call_name;
	hash_item *item;
	hash_item_local *item_loc;
	hash_table_local item_ll;
	char *pom_typ_id;

	//printf("V STATE ID\n");

	if(c.type == simple_identifier)
	{	//printf("som v simple_identifier\n");
		id_called_func = c.data;
		id_called_class = class_store;
		pom_token.type = c.type;
		pom_token.data = c.data;
		if((item = hashTab_search (T, c.data, type_func, class_store)) != NULL)
		{
			//printf("som tu \n");
			if((strcmp(item->struct_type.itemF->return_type, "void")) != 0)
				return ERR_SEM_TYPE;

			func_call_name = c.data;

			if((TokenTypeEqual((c = getTokenList()), error)) == 1)
				return ERR_LEX;
			else if((TokenTypeEqual(c, error99)) == 1)
				return ERR_INTER;

			if(c.type != bracket_left)
				return ERR_SYN;

			argument_index = 0;

			retV = StateFunctionCall(func_call_name, class_store);
			if(retV == ERR_SYN)
				return ERR_SYN;
			else if(retV == ERR_LEX)
				return ERR_LEX;
			else if(retV == ERR_INTER)
				return ERR_INTER;
			else if(retV == ERR_SEM_UNDEF)
				return ERR_SEM_UNDEF;
			else if(retV == ERR_SEM_TYPE)
				return ERR_SEM_TYPE;
			else if(retV == ERR_SEM)
				return ERR_SEM;
			else if(retV == ERR_VAR)
				return ERR_VAR;
			else if(retV == ERR_OTHER)
				return ERR_OTHER;

			addElement(&functionDefinitions, f_call, &(item->struct_type.itemF->def_adress), mallocInt(item->struct_type.itemF->arg_num), NULL);

			if((TokenTypeEqual((c = getTokenList()), error)) == 1)
				return ERR_LEX;
			else if((TokenTypeEqual(c, error99)) == 1)
				return ERR_INTER;

			if(c.type == semicolon)
				return ERR_OK;
			else
				return ERR_SYN;
		}
		else if((item_loc = hashTab_search_local (T_loc, c.data)) != NULL)
		{
			item_loc->init_local = 1;
			pom_typ_id = item_loc->datatype_local;
		}
		else if((item = hashTab_search (T, c.data, type_id, class_store)) != NULL)
		{
			item->struct_type.itemID->initialized = 1;
			pom_typ_id = item->struct_type.itemID->type;
		}
		else
		{
		//	printTokenInfo(c);
		//		printf("%s\n",class_store );
			return ERR_SEM_UNDEF;
		}

	}
	else if(c.type == fully_qualified_identifier)
	{
		char *first_p;
		char *second_p;
		int pom = breakIdentifier(c.data, &first_p, &second_p);
		if(pom == ERR_INTER)
			return ERR_INTER;
		id_called_class = first_p;
		id_called_func = second_p;
		pom_token.type = c.type;
		pom_token.data = c.data;

		if((item = hashTab_search (T, second_p, type_func, first_p)) != NULL)
		{

			if((strcmp(item->struct_type.itemF->return_type, "void")) != 0)
				return ERR_SEM_TYPE;

			func_call_name = second_p;

			if((TokenTypeEqual((c = getTokenList()), error)) == 1)
				return ERR_LEX;
			else if((TokenTypeEqual(c, error99)) == 1)
				return ERR_INTER;

			if(c.type != bracket_left)
				return ERR_SYN;

			argument_index = 0;

			retV = StateFunctionCall(func_call_name, first_p);
			if(retV == ERR_SYN)
				return ERR_SYN;
			else if(retV == ERR_LEX)
				return ERR_LEX;
			else if(retV == ERR_INTER)
				return ERR_INTER;
			else if(retV == ERR_SEM_UNDEF)
				return ERR_SEM_UNDEF;
			else if(retV == ERR_SEM_TYPE)
				return ERR_SEM_TYPE;
			else if(retV == ERR_SEM)
				return ERR_SEM;
			else if(retV == ERR_VAR)
				return ERR_VAR;
			else if(retV == ERR_OTHER)
				return ERR_OTHER;


			addElement(&functionDefinitions, f_call, &(item->struct_type.itemF->def_adress), mallocInt(item->struct_type.itemF->arg_num), NULL);

			if((TokenTypeEqual((c = getTokenList()), error)) == 1)
				return ERR_LEX;
			else if((TokenTypeEqual(c, error99)) == 1)
				return ERR_INTER;

			if(c.type == semicolon)
				return ERR_OK;
			else
				return ERR_SYN;


		}
		else if((item = hashTab_search (T, second_p, type_id, first_p)) == NULL)
			return ERR_SEM_UNDEF;
		else
		{
			item->struct_type.itemID->initialized = 1;
			pom_typ_id = item->struct_type.itemID->type;
		}


	}


	if ((TokenTypeEqual((c = getTokenList()), error)) == 1)
		return ERR_LEX;
	else if((TokenTypeEqual(c, error99)) == 1)
		return ERR_INTER;
	//Chybovy stav
	if(c.type == operator_relation)
	{

		//Rozhodneme ci sa jedna o volanie funkcie, alebo moze ist o vyraz
		if((TokenTypeEqual((c = getTokenList()), error)) == 1)
			return ERR_LEX;
		else if((TokenTypeEqual(c, error99)) == 1)
			return ERR_INTER;


		if 	((c.type == simple_identifier) ||
			   (c.type == fully_qualified_identifier) ||
			   (c.type == keyword_false) ||
			   (c.type == keyword_true) ||
			   (c.type == bracket_left) ||
			   (c.type == builtInFunction) ||
			   (c.type == num_i) || (c.type == num_d) ||
			   (c.type == string))
		{
			if(c.type == simple_identifier)
			{
				//printTokenInfo(c);
				//printf("%s\n",class_store );
				if((item = hashTab_search (T, c.data, type_func, class_store)) != NULL)
				{
					//printf("sem by malo skocit\n");
					if((strcmp(item->struct_type.itemF->return_type, "void")) == 0)
						return ERR_SEM_TYPE;

					func_call_name = c.data;

					if((TokenTypeEqual((c = getTokenList()), error)) == 1)
						return ERR_LEX;
					else if((TokenTypeEqual(c, error99)) == 1)
						return ERR_INTER;

					if(c.type != bracket_left)
						return ERR_SYN;

					argument_index = 0;
					//printf("idem do state func call\n");

					retV = StateFunctionCall(func_call_name, class_store);
					if(retV == ERR_SYN)
						return ERR_SYN;
					else if(retV == ERR_LEX)
						return ERR_LEX;
					else if(retV == ERR_INTER)
						return ERR_INTER;
					else if(retV == ERR_SEM_UNDEF)
						return ERR_SEM_UNDEF;
					else if(retV == ERR_SEM_TYPE)
						return ERR_SEM_TYPE;
					else if(retV == ERR_SEM)
						return ERR_SEM;
					else if(retV == ERR_VAR)
						return ERR_VAR;
					else if(retV == ERR_OTHER)
						return ERR_OTHER;
					//printf("vratil som sa naspat po nacitani parametrov\n");

					addElement(&functionDefinitions, f_call, &(item->struct_type.itemF->def_adress), mallocInt(item->struct_type.itemF->arg_num), NULL);

					if(pom_token.type == simple_identifier)
					{
						//printf("skocilo sem\n");
						hash_item_local *tmp_loc = hashTab_search_local(T_loc, id_called_func);
						if(tmp_loc == NULL)
						{
							//printf("sem by skoit nemalo\n");
							hash_item *tmp_stat = hashTab_search(T, id_called_func, type_id ,id_called_class);
							assign_value = tmp_stat->struct_type.itemID->value;
						}
						else
						{
							//printf("idem priradovat\n");
							if(strcmp(item->struct_type.itemF->return_type, "int") == 0){
								writeToLocal(tmp_loc, ret_i, Int);
							} else if(strcmp(item->struct_type.itemF->return_type, "double") == 0){
								writeToLocal(tmp_loc, ret_d, Double);
							} else if(strcmp(item->struct_type.itemF->return_type, "String") == 0){
								writeToLocal(tmp_loc, ret_s, String);
							}
						}
					}
					else
					{
						hash_item *tmp_stat = hashTab_search(T, id_called_func, type_id ,id_called_class);
						assign_value = tmp_stat->struct_type.itemID->value;

						if(((strcmp(item->struct_type.itemF->return_type, "int")) == 0) && ((strcmp(pom_typ_id, "int")) == 0))
							addElement(&functionDefinitions, assign_i, ret_i, NULL, assign_value);
						else if(((strcmp(item->struct_type.itemF->return_type, "double")) == 0) && ((strcmp(pom_typ_id, "double")) == 0))
							addElement(&functionDefinitions, assign_f, ret_d, NULL, assign_value);
						else if(((strcmp(item->struct_type.itemF->return_type, "int")) == 0) && ((strcmp(pom_typ_id, "double")) == 0)){
							addElement(&functionDefinitions, c_itod, ret_i, NULL, reg_d1);
							addElement(&functionDefinitions, assign_f, reg_d1, NULL, assign_value);
						} else if(((strcmp(item->struct_type.itemF->return_type, "String")) == 0) && ((strcmp(pom_typ_id, "String")) == 0))
							addElement(&functionDefinitions, assign_s, ret_s, NULL, assign_value);
					}

					if((TokenTypeEqual((c = getTokenList()), error)) == 1)
						return ERR_LEX;
					else if((TokenTypeEqual(c, error99)) == 1)
						return ERR_INTER;

					if(c.type == semicolon)
						return ERR_OK;
					else
						return ERR_SYN;

				}
				else
				{
				//	printf("idem do StateExpression z stateID\n");
					retV = StateExpression();
					if(retV == ERR_LEX)
						return ERR_LEX;
					else if(retV == ERR_SYN)
						return ERR_SYN;
					else if(retV == ERR_SEM_UNDEF)
						return ERR_SEM_UNDEF;
					else if(retV == ERR_SEM_TYPE)
						return ERR_SEM_TYPE;
					else if(retV == ERR_SEM)
						return ERR_SEM;
					else if(retV == ERR_READ)
						return ERR_READ;
					else if(retV == ERR_VAR)
						return ERR_VAR;
					else if(retV == ERR_ZERO)
						return ERR_ZERO;
					else if(retV == ERR_OTHER)
						return ERR_OTHER;
					else if(retV == ERR_INTER)
						return ERR_INTER;
					else
					{
					//	printf("koncim uspesne StateExpression v stateID\n");
						return ERR_OK;
					}

				}
			}
			else if(c.type == fully_qualified_identifier)
			{
				char *first_p;
				char *second_p;
				int pom = breakIdentifier(c.data, &first_p, &second_p);
				if(pom == ERR_INTER)
					return ERR_INTER;

				if((item = hashTab_search (T, second_p, type_func, first_p)) != NULL)
				{
					if((strcmp(item->struct_type.itemF->return_type, "void")) == 0)
						return ERR_SEM_TYPE;

					func_call_name = second_p;

					if((TokenTypeEqual((c = getTokenList()), error)) == 1)
						return ERR_LEX;
					else if((TokenTypeEqual(c, error99)) == 1)
						return ERR_INTER;

					if(c.type != bracket_left)
						return ERR_SYN;

					argument_index = 0;

					retV = StateFunctionCall(func_call_name, first_p);
					if(retV == ERR_SYN)
						return ERR_SYN;
					else if(retV == ERR_LEX)
						return ERR_LEX;
					else if(retV == ERR_INTER)
						return ERR_INTER;
					else if(retV == ERR_SEM_UNDEF)
						return ERR_SEM_UNDEF;
					else if(retV == ERR_SEM_TYPE)
						return ERR_SEM_TYPE;
					else if(retV == ERR_SEM)
						return ERR_SEM;
					else if(retV == ERR_VAR)
						return ERR_VAR;
					else if(retV == ERR_OTHER)
						return ERR_OTHER;

					addElement(&functionDefinitions, f_call, &(item->struct_type.itemF->def_adress), mallocInt(item->struct_type.itemF->arg_num), NULL);

					if(pom_token.type == simple_identifier)
					{
						hash_item_local *tmp_loc = hashTab_search_local(T_loc, func_call_name);
						if(tmp_loc == NULL)
						{
							hash_item *tmp_stat = hashTab_search(T, id_called_func, type_id ,id_called_class);
							assign_value = tmp_stat->struct_type.itemID->value;
							tmp_stat->struct_type.itemID->initialized = 1;

						}
						else
						{
							tmp_loc->init_local = 1;

							if(strcmp(item->struct_type.itemF->return_type, "int") == 0){
								writeToLocal(tmp_loc, ret_i, Int);
							} else if(strcmp(item->struct_type.itemF->return_type, "double") == 0){
								writeToLocal(tmp_loc, ret_d, Double);
							} else if(strcmp(item->struct_type.itemF->return_type, "String") == 0){
								writeToLocal(tmp_loc, ret_s, String);
							}
						}
					}
					else
					{
						hash_item *tmp_stat = hashTab_search(T, id_called_func, type_id ,id_called_class);
						assign_value = tmp_stat->struct_type.itemID->value;
						tmp_stat->struct_type.itemID->initialized = 1;

						if(((strcmp(item->struct_type.itemF->return_type, "int")) == 0) && ((strcmp(pom_typ_id, "int")) == 0))
							addElement(&functionDefinitions, assign_i, ret_i, NULL, assign_value);
						else if(((strcmp(item->struct_type.itemF->return_type, "double")) == 0) && ((strcmp(pom_typ_id, "double")) == 0))
							addElement(&functionDefinitions, assign_f, ret_d, NULL, assign_value);
						else if(((strcmp(item->struct_type.itemF->return_type, "int")) == 0) && ((strcmp(pom_typ_id, "double")) == 0))
						{
							addElement(&functionDefinitions, c_itod, ret_i, NULL, reg_d1);
							addElement(&functionDefinitions, assign_f, reg_d1, NULL, assign_value);
						}
						else if(((strcmp(item->struct_type.itemF->return_type, "String")) == 0) && ((strcmp(pom_typ_id, "String")) == 0))
							addElement(&functionDefinitions, assign_s, ret_s, NULL, assign_value);
					}


					if((TokenTypeEqual((c = getTokenList()), error)) == 1)
						return ERR_LEX;
					else if((TokenTypeEqual(c, error99)) == 1)
						return ERR_INTER;

					if(c.type == semicolon)
						return ERR_OK;
					else
						return ERR_SYN;

				}
				else
				{
					retV = StateExpression();
					if(retV == ERR_LEX)
						return ERR_LEX;
					else if(retV == ERR_SYN)
						return ERR_SYN;
					else if(retV == ERR_SEM_UNDEF)
						return ERR_SEM_UNDEF;
					else if(retV == ERR_SEM_TYPE)
						return ERR_SEM_TYPE;
					else if(retV == ERR_SEM)
						return ERR_SEM;
					else if(retV == ERR_READ)
						return ERR_READ;
					else if(retV == ERR_VAR)
						return ERR_VAR;
					else if(retV == ERR_ZERO)
						return ERR_ZERO;
					else if(retV == ERR_OTHER)
						return ERR_OTHER;
					else if(retV == ERR_INTER)
						return ERR_INTER;
					else
						return ERR_OK;
				}

			}
			else if(c.type == builtInFunction)
			{
				//printf("som v buildinfunc v stateID\n");
				char *first_p;
				char *second_p;
				int pom = breakIdentifier(builtInFunctions[*(unsigned char*)c.data], &first_p, &second_p);
				if(pom == ERR_INTER)
					return ERR_INTER;

				if((item = hashTab_search (T, second_p, type_func, first_p)) != NULL)
				{
					if((strcmp(item->struct_type.itemF->return_type, pom_typ_id)) != 0)
						return ERR_SEM_TYPE;

					//printf("idem do buildinfunc\n");
					retV = StateFunction();
					if(retV == ERR_SYN)
						return ERR_SYN;
					else if(retV == ERR_LEX)
						return ERR_LEX;
					else if(retV == ERR_INTER)
						return ERR_INTER;
					else if(retV == ERR_SEM_UNDEF)
						return ERR_SEM_UNDEF;
					else if(retV == ERR_SEM_TYPE)
						return ERR_SEM_TYPE;
					else if(retV == ERR_SEM)
						return ERR_SEM;
					else if(retV == ERR_VAR)
						return ERR_VAR;
					else if(retV == ERR_OTHER)
						return ERR_OTHER;



				}
			}
			else
			{
				retV = StateExpression ();
				if(retV == ERR_LEX)
					return ERR_LEX;
				else if(retV == ERR_SYN)
					return ERR_SYN;
				else if(retV == ERR_SEM_UNDEF)
					return ERR_SEM_UNDEF;
				else if(retV == ERR_SEM_TYPE)
					return ERR_SEM_TYPE;
				else if(retV == ERR_SEM)
					return ERR_SEM;
				else if(retV == ERR_READ)
					return ERR_READ;
				else if(retV == ERR_VAR)
					return ERR_VAR;
				else if(retV == ERR_ZERO)
					return ERR_ZERO;
				else if(retV == ERR_OTHER)
					return ERR_OTHER;
				else if(retV == ERR_INTER)
					return ERR_INTER;
				else
					return ERR_OK;
			}
		}
		else
			return ERR_SYN;
	}
	else
		return ERR_SYN;


}
//funckia pre kontrolu if statementu
int StateIf()
{
	int RetV, retV3;
	//printf("som v StateIf\n");
	//pravidlo if -> "(" -> EXPRESSION -> ")" -> "{"
	if((TokenTypeEqual((c = getTokenList()), error)) == 1)
		return ERR_LEX;
	else if((TokenTypeEqual(c, error99)) == 1)
		return ERR_INTER;

	if (c.type != bracket_left)
		return ERR_SYN;

	relationOperation = 1;
	//vykona sa syntakticka analyza vyrazu
	RetV = StateExpAnalysis();	// VYRAZ
	if(RetV == ERR_LEX)
		return ERR_LEX;
	else if(RetV == ERR_SYN)
		return ERR_SYN;
	else if(RetV == ERR_SEM_UNDEF)
		return ERR_SEM_UNDEF;
	else if(RetV == ERR_SEM_TYPE)
		return ERR_SEM_TYPE;
	else if(RetV == ERR_SEM)
		return ERR_SEM;
	else if(RetV == ERR_READ)
		return ERR_READ;
	else if(RetV == ERR_VAR)
		return ERR_VAR;
	else if(RetV == ERR_ZERO)
		return ERR_ZERO;
	else if(RetV == ERR_OTHER)
		return ERR_OTHER;
	else if(RetV == ERR_INTER)
		return ERR_INTER;

	if_level++;

	AdressStruct* actual = createNewConditionalOnStack(&if_adress_stack);

	addElement(&functionDefinitions, j_a_eq_i, reg_i1, mallocInt(1), &(actual->if_addr));
	addElement(&functionDefinitions, j_a_neq_i, reg_i1, mallocInt(1), &(actual->else_addr));
	actual->if_addr = addElement(&functionDefinitions, label, NULL, NULL, NULL);

	//printTokenInfo(c);
	//printf("\n");
	relationOperation = 0;
	if (c.type == bracket_open)
	{
	//	printf("sem preslo\n");
		RetV = StateCore();
		if(RetV == ERR_LEX)
			return ERR_LEX;
		else if(RetV == ERR_SYN)
			return ERR_SYN;
		else if(RetV == ERR_SEM_UNDEF)
			return ERR_SEM_UNDEF;
		else if(RetV == ERR_SEM_TYPE)
			return ERR_SEM_TYPE;
		else if(RetV == ERR_SEM)
			return ERR_SEM;
		else if(RetV == ERR_READ)
			return ERR_READ;
		else if(RetV == ERR_VAR)
			return ERR_VAR;
		else if(RetV == ERR_ZERO)
			return ERR_ZERO;
		else if(RetV == ERR_OTHER)
			return ERR_OTHER;
		else if(RetV == ERR_INTER)
			return ERR_INTER;

		if((TokenTypeEqual((c = getTokenList()), error)) == 1)
			return ERR_LEX;
		else if((TokenTypeEqual(c, error99)) == 1)
			return ERR_INTER;

		if(c.type != keyword_normal)
			return ERR_SYN;

		addElement(&functionDefinitions, j_a_jmp, NULL, NULL, &(actual->end_addr));
		actual->else_addr = addElement(&functionDefinitions, label, NULL, NULL, NULL);
		if (keywords[*(unsigned char*)c.data] == keywords[10])
		{
			retV3 = StateElse();
			if(retV3 == ERR_LEX)
				return ERR_LEX;
			else if(retV3 == ERR_SYN)
				return ERR_SYN;
			else if(retV3 == ERR_SEM_UNDEF)
				return ERR_SEM_UNDEF;
			else if(retV3 == ERR_SEM_TYPE)
				return ERR_SEM_TYPE;
			else if(retV3 == ERR_SEM)
				return ERR_SEM;
			else if(retV3 == ERR_READ)
				return ERR_READ;
			else if(retV3 == ERR_VAR)
				return ERR_VAR;
			else if(retV3 == ERR_ZERO)
				return ERR_ZERO;
			else if(retV3 == ERR_OTHER)
				return ERR_OTHER;
			else if(retV3 == ERR_INTER)
				return ERR_INTER;
			else
				actual->end_addr = addElement(&functionDefinitions, label, NULL, NULL, NULL);
				popAdressStruct(&if_adress_stack);
				return ERR_OK;
		}
		else
			return ERR_SYN;
	}
	else
		return ERR_SYN;
}
//funckia pre kontrolu while statementu
int StateWhile()
{
	int RetV;

	//pravidlo if -> "(" -> EXPRESSION -> ")" -> "{"
	if((TokenTypeEqual((c = getTokenList()), error)) == 1)
		return ERR_LEX;
	else if((TokenTypeEqual(c, error99)) == 1)
		return ERR_INTER;

	AdressStruct* actual = createNewConditionalOnStack(&while_adress_stack);
	actual->while_addr = addElement(&functionDefinitions, label, NULL, NULL, NULL);

	if (c.type != bracket_left)
		return ERR_SYN;
	relationOperation = 1;
	RetV = StateExpAnalysis ();	// VYRAZ
	if(RetV == ERR_LEX)
		return ERR_LEX;
	else if(RetV == ERR_SYN)
		return ERR_SYN;
	else if(RetV == ERR_SEM_UNDEF)
		return ERR_SEM_UNDEF;
	else if(RetV == ERR_SEM_TYPE)
		return ERR_SEM_TYPE;
	else if(RetV == ERR_SEM)
		return ERR_SEM;
	else if(RetV == ERR_READ)
		return ERR_READ;
	else if(RetV == ERR_VAR)
		return ERR_VAR;
	else if(RetV == ERR_ZERO)
		return ERR_ZERO;
	else if(RetV == ERR_OTHER)
		return ERR_OTHER;
	else if(RetV == ERR_INTER)
		return ERR_INTER;
	relationOperation = 0;

	addElement(&functionDefinitions, j_a_neq_i, reg_i1, mallocInt(1), &(actual->end_addr));

	if (c.type == bracket_open)
	{
		RetV = StateCore();
		if(RetV == ERR_LEX)
			return ERR_LEX;
		else if(RetV == ERR_SYN)
			return ERR_SYN;
		else if(RetV == ERR_SEM_UNDEF)
			return ERR_SEM_UNDEF;
		else if(RetV == ERR_SEM_TYPE)
			return ERR_SEM_TYPE;
		else if(RetV == ERR_SEM)
			return ERR_SEM;
		else if(RetV == ERR_READ)
			return ERR_READ;
		else if(RetV == ERR_VAR)
			return ERR_VAR;
		else if(RetV == ERR_ZERO)
			return ERR_ZERO;
		else if(RetV == ERR_OTHER)
			return ERR_OTHER;
		else if(RetV == ERR_INTER)
			return ERR_INTER;
		else
		{
			addElement(&functionDefinitions, j_a_jmp, NULL, NULL, &(actual->while_addr));
			actual->end_addr = addElement(&functionDefinitions, label, NULL, NULL, NULL);
			return ERR_OK;
		}
	}
	else
		return ERR_SYN;


}

//vyraz return pre non void funkciu
int StateReturn()
{
	int RetV;
	hash_item *item;

	isReturnExp = 1; //zabrání hledání v hash tabulce na konci exp. analysis
	return_bool = 1;

	if((TokenTypeEqual((c = getTokenList()), error)) == 1)
		return ERR_LEX;
	else if((TokenTypeEqual(c, error99)) == 1)
		return ERR_INTER;
//printf("idem do StateExpAnalysis v StateReturn\n");
	RetV = StateExpAnalysis();
	if(RetV == ERR_LEX)
		return ERR_LEX;	
	else if(RetV == ERR_SYN)
		return ERR_SYN;
	else if(RetV == ERR_SEM_UNDEF)
		return ERR_SEM_UNDEF;
	else if(RetV == ERR_SEM_TYPE)
		return ERR_SEM_TYPE;
	else if(RetV == ERR_SEM)
		return ERR_SEM;
	else if(RetV == ERR_READ)
		return ERR_READ;
	else if(RetV == ERR_VAR)
		return ERR_VAR;
	else if(RetV == ERR_ZERO)
		return ERR_ZERO;
	else if(RetV == ERR_OTHER)
		return ERR_OTHER;
	else if(RetV == ERR_INTER)
		return ERR_INTER;
//	printf("%d vystup state StateExpAnalysis\n",RetV );
//	printf("vratil som sa zo StateExpAnalysis v StateReturn\n");

	/*if((TokenTypeEqual((c = getTokenList()), error)) == 1)
		return ERR_LEX;
	else if((TokenTypeEqual(c, error99)) == 1)
		return ERR_INTER;*/

	if (c.type == semicolon)
	{
		item = hashTab_search(T, func_store, type_func, class_store);
		if(item == NULL)
			return ERR_SEM_UNDEF;

		returned = popFromTop(&tokenStack);

		if((((EData)returned.data)->type) == Int){
			addElement(&functionDefinitions, assign_i, ((EData)returned.data)->value, NULL, ret_i);
		} else if(((EData)returned.data)->type == Double){
			addElement(&functionDefinitions, assign_f, ((EData)returned.data)->value, NULL, ret_d);
		} else if(((EData)returned.data)->type == String){
			addElement(&functionDefinitions, assign_s, ((EData)returned.data)->value, NULL, ret_s);
		}

		hash_item* item = hashTab_search(T, func_store, type_func, class_store);
		if(item == NULL){
			return ERR_SEM_UNDEF;
		}
		addElement(&functionDefinitions, f_ret, mallocInt(T_loc->number_variable), NULL, NULL);

		//item->struct_type.itemID->value = RetV;
		return ERR_OK;
	}
	else
		return ERR_SYN;
}
//vyraz return pre void funkciu
int StateReturnVoid()
{
//	printf("som vo void funkcii\n");
	//printTokenInfo(c);
	//printf("\n");
	if((TokenTypeEqual((c = getTokenList()), error)) == 1)
		return ERR_LEX;
	else if((TokenTypeEqual(c, error99)) == 1)
		return ERR_INTER;

	//printTokenInfo(c);
	//printf("\n");

	if (c.type == semicolon)
	{
		hash_item* item = hashTab_search(T, func_store, type_func, class_store);
		if(item == NULL){
			return ERR_SEM_UNDEF;
		}

		//addElement(&functionDefinitions, f_ret,mallocInt(item->struct_type.itemF->arg_num), NULL, NULL);
		addElement(&functionDefinitions, f_ret,mallocInt(T_loc->number_variable), NULL, NULL);
		return ERR_OK;
	}
	else
		return ERR_SYN;
}
//funckie volana z funcke StateIf pre kontrolu vetvy else
int StateElse()
{
	int RetV;

	if((TokenTypeEqual((c = getTokenList()), error)) == 1)
		return ERR_LEX;
	else if((TokenTypeEqual(c, error99)) == 1)
		return ERR_INTER;

	if (c.type == bracket_open)
	{
		RetV = StateCore();
		if(RetV == ERR_LEX)
			return ERR_LEX;
		else if(RetV == ERR_SYN)
			return ERR_SYN;
		else if(RetV == ERR_SEM_UNDEF)
			return ERR_SEM_UNDEF;
		else if(RetV == ERR_SEM_TYPE)
			return ERR_SEM_TYPE;
		else if(RetV == ERR_SEM)
			return ERR_SEM;
		else if(RetV == ERR_READ)
			return ERR_READ;
		else if(RetV == ERR_VAR)
			return ERR_VAR;
		else if(RetV == ERR_ZERO)
			return ERR_ZERO;
		else if(RetV == ERR_OTHER)
			return ERR_OTHER;
		else if(RetV == ERR_INTER)
			return ERR_INTER;
		else
			return ERR_OK;
	}
	else
		return ERR_SYN;
}
//funckia reprezentujuca zlozeny prikaz za if, else, while
int StateCore()
{
	int retValue, pom;
	char *first_p;
	char *second_p;
	hash_item *item;
	//printf("som v StateCore\n");

	while((TokenTypeEqual((c = getTokenList()), error)) == -1)
	{
		//printf("tu sa vratim\n");
		//printTokenInfo(c);
		//printf("\n");
		if((TokenTypeEqual(c, error99)) == 1)
			return ERR_INTER;

		if(c.type == bracket_close)
			return ERR_OK;

		switch (c.type)
		{
			case simple_identifier:		//premenna
		//		printf("idem do StateID zo StateCore\n");
				if ((retValue = StateID()) == ERR_SYN)
					return ERR_SYN;
				else if(retValue == ERR_LEX)
					return ERR_LEX;
				else if(retValue == ERR_SEM_UNDEF)
					return ERR_SEM_UNDEF;
				else if(retValue == ERR_SEM_TYPE)
					return ERR_SEM_TYPE;
				else if(retValue == ERR_SEM)
					return ERR_SEM;
				else if(retValue == ERR_READ)
					return ERR_READ;
				else if(retValue == ERR_VAR)
					return ERR_VAR;
				else if(retValue == ERR_ZERO)
					return ERR_ZERO;
				else if(retValue == ERR_OTHER)
					return ERR_OTHER;
				else if(retValue == ERR_INTER)
					return ERR_INTER;
				break;

			case fully_qualified_identifier:		//premenna
				if ((retValue = StateID()) == ERR_SYN)
					return ERR_SYN;
				else if(retValue == ERR_LEX)
					return ERR_LEX;
				else if(retValue == ERR_SEM_UNDEF)
					return ERR_SEM_UNDEF;
				else if(retValue == ERR_SEM_TYPE)
					return ERR_SEM_TYPE;
				else if(retValue == ERR_SEM)
					return ERR_SEM;
				else if(retValue == ERR_READ)
					return ERR_READ;
				else if(retValue == ERR_VAR)
					return ERR_VAR;
				else if(retValue == ERR_ZERO)
					return ERR_ZERO;
				else if(retValue == ERR_OTHER)
					return ERR_OTHER;
				else if(retValue == ERR_INTER)
					return ERR_INTER;
				break;

			case keyword_normal:

				if (keywords[*(unsigned char*)c.data] == keywords[13])	// if
				{
					if ((retValue = StateIf()) == ERR_SYN)
						return ERR_SYN;
					else if(retValue == ERR_LEX)
						return ERR_LEX;
					else if(retValue == ERR_SEM_UNDEF)
						return ERR_SEM_UNDEF;
					else if(retValue == ERR_SEM_TYPE)
						return ERR_SEM_TYPE;
					else if(retValue == ERR_SEM)
						return ERR_SEM;
					else if(retValue == ERR_READ)
						return ERR_READ;
					else if(retValue == ERR_VAR)
						return ERR_VAR;
					else if(retValue == ERR_ZERO)
						return ERR_ZERO;
					else if(retValue == ERR_OTHER)
						return ERR_OTHER;
					else if(retValue == ERR_INTER)
						return ERR_INTER;
					break;
				}

				else if (keywords[*(unsigned char*)c.data] == keywords[16])	// while
				{
					if ((retValue = StateWhile()) == ERR_SYN)
						return ERR_SYN;
					else if(retValue == ERR_LEX)
						return ERR_LEX;
					else if(retValue == ERR_SEM_UNDEF)
						return ERR_SEM_UNDEF;
					else if(retValue == ERR_SEM_TYPE)
						return ERR_SEM_TYPE;
					else if(retValue == ERR_SEM)
						return ERR_SEM;
					else if(retValue == ERR_READ)
						return ERR_READ;
					else if(retValue == ERR_VAR)
						return ERR_VAR;
					else if(retValue == ERR_ZERO)
						return ERR_ZERO;
					else if(retValue == ERR_OTHER)
						return ERR_OTHER;
					else if(retValue == ERR_INTER)
						return ERR_INTER;
					break;
				}

				else if (keywords[*(unsigned char*)c.data] == keywords[14])	// return
				{
					if ((retValue = StateReturn()) == ERR_SYN)
						return ERR_SYN;
					else if(retValue == ERR_LEX)
						return ERR_LEX;
					else if(retValue == ERR_SEM_UNDEF)
						return ERR_SEM_UNDEF;
					else if(retValue == ERR_SEM_TYPE)
						return ERR_SEM_TYPE;
					else if(retValue == ERR_SEM)
						return ERR_SEM;
					else if(retValue == ERR_READ)
						return ERR_READ;
					else if(retValue == ERR_VAR)
						return ERR_VAR;
					else if(retValue == ERR_ZERO)
						return ERR_ZERO;
					else if(retValue == ERR_OTHER)
						return ERR_OTHER;
					else if(retValue == ERR_INTER)
						return ERR_INTER;
					break;
				}

				else
					return ERR_SYN;

			case builtInFunction:	//funkcia
				pom = breakIdentifier(builtInFunctions[*(unsigned char*)c.data], &first_p, &second_p);
				if(pom == ERR_INTER)
					return ERR_INTER;

				if((item = hashTab_search (T, second_p, type_func, first_p)) != NULL)
				{
					if((strcmp(item->struct_type.itemF->return_type, "void")) != 0)
						return ERR_SEM_TYPE;
				}
				else
					return ERR_SEM_UNDEF;

				if ((retValue = StateFunction()) == ERR_SYN)
					return ERR_SYN;
				else if(retValue == ERR_LEX)
					return ERR_LEX;
				else if(retValue == ERR_SEM_UNDEF)
					return ERR_SEM_UNDEF;
				else if(retValue == ERR_SEM_TYPE)
					return ERR_SEM_TYPE;
				else if(retValue == ERR_SEM)
					return ERR_SEM;
				else if(retValue == ERR_READ)
					return ERR_READ;
				else if(retValue == ERR_VAR)
					return ERR_VAR;
				else if(retValue == ERR_ZERO)
					return ERR_ZERO;
				else if(retValue == ERR_OTHER)
					return ERR_OTHER;
				else if(retValue == ERR_INTER)
					return ERR_INTER;
				break;

			default:
				return ERR_SYN;
		}
	}

	return ERR_LEX;
}
//funckia pre deklaraciu statickych premennych premennych a funkcii
int StateDeclaration()
{
	int retV;
	hash_item *item;

	//printf("zacalo declaration\n");

	while((TokenTypeEqual((c = getTokenList()), error)) == -1)
	{
		if(c.type == simple_identifier)
		{
			func_store = c.data;

			if((TokenTypeEqual((c = getTokenList()), error)) == 1)
				return ERR_LEX;
			else if((TokenTypeEqual(c, error99)) == 1)
				return ERR_INTER;

			if (c.type == operator_relation)
			{
				if((TokenTypeEqual((c = getTokenList()), error)) == 1)
					return ERR_LEX;
				else if((TokenTypeEqual(c, error99)) == 1)
					return ERR_INTER;


				if 	((c.type == num_d) ||
					   (c.type == fully_qualified_identifier) ||
					   (c.type == simple_identifier) ||
					   (c.type == num_i) ||
					   (c.type == string) ||
					   (c.type == keyword_true) ||
					   (c.type == keyword_false))

				{

					if((TokenTypeEqual((c = getTokenList()), error)) == 1)
						return ERR_LEX;
					else if((TokenTypeEqual(c, error99)) == 1)
						return ERR_INTER;

					if (c.type == semicolon)
					{
						//printf("dokoncilo declaration v declaration \n");
						return ERR_OK;
					}
					else
						return ERR_SYN;
				}
				else
					return ERR_SYN;
			}
			else if (c.type == semicolon)
			{
				return ERR_OK;
			}
			else
			{
				//printf("som v declaration function a toto je dlhy vypis\n");
				if((retV = StateDeclarationFunctionOK()) == ERR_SYN)
					return ERR_SYN;

				if((TokenTypeEqual((c = getTokenList()), error)) == 1)
					return ERR_LEX;
				else if((TokenTypeEqual(c, error99)) == 1)
					return ERR_INTER;

				if (c.type != bracket_open)
					return ERR_SYN;

				//printf("pred core function class store obsahuje %s &&&&&&&\n",class_store );

				if((item = hashTab_search (T, func_store, type_func, class_store)) != NULL)
				{
					item->struct_type.itemF->def_adress = addElement(&functionDefinitions, label, NULL, NULL, NULL);
				}
				else
					return ERR_INTER;

				//printf("idem do core func\n");
				return_bool = 0;
				if((retV = StateCoreFunc()) == ERR_SYN)
					return ERR_SYN;
				else if(retV == ERR_LEX)
					return ERR_LEX;
				else if(retV == ERR_SEM_UNDEF)
					return ERR_SEM_UNDEF;
				else if(retV == ERR_SEM_TYPE)
					return ERR_SEM_TYPE;
				else if(retV == ERR_SEM)
					return ERR_SEM;
				else if(retV == ERR_READ)
					return ERR_READ;
				else if(retV == ERR_VAR)
					return ERR_VAR;
				else if(retV == ERR_ZERO)
					return ERR_ZERO;
				else if(retV == ERR_OTHER)
					return ERR_OTHER;
				else if(retV == ERR_INTER)
					return ERR_INTER;
				else
				{
					//printf("vratil som sa do StateDeclaration\n");
					if(return_bool == 1)
					{
						//printf("v StateDeclaration je vsetko ok\n");
						return ERR_OK;
					}
					else
					{
						//printf("tu nam to crashuje\n");
						return ERR_VAR;
					}
				}
			}
		}
		else
			return ERR_SYN;
	}
	return ERR_LEX;
}

//funckia spustajuca 2. prechod rekurzivneho zostupu
int Start()
{
	int retV;
	//printf("zacal start1188888888888888888888888888888888888888888888\n");

	while((TokenTypeEqual((c = getTokenList()), end)) == -1)
	{
		//printf("while preslo\n");
		if(TokenTypeEqual(c, error) == 1)
			return ERR_LEX;
		else if((TokenTypeEqual(c, error99)) == 1)
			return ERR_INTER;

		if (keywords[*(unsigned char*)c.data] != keywords[7]) //class
			return ERR_SYN;
		//printf("prebehlo start idem do class\n");

		if((retV = StateClass()) == ERR_SYN)
			return ERR_SYN;
		else if(retV == ERR_LEX)
			return ERR_LEX;
		else if(retV == ERR_SEM_UNDEF)
			return ERR_SEM_UNDEF;
		else if(retV == ERR_SEM_TYPE)
			return ERR_SEM_TYPE;
		else if(retV == ERR_SEM)
			return ERR_SEM;
		else if(retV == ERR_READ)
			return ERR_READ;
		else if(retV == ERR_VAR)
			return ERR_VAR;
		else if(retV == ERR_ZERO)
			return ERR_ZERO;
		else if(retV == ERR_OTHER)
			return ERR_OTHER;
		else if(retV == ERR_INTER)
			return ERR_INTER;

		//printf("dokoncilo start\n");

	}
	hash_item *item = hashTab_search(T, "run", type_func, "Main");
	addElement(&program, f_call, &(item->struct_type.itemF->def_adress), mallocInt(item->struct_type.itemF->arg_num), NULL);

	return ERR_OK;
}

//funckia reprezentujuca zaciatok statickej deklaracie
int StateStatic()
{
	int RetV;

	//printf("zacalo static\n");

	if((TokenTypeEqual((c = getTokenList()), error)) == 1)
		return ERR_LEX;
	else if((TokenTypeEqual(c, error99)) == 1)
		return ERR_INTER;

	if(c.type == keyword_data_type)
	{
		if((RetV = StateDeclaration()) == ERR_SYN)
			return ERR_SYN;
		else if(RetV == ERR_LEX)
			return ERR_LEX;
		else if(RetV == ERR_SEM_UNDEF)
			return ERR_SEM_UNDEF;
		else if(RetV == ERR_SEM_TYPE)
			return ERR_SEM_TYPE;
		else if(RetV == ERR_SEM)
			return ERR_SEM;
		else if(RetV == ERR_READ)
			return ERR_READ;
		else if(RetV == ERR_VAR)
			return ERR_VAR;
		else if(RetV == ERR_ZERO)
			return ERR_ZERO;
		else if(RetV == ERR_OTHER)
			return ERR_OTHER;
		else if(RetV == ERR_INTER)
			return ERR_INTER;
		else
		{
			//printf("dokoncilo static\n");
			return ERR_OK;
		}
	}
	else
		return ERR_SYN;
}
//funckia reprezentujuca deklaraciu funkcie
int StateDeclarationFunction()
{
	//printf("som v declaration function8525522\n");
	//FArg *pole[256];
	int retV, cont1;
	//printf("####\n");
	if(tmp.type != bracket_left)
		return ERR_SYN;
	arg_counter = 0;
	stackDelete_param(&ParamStack);
	//printf("deleteparam sa podarilo\n");
	if((retV = DecList()) == ERR_SYN)
		return ERR_SYN;
	else if(retV == ERR_LEX)
		return ERR_LEX;
	else if(retV == ERR_SEM_UNDEF)
		return ERR_SEM_UNDEF;
	else if(retV == ERR_SEM_TYPE)
		return ERR_SEM_TYPE;
	else if(retV == ERR_SEM)
		return ERR_SEM;
	else if(retV == ERR_READ)
		return ERR_READ;
	else if(retV == ERR_VAR)
		return ERR_VAR;
	else if(retV == ERR_ZERO)
		return ERR_ZERO;
	else if(retV == ERR_OTHER)
		return ERR_OTHER;
	else if(retV == ERR_INTER)
		return ERR_INTER;
	else
	{
		//printf("vratil som sa z declistu\n");
//		printf("%dpoce arg\n",arg_counter );
		/*for(int i = 0; i < arg_counter; i++)
			arguments[i]->type = pole[i].type;*/

//		printf("koniec decfunction8555555555555555\n");
		return ERR_OK;
	}
}
//funckia reprezentujuca nacitanie a ulozenie parametrov pri deklaracii funkcii
int DecList()
{
	int retV;
	FArg polozka;
	//int arg_counter = 0;

//	printf("som v dec list\n");

	tmp = getRealToken();
	if((TokenTypeEqual(tmp, error)) == 1)
		return ERR_LEX;
	else if((TokenTypeEqual(tmp, error99)) == 1)
		return ERR_INTER;

	DLInsertFirst (&TokenList, &tmp);

//	printf("keyword datatype ok\n");
//	printTokenInfo(c);
//	printf("%dargcounter\n",arg_counter );

	if(tmp.type == keyword_data_type)
	{
		int len = sizeof(keywords[*(unsigned char*)(tmp.data)]) + 1;
		polozka.type = my_malloc(sizeof(char) * len);
		strcpy(polozka.type, keywords[*(unsigned char*)(tmp.data)]);
//		printf("priradilo\n");
//		printf("%svypisovanie typu\n", polozka.type);

		tmp = getRealToken();
		if((TokenTypeEqual(tmp, error)) == 1)
			return ERR_LEX;
		else if((TokenTypeEqual(tmp, error99)) == 1)
			return ERR_INTER;

		DLInsertFirst (&TokenList, &tmp);

		arg_counter++;
//		printf("%dparameter\n",arg_counter );

		if(tmp.type == simple_identifier)
		{
			int leng = sizeof((unsigned char*)(tmp.data)) + 1;
			polozka.id_name = my_malloc(sizeof(char) * leng);
			strcpy(polozka.id_name, (unsigned char*)(tmp.data));
//			printf("priradilo id\n");
	//		printf("%s vypisovanie id\n", polozka.id_name);

			tmp = getRealToken();
			if((TokenTypeEqual(tmp, error)) == 1)
				return ERR_LEX;
			else if((TokenTypeEqual(tmp, error99)) == 1)
				return ERR_INTER;

			DLInsertFirst (&TokenList, &tmp);

			stackPush_param (&ParamStack, &polozka );

			if(tmp.type == comma)
				if((retV = DecList()) == ERR_SYN)
					return ERR_SYN;
				else if(retV == ERR_LEX)
					return ERR_LEX;
				else if(retV == ERR_SEM_UNDEF)
					return ERR_SEM_UNDEF;
				else if(retV == ERR_SEM_TYPE)
					return ERR_SEM_TYPE;
				else if(retV == ERR_SEM)
					return ERR_SEM;
				else if(retV == ERR_READ)
					return ERR_READ;
				else if(retV == ERR_VAR)
					return ERR_VAR;
				else if(retV == ERR_ZERO)
					return ERR_ZERO;
				else if(retV == ERR_OTHER)
					return ERR_OTHER;
				else if(retV == ERR_INTER)
					return ERR_INTER;

				else if(tmp.type == bracket_right)
				{
	//				printf("skoncilo bracket right\n");
		//			printf("vraciam sa do statedecfunc\n");
					return ERR_OK;
				}
		}
		else
			return ERR_SYN;
	}
	else if(tmp.type == bracket_right)
	{
		//printf("bracket right\n");

		return ERR_OK;
	}


	else
		return ERR_SYN;
}
//funckia reprezentujuca volanie funckie
int StateFunctionCall(char *name_func, char *class_f)
{
	int retV;

	hash_item_local *tmp_local;
	hash_item *tmp_static;

	if((TokenTypeEqual((c = getTokenList()), error)) == 1)
		return ERR_LEX;
	else if((TokenTypeEqual(c, error99)) == 1)
		return ERR_INTER;

	//printf("na vstupe mam #######: \t\t %s \n", tokenTypes[(int) c.type]);
	//printf("%s\n",name_func);
	//printf("%s\n",class_store);

	hash_item *item = hashTab_search (T, name_func, type_func, class_f);
	//printf("hladanie provedlo\n");

	if((c.type == num_d) && ((strcmp(item->struct_type.itemF->arguments[argument_index].type, "double")) == 0))
	{
		//printf("poznamka tu by sme mali sa dostat\n");
		addElement(&functionDefinitions, f_pushp, mallocDouble(*(double*)c.data), typeDouble, NULL);
	}
	else if((c.type == num_i) && ((strcmp(item->struct_type.itemF->arguments[argument_index].type, "double")) == 0))
	{
        addElement(&functionDefinitions, f_pushp, mallocDouble((double)*(int*)c.data), typeDouble, NULL);
	}
	else if((c.type == num_i) && ((strcmp(item->struct_type.itemF->arguments[argument_index].type, "int")) == 0))
	{
		addElement(&functionDefinitions, f_pushp, mallocInt(*(int*)c.data), typeInt, NULL);
	}
	else if((c.type == string) && ((strcmp(item->struct_type.itemF->arguments[argument_index].type, "string")) == 0))
	{
		addElement(&functionDefinitions, f_pushp, mallocString((char*)c.data), typeString, NULL);
	}
	else if(c.type == simple_identifier)
	{

		if((tmp_local= hashTab_search_local (T_loc, (char*)c.data)) != NULL)
		{
			if(((strcmp(tmp_local->datatype_local, "int")) == 0) || ((strcmp("double", item->struct_type.itemF->arguments[argument_index].type)) == 0))
				if((strcmp(tmp_local->datatype_local, item->struct_type.itemF->arguments[argument_index].type)) != 0)
					return ERR_SEM_TYPE;

			if(tmp_local->init_local == 0)
				return ERR_VAR;

			if((strcmp(tmp_local->datatype_local, "double")) == 0){
                readFromLocal(tmp_local, reg_d1, Double);
                addElement(&functionDefinitions, f_pushp, reg_d1, typeDouble, NULL);
            }
			else if(((strcmp(tmp_local->datatype_local, "int")) == 0) && ((strcmp("double", item->struct_type.itemF->arguments[argument_index].type)) == 0)){
				readFromLocal(tmp_local, reg_d1, Double);
				addElement(&functionDefinitions, f_pushp, reg_d1, typeDouble, NULL);
			}
			else if((strcmp(tmp_local->datatype_local, "int")) == 0){
				readFromLocal(tmp_local, reg_i1, Int);
                addElement(&functionDefinitions, f_pushp, reg_i1, typeInt, NULL);
            }

            else if((strcmp(tmp_local->datatype_local, "String")) == 0){
				readFromLocal(tmp_local, reg_s1, String);
				addElement(&functionDefinitions, f_pushp, reg_s1, typeString, NULL);
            }
		}
		else if((tmp_static = hashTab_search (T, (char*)c.data, type_id, class_store)) != NULL)
		{
			if(((strcmp(tmp_static->struct_type.itemID->type, "int")) == 0) || ((strcmp("double", item->struct_type.itemF->arguments[argument_index].type)) == 0))
				if((strcmp(tmp_static->struct_type.itemID->type, item->struct_type.itemF->arguments[argument_index].type)) != 0)
					return ERR_SEM_TYPE;
			if(tmp_static->struct_type.itemID->initialized == 0)
				return ERR_VAR;

			if((strcmp(tmp_static->struct_type.itemID->type, "double")) == 0)
				addElement(&functionDefinitions, f_pushp,tmp_static->struct_type.itemID->value, typeDouble, NULL);
			else if(((strcmp(tmp_static->struct_type.itemID->type, "int")) == 0) && ((strcmp("double", item->struct_type.itemF->arguments[argument_index].type)) == 0))
				addElement(&functionDefinitions, f_pushp, mallocDouble((double)*(int*)tmp_static->struct_type.itemID->value), typeDouble, NULL);
			else if((strcmp(tmp_static->struct_type.itemID->type, "int")) == 0)
				addElement(&functionDefinitions, f_pushp,tmp_static->struct_type.itemID->value, typeInt, NULL);
			else if((strcmp(tmp_static->struct_type.itemID->type, "String")) == 0)
				addElement(&functionDefinitions, f_pushp, tmp_static->struct_type.itemID->value, typeString, NULL);

		}
		else{
            return ERR_SEM_UNDEF;
        }


	}
	else if(c.type == fully_qualified_identifier)
	{
		char *first_p;
		char *second_p;
		int pom = breakIdentifier(c.data, &first_p, &second_p);
		if(pom == ERR_INTER)
			return ERR_INTER;

		if((tmp_static = hashTab_search (T, second_p, type_id, first_p)) != NULL)
		{
			if(((strcmp(tmp_static->struct_type.itemID->type, "int")) == 0) || ((strcmp("double", item->struct_type.itemF->arguments[argument_index].type)) == 0))
				if((strcmp(tmp_static->struct_type.itemID->type, item->struct_type.itemF->arguments[argument_index].type)) != 0)
					return ERR_SEM_TYPE;
			if(tmp_static->struct_type.itemID->initialized == 0)
				return ERR_VAR;

			if((strcmp(tmp_static->struct_type.itemID->type, "double")) == 0)
				addElement(&functionDefinitions, f_pushp,tmp_static->struct_type.itemID->value, typeDouble, NULL);
			else if(((strcmp(tmp_static->struct_type.itemID->type, "int")) == 0) && ((strcmp("double", item->struct_type.itemF->arguments[argument_index].type)) == 0))
				addElement(&functionDefinitions, f_pushp, mallocDouble((double)*(int*)tmp_static->struct_type.itemID->value), typeDouble, NULL);
			else if((strcmp(tmp_static->struct_type.itemID->type, "int")) == 0)
				addElement(&functionDefinitions, f_pushp,tmp_static->struct_type.itemID->value, typeInt, NULL);
			else if((strcmp(tmp_static->struct_type.itemID->type, "String")) == 0)
				addElement(&functionDefinitions, f_pushp, tmp_static->struct_type.itemID->value, typeString, NULL);
		}
		else
			return ERR_SEM_UNDEF;
	}
	else if((item->struct_type.itemF->arg_num == 0) && (c.type == bracket_right))
	{
		//printf("tu som skocil\n");
		return ERR_OK;
	}
	else
	{
		//printf("tu by sme mali byt\n");
		return ERR_SEM_TYPE;
	}


	if((TokenTypeEqual((c = getTokenList()), error)) == 1)
		return ERR_LEX;
	else if((TokenTypeEqual(c, error99)) == 1)
		return ERR_INTER;

	//printf("na vstupe mam #######: \t\t %s \n", tokenTypes[(int) c.type]);

	if(c.type == comma)
	{
		argument_index++;
		if(argument_index >= item->struct_type.itemF->arg_num)
			return ERR_SEM_TYPE;

		retV = StateFunctionCall(name_func, class_f);
		if(retV == ERR_SYN)
			return ERR_SYN;
		else if(retV == ERR_LEX)
			return ERR_LEX;
		else if(retV == ERR_SEM_UNDEF)
			return ERR_SEM_UNDEF;
		else if(retV == ERR_SEM_TYPE)
			return ERR_SEM_TYPE;
		else if(retV == ERR_SEM)
			return ERR_SEM;
		else if(retV == ERR_READ)
			return ERR_READ;
		else if(retV == ERR_VAR)
			return ERR_VAR;
		else if(retV == ERR_ZERO)
			return ERR_ZERO;
		else if(retV == ERR_OTHER)
			return ERR_OTHER;
		else if(retV == ERR_INTER)
			return ERR_INTER;
	}
	else if(c.type == bracket_right)
		return ERR_OK;

}
//funckia reprezentujuca volanie vstavanej funkcie z triedy ifj16
int StateFunctionCallBuildIn(char *name_func, char *class_f)
{
	int retV;

	hash_item_local *tmp_local;
	hash_item *tmp_static;

	if((strcmp(name_func, "print")) == 0)
	{
		isVoid = 1;
		retV = StateExpression();
		isVoid = 0;

		if(retV == ERR_LEX)
			return ERR_LEX;
		else if(retV == ERR_SYN)
			return ERR_SYN;
		else if(retV == ERR_SEM_UNDEF)
			return ERR_SEM_UNDEF;
		else if(retV == ERR_SEM_TYPE)
			return ERR_SEM_TYPE;
		else if(retV == ERR_SEM)
			return ERR_SEM;
		else if(retV == ERR_READ)
			return ERR_READ;
		else if(retV == ERR_VAR)
			return ERR_VAR;
		else if(retV == ERR_ZERO)
			return ERR_ZERO;
		else if(retV == ERR_OTHER)
			return ERR_OTHER;
		else if(retV == ERR_INTER)
			return ERR_INTER;
		else{
			returned = popFromTop(&tokenStack);

			//printf("-----------------------------------------------------Returned is %d\n\n\n", (((EData)returned.data)->type));

			if(((EData)returned.data)->type == Int){
				addElement(&functionDefinitions, c_itos, ((EData)returned.data)->value, NULL, reg_s1);
				addElement(&functionDefinitions, f_pushp, reg_s1, typeString, NULL);
			} else if(((EData)returned.data)->type == Double){
				addElement(&functionDefinitions, c_dtos, ((EData)returned.data)->value, NULL, reg_s1);
				addElement(&functionDefinitions, f_pushp, reg_s1, typeString, NULL);
			} else if(((EData)returned.data)->type == String){
				addElement(&functionDefinitions, f_pushp, ((EData)returned.data)->value, typeString, NULL);
			}

			return ERR_OK;
		}

	}

	//printf("%s\n",name_func );

	if((TokenTypeEqual((c = getTokenList()), error)) == 1)
		return ERR_LEX;
	else if((TokenTypeEqual(c, error99)) == 1)
		return ERR_INTER;
	//printf("idem hladat vstavanu funkciu\n");
	hash_item *item = hashTab_search (T, name_func, type_func, class_f);
	//printf("nasiel som\n");
	//printTokenInfo(c);
	//printf("\n");
	//printf("%d\n", argument_index );
	
	if((item->struct_type.itemF->arg_num == 0) || (c.type == bracket_right))
	{
		//printf("sem by malo skocit\n");
		return ERR_OK;
	}
	else if((c.type == num_d) && ((strcmp(item->struct_type.itemF->arguments[argument_index].type, "double")) == 0))
	{
		addElement(&functionDefinitions, f_pushp, mallocDouble(*(double*)c.data), typeDouble, NULL);
	}
	else if((c.type == num_i) && ((strcmp(item->struct_type.itemF->arguments[argument_index].type, "double")) == 0))
	{
		addElement(&functionDefinitions, f_pushp, mallocDouble((double)*(int*)c.data), typeDouble, NULL);
	}
	else if((c.type == num_i) && ((strcmp(item->struct_type.itemF->arguments[argument_index].type, "int")) == 0))
	{
		addElement(&functionDefinitions, f_pushp, mallocInt(*(int*)c.data), typeInt, NULL);
	}
	else if((c.type == string) && ((strcmp(item->struct_type.itemF->arguments[argument_index].type, "String")) == 0))
	{
		//printf("som vo vetve string\n");
		addElement(&functionDefinitions, f_pushp, mallocString((char*)c.data), typeString, NULL);
	}
	else if(c.type == simple_identifier)
	{

		if((tmp_local= hashTab_search_local (T_loc, (char*)c.data)) != NULL)
		{
			if(((strcmp(tmp_local->datatype_local, "int")) == 0) && ((strcmp("double", item->struct_type.itemF->arguments[argument_index].type)) == 0))
				if((strcmp(tmp_local->datatype_local, item->struct_type.itemF->arguments[argument_index].type)) != 0)
					return ERR_SEM_TYPE;

			if(tmp_local->init_local == 0)
				return ERR_VAR;


			if((strcmp(tmp_local->datatype_local, "double")) == 0){
				readFromLocal(tmp_local, reg_d1, Double);
				addElement(&functionDefinitions, f_pushp, reg_d1, typeDouble, NULL);
            }
			else if(((strcmp(tmp_local->datatype_local, "int")) == 0) && ((strcmp("double", item->struct_type.itemF->arguments[argument_index].type)) == 0)){
				readFromLocal(tmp_local, reg_d1, Double);
				addElement(&functionDefinitions, f_pushp, reg_d1, typeDouble, NULL);
			}
			else if((strcmp(tmp_local->datatype_local, "int")) == 0){
				readFromLocal(tmp_local, reg_i1, Int);
				addElement(&functionDefinitions, f_pushp, reg_i1, typeInt, NULL);
            }
			else if((strcmp(tmp_local->datatype_local, "String")) == 0){
				readFromLocal(tmp_local, reg_s1, String);
				addElement(&functionDefinitions, f_pushp, reg_s1, typeString, NULL);
            }


		}
		else if((tmp_static = hashTab_search (T, (char*)c.data, type_id, class_store)) != NULL)
		{
			if(((strcmp(tmp_static->struct_type.itemID->type, "int")) == 0) && ((strcmp("double", item->struct_type.itemF->arguments[argument_index].type)) == 0))
				if((strcmp(tmp_static->struct_type.itemID->type, item->struct_type.itemF->arguments[argument_index].type)) != 0)
					return ERR_SEM_TYPE;
			if(tmp_static->struct_type.itemID->initialized == 0)
				return ERR_VAR;

			if((strcmp(tmp_static->struct_type.itemID->type, "double")) == 0)
				addElement(&functionDefinitions, f_pushp,tmp_static->struct_type.itemID->value, typeDouble, NULL);
			else if(((strcmp(tmp_static->struct_type.itemID->type, "int")) == 0) && ((strcmp("double", item->struct_type.itemF->arguments[argument_index].type)) == 0))
				addElement(&functionDefinitions, f_pushp, mallocDouble((double)*(int*)tmp_static->struct_type.itemID->value), typeDouble, NULL);
			else if((strcmp(tmp_static->struct_type.itemID->type, "int")) == 0)
				addElement(&functionDefinitions, f_pushp,tmp_static->struct_type.itemID->value, typeInt, NULL);
			else if((strcmp(tmp_static->struct_type.itemID->type, "String")) == 0)
				addElement(&functionDefinitions, f_pushp, tmp_static->struct_type.itemID->value, typeString, NULL);

		}
		else
			return ERR_SEM_UNDEF;
	}
	else if(c.type == fully_qualified_identifier)
	{
		char *first_p;
		char *second_p;
		int pom = breakIdentifier(c.data, &first_p, &second_p);
		if(pom == ERR_INTER)
			return ERR_INTER;


		if((tmp_static = hashTab_search (T, second_p, type_id, first_p)) != NULL)
		{
			if(((strcmp(tmp_static->struct_type.itemID->type, "int")) == 0) && ((strcmp("double", item->struct_type.itemF->arguments[argument_index].type)) == 0))
				if((strcmp(tmp_static->struct_type.itemID->type, item->struct_type.itemF->arguments[argument_index].type)) != 0)
					return ERR_SEM_TYPE;
			if(tmp_static->struct_type.itemID->initialized == 0)
				return ERR_VAR;

			if((strcmp(tmp_static->struct_type.itemID->type, "double")) == 0)
				addElement(&functionDefinitions, f_pushp,tmp_static->struct_type.itemID->value, typeDouble, NULL);
			else if(((strcmp(tmp_static->struct_type.itemID->type, "int")) == 0) && ((strcmp("double", item->struct_type.itemF->arguments[argument_index].type)) == 0))
				addElement(&functionDefinitions, f_pushp, mallocDouble((double)*(int*)tmp_static->struct_type.itemID->value), typeDouble, NULL);
			else if((strcmp(tmp_static->struct_type.itemID->type, "int")) == 0)
				addElement(&functionDefinitions, f_pushp,tmp_static->struct_type.itemID->value, typeInt, NULL);

			else if((strcmp(tmp_static->struct_type.itemID->type, "String")) == 0)
				addElement(&functionDefinitions, f_pushp, tmp_static->struct_type.itemID->value, typeString, NULL);
		}
		else
			return ERR_SEM_UNDEF;
	}
	else
		return ERR_SEM_TYPE;


	if((TokenTypeEqual((c = getTokenList()), error)) == 1)
		return ERR_LEX;
	else if((TokenTypeEqual(c, error99)) == 1)
		return ERR_INTER;

	if(c.type == comma)
	{
		argument_index++;
		if(argument_index >= item->struct_type.itemF->arg_num)
			return ERR_SEM_TYPE;

		retV = StateFunctionCallBuildIn(name_func, class_f);
		if(retV == ERR_SYN)
			return ERR_SYN;
		else if(retV == ERR_LEX)
			return ERR_LEX;
		else if(retV == ERR_SEM_UNDEF)
			return ERR_SEM_UNDEF;
		else if(retV == ERR_SEM_TYPE)
			return ERR_SEM_TYPE;
		else if(retV == ERR_SEM)
			return ERR_SEM;
		else if(retV == ERR_READ)
			return ERR_READ;
		else if(retV == ERR_VAR)
			return ERR_VAR;
		else if(retV == ERR_ZERO)
			return ERR_ZERO;
		else if(retV == ERR_OTHER)
			return ERR_OTHER;
		else if(retV == ERR_INTER)
			return ERR_INTER;

	}
	else if(c.type == bracket_right)
		return ERR_OK;

}

//funckia reprezentujuca zaciatok volania vstavanej funkcie
int StateFunction()
{
	int retV;
	char *first_p;
	char *second_p;
	//printf("som v state function\n");
	int pom = breakIdentifier(builtInFunctions[*(unsigned char*)c.data], &first_p, &second_p);
	if(pom == ERR_INTER)
		return ERR_INTER;

	if((TokenTypeEqual((c = getTokenList()), error)) == 1)
		return ERR_LEX;
	else if((TokenTypeEqual(c, error99)) == 1)
		return ERR_INTER;

	if(c.type != bracket_left)
		return ERR_SYN;
//printf("volanie parametrov buildinfunc\n");

	argument_index = 0;
	if((retV = StateFunctionCallBuildIn(second_p, first_p)) == ERR_SYN)
		return ERR_SYN;
	else if(retV == ERR_LEX)
		return ERR_LEX;
	else if(retV == ERR_SEM_UNDEF)
		return ERR_SEM_UNDEF;
	else if(retV == ERR_SEM_TYPE)
		return ERR_SEM_TYPE;
	else if(retV == ERR_SEM)
		return ERR_SEM;
	else if(retV == ERR_READ)
		return ERR_READ;
	else if(retV == ERR_VAR)
		return ERR_VAR;
	else if(retV == ERR_ZERO)
		return ERR_ZERO;
	else if(retV == ERR_OTHER)
		return ERR_OTHER;
	else if(retV == ERR_INTER)
		return ERR_INTER;
	else
	{
		//printf("vratil som sa po nacitani parametrov buildinfunc\n");
		hash_item *item = hashTab_search(T, second_p, type_func ,first_p);

		if((strcmp(item->struct_type.itemF->return_type, "void")) == 0)
		{
			if((strcmp(second_p, "print")) == 0)
			{
				//printf("je print ifj ok \n");
				addElement(&functionDefinitions, b_print, NULL, NULL, NULL);

				/*if((TokenTypeEqual((c = getTokenList()), error)) == 1)
					return ERR_LEX;
				else if((TokenTypeEqual(c, error99)) == 1)
					return ERR_INTER;

				printf("\n");
				printTokenInfo(c);
				printf("\n");*/

				if(c.type == semicolon)
					return ERR_OK;
			}
			else
				return ERR_SEM_TYPE;
		}
		
		//printf("vratil som sa po nacitani parametrov vstavanych funkcii\n");
		
		if(pom_token.type == simple_identifier)
		{
			hash_item_local *tmp_loc = hashTab_search_local(T_loc, id_called_func);
			if(tmp_loc == NULL)
			{
				hash_item *tmp_stat = hashTab_search(T, id_called_func, type_id ,id_called_class);
				assign_value = tmp_stat->struct_type.itemID->value;
				assign_type = tmp_stat->struct_type.itemID->type;
				tmp_stat->struct_type.itemID->initialized = 1;

			}
			else
			{
				tmp_loc->init_local = 1;

				if(((strcmp(tmp_loc->datatype_local, "double")) == 0) && ((strcmp("int", item->struct_type.itemF->return_type)) == 0))
				{
					if((strcmp(second_p, "readInt")) == 0)
					{
						addElement(&functionDefinitions, b_readint, NULL, NULL, reg_i1);
						//addElement(&functionDefinitions, c_itod, reg_i1, NULL, assign_value);
						writeToLocal(tmp_loc, reg_i1, Int);
					}
					else if((strcmp(second_p, "length")) == 0)
					{
						addElement(&functionDefinitions, b_length, NULL, NULL, reg_i1);
						//addElement(&functionDefinitions, c_itod, reg_i1,NULL, assign_value);
						writeToLocal(tmp_loc, reg_i1, Int);
					}
					else if((strcmp(second_p, "compare")) == 0)
					{
						addElement(&functionDefinitions, b_compare, NULL, NULL, reg_i1);
						//addElement(&functionDefinitions, c_itod, reg_i1,NULL, assign_value);
						writeToLocal(tmp_loc, reg_i1, Int);
					}
					else if((strcmp(second_p, "find")) == 0)
					{
						addElement(&functionDefinitions, b_find, NULL, NULL, reg_i1);
						//addElement(&functionDefinitions, c_itod, reg_i1,NULL, assign_value);
						writeToLocal(tmp_loc, reg_i1, Int);
					}

				}
				else if(((strcmp(tmp_loc->datatype_local, "int")) == 0) && ((strcmp("int", item->struct_type.itemF->return_type)) == 0))
				{
					if((strcmp(second_p, "readInt")) == 0)
					{
						addElement(&functionDefinitions, b_readint, NULL, NULL, reg_i1);
						writeToLocal(tmp_loc, reg_i1, Int);
					}
					else if((strcmp(second_p, "length")) == 0)
					{
						addElement(&functionDefinitions, b_length, NULL, NULL, reg_i1);
						writeToLocal(tmp_loc, reg_i1, Int);
					}
					else if((strcmp(second_p, "compare")) == 0)
					{
						addElement(&functionDefinitions, b_compare, NULL, NULL, reg_i1);
						writeToLocal(tmp_loc, reg_i1, Int);
					}
					else if((strcmp(second_p, "find")) == 0)
					{
						addElement(&functionDefinitions, b_find, NULL, NULL, reg_i1);
						writeToLocal(tmp_loc, reg_i1, Int);
					}
				}
				else if(((strcmp(tmp_loc->datatype_local, "double")) == 0) && ((strcmp("double", item->struct_type.itemF->return_type)) == 0))
				{
					if((strcmp(second_p, "readDouble")) == 0)
					{
						addElement(&functionDefinitions, b_readdouble, NULL, NULL, reg_d1);
						writeToLocal(tmp_loc, reg_d1, Double);
					}
				}
				else if(((strcmp(tmp_loc->datatype_local, "String")) == 0) && ((strcmp("String", item->struct_type.itemF->return_type)) == 0))
				{
					if((strcmp(second_p, "readString")) == 0)
					{
						addElement(&functionDefinitions, b_readstring, NULL, NULL, reg_s1);
						writeToLocal(tmp_loc, reg_s1, String);
					}
					else if((strcmp(second_p, "substr")) == 0)
					{
						addElement(&functionDefinitions, b_substr, NULL, NULL, reg_s1);
						writeToLocal(tmp_loc, reg_s1, String);
					}
					else if((strcmp(second_p, "sort")) == 0)
					{
						addElement(&functionDefinitions, b_sort, NULL, NULL, reg_s1);
						writeToLocal(tmp_loc, reg_s1, String);
					}
				}
				else
					return ERR_SEM_TYPE;

				/*if(strcmp(item->struct_type.itemF->return_type, "int") == 0){
					writeToLocal(tmp_loc, T_loc->number_variable, ret_i, Int);
				} else if(strcmp(item->struct_type.itemF->return_type, "double") == 0){
					writeToLocal(tmp_loc, T_loc->number_variable, ret_d, Double);
				} else if(strcmp(item->struct_type.itemF->return_type, "String") == 0){
					writeToLocal(tmp_loc, T_loc->number_variable, ret_s, String);
				}*/
			}
		}
		else
		{
			hash_item *tmp_stat = hashTab_search(T, id_called_func, type_id ,id_called_class);
			assign_value = tmp_stat->struct_type.itemID->value;
			assign_type = tmp_stat->struct_type.itemID->type;
			tmp_stat->struct_type.itemID->initialized = 1;

			if(((strcmp(assign_type, "double")) == 0) && ((strcmp("int", item->struct_type.itemF->return_type)) == 0))
			{
				if((strcmp(second_p, "readInt")) == 0)
				{
					addElement(&functionDefinitions, b_readint, NULL, NULL, reg_i1);
					addElement(&functionDefinitions, c_itod, reg_i1, NULL, assign_value);
				}
				else if((strcmp(second_p, "length")) == 0)
				{
					addElement(&functionDefinitions, b_length, NULL, NULL, reg_i1);
					addElement(&functionDefinitions, c_itod, reg_i1,NULL, assign_value);
				}
				else if((strcmp(second_p, "compare")) == 0)
				{
					addElement(&functionDefinitions, b_compare, NULL, NULL, reg_i1);
					addElement(&functionDefinitions, c_itod, reg_i1,NULL, assign_value);
				}
				else if((strcmp(second_p, "find")) == 0)
				{
					addElement(&functionDefinitions, b_find, NULL, NULL, reg_i1);
					addElement(&functionDefinitions, c_itod, reg_i1,NULL, assign_value);
				}

			}
			else if(((strcmp(assign_type, "int")) == 0) && ((strcmp("int", item->struct_type.itemF->return_type)) == 0))
			{
				if((strcmp(second_p, "readInt")) == 0)
				{
					addElement(&functionDefinitions, b_readint, NULL, NULL, assign_value);
				}
				else if((strcmp(second_p, "length")) == 0)
				{
					addElement(&functionDefinitions, b_length, NULL, NULL, assign_value);
				}
				else if((strcmp(second_p, "compare")) == 0)
				{
					addElement(&functionDefinitions, b_compare, NULL, NULL, assign_value);
				}
				else if((strcmp(second_p, "find")) == 0)
				{
					addElement(&functionDefinitions, b_find, NULL, NULL, assign_value);
				}
			}
			else if(((strcmp(assign_type, "double")) == 0) && ((strcmp("double", item->struct_type.itemF->return_type)) == 0))
			{
				if((strcmp(second_p, "readDouble")) == 0)
				{
					addElement(&functionDefinitions, b_readdouble, NULL, NULL, assign_value);
				}
			}
			else if(((strcmp(assign_type, "String")) == 0) && ((strcmp("String", item->struct_type.itemF->return_type)) == 0))
			{
				if((strcmp(second_p, "readString")) == 0)
				{
					addElement(&functionDefinitions, b_readstring, NULL, NULL, assign_value);
				}
				else if((strcmp(second_p, "substr")) == 0)
				{
					addElement(&functionDefinitions, b_substr, NULL, NULL, assign_value);
				}
				else if((strcmp(second_p, "sort")) == 0)
				{
					addElement(&functionDefinitions, b_sort, NULL, NULL, assign_value);
				}
			}
			else
				return ERR_SEM_TYPE;
		}

		if((TokenTypeEqual((c = getTokenList()), error)) == 1)
			return ERR_LEX;
		else if((TokenTypeEqual(c, error99)) == 1)
			return ERR_INTER;
		//printf("opustam state functon\n");
		if(c.type == semicolon)
			return ERR_OK;
	}

}



//funckia reprezentujuca deklaraciu a zaciatok triedy
int StateClass()
{
	int retV;

//	printf("zacal class\n");

	if((TokenTypeEqual((c = getTokenList()), error)) == 1)
		return ERR_LEX;
	else if((TokenTypeEqual(c, error99)) == 1)
		return ERR_INTER;

	if(c.type != simple_identifier)
		return ERR_SYN;
//	printf("%s som v clas a pred tymto je main\n", (char*)c.data );

	class_store = c.data;

//	printf("%s po priradeni class store je v nom main\n",class_store );

	if((TokenTypeEqual((c = getTokenList()), error)) == 1)
		return ERR_LEX;
	else if((TokenTypeEqual(c, error99)) == 1)
		return ERR_INTER;

	if(c.type != bracket_open)
		return ERR_SYN;

	c = getTokenList();

	while(c.type != bracket_close)
	{

		if(TokenTypeEqual(c, error) == 1)
			return ERR_LEX;
		else if((TokenTypeEqual(c, error99)) == 1)
			return ERR_INTER;

		if(keywords[*(unsigned char*)c.data] != keywords[5])
			return ERR_SYN;

		if((retV = StateStatic()) == ERR_SYN)
			return ERR_SYN;
		else if(retV == ERR_LEX)
			return ERR_LEX;
		else if(retV == ERR_SEM_UNDEF)
			return ERR_SEM_UNDEF;
		else if(retV == ERR_SEM_TYPE)
			return ERR_SEM_TYPE;
		else if(retV == ERR_SEM)
			return ERR_SEM;
		else if(retV == ERR_READ)
			return ERR_READ;
		else if(retV == ERR_VAR)
			return ERR_VAR;
		else if(retV == ERR_ZERO)
			return ERR_ZERO;
		else if(retV == ERR_OTHER)
			return ERR_OTHER;
		else if(retV == ERR_INTER)
			return ERR_INTER;

		c = getTokenList();
		if(c.type == end)
			return ERR_SYN;
	}

	//printf("dokoncilo class\n");

	return ERR_OK;

}
//funckia reprezentujuca prikazy volane z tela funkcie
int StateCoreFunc()
{
	int retValue, ret, pom;
	char *local_type;
	char *local_id_name;
	char *first_p;
	char *second_p;
	hash_item *item;
//	printf("som v core function\n");

	hash_item_local *tmp;
//	printf("%s func_store\n",func_store );
//	printf("%s class_store\n", class_store);

	hash_item *item_local = hashTab_search(T, func_store, type_func, class_store);
	if((strcmp(item_local->struct_type.itemF->return_type, "void")) == 0)
		return_bool = 1;

//	printf("som po hash search\n");
	if(item_local == NULL)
	{
	//	printf("koncim s return other\n");
		return ERR_SEM_UNDEF;
	}
//	printf("prebehlo pred for cyklus\n");
//	printf("pocet argumentov danej funkcie : %d\n",item_local->struct_type.itemF->arg_num );

	for(int i = 0; i < item_local->struct_type.itemF->arg_num; i++)
	{
		local_type = item_local->struct_type.itemF->arguments[i].type;
		local_id_name = item_local->struct_type.itemF->arguments[i].id_name;
	//	printf("parameter %d do TS typ : %s\n",i+1, local_type);
	//	printf("parameter %d do TS meno : %s\n",i+1, local_id_name);


		if((tmp = hashTab_search_local (T_loc, local_id_name)) == NULL)
		{
			ret = hashTab_insert_local(T_loc,local_id_name,local_type, NULL, 1);
			if(ret == ERR_INTER)
				return ERR_INTER;

	//		printf("preslo id local declaration\n");

			/*tmp = hashTab_search_local (T_loc, local_id_name);
			addElement(&functionDefinitions, f_readp, mallocInt(item_local->struct_type.itemF->arg_num - i - 1), NULL, tmp->value_local);*/
			//TODO - možná je potřeba
		}
		else
		{
			return ERR_SEM_UNDEF;
		}


	}

	//printf("som pre velkym while\n");

	while((TokenTypeEqual((c = getTokenList()), error)) == -1)
	{
	//	printf("tu by som mal byt po breaku\n");
	//	printTokenInfo(c);
	//	printf("\n");
		if((TokenTypeEqual(c, error99)) == 1)
			return ERR_INTER;
		//printTokenInfo(c);
		//printf("\n");

		if(c.type == bracket_close)
		{

			if(strcmp(item_local->struct_type.itemF->return_type, "void") == 0){
				//addElement(&functionDefinitions, f_ret,mallocInt(item_local->struct_type.itemF->arg_num), NULL, NULL);
				addElement(&functionDefinitions, f_ret,mallocInt(T_loc->number_variable), NULL, NULL);
			}

			hashTab_clear_local(T_loc);
			//printf("vraciam sa zo StateCoreFunc\n");

			return ERR_OK;
		}

		switch (c.type)
		{
			case simple_identifier:		//premenna
			//	printf("idem do stateID\n");
				if ((retValue = StateID()) == ERR_SYN)
					return ERR_SYN;
				else if(retValue == ERR_LEX)
					return ERR_LEX;
				else if(retValue == ERR_SEM_UNDEF)
					return ERR_SEM_UNDEF;
				else if(retValue == ERR_SEM_TYPE)
					return ERR_SEM_TYPE;
				else if(retValue == ERR_SEM)
					return ERR_SEM;
				else if(retValue == ERR_READ)
					return ERR_READ;
				else if(retValue == ERR_VAR)
					return ERR_VAR;
				else if(retValue == ERR_ZERO)
					return ERR_ZERO;
				else if(retValue == ERR_OTHER)
					return ERR_OTHER;
				else if(retValue == ERR_INTER)
					return ERR_INTER;
			//	printf("vratil som sa do StateCoreFunc zo StateID\n");
			//	printTokenInfo(c);
			//	printf("\n");
				//printList(&TokenList);
				break;

			case fully_qualified_identifier:		//premenna
				if ((retValue = StateID()) == ERR_SYN)
					return ERR_SYN;
				else if(retValue == ERR_LEX)
					return ERR_LEX;
				else if(retValue == ERR_SEM_UNDEF)
					return ERR_SEM_UNDEF;
				else if(retValue == ERR_SEM_TYPE)
					return ERR_SEM_TYPE;
				else if(retValue == ERR_SEM)
					return ERR_SEM;
				else if(retValue == ERR_READ)
					return ERR_READ;
				else if(retValue == ERR_VAR)
					return ERR_VAR;
				else if(retValue == ERR_ZERO)
					return ERR_ZERO;
				else if(retValue == ERR_OTHER)
					return ERR_OTHER;
				else if(retValue == ERR_INTER)
					return ERR_INTER;
				break;

			case keyword_data_type:
			//	printf("idem do state declaration ID\n");
				if ((retValue = StateDeclarationID()) == ERR_SYN)
					return ERR_SYN;
				else if(retValue == ERR_LEX)
					return ERR_LEX;
				else if(retValue == ERR_SEM_UNDEF)
					return ERR_SEM_UNDEF;
				else if(retValue == ERR_SEM_TYPE)
					return ERR_SEM_TYPE;
				else if(retValue == ERR_SEM)
					return ERR_SEM;
				else if(retValue == ERR_READ)
					return ERR_READ;
				else if(retValue == ERR_VAR)
					return ERR_VAR;
				else if(retValue == ERR_ZERO)
					return ERR_ZERO;
				else if(retValue == ERR_OTHER)
					return ERR_OTHER;
				else if(retValue == ERR_INTER)
					return ERR_INTER;
				break;

			case keyword_normal:

				if (keywords[*(unsigned char*)c.data] == keywords[13])	// if
				{
					if ((retValue = StateIf()) == ERR_SYN)
						return ERR_SYN;
					else if(retValue == ERR_LEX)
						return ERR_LEX;
					else if(retValue == ERR_SEM_UNDEF)
						return ERR_SEM_UNDEF;
					else if(retValue == ERR_SEM_TYPE)
						return ERR_SEM_TYPE;
					else if(retValue == ERR_SEM)
						return ERR_SEM;
					else if(retValue == ERR_READ)
						return ERR_READ;
					else if(retValue == ERR_VAR)
						return ERR_VAR;
					else if(retValue == ERR_ZERO)
						return ERR_ZERO;
					else if(retValue == ERR_OTHER)
						return ERR_OTHER;
					else if(retValue == ERR_INTER)
						return ERR_INTER;
				//	printf("vraciam sa zo stateIf do StateCoreFunc\n");
					break;
				}

				else if (keywords[*(unsigned char*)c.data] == keywords[16])	// while
				{
					if ((retValue = StateWhile()) == ERR_SYN)
						return ERR_SYN;
					else if(retValue == ERR_LEX)
						return ERR_LEX;
					else if(retValue == ERR_SEM_UNDEF)
						return ERR_SEM_UNDEF;
					else if(retValue == ERR_SEM_TYPE)
						return ERR_SEM_TYPE;
					else if(retValue == ERR_SEM)
						return ERR_SEM;
					else if(retValue == ERR_READ)
						return ERR_READ;
					else if(retValue == ERR_VAR)
						return ERR_VAR;
					else if(retValue == ERR_ZERO)
						return ERR_ZERO;
					else if(retValue == ERR_OTHER)
						return ERR_OTHER;
					else if(retValue == ERR_INTER)
						return ERR_INTER;
					break;
				}

				else if (keywords[*(unsigned char*)c.data] == keywords[14])	// return
				{
					if((strcmp(item_local->struct_type.itemF->return_type, "void")) == 0)
					{
						//printf("idem do void return\n");
						if ((retValue = StateReturnVoid()) == ERR_SYN)
							return ERR_SYN;
						else if (retValue == ERR_LEX)
							return ERR_LEX;
					}
					else
					{
						//printf("idem do non void return\n");
						return_bool = 1;
					//	printf("ideme do return non void\n");
						if ((retValue = StateReturn()) == ERR_SYN)
							return ERR_SYN;
						else if(retValue == ERR_LEX)
							return ERR_LEX;
						else if(retValue == ERR_SEM_UNDEF)
							return ERR_SEM_UNDEF;
						else if(retValue == ERR_SEM_TYPE)
							return ERR_SEM_TYPE;
						else if(retValue == ERR_SEM)
							return ERR_SEM;
						else if(retValue == ERR_READ)
							return ERR_READ;
						else if(retValue == ERR_VAR)
							return ERR_VAR;
						else if(retValue == ERR_ZERO)
							return ERR_ZERO;
						else if(retValue == ERR_OTHER)
							return ERR_OTHER;
						else if(retValue == ERR_INTER)
							return ERR_INTER;
						//printf("%d vraciam sa do StateCoreFunc\n", retValue);
					}
					break;

				}

				else
					return ERR_SYN;

			case builtInFunction:	//funkcia
				pom = breakIdentifier(builtInFunctions[*(unsigned char*)c.data], &first_p, &second_p);
				if(pom == ERR_INTER)
					return ERR_INTER;

				if((item = hashTab_search (T, second_p, type_func, first_p)) != NULL)
				{
					if((strcmp(item->struct_type.itemF->return_type, "void")) != 0)
						return ERR_SEM_TYPE;
				}
				else
					return ERR_SEM_UNDEF;

				if ((retValue = StateFunction()) == ERR_SYN)
					return ERR_SYN;
				else if(retValue == ERR_LEX)
					return ERR_LEX;
				else if(retValue == ERR_SEM_UNDEF)
					return ERR_SEM_UNDEF;
				else if(retValue == ERR_SEM_TYPE)
					return ERR_SEM_TYPE;
				else if(retValue == ERR_SEM)
					return ERR_SEM;
				else if(retValue == ERR_READ)
					return ERR_READ;
				else if(retValue == ERR_VAR)
					return ERR_VAR;
				else if(retValue == ERR_ZERO)
					return ERR_ZERO;
				else if(retValue == ERR_OTHER)
					return ERR_OTHER;
				else if(retValue == ERR_INTER)
					return ERR_INTER;
				//printf("sem som sa vratil ok \n");
				break;

			default:
			//	printf("som v default\n");
				return ERR_SYN;
		}
	}

	return ERR_LEX;
}
//funkcia preprezentujuca zaciatok vyhodnocovania vyrazov
int StateExpression()
{
	tStack Stack;
	int retValue;
//	printf("som v StateExpression a idem do StateExpAnalysis\n");

	if ((retValue = StateExpAnalysis()) != ERR_OK)
	{
	//	printf("vratil som sa zo StateExpAnalysis a navratova hodnota je %d\n",retValue );

		if(retValue == ERR_SYN)
		{
		//	printf("2 vracia StateExpAnalysis\n");
			return ERR_SYN;
		}
		else if(retValue == ERR_LEX)
			return ERR_LEX;
		else if(retValue == ERR_SEM_UNDEF)
			return ERR_SEM_UNDEF;
		else if(retValue == ERR_SEM_TYPE)
			return ERR_SEM_TYPE;
		else if(retValue == ERR_SEM)
			return ERR_SEM;
		else if(retValue == ERR_READ)
			return ERR_READ;
		else if(retValue == ERR_VAR)
			return ERR_VAR;
		else if(retValue == ERR_ZERO)
			return ERR_ZERO;
		else if(retValue == ERR_OTHER)
			return ERR_OTHER;
		else if(retValue == ERR_INTER)
			return ERR_INTER;
	}
	else
	{
	//	printf("vratil som sa zo StateExpAnalysis a navratova hodnota je %d\n",retValue );

	//	printTokenInfo(c);
	//	printf("\n");

		if(c.type != semicolon)
		{
		//	printf("kontrolujem v expression semicolon\n");
			return ERR_SYN;
		}
		//printf("koncim StateExpression ok\n");

	//	printTokenInfo(c);
	//	printf("\n");

		return ERR_OK;
	}
}

//funckia reprezentujuca deklaraciu lokalnych premennych v tele funkcie
int StateDeclarationID()
{
	int retV, ret;
	hash_item_local *tmp;
	hash_item *id_item;
	void *value;
	char *id_store;

	char *datatype = keywords[*(unsigned char*)c.data];

	while((TokenTypeEqual((c = getTokenList()), error)) == -1)
	{
		if(c.type == simple_identifier)
		{
			
			id_store = (char*) c.data;
			id_called_func = c.data;
			//printf("%s id_called_func\n",id_called_func );
			id_called_class = class_store;
			pom_token.type = c.type;
			pom_token.data = c.data;

			if((TokenTypeEqual((c = getTokenList()), error)) == 1)
				return ERR_LEX;
			else if((TokenTypeEqual(c, error99)) == 1)
				return ERR_INTER;

			if (c.type == operator_relation)
			{

				if((tmp = hashTab_search_local (T_loc, id_store)) == NULL)
				{
					ret = hashTab_insert_local(T_loc,id_store,datatype, NULL, 1);
					if(ret == ERR_INTER)
						return ERR_INTER;
					//printf("funkcia state decID vlakadanie to TS meno %s\n",id_store );
					//printf("funkcia state decID vlakadanie to TS typ %s\n",datatype );
					//printf("preslo id declaration\n");

					//TODO - vytváření lokálních parametrů
					if(strcmp(datatype, "int") == 0){
						addElement(&functionDefinitions, f_pushp, mallocInt(0), typeInt, NULL);
					} else if(strcmp(datatype, "double") == 0){
						addElement(&functionDefinitions, f_pushp, mallocDouble(0), typeDouble, NULL);
					} else if(strcmp(datatype, "String") == 0){
						addElement(&functionDefinitions, f_pushp, mallocString(""), typeString, NULL);
					}
				}
				else
				{
					//printf("nenasiel sa lokalny identifikator vkladany do pamati\n");
					return ERR_SEM_UNDEF;
				}
		
				if((TokenTypeEqual((c = getTokenList()), error)) == 1)
					return ERR_LEX;
				else if((TokenTypeEqual(c, error99)) == 1)
					return ERR_INTER;

				if 	((c.type == num_d) || 
					(c.type == fully_qualified_identifier) ||
					(c.type == simple_identifier) ||
					(c.type == num_i) || 
					(c.type == string) || 
					(c.type == builtInFunction) ||
					(c.type == bracket_left))
				{
					if(c.type == simple_identifier)
					{
						//printf("som v simple_identifier\n");
						hash_item_local *local_item;
					
						if((id_item = hashTab_search (T, c.data, type_func, class_store)) != NULL)
						{

							if((strcmp(id_item->struct_type.itemF->return_type, "void")) == 0)
								return ERR_SEM_TYPE;

							//printf("JE TO FUNKCIA\n");
							char *func_call_name = c.data;

							if((TokenTypeEqual((c = getTokenList()), error)) == 1)
								return ERR_LEX;
							else if((TokenTypeEqual(c, error99)) == 1)
								return ERR_INTER;

							if(c.type != bracket_left)
								return ERR_SYN;

							argument_index = 0;

							retV = StateFunctionCall(func_call_name, class_store);
							if (retV == ERR_LEX)
								return ERR_LEX;
							else if (retV == ERR_SYN)
								return ERR_SYN;
							else if (retV == ERR_SEM_UNDEF)
								return ERR_SEM_UNDEF;
							else if (retV == ERR_SEM_TYPE)
								return ERR_SEM_TYPE;
							else if (retV == ERR_SEM)
								return ERR_SEM;
							else if (retV == ERR_VAR)
								return ERR_VAR;
							else if (retV == ERR_OTHER)
								return ERR_OTHER;
							else if(retV == ERR_INTER)
								return ERR_INTER;

							addElement(&functionDefinitions, f_call, &(id_item->struct_type.itemF->def_adress), mallocInt(id_item->struct_type.itemF->arg_num), NULL);

							hash_item_local *tmp_loc = hashTab_search_local(T_loc, id_store);
							assign_value = tmp_loc->value_local;
							assign_type = tmp_loc->datatype_local;

							//printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");

							if(((strcmp(id_item->struct_type.itemF->return_type, "int")) == 0) && ((strcmp(assign_type, "int")) == 0))
							{
								writeToLocal(tmp_loc, ret_i, Int);
							}
							else if(((strcmp(id_item->struct_type.itemF->return_type, "double")) == 0) && ((strcmp(assign_type, "double")) == 0)){
								writeToLocal(tmp_loc, ret_d, Double);
                            }
							else if(((strcmp(id_item->struct_type.itemF->return_type, "int")) == 0) && ((strcmp(assign_type, "double")) == 0))
							{
								writeToLocal(tmp_loc, ret_i, Int);
							}
							else if(((strcmp(id_item->struct_type.itemF->return_type, "String")) == 0) && ((strcmp(assign_type, "String")) == 0))
							{
								writeToLocal(tmp_loc, ret_s, String);
							}
							else
								return ERR_SEM_TYPE;
							
							if((TokenTypeEqual((c = getTokenList()), error)) == 1)
								return ERR_LEX;
							else if((TokenTypeEqual(c, error99)) == 1)
								return ERR_INTER;

							if(c.type == semicolon)
								return ERR_OK;
							else
								return ERR_SYN;
						}
						else
						{
							retV = StateExpression();
							if(retV == ERR_LEX)
								return ERR_LEX;
							else if(retV == ERR_SYN)
								return ERR_SYN;
							else if(retV == ERR_SEM_UNDEF)
								return ERR_SEM_UNDEF;
							else if(retV == ERR_SEM_TYPE)
								return ERR_SEM_TYPE;
							else if(retV == ERR_SEM)
								return ERR_SEM;
							else if(retV == ERR_READ)
								return ERR_READ;
							else if(retV == ERR_VAR)
								return ERR_VAR;
							else if(retV == ERR_ZERO)
								return ERR_ZERO;
							else if(retV == ERR_OTHER)
								return ERR_OTHER;
							else if(retV == ERR_INTER)
								return ERR_INTER;
							else
							{
								//printf("koncim uspesne StateExpression v stateDecID\n");
								return ERR_OK;
							}
						}
					}
					else if(c.type == fully_qualified_identifier)
					{
						//printf("som vo fully_qualified_identifier\n");
						char *first_p;
						char *second_p;
						int pom = breakIdentifier(c.data, &first_p, &second_p);
						if(pom == ERR_INTER)
							return ERR_INTER;

						//printf("prva cas ala trieda %s\n",first_p );
						//printf("druha cast ala funkcia %s\n",second_p );

						if((id_item = hashTab_search (T, second_p, type_func, first_p)) != NULL)
						{

							if((strcmp(id_item->struct_type.itemF->return_type, "void")) == 0)
								return ERR_SEM_TYPE;

							//printf("FUNC\n");
							char *func_call_name = second_p;

							if((TokenTypeEqual((c = getTokenList()), error)) == 1)
								return ERR_LEX;
							else if((TokenTypeEqual(c, error99)) == 1)
								return ERR_INTER;

							if(c.type != bracket_left)
								return ERR_SYN;

							argument_index = 0;

							//printf("idem do StateFunctionCall\n");
							retV = StateFunctionCall(func_call_name, first_p);
							if (retV == ERR_LEX)
								return ERR_LEX;
							else if (retV == ERR_SYN)
								return ERR_SYN;
							else if (retV == ERR_SEM_UNDEF)
								return ERR_SEM_UNDEF;
							else if (retV == ERR_SEM_TYPE)
								return ERR_SEM_TYPE;
							else if (retV == ERR_SEM)
								return ERR_SEM;
							else if (retV == ERR_VAR)
								return ERR_VAR;
							else if (retV == ERR_OTHER)
								return ERR_OTHER;
							else if(retV == ERR_INTER)
								return ERR_INTER;

							addElement(&functionDefinitions, f_call, &(id_item->struct_type.itemF->def_adress), mallocInt(id_item->struct_type.itemF->arg_num), NULL);

							hash_item_local *tmp_loc = hashTab_search_local(T_loc, id_store);
							assign_value = tmp_loc->value_local;
							assign_type = tmp_loc->datatype_local;

							if(((strcmp(id_item->struct_type.itemF->return_type, "int")) == 0) && ((strcmp(assign_type, "int")) == 0)){
								writeToLocal(tmp_loc, ret_i, Int);
                            }
							else if(((strcmp(id_item->struct_type.itemF->return_type, "double")) == 0) && ((strcmp(assign_type, "double")) == 0)){
								writeToLocal(tmp_loc, ret_d, Double);
                            }

							else if(((strcmp(id_item->struct_type.itemF->return_type, "int")) == 0) && ((strcmp(assign_type, "double")) == 0))
							{
								writeToLocal(tmp_loc, ret_i, Int);
							}
							else if(((strcmp(id_item->struct_type.itemF->return_type, "String")) == 0) && ((strcmp(assign_type, "String")) == 0)){
								writeToLocal(tmp_loc, ret_s, String);
                            }
							else
								return ERR_SEM_TYPE;
									
							if((TokenTypeEqual((c = getTokenList()), error)) == 1)
								return ERR_LEX;
							else if((TokenTypeEqual(c, error99)) == 1)
								return ERR_INTER;

							if(c.type == semicolon)
								return ERR_OK;
							else
								return ERR_SYN;
						}
						else
						{
							

							retV = StateExpression();
							if(retV == ERR_LEX)
								return ERR_LEX;
							else if(retV == ERR_SYN)
								return ERR_SYN;
							else if(retV == ERR_SEM_UNDEF)
								return ERR_SEM_UNDEF;
							else if(retV == ERR_SEM_TYPE)
								return ERR_SEM_TYPE;
							else if(retV == ERR_SEM)
								return ERR_SEM;
							else if(retV == ERR_READ)
								return ERR_READ;
							else if(retV == ERR_VAR)
								return ERR_VAR;
							else if(retV == ERR_ZERO)
								return ERR_ZERO;
							else if(retV == ERR_OTHER)
								return ERR_OTHER;
							else if(retV == ERR_INTER)
								return ERR_INTER;
							else
							{
								//printf("koncim uspesne StateExpression v stateDecID\n");
								return ERR_OK;
							}
						}
					}
					else if(c.type == builtInFunction)
					{
						//printf("som v buildinfunc ############################################\n");
						char *first_p;
						char *second_p;
						hash_item *item_buildin;
						int pom = breakIdentifier(builtInFunctions[*(unsigned char*)c.data], &first_p, &second_p);
						if(pom == ERR_INTER)
							return ERR_INTER;
						//printf("idem hladat buildinfunc\n");
						if((id_item = hashTab_search (T, second_p, type_func, first_p)) != NULL)
						{

							//printf("idem do StateFunction\n");
							retV = StateFunction();
							if (retV == ERR_LEX)
								return ERR_LEX;
							else if (retV == ERR_SYN)
								return ERR_SYN;
							else if (retV == ERR_SEM_UNDEF)
								return ERR_SEM_UNDEF;
							else if (retV == ERR_SEM_TYPE)
								return ERR_SEM_TYPE;
							else if (retV == ERR_SEM)
								return ERR_SEM;
							else if (retV == ERR_VAR)
								return ERR_VAR;
							else if (retV == ERR_OTHER)
								return ERR_OTHER;
							else if(retV == ERR_INTER)
								return ERR_INTER;
							else
							{
								return ERR_OK;
							}

						}
						else
							return ERR_SEM_UNDEF;
					}
					else
					{

						retV = StateExpression();
						if(retV == ERR_LEX)
							return ERR_LEX;
						else if(retV == ERR_SYN)
							return ERR_SYN;
						else if(retV == ERR_SEM_UNDEF)
							return ERR_SEM_UNDEF;
						else if(retV == ERR_SEM_TYPE)
							return ERR_SEM_TYPE;
						else if(retV == ERR_SEM)
							return ERR_SEM;
						else if(retV == ERR_READ)
							return ERR_READ;
						else if(retV == ERR_VAR)
							return ERR_VAR;
						else if(retV == ERR_ZERO)
							return ERR_ZERO;
						else if(retV == ERR_OTHER)
							return ERR_OTHER;
						else if(retV == ERR_INTER)
							return ERR_INTER;
						else
						{
							//printf("koncim uspesne StateExpression v stateDecID\n");
							return ERR_OK;
						}
					}

				}
				else
					return ERR_SYN;
			}
			else if (c.type == semicolon)
			{
				if((tmp = hashTab_search_local (T_loc, id_store)) == NULL)
				{
					ret = hashTab_insert_local(T_loc,id_store,datatype, NULL, 0);
					if(ret == ERR_INTER)
						return ERR_INTER;

					//printf("preslo id declaration\n");
					//printf("funkcia state decID vlakadanie to TS meno %s\n",id_store );

					//printf("funkcia state decID vlakadanie to TS typ %s\n",datatype );

					//TODO - vytváření lokálních parametrů
					if(strcmp(datatype, "int") == 0){
						addElement(&functionDefinitions, f_pushp, mallocInt(0), typeInt, NULL);
					} else if(strcmp(datatype, "double") == 0){
						addElement(&functionDefinitions, f_pushp, mallocDouble(0), typeDouble, NULL);
					} else if(strcmp(datatype, "String") == 0){
						addElement(&functionDefinitions, f_pushp, mallocString(""), typeString, NULL);
					}
				}
				else
				{
					return ERR_SEM_UNDEF;
				}

				return ERR_OK;
			}
			else
				return ERR_SYN;
		}
	}
}

//pomocna funkcia pre preskocenie overenia deklaracie funkcie(vykonane v prvom prechode)
int StateDeclarationFunctionOK()
{
	c = getTokenList();
	while(c.type != bracket_right)
	{
		c = getTokenList();
	}

	return ERR_OK;
}
//naplnenie tabulky symbolov vstavanymi funkciami
int BuiltInFuncDeclaration()
{
	FSymbolTable *data_in;
	int ret;

	data_in = my_malloc(sizeof(FSymbolTable));
	if(data_in == NULL)
		return ERR_INTER;

	data_in->return_type = "int";
	data_in->arg_num = 0;
	ret = hashTab_insert(T,"readInt",NULL,data_in,type_func,"ifj16" );

	data_in = my_malloc(sizeof(FSymbolTable));
	if(data_in == NULL)
		return ERR_INTER;

	data_in->return_type = "double";
	data_in->arg_num = 0;
	ret = hashTab_insert(T,"readDouble",NULL,data_in,type_func,"ifj16" );

	data_in = my_malloc(sizeof(FSymbolTable));
	if(data_in == NULL)
		return ERR_INTER;


	data_in->return_type = "String";
	data_in->arg_num = 0;
	ret = hashTab_insert(T,"readString",NULL,data_in,type_func,"ifj16" );

	data_in = my_malloc(sizeof(FSymbolTable) + sizeof(FArg));
	if(data_in == NULL)
		return ERR_INTER;

	data_in->return_type = "void";
	data_in->arg_num = 1;
	data_in->arguments[0].type = "String";
	data_in->arguments[0].id_name = "s";
	ret = hashTab_insert(T,"print",NULL,data_in,type_func,"ifj16" );

	data_in = my_malloc(sizeof(FSymbolTable) + sizeof(FArg));
	if(data_in == NULL)
		return ERR_INTER;

	data_in->return_type = "int";
	data_in->arg_num = 1;
	data_in->arguments[0].type = "String";
	data_in->arguments[0].id_name = "s";
	ret = hashTab_insert(T,"length",NULL,data_in,type_func,"ifj16" );

	data_in = my_malloc(sizeof(FSymbolTable) + sizeof(FArg)*3);
	if(data_in == NULL)
		return ERR_INTER;

	data_in->return_type = "String";
	data_in->arg_num = 3;
	data_in->arguments[0].type = "String";
	data_in->arguments[0].id_name = "s";
	data_in->arguments[1].type = "int";
	data_in->arguments[1].id_name = "i";
	data_in->arguments[2].type = "int";
	data_in->arguments[2].id_name = "n";
	ret = hashTab_insert(T,"substr",NULL,data_in,type_func,"ifj16" );

	data_in = my_malloc(sizeof(FSymbolTable) + sizeof(FArg)*2);
	if(data_in == NULL)
		return ERR_INTER;

	data_in->return_type = "int";
	data_in->arg_num = 2;
	data_in->arguments[0].type = "String";
	data_in->arguments[0].id_name = "s1";
	data_in->arguments[1].type = "String";
	data_in->arguments[1].id_name = "s2";
	ret = hashTab_insert(T,"compare",NULL,data_in,type_func,"ifj16" );

	data_in = my_malloc(sizeof(FSymbolTable) + sizeof(FArg)*2);
	if(data_in == NULL)
		return ERR_INTER;

	data_in->return_type = "int";
	data_in->arg_num = 2;
	data_in->arguments[0].type = "String";
	data_in->arguments[0].id_name = "s";
	data_in->arguments[1].type = "String";
	data_in->arguments[1].id_name = "search";
	ret = hashTab_insert(T,"find",NULL,data_in,type_func,"ifj16" );

	data_in = my_malloc(sizeof(FSymbolTable) + sizeof(FArg));
	if(data_in == NULL)
		return ERR_INTER;

	data_in->return_type = "String";
	data_in->arg_num = 1;
	data_in->arguments[0].type = "String";
	data_in->arguments[0].id_name = "s";
	ret = hashTab_insert(T,"sort",NULL,data_in,type_func,"ifj16" );

	return ERR_OK;

}

/*
int main()
{
	T = hashTab_init(2000);
	if(T == NULL)
		return ERR_INTER;
	printf("hashtabulka ok\n");

	int dec = BuiltInFuncDeclaration();
	if(dec != 0)
	{
		printf("chyba alokacie vstavanych funkcii\n");
		return ERR_INTER;
	}

	T_loc = hashTab_init_local(2000);
	if(T_loc == NULL)
		return ERR_INTER;
	printf("local hash tabulka ok\n");

	DLInitList(&TokenList);
	printf("tokenlist ok\n");

	stackInit_param(&ParamStack);

	int ret = Parse();
	printf("preslo za parse\n");

	printf("%d\n",ret );

	//printList(&TokenList);

	int mainv = CheckMain();
	printf("%d main value\n",mainv );

	stackDelete_param(&ParamStack);

	int pom = DLDisposeList(&TokenList);
	if(pom == ERR_INTER)
		return ERR_INTER;

	emptyGarbage();

	hashTab_free(T);
	hashTab_free_local(T_loc);
	return 0;
}
*/

#endif //PARSER_C
