//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj

/* KA obecně
 * KAx je funkce implementující konečný automat, který identifikuje daný lexém
 * Funkce vrací počet znaků, které tvoří lexém - 1 v případě, že KAx daný lexém akceptuje
 * V případě, že KAx lexém neakceptuje, vrací KAx hodnotu -1
 * Došlo-li v průběhu analýzy k chybě, vrací KAx zápornou hodnotu <-1, podle typu chyby
 *
 * Všechny KA mají tři a více parametrů. Společné jsou hay - ukazatel na začátek vstupu, start - index prvního čteného znaku a length - celková delka vstupu
 * Některé KA mají další parametry určené k bližší specifikaci jejich chování (pokud jsou například použity v rámci jiných KA), nebo slouží pro předání hodnoty lexému odkazem
 */

#ifndef FSM_H
#define FSM_H

long KA1(char* hay, long start, long length, char partOfFull, char** resultPointer);

long KA2(char* hay, long start, long length, char** resultPointer);

long KA3(char* hay, long start, long length, unsigned char** kw_id);

long KA4(char* hay, long start, long length, int** value, char value_needed);

long KA5(char* hay, long start, long length, double** value, char value_needed);

long KA6(char* hay, long start, long length, char** resultPointer);

long KA7(char* hay, long start, long length);

long KA8(char* hay, long start, long length);

long KA9(char* hay, long start, long length, unsigned char** op_id, char value_needed);

long KA10(char* hay, long start, long length, unsigned char** br_id);

long KA11(char* hay, long start, long length);

long KA12(char* hay, long start, long length, unsigned char** fn_id);

long lastStart = 0; //index prvního znaku potenciálního tokenu
#endif
