//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj

#ifndef _STACK_H_
#define _STACK_H_

#include <stdio.h>
#include "token.h"
#include "parser.h"
#include <string.h>
#include <stdlib.h>
#include "tac_memory_lists.h"

#define MAX_STACK 200
extern int STACK_SIZE; 
/* 
 * Hodnota MAX_STACK udává skutečnou velikost statického pole pro uložení
 * hodnot zásobníku. Při implementaci operací nad ADT zásobník však
 * předpokládejte, že velikost tohoto pole je pouze STACK_SIZE. Umožní to
 * jednoduše měnit velikost zásobníku v průběhu testování. Při implementaci
 * tedy hodnotu MAX_STACK vůbec nepoužívejte. 
 */

extern int solved;                      /* Indikuje, zda byla operace řešena. */
extern int err_flag;                   /* Indikuje, zda operace volala chybu. */

                                        /* Chybové kódy pro funkci stackError */
#define MAX_SERR    3                                   /* počet možných chyb */
#define SERR_INIT   1                                  /* chyba při stackInit */
#define SERR_PUSH   2                                  /* chyba při stackPush */
#define SERR_TOP    3                                   /* chyba při stackTop */

                             /* ADT zásobník implementovaný ve statickém poli */

typedef struct {
	int arr[MAX_STACK];                             /* pole pro uložení hodnot */
	int top;                                /* index prvku na vrcholu zásobníku */
} tsStack;


//typ polozky v tabulke symbolov
typedef enum item_type {
	type_func,
	type_id
} item_type;

union {
		struct IDSymbolTable *itemID;
		struct FSymbolTable *itemF;
	} struct_type;

//prvok tabulky symbolov
typedef struct hash_item {
	char *key;
	item_type type; //vychadza z enumu nad strukturou item_type
	char *class_item;

	union {
		struct IDSymbolTable *itemID;
		struct FSymbolTable *itemF;
	} struct_type;

	struct hash_item* next;
} hash_item;

//tabulka symbolov
typedef struct hash_table {
	unsigned long size;
	struct hash_item *list[];
} hash_table;

//polozka prvku tabulky symbolov - identifikator
typedef struct IDSymbolTable {
	char *type;	//datovy typ identifikatora
	void *value;
	unsigned initialized;
} IDSymbolTable;

// struktura pre argumenty funkcie
typedef struct FArg {
	char *type;
	char *id_name;
} FArg;

//polozka prvku tabulky symbolov - funkcia
typedef struct FSymbolTable {
	TACListElement def_adress;
	char *return_type;
	int arg_num;
	struct FArg arguments[];
} FSymbolTable;

//tabulka symbolov pre lokalne premenne
typedef struct hash_item_local {
	char *local_key;
	char *datatype_local;
	void *value_local;
	unsigned init_local;
	unsigned offset;
	struct hash_item_local *next;
} hash_item_local;

typedef struct hash_table_local {
	unsigned long size_local;
	unsigned number_variable;
	struct hash_item_local *list_local[];
} hash_table_local;

                                  /* Hlavičky funkcí pro práci se zásobníkem. */
void stackError ( int error_code );
void stackInit ( tsStack* s );
int stackEmpty ( const tsStack* s );
int stackFull ( const tsStack* s );
void stackTop ( const tsStack* s, char* c );
void stackPop ( tsStack* s );
void stackPush ( tsStack* s, char c );



unsigned long hash_function(const char *name, unsigned long size);
hash_table *hashTab_init(unsigned long size);
int hashTab_clear(hash_table *table);
int hashTab_free(hash_table *table);
hash_item *hashTab_search (hash_table* table, const char *key, item_type wanted_type, char *wanted_class);
int hashTab_remove(hash_table *table, const char *key);
int hashTab_insert(hash_table *table, char *key, IDSymbolTable *IDsymbol, FSymbolTable *Fsymbol, item_type type, char *class_in);
hash_table_local *hashTab_init_local(unsigned size);
int hashTab_clear_local(hash_table_local *table);
int hashTab_free_local(hash_table_local *table);
hash_item_local *hashTab_search_local (hash_table_local* table, const char *key );
int hashTab_remove_local(hash_table_local *table, const char *key);
int hashTab_insert_local(hash_table_local *table, char *key, char *type, void *value, unsigned init);

char * sort(char *str);
int find(char *s, char *search);
void quicksort(char * item, int l, int r);
void partition(char * item, int l, int r, int * ptr_i, int * ptr_j);

#endif
