//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj

#ifndef LEXICALANALYSIS_TAC_LISTS_H
#define LEXICALANALYSIS_TAC_LISTS_H

#include "tac.h"
#include "memory_functions.h"

//struktura pro jeden záznam v seznamu uchovávající instrukci v TAC
typedef struct sTACListElement {
    TAC data;
    struct sTACListElement* prev;
    struct sTACListElement* next;
} *TACListElement;

//struktura pro seznam instrukcí v TAC
typedef struct {
    TACListElement first;
    TACListElement active;
    TACListElement last;
} TACList;

void initTACList(TACList* L);
TACListElement addElement(TACList* L, TACInstruction instruction, void* operand1, void* operand2, void* result);
void setActivityToFirst(TACList* L);

void printTACList(TACList* L, int listNO);
void printOneElement(TACList* L, TACListElement element);

#endif //LEXICALANALYSIS_TAC_LISTS_H
