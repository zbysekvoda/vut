//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj
#include "tac_program.h"

#define STRING_CHARS 100

//vrátí délku čísla typu int
int intLen(int i) {
    int len = 0;

    while(i != 0) {
        i = i / 10;
        len++;
    }

    return len;
}

//vytiskne informace o instrukci
void printInstructionLegend(TACInstruction instruction, int t) {
    for(int i = 0; i < t; i++) {
        ////printf("\t");
    }

    //printf("| [%s] ", TACInstructionName[instruction]);
}

//vymaže pomocný řetězec
void clearString(char* s) {
    char c;
    int counter = 0;

    while(s[counter] != '\0') {
        s[counter] = '\0';
        counter++;
    }
}

//interpretuje jednu instrukci v TAC
ERROR_TYPE interpretNextElement(TACList* program, TACList* functions, TACStack* returnAdresses, TACParamStack* paramStack) {
    (void)program;
    (void)functions;
    (void)returnAdresses;
    (void)paramStack;

    TACListElement element = program->active; //načte si aktivní instrukci

    //načte data aktivní instrukce
    TACInstruction instruction = element->data->instruction;
    void* operand1 = element->data->op1;
    void* operand2 = element->data->op2;
    void* result = element->data->result;

    //printOneElement(program, element);

    TACListElement next = program->active->next; //zajistí posun na následující při vykonání instrukce, u skokových instrukcí se hodnota přepisuje dle parametrů

    char* pom_s_1;
    char* pom_s_2;
    int pom_i_1;
    int pom_i_2;

    TACParamStackElement param2;

    char s[STRING_CHARS + 1];
    for(int i = 0; i < STRING_CHARS + 1; i++) {
        s[i] = '\0';
    }

    //provede patřičné kroky podle instrukce
    switch(instruction) {
        case label:
            printInstructionLegend(instruction, 3);
            //printf("\tLabel with adress %p", element);
            break;

        case assign_i:
            printInstructionLegend(instruction, 2);
            //printf("\t%p <= %d (%p)", result, *(int*)operand1, operand1);
            *(int*)result = *(int*)operand1;
            break;
        case assign_f:
            printInstructionLegend(instruction, 1);
            //printf("\t%p <= %lf", result, *(double*)operand1);
            *(double*)result = *(double*)operand1;
            break;
        case assign_s:
            printInstructionLegend(instruction, 2);
            //printf("\t%p <= %p (%s)", result, operand1, *(char**)operand1);
            *(char**)result = *(char**)operand1;
            break;

        case m_sum_i:
            printInstructionLegend(instruction, 1);
            //printf("\t%p <= %d + %d \t(=%d)", result, *(int*)operand1, *(int*)operand2, *(int*)operand1 + *(int*)operand2);
            *(int*)result = *(int*)operand1 + *(int*)operand2;
            break;
        case m_sub_i:
            printInstructionLegend(instruction, 1);
            //printf("\t%p <= %d - %d \t(=%d)", result, *(int*)operand1, *(int*)operand2, *(int*)operand1 - *(int*)operand2);
            *(int*)result = *(int*)operand1 - *(int*)operand2;
            break;
        case m_mul_i:
            printInstructionLegend(instruction, 1);
            //printf("\t%p <= %d * %d \t(=%d)", result, *(int*)operand1, *(int*)operand2, *(int*)operand1 * *(int*)operand2);
            *(int*)result = *(int*)operand1 * *(int*)operand2;
            break;
        case m_div_i:
            printInstructionLegend(instruction, 1);

            if(*(int*)operand2 == 0){
                return ERR_ZERO;
            }

            //printf("\t%p <= %d / %d \t(=%d)", result, *(int*)operand1, *(int*)operand2, *(int*)operand1 / *(int*)operand2);

            *(int*)result = *(int*)operand1 / *(int*)operand2;
            break;

        case m_sum_f:
            printInstructionLegend(instruction, 1);
            //printf("\t%p <= %lf + %lf \t(=%lf)", result, *(double*)operand1, *(double*)operand2, *(double*)operand1 + *(double*)operand2);
            *(double*)result = *(double*)operand1 + *(double*)operand2;
            break;
        case m_sub_f:
            printInstructionLegend(instruction, 1);
            //printf("\t%p <= %lf - %lf \t(=%lf)", result, *(double*)operand1, *(double*)operand2, *(double*)operand1 - *(double*)operand2);
            *(double*)result = *(double*)operand1 - *(double*)operand2;
            break;
        case m_mul_f:
            printInstructionLegend(instruction, 1);
            //printf("\t%p <= %lf * %lf \t(=%lf)", result, *(double*)operand1, *(double*)operand2, *(double*)operand1 * *(double*)operand2);
            *(double*)result = *(double*)operand1 * *(double*)operand2;
            break;
        case m_div_f:
            printInstructionLegend(instruction, 1);
            //printf("\t%p <= %lf / %lf \t(=%lf)", result, *(double*)operand1, *(double*)operand2, *(double*)operand1 / *(double*)operand2);

            if(*(double*)operand2 == 0){
                return ERR_ZERO;
            }

            *(double*)result = *(double*)operand1 / *(double*)operand2;
            break;

        case s_concat:
            printInstructionLegend(instruction, 1);
            //printf("\t%p <= %p (%s) + %p (%s)", result, operand1, *(char**)operand1, operand2, *(char**)operand2);
            pom_i_1 = strlen(*(char**)operand1);
            pom_i_2 = strlen(*(char**)operand2);
            pom_s_1 = my_malloc(pom_i_1 + pom_i_2 + 1);
            for(int i = 0; i < pom_i_1; i++) {
                pom_s_1[i] = (*(char**)operand1)[i];
            }
            for(int i = 0; i <= pom_i_2; i++) {
                pom_s_1[i + pom_i_1] = (*(char**)operand2)[i];
            }
            *(char**)result = pom_s_1;
            break;

        case f_pushp:
            printInstructionLegend(instruction, 1);
            //printf("\tPushing value ");
            switch(*(paramType*)operand2) {
                case Int:
                    //printf("%d of type Int", *(int*)operand1);
                    break;
                case Double:
                    //printf("%f of type Double", *(double*)operand1);
                    break;
                case String:
                    //printf("%s of type String", *(char**)operand1);
                    break;
            }
            //printf(" to the paramStack.");
            pushParam(paramStack, operand1, *(paramType*)operand2);
            break;
        case f_call:
            printInstructionLegend(instruction, 1);
            //printf("\tCalling function on adress %p with %d params, ", *(TACListElement*)operand1, *(int*)operand2);
            //printf("Pushing adress to stack %p", program->active->next);
            pushReturnAdress(returnAdresses, program->active->next);

            //na stack pushne aktuální počet prvků na paramStacku a počet parametrů funkce
            pushParam(&paramOffsetStack, mallocInt(*(int*)operand2), Int);
            pushParam(&paramOffsetStack, mallocInt(paramStack->elementsOnStack), Int);

            next = *(TACListElement*)operand1;
            break;
        case f_ret:
            printInstructionLegend(instruction, 2);
            next = popReturnAdress(returnAdresses);
            for(int i = 0; i < *(int*)operand1; i++) {
                popParam(paramStack);
            }

            popParam(&paramOffsetStack);
            popParam(&paramOffsetStack);

            //printf("\tReturning to adress %p", next);
            break;
        case f_readp:
            printInstructionLegend(instruction, 2);

            if(operand1 == NULL){
                param2 = readParam(paramStack, &paramOffsetStack, 0);
            } else {
                param2 = readParam(paramStack, &paramOffsetStack, *(int*)operand1);
            }
            //printf("\t%p <= ", result);

            switch(param2->type) {
                case Int:
                    //printf("%d", *(int*)param2->data);
                    *(int*)result = *(int*)param2->data;
                    break;
                case Double:
                    //printf("%f", *(double*)param2->data);
                    *(double*)result = *(double*)param2->data;
                    break;
                case String:
                    //printf("%s", *(char**)param2->data);
                    *(char**)result = *(char**)param2->data;
                    break;
            }
            //printf(" (read)");
            break;
        case f_changep:
            printInstructionLegend(instruction, 1);

            if(operand1 == NULL){
                param2 = readParam(paramStack, &paramOffsetStack, 0);
            } else {
                param2 = readParam(paramStack, &paramOffsetStack, *(int*)operand1);
            }

            if(param2->type == Int){
                //printf("\tChanging param value from %d to %d", *(int*)param2->data, *(int*)operand2);
                *(int*)param2->data = *(int*)operand2;
            }
            else if(param2->type == Double){
                //printf("\tChanging param value from %g to %g", *(double*)param2->data, *(double*)operand2);
                *(double*)param2->data = *(double*)operand2;
            }
            else if(param2->type == String){
                //printf("\tChanging param value from %s to %s", *(char**)param2->data, *(char**)operand2);
                *(char**)param2->data = *(char**)operand2;
            }

            break;


        case j_a_jmp:
            printInstructionLegend(instruction, 3);
            //printf("\tJump to %p", result);
            next = *(TACListElement*)result;
            break;
        case j_a_eq_i:
            printInstructionLegend(instruction, 1);
            //printf("\tJump to %p if %d is equal to %d", *(TACListElement*)result, *(int*)operand1, *(int*)operand2);

            if(*(int*)operand1 == *(int*)operand2){
                //printf(" (TRUE) -> jumping");
                next = *(TACListElement*)result;
            } else {
                //printf(" (FALSE) -> not jumping");
            }
            break;
        case j_a_neq_i:
            printInstructionLegend(instruction, 1);
            //printf("\tJump to %p if %d is not equal to %d", *(TACListElement*)result, *(int*)operand1, *(int*)operand2);

            if(*(int*)operand1 != *(int*)operand2){
                //printf(" (TRUE) -> jumping");
                next = *(TACListElement*)result;
            } else {
                //printf(" (FALSE) -> not jumping");
            }
            break;

        case b_readint:
            printInstructionLegend(instruction, 3);
            //printf("\tReading integer from input");
            *(int*)result = readInt();

            if(readError == 1 && *(int*)result == 7){
                return ERR_READ;
            }

            break;
        case b_readdouble:
            printInstructionLegend(instruction, 2);
            //printf("\tReading double from input");
            *(double*)result = readDouble();

            if(readError == 1 && *(double*)result == 7){
                return ERR_READ;
            }

            break;
        case b_readstring:
            printInstructionLegend(instruction, 2);
            //printf("\tReading string from input");
            *(char**)result = readString();
            break;
        case b_print:
            printInstructionLegend(instruction, 3);
            pom_s_1 = *(char**)popParam(paramStack)->data;
            //printf("\tPrinting value: ");
            print(pom_s_1);
            break;
        case b_length:
            printInstructionLegend(instruction, 3);
            pom_s_1 = *(char**)popParam(paramStack)->data;
            //printf("\tCounting length of %s", pom_s_1);
            *(int*)result = length(pom_s_1);
            break;
        case b_substr:
            printInstructionLegend(instruction, 3);
            pom_i_2 = *(int*)popParam(paramStack)->data;
            pom_i_1 = *(int*)popParam(paramStack)->data;
            pom_s_1 = *(char**)popParam(paramStack)->data;
            //printf("\tSubstring of %s from %d to %d", pom_s_1, pom_i_1, pom_i_2);
            *(char**)result = substr(pom_s_1, pom_i_1, pom_i_2);
            break;
        case b_compare:
            printInstructionLegend(instruction, 3);
            pom_s_2 = *(char**)popParam(paramStack)->data;
            pom_s_1 = *(char**)popParam(paramStack)->data;
            //printf("\tComparing strings %s and %s", pom_s_1, pom_s_2);
            *(int*)result = compare(pom_s_1, pom_s_2);
            break;
        case b_find:
            printInstructionLegend(instruction, 3);
            pom_s_2 = *(char**)popParam(paramStack)->data;
            pom_s_1 = *(char**)popParam(paramStack)->data;
            //printf("\tSearching string %s in %s", pom_s_1, pom_s_2);
            *(int*)result = find(pom_s_1, pom_s_2);
            break;
        case b_sort:
            printInstructionLegend(instruction, 3);
            pom_s_1 = *(char**)popParam(paramStack)->data;
            //printf("\tSorting string %s", pom_s_1);
            *(char**)result = sort(pom_s_1);
            break;

        case c_itod:
            printInstructionLegend(instruction, 2);
            *(double*)result = (double)*(int*)operand1;
            //printf("\tConverting integer %d to double %f", *(int*)operand1, *(double*)result);
            break;
        case c_itos:
            printInstructionLegend(instruction, 2);

            pom_i_1 = intLen(*(int*)operand1);
            pom_s_1 = my_malloc(pom_i_1 + 1);
            sprintf(pom_s_1, "%d", *(int*)operand1);
            //printf("\tConverting integer %d to string %s", *(int*)operand1, (char*)pom_s_1);

            *(char**)result = pom_s_1;

            break;
        case c_dtos:
            printInstructionLegend(instruction, 2);

            clearString(s);
            sprintf(s, "%g", *(double*)operand1);
            //printf("\tConverting double %f to string %s", *(double*)operand1, (char*)s);

            pom_s_1 = my_malloc(strlen(s) + 1);
            strcpy(pom_s_1, s);
            *(char**)result = pom_s_1;
            break;

        case cmp_eq_i:
            printInstructionLegend(instruction, 1);
            *(int*)result = *(int*)operand1 == *(int*)operand2 ? 1 : 0;
            //printf("\t%d == %d ? %d", *(int*)operand1, *(int*)operand2, *(int*)result);
            break;
        case cmp_neq_i:
            printInstructionLegend(instruction, 1);
            *(int*)result = *(int*)operand1 != *(int*)operand2 ? 1 : 0;
            //printf("\t%d != %d ? %d", *(int*)operand1, *(int*)operand2, *(int*)result);
            break;
        case cmp_gr_i:
            printInstructionLegend(instruction, 1);
            *(int*)result = *(int*)operand1 > *(int*)operand2 ? 1 : 0;
            //printf("\t%d > %d ? %d", *(int*)operand1, *(int*)operand2, *(int*)result);
            break;
        case cmp_ls_i:
            printInstructionLegend(instruction, 1);
            *(int*)result = *(int*)operand1 < *(int*)operand2 ? 1 : 0;
            //printf("\t%d < %d ? %d", *(int*)operand1, *(int*)operand2, *(int*)result);
            break;
        case cmp_geq_i:
            printInstructionLegend(instruction, 1);
            *(int*)result = *(int*)operand1 >= *(int*)operand2 ? 1 : 0;
            //printf("\t%d >= %d ? %d", *(int*)operand1, *(int*)operand2, *(int*)result);
            break;
        case cmp_leq_i:
            printInstructionLegend(instruction, 1);
            *(int*)result = *(int*)operand1 <= *(int*)operand2 ? 1 : 0;
            //printf("\t%d <= %d ? %d", *(int*)operand1, *(int*)operand2, *(int*)result);
            break;
        case cmp_eq_d:
            printInstructionLegend(instruction, 1);
            *(int*)result = *(double*)operand1 == *(double*)operand2 ? 1 : 0;
            //printf("\t%f == %f ? %d", *(double*)operand1, *(double*)operand2, *(int*)result);
            break;
        case cmp_neq_d:
            printInstructionLegend(instruction, 1);
            *(int*)result = *(double*)operand1 != *(double*)operand2 ? 1 : 0;
            //printf("\t%f != %f ? %d", *(double*)operand1, *(double*)operand2, *(int*)result);
            break;
        case cmp_gr_d:
            printInstructionLegend(instruction, 1);
            *(int*)result = *(double*)operand1 > *(double*)operand2 ? 1 : 0;
            //printf("\t%f > %f ? %d", *(double*)operand1, *(double*)operand2, *(int*)result);
            break;
        case cmp_ls_d:
            printInstructionLegend(instruction, 1);
            *(int*)result = *(double*)operand1 < *(double*)operand2 ? 1 : 0;
            //printf("\t%f < %f ? %d", *(double*)operand1, *(double*)operand2, *(int*)result);
            break;
        case cmp_geq_d:
            printInstructionLegend(instruction, 1);
            *(int*)result = *(double*)operand1 >= *(double*)operand2 ? 1 : 0;
            //printf("\t%f >= %f ? %d", *(double*)operand1, *(double*)operand2, *(int*)result);
            break;
        case cmp_leq_d:
            printInstructionLegend(instruction, 1);
            *(int*)result = *(double*)operand1 <= *(double*)operand2 ? 1 : 0;
            //printf("\t%f <= %f ? %d", *(double*)operand1, *(double*)operand2, *(int*)result);
            break;

        default:
            ////printf("\t\t\t");
            ////printf("| \t----NOT IMPLEMENTED----");
            break;
    }

    //printf("\n");

    program->active = next;

    return ERR_OK;
}

//interpretuje celý program opakovaným voláním funkce interpretNextElement()
ERROR_TYPE interpretProgram(TACList* program, TACList* functions, TACStack* returnAdresses, TACParamStack* paramStack) {
    int counter = 1;

    //printf("-------------------------------------------------------Interpretting-------------------------------------------------------------------------\n");
    //printf("#\t| adress\t| flags | TAC\t\t\t\t\t\t\t\t| Interpretation\n");

    while(program->active != NULL) {
        //printf("%d\t| ", counter);
        //printf("%p\t| ", program->active);
        int ret = interpretNextElement(program, functions, returnAdresses, paramStack);
        if(ret != ERR_OK){
            return ret;
        }

        counter++;
    }

    return ERR_OK;
}

//pomocná funkce pro generování instrukcí, které čtou hodnotu lokálních proměnných
int readFromLocal(hash_item_local* variable, void *to, paramType toType){
    //funkce dostane identifikátor v tabulce symbolů a podle datovéhi typu identifikátoru a zapisované hodnoty generuje patřičné instrukce

    /*
        void abc(int a, int b){
            int c ...;
        }

        STACK
        0 | int c | variable.offset 2 |
        1 | int b | variable.offset 1 |
        2 | int a | variable.offset 0 |

        - podívá se na počet parametrů ve funkci a na proměnnou
     */
    int* readOffset = mallocInt(variable->offset);

    if(strcmp(variable->datatype_local, "int") == 0 && toType == Int){
        // int -> int
        addElement(&functionDefinitions, f_readp, readOffset, NULL, to);
    } else if(strcmp(variable->datatype_local, "int") == 0 && toType == Double){
        // int -> double
        addElement(&functionDefinitions, f_readp, readOffset, NULL, reg_i1);
        addElement(&functionDefinitions, c_itod, reg_i1, NULL, to);
    } else if(strcmp(variable->datatype_local, "double") == 0 && toType == Double){
        // double -> double
        addElement(&functionDefinitions, f_readp, readOffset, NULL, to);
    } else if(strcmp(variable->datatype_local, "String") == 0 && toType == String){
        // String -> String
        addElement(&functionDefinitions, f_readp, readOffset, NULL, to);
    } else {
        return ERR_SEM_TYPE;
    }
}

//pmocná funkce pro generování instrukcí, které zapisují do lokálních proměnných
int writeToLocal(hash_item_local* variable, void *from, paramType fromType){
    //funkce dostane identifikátor v tabulce symbolů a podle datovéhi typu identifikátoru a zapisované hodnoty generuje patřičné instrukce

    int* writeOffset = mallocInt(variable->offset);

    ////printf("\n\n OFfset promenne je %d. \n\n", variable->offset);

    if(strcmp(variable->datatype_local, "int") == 0 && fromType == Int){
        // int -> int
        addElement(&functionDefinitions, f_changep, writeOffset, from, NULL);
    } else if(strcmp(variable->datatype_local, "double") == 0 && fromType == Int){
        // int -> double
        addElement(&functionDefinitions, c_itod, from, NULL, reg_d1);
        addElement(&functionDefinitions, f_changep, writeOffset, reg_d1, NULL);
    } else if(strcmp(variable->datatype_local, "double") == 0 && fromType == Double){
        // double -> double
        addElement(&functionDefinitions, f_changep, writeOffset, from, NULL);
    } else if(strcmp(variable->datatype_local, "String") == 0 && fromType == String){
        // String -> String
        addElement(&functionDefinitions, f_changep, writeOffset, from, NULL);
    } else {
        return ERR_SEM_TYPE;
    }
}