//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj

#ifndef LEXICALANALYSIS_TOKEN_H
#define LEXICALANALYSIS_TOKEN_H

//výčtový typ možných typů tokenů
typedef enum {
    simple_identifier,
    fully_qualified_identifier,
    num_i,
    num_d,
    comment,
    semicolon,
    bracket_left,
    bracket_right,
    bracket_open,
    bracket_close,
    comma,
    string,
    builtInFunction,
    operator_equal,
    operator_plus,
    operator_minus,
    operator_multiply,
    operator_divide,
    operator_less,
    operator_greater,
    operator_less_equal,
    operator_greater_equal,
    operator_relation,
    operator_not_equal,
    keyword_data_type,
    keyword_normal,
    keyword_false,
    keyword_true,
    undefined,
    error,
    error99,
    end,
    keyword,
    operator,
    E,
    DOLAR,
    LLL
} TokenType;

//struktura uchovávající informace o jednom tokenu
typedef struct {
    TokenType type;
    void *data;
    unsigned long lineNumber;
} SToken;

char *tokenTypes[] = {
        "simple_identifier",
        "fully_qualified_identifier",
        "num_i",
        "num_d",
        "comment",
        "semicolon",
        "bracket_left",
        "bracket_right",
        "bracket_open",
        "bracket_close",
        "comma",
        "string",
        "builtInFunction",
        "operator_equal",
        "operator_plus",
        "operator_minus",
        "operator_multiply",
        "operator_divide",
        "operator_less",
        "operator_greater",
        "operator_less_equal",
        "operator_greater_equal",
        "operator_relation",
        "operator_not_equal",
        "keyword_data_type",
        "keyword_normal",
        "keyword_false",
        "keyword_true",
        "undefined",
        "error",
        "error99",
        "end",
        "keyword",
        "operator",
        "E",
        "DOLAR",
        "LLL"
};

int numberOfKeywords = 17;
// neměnit pořadí!
char *keywords[] = {
        "boolean",
        "double",
        "int",
        "String",
        "void",
        "static",
        "break",
        "class",
        "continue",
        "do",
        "else",
        "false",
        "for",
        "if",
        "return",
        "true",
        "while"
};

int numberOfOperators = 11;
// neměnit pořadí!
char *operators[] = { //musí být seřazeny od kratších k delším
        "=",
        "+",
        "-",
        "*",
        "/",
        "<",
        ">",
        "<=",
        ">=",
        "==",
        "!="
};

int numberOfBrackets = 4;
char brackets[] = {
        '(',
        ')',
        '{',
        '}'
};
int numberOfBuiltInFunctions = 9;
char *builtInFunctions[] = {
        "ifj16.readInt",
        "ifj16.readDouble",
        "ifj16.readString",
        "ifj16.print",
        "ifj16.length",
        "ifj16.substr",
        "ifj16.compare",
        "ifj16.find",
        "ifj16.sort"
};

#endif
