//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj

#ifndef LEX_FINIT_STATE_MACHINES_C
#define LEX_FINIT_STATE_MACHINES_C

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>

#include "lex_finit_state_machines.h"

int getInt(char c) {
    return c - '0';
}

long KA1(char* hay, long start, long length, char firstPartOfFull, char** resultPointer) { // jednoduchý identifikátor
    // KA se nejdříve podívá, jestli aktuální vstup začáná na _, $, nebo znak, pokud ne, ihned je ukončen
    // Dále kontroluje, jestli identifikátor obsahuje alfanumerické znaky, _, nebo $ a načítá znaky ze vstupu
    // parametrem resultPointer předává název identifikátoru

    int i = 0;
    char c;

    c = hay[start];
    if(c != '_' && c != '$' && !isalpha(c)){ //může začínat písmenem/podtržítkem/dolarem
        return -1;
    } else {
        i++;
    }

    while(start + i < length) {
        c = hay[start + i];

        if(isalnum(c) || c == '_' || c == '$'){
            i++;
        } else {
            break;
        }
    }

    if(start + i > length){
        return -2;
    }

    if(i == 0){
        return -1;
    }

    //pokud je KA1 použit k identifikaci první části plně kvalifikov. id., nebude se kontrolovat následující znak.
    if(!firstPartOfFull){
        if((KA9(hay, start + i, length, NULL, 0)) >= 0
           || isspace(hay[start + i])
           || hay[start + i] == '{'
           || hay[start + i] == '}'
           || hay[start + i] == '('
           || hay[start + i] == ')'
           || hay[start + i] == ';'
           || hay[start + i] == ','
           || hay[start + i] == '\0'){

        } else {
            return -1;
        }
    }

    *resultPointer = my_malloc(sizeof(char) * (i + 1));
    if(*resultPointer == NULL){
        return -3;
    }

    for(int j = 0; j < i; j++) {
        (*resultPointer)[j] = hay[start + j];
    }
    (*resultPointer)[i] = '\0';

    return start + i - 1;
}

long KA2(char* hay, long start, long length, char** resultPointer) { // plně kvantifikovaný identifikátor
    // zkoumá, jestli se jedná o dva jednoduché identifikátory oddělené tečkou
    // parametrem resultPointer předává název identifikátoru

    int result;
    char* idPartName = NULL;

    if((result = KA1(hay, start, length, 1, &idPartName)) >= 0 && result < length){
        if(hay[result + 1] == '.'){
            result = KA1(hay, result + 2, length, 0, &idPartName);
            if(result <= -2){
                return result;
            }
        } else {
            result = -1;
        }
    }

    if(result >= 0){
        if(result < length){
            int charsInNewString = result - start + 1;

            *resultPointer = (char*)my_malloc(sizeof(char) * (charsInNewString + 1));
            if(*resultPointer == NULL){
                return -3;
            }

            for(int j = 0; j <= result - start; j++) {
                (*resultPointer)[j] = hay[start + j];
            }
            (*resultPointer)[charsInNewString] = '\0';

            return result;
        } else {
            return -2;
        }
    }

    return result;
}

long KA3(char* hay, long start, long length, unsigned char** kw_id) { // klíčové slovo
    //hledá klíčová slova z předdefinovaného pole keywords
    //parametrem kw_id předává index kl. slova v tomto poli

    char result = 1;

    int i = 0;
    int j = 0;

    for(i = 0; i < numberOfKeywords; i++) {
        int keywordLength = strlen(keywords[i]);
        result = keywordLength;

        for(j = 0; j < keywordLength; j++) {
            if(start + j >= length || hay[start + j] != keywords[i][j]){
                result = 0;
                break;
            }
        }

        if(result){
            if(start + keywordLength > length){
                return -2;
            }

            if((KA9(hay, start + keywordLength, length, NULL, 0)) >= 0
               || isspace(hay[start + keywordLength])
               || hay[start + keywordLength] == '{'
               || hay[start + keywordLength] == '}'
               || hay[start + keywordLength] == '('
               || hay[start + keywordLength] == ')'
               || hay[start + keywordLength] == ';'
               || hay[start + keywordLength] == '\0'){
                break;
            } else {
                result = 0;
                continue;
            }
        }
    }

    if(result > 0){
        *kw_id = (unsigned char*)my_malloc(sizeof(unsigned int));
        if(*kw_id == NULL){
            return -3;
        }

        **kw_id = (unsigned char)i;

        return start + result - 1;
    } else {
        return -1;
    }

    return -5;
}

long KA4(char* hay, long start, long length, int** value, char value_needed) { // celočíselný literál
    //pokud není první znak na vstupu číslo, je KA ihned ukončen
    //pokud je číslo, provádí se čtení následujících znaků, dokud jsou tyto znaky čísla
    //parametrem value předává hodnotu načteného literálu

    int i = 0;
    char c;

    if(!isdigit(hay[start])){
        return -1;
    }

    do {
        c = hay[start + i];

        if(isdigit(c)){
            i++;
        } else {
            break;
        }
    } while(start + i < length);

    if(start + i > length){
        return -2;
    }

    unsigned long parsed_value = strtoul(hay + start, NULL, 10);

    if(value_needed){
        *value = (int*)my_malloc(sizeof(int));
        if(*value == NULL){
            return -3;
        }

        if(parsed_value > INT_MAX){
            // TODO co s čísly mimo rozsah C-int?
            **value = (int)-1;
        } else {
            **value = (int)parsed_value;
        }
    }

    return start + i - 1;
}

long KA5(char* hay, long start, long length, double** value, char value_needed) { // desetinný literál
    //hledá na vstupu celočíselný literál pomocí KA4
    //poté hledá znak ., nebo exponent
        //najde li tečku, hledá nový celočíselný literál a dále exponent (nepovinné), +/- (nepovinné) a opět číslo
        //najde li exponent, hledá +/- (nepovinné) a číslo
    //parametrem value předává hodnotu načteného literálu

    long i = 0;

    if((i = KA4(hay, start, length, NULL, 0)) >= 0 && i + 2 <= length){
        if(hay[i + 1] == '.'){
            if((i = KA4(hay, i + 2, length, NULL, 0)) >= 0 && i + 1 <= length){
                if(hay[i + 1] == 'E' || hay[i + 1] == 'e'){
                    if(hay[i + 2] == '+' || hay[i + 2] == '-'){
                        i++;
                    }

                    if((i = KA4(hay, i + 2, length, NULL, 0)) < 0){
                        return -1;
                    }
                }
            } else {
                return i;
            }
        } else if(hay[i + 1] == 'e' || hay[i + 1] == 'E'){
            if(hay[i + 2] == '+' || hay[i + 2] == '-'){
                i++;
            }

            if((i = KA4(hay, i + 2, length, NULL, 0)) < 0){
                return i;
            }
        } else {
            return -1;
        }
    } else {
        return -1;
    }

    if(i < 0){
        return i;
    }

    if(value_needed){
        *value = (double*)my_malloc(sizeof(double));
        if(*value == NULL){
            return -3;
        }

        **value = strtod(hay + start, NULL);
    }

    return i;
}

long _KA6(char* hay, long start, long length, char** resultPointer) { // pomocná funkce pro KA6
    //pomocná funkce pro KA6 - detekce řetězcového literálu
    //Podívá se, zda vstup začíná znakem "
    //Dále čte vstup, dokud nenarazí opět na znak "
    //parametrem resultPointer předává načtený řetězec

    if(hay[start] != '\"'){
        return -1;
    }

    char loaded;
    char output;
    char error = 0;
    int i = 1;
    int charCounter = 0;

    // nesmí obsahovat zalomení
    do {
        loaded = hay[start + i];

        if(loaded <= 31){ //speciální znaky
            error = 1;
        } else if(loaded == '\\'){
            if(length >= start + i + 1){
                if(hay[start + i + 1] == '"'){
                    output = '\"';
                    i++;
                } else if(hay[start + i + 1] == 'n'){
                    output = '\n';
                    i++;
                } else if(hay[start + i + 1] == 't'){
                    output = '\t';
                    i++;
                } else if(hay[start + i + 1] == '\\'){
                    output = '\\';
                    i++;
                } else if(length >= start + i + 3){
                    if(isdigit(hay[start + i + 1]) && isdigit(hay[start + i + 2]) && isdigit(hay[start + i + 2])){
                        int value = getInt(hay[start + i + 1]) * 64 + getInt(hay[start + i + 2]) * 8 +
                                    getInt(hay[start + i + 3]);
                        if(value >= 0001 && value <= 0377){
                            output = (char)value;
                            i += 3;
                        } else {
                            error = 1;
                        }
                    } else {
                        error = 1;
                    }
                }
            } else {
                error = 1;
            }
        } else if(loaded == '\"'){
            break;
        } else {
            output = loaded;
        }

        if(error){
            // TODO chyba vstupu
            return -2;
        }

        if(resultPointer != NULL){
            (*resultPointer)[charCounter] = output;
        }

        i++;
        charCounter++;
    } while(1);

    if(resultPointer == NULL){
        return charCounter;
    } else {
        return i;
    }
}

long KA6(char* hay, long start, long length, char** resultPointer) { //řetězcový literál
    //nejdříve dojde ke zjištění délky řetězce pomocí pomocné funkce _KA6
    //na jejím základě je pro řetězec alokována paměť
    //poté dojde k opětovnému průchodu _KA6, tentokrát i se zápisem přečtených znaků do alokované paměti
    //parametrem resultPointer předává načtený řetězec

    long charsInNewString = _KA6(hay, start, length, NULL); //délka řetězce BEZ posledních uvozovek

    if(charsInNewString < 0){
        return charsInNewString;
    }

    if(charsInNewString >= 0){
        *resultPointer = (char*)my_malloc(sizeof(char) * (charsInNewString + 1)); //alokuje prostor pro string
        if(*resultPointer == NULL){
            return -3;
        }

        long usedChars = _KA6(hay, start, length, resultPointer);
        (*resultPointer)[charsInNewString] = '\0';

        if(usedChars < 0){
            return usedChars;
        }

        return start + usedChars;
    } else {
        return -1;
    }
}

long KA7(char* hay, long start, long length) { // komentář
    //hledá výskyt //, po nalezení hledá konec řádku nebo vstupu
    //hledá výskyt /*, po nalezení hledá */ nebo konec vstupu
    //vrátí index posledního znaku komentáře

    char c = 0;

    if(hay[start] == '/'){
        if(start < length){
            if(hay[start + 1] == '/'){
                do {
                    c = hay[start];
                    start++;
                } while(start < length && c != '\n' && c != '\0');

                if(start > length){
                    return -2;
                }

                start -= 1;

                return start - 1;
            } else if(hay[start + 1] == '*'){
                start += 2;
                do {
                    start++;
                    if(hay[start] == '\n'){
                        lineNumber++;
                    }
                } while(start <= length && !(hay[start - 1] == '*' && hay[start] == '/'));

                if(start >= length){
                    return -2;
                }

                return start;
            }
        } else {
            return -2;
        }
    }

    return -1;
}

long KA8(char* hay, long start, long length) { // středník
    (void) length;

    if(hay[start] == ';'){
        return start;
    }

    return -1;
}

long KA9(char* hay, long start, long length, unsigned char** op_id, char value_needed) { // operátor
    //hledá shodu znaků na vstupu s operátory z pole operators
    //parametrem op_id předává index nalezeného operátoru v tomto poli

    char result;
    int i = 0;

    for(i = numberOfOperators - 1; i >= 0; i--) {
        int operatorLength = strlen(operators[i]);
        result = operatorLength;

        for(int j = 0; j < operatorLength; j++) {
            if(start + j > length){
                return -2;
            }

            if(hay[start + j] != operators[i][j]){
                result = -1;
                break;
            }
        }

        if(result > 0){
            break;
        }
    }

    if(result > 0){
        if(value_needed){
            *op_id = (unsigned char*)my_malloc(sizeof(unsigned char));
            if(*op_id == NULL){
                return -3;
            }

            **op_id = (unsigned char)i;
        }

        return start + result - 1;
    }

    return -1;
}

long KA10(char* hay, long start, long length, unsigned char** br_id) { // závorky
    //hledá shodu znaku na vstupu se znakem z pole brackets
    //parametrem br_id předává index závorky v tomto poli

    (void) length;

    char result;
    int i = 0;

    for(i = 0; i < numberOfBrackets; i++) {
        result = 0;
        if(hay[start] == brackets[i]){
            result = 1;
            break;
        }
    }

    if(result > 0){
        *br_id = (unsigned char*)my_malloc(sizeof(unsigned char));
        if(*br_id == NULL){
            return -3;
        }

        **br_id = (unsigned char)i;
        return start;
    }

    return -1;
}

long KA11(char* hay, long start, long length) { // čárka
    (void) length;

    if(hay[start] == ','){
        return start;
    }

    return -1;
}

long KA12(char* hay, long start, long length, unsigned char** fn_id) { // vestavěná funkce
    //hledá vestavěné funkce z předdefinovaného pole buildInFunctions
    //parametrem fn_id udává index funkce v tomto poli

    char result = 1;

    int i = 0;
    int j = 0;

    for(i = 0; i < numberOfBuiltInFunctions; i++) {
        int builtInFunctionLength = strlen(builtInFunctions[i]);
        result = builtInFunctionLength;

        for(j = 0; j < builtInFunctionLength; j++) {
            if(start + j > length){
                return -2;
            }

            if(hay[start + j] != builtInFunctions[i][j]){
                result = 0;
                break;
            }
        }

        if(result){
            break;
        }
    }

    if(result > 0){
        *fn_id = (unsigned char*)my_malloc(sizeof(unsigned char));
        if(*fn_id == NULL){
            return -3;
        }

        **fn_id = (unsigned char)i;
        return start + result - 1;
    } else {
        return -1;
    }
}

#endif //LEX_FINIT_STATE_MACHINES_C
