//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj

#ifndef LEXICALANALYSIS_TAC_STACKS_H
#define LEXICALANALYSIS_TAC_STACKS_H

#include "tac_memory_lists.h"

//struktura pro ukládání návratových adres z funkcí
typedef struct sTACStaclElement{
    TACListElement adress;
    struct sTACStaclElement* bottom;
} *TACStackElement;

//struktura zásobníku návratových adres z fcí
typedef struct {
    TACStackElement top;
    TACStackElement bottom;
} TACStack; //pro ukládání návratových adres funkcí

void initFunctionReturnStack(TACStack* S);
void pushReturnAdress(TACStack* S, TACListElement new);
TACListElement popReturnAdress(TACStack* S);
TACListElement readReturnAdressFromTop(TACStack* S);

//struktura pro uchovávání parametrů při volání funkcí (je využita i ke tvorbě lokálního scope)
typedef struct sTACParamStackElement {
    void* data;
    paramType type;
    struct sTACParamStackElement* before;
    struct sTACParamStackElement* next;
} *TACParamStackElement;

typedef struct {
    TACParamStackElement first;
    TACParamStackElement last;
    int elementsOnStack;
} TACParamStack;

void initTACParamStack(TACParamStack* S);
void pushParam(TACParamStack* S, void* val, paramType type);
TACParamStackElement popParam(TACParamStack* S);
TACParamStackElement readParam(TACParamStack* S, TACParamStack* offsets, int offset);
void printTACParamStack(TACParamStack* S);

#endif //LEXICALANALYSIS_TAC_STACKS_H

