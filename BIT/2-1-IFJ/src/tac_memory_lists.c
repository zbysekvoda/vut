//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj

#include "tac_memory_lists.h"

//insicializace TAC seznamu
void initTACList(TACList* L) {
    L->active = NULL;
    L->first = NULL;
    L->last = NULL;
}

//přidá novou instrukci na seznam TAC instrukcí
TACListElement addElement(TACList* L, TACInstruction instruction, void* operand1, void* operand2, void* result) {
    TACListElement newListElement = my_malloc(sizeof(struct sTACListElement));

    if(newListElement == NULL){
        return NULL;
    }

    TAC newTAC = my_malloc(sizeof(struct sTAC));

    if(newTAC == NULL){
        return NULL;
    }

    //naplní instrukci podle hodnot parametrů
    newTAC->instruction = instruction;
    newTAC->op1 = operand1;
    newTAC->op2 = operand2;
    newTAC->result = result;

    if(L->last == NULL){
        newListElement->prev = NULL;
    } else {
        newListElement->prev = L->last;
    }

    newListElement->data = newTAC;
    newListElement->next = NULL;

    if(L->first == NULL){
        L->first = newListElement;
    }

    if(L->last != NULL){
        L->last->next = newListElement;
    }

    L->last = newListElement;

    return newListElement;
}

//nastaví aktivitu seznamu na první prvek
void setActivityToFirst(TACList* L) {
    L->active = L->first;
}

//vytiskne informace o jedné instrukci v TAC
void printOneElement(TACList* L, TACListElement element) {
    if(element == L->first){
        printf("f");
    } else {
        printf(" ");
    }
    if(element == L->last){
        printf("l");
    } else {
        printf(" ");
    }

    printf("   ");
    printf(" | [%s;\t%p;\t%p;\t%p;)]", TACInstructionName[element->data->instruction], element->data->op1, element->data->op2, element->data->result);
}

//vytiskne celý seznam
void printTACList(TACList* L, int listNO) {
    int counter = 1;

    if(listNO == 0){
        //printf("-------------------------------------------------------Program stack-------------------------------------------------------------------------\n");
    } else if(listNO == 1){
        //printf("-------------------------------------------------------Function stack------------------------------------------------------------------------\n");
    }

    //printf("#\t| adress\t| flags | TAC\n");

    TACListElement h = L->first;

    while(h != NULL) {
        //printf("%d\t| ", counter);
        //printf("%p\t| ", h);
        printOneElement(L, h);
        //printf("\n");
        counter++;
        h = h->next;
    }
}
