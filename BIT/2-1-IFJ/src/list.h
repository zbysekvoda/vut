//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj

#ifndef LIST_H
#define LIST_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "parser.h"
#include "token.h"

#define FALSE 0
#define TRUE 1

extern int errflg;
extern int solved;

 
typedef struct tDLElem {                 /* prvek dvousměrně vázaného seznamu */ 
        SToken token_data;                                            /* užitečná data */
        struct tDLElem *lptr;          /* ukazatel na předchozí prvek seznamu */
        struct tDLElem *rptr;        /* ukazatel na následující prvek seznamu */
} *tDLElemPtr;

typedef struct {                                  /* dvousměrně vázaný seznam */
    tDLElemPtr First;                      /* ukazatel na první prvek seznamu */
    tDLElemPtr Act;                     /* ukazatel na aktuální prvek seznamu */
    tDLElemPtr Last;                    /* ukazatel na posledni prvek seznamu */
} tDLList;

                                             /* prototypy jednotlivých funkcí */
void DLInitList (tDLList *);
int DLDisposeList (tDLList *);
int DLInsertFirst (tDLList *, SToken*);
int DLInsertLast(tDLList *, SToken*);
void DLFirst (tDLList *);
void DLLast (tDLList *);
int DLCopyFirst (tDLList *, SToken*);
int DLCopyLast (tDLList *, SToken*);
void DLDeleteFirst (tDLList *);
void DLDeleteLast (tDLList *);
void DLPostDelete (tDLList *);
void DLPreDelete (tDLList *);
int DLPreInsert (tDLList *L, SToken *val);
int DLCopy (tDLList *, SToken*);
void DLSucc (tDLList *);
void DLPred (tDLList *);
int DLActive (tDLList *);
void printTokenInfo(SToken f);

#endif	
