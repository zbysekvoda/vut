//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj

#ifndef LEXICALANALYSIS_MEMORY_FUNCTIONS_C
#define LEXICALANALYSIS_MEMORY_FUNCTIONS_C

#include "memory_functions.h"

//inicializuje globální strukturu bin pro správu alokované paměti
void initBin() {
    bin.top = NULL;
}

//přidá do seznamu alokovaných míst nové s předanou adresou
void addGarbage(void* data){
    Garbage new = malloc(sizeof(struct sGarbage));
    if(new == NULL){
        return;
    }

    new->bottom = bin.top;
    new->data= data;

    bin.top = new;
}

//projde celý seznam alokovaných míst a vyčistí je (místo samotné i paměť držící informaci i něm)
void emptyGarbage(){
    Garbage del;

    int counter = 0;
    while(bin.top != NULL){
        del = bin.top;
        bin.top = bin.top->bottom;

        free(del->data);
        free(del);
    }
}

//vytiskne aktuálně alokovaná místa v paměti
void printBin(){
    Garbage garbage = bin.top;
    int counter = 0;

    //printf("-------------------------------------------------------Garbage dump--------------------------------------------------------------------------\n");

    while(garbage != NULL){
        //printf("%d:\tGarbage on adress: %p \n", ++counter, garbage);
        garbage = garbage->bottom;
    }

    //printf("-------------------------------------------------------Garbage dump printed------------------------------------------------------------------\n");
}

//funkce se stejným rozhraním jako malloc
//mimo vytvoření místa v paměti ještě přidá informaci o vytvoření na seznam
void* my_malloc(size_t size) {
    void* new = malloc(size);

    if(new == NULL){
        //TODO error99
        return NULL;
    }

    addGarbage(new);

    return new;
}

//pomocné funkce alokování čísla typu int
int* mallocInt(int i) {
    int* new = my_malloc(sizeof(int));

    if(new != NULL){
        *new = i;
    }

    return new;
}

//pomocné funkce alokování čísla typu double
double* mallocDouble(double d) {
    double* new = my_malloc(sizeof(double));

    if(new != NULL){
        *new = d;
    }

    return new;
}

//pomocné funkce alokování řetězce
char** mallocString(char* s) {
    int len = strlen(s);

    char** new = my_malloc(sizeof(char*));
    if(new == NULL) return NULL;

    char* newConent = my_malloc((len + 1) * sizeof(char));
    if(newConent == NULL) return NULL;

    memcpy(newConent, s, len);
    newConent[len] = '\0';
    *new = newConent;

    return new;
}

#endif //LEXICALANALYSIS_MEMORY_FUNCTIONS_C
