//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj

#ifndef EMBEDDED_FUNCTIONS_C
#define EMBEDDED_FUNCTIONS_C

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include "embedded_functions.h"


//VESTAVENE FUNKCE PRO PRACI S RETEZCI


//funkcia pre vynatie substringu zo stringu
char * substr(char *s, int i, int n) {     // i - zacatek podretezce, n - delka podretezce
    char * sub_str;
    sub_str = malloc(sizeof(char) * (n+1));
    if (sub_str == NULL) {
        return NULL;                // Memory allocation error!
    }

    for (int j = 0; j < n; j++) {
        sub_str[j] = s[i + j];
    }
    sub_str[n] = '\0';

    return sub_str;             // pri pouziti teto funkce v jine funkci je potreba na zaver zavolat free()
}
//funkcie pre zistenie dlzky retazca
int length(char *s) {
    return strlen(s);
}
//funkcia pre porovnanie 2 retazcov
int compare(char *s1, char *s2) {
    int strcmp_result = strcmp(s1, s2);

    if (strcmp_result == 0) {          // retezce jsou stejne
        return 0;
    }

    if (strcmp_result < 0) {           // retezec s2 je vetsi nez s1
        return -1;
    }

    if (strcmp_result > 0) {           // retezec s1 je vetsi nez s2
        return 1;
    }

}

// VESTAVENE FUNKCE PRO VYPIS LITERALU A VYPIS TERMU
//funkcia pre nacitanie  celociselnej hodnoty zo vstupu
unsigned int readInt() {
    char *input = readLine();
    unsigned long int number;
    char *konec;

    for(int i = 0; i < strlen(input); i++){
        if(!isdigit(input[i])){
            readError = 1;
            return 7;
        }
    }

    // Prevod string -> unsigned long int
    number = strtoul(input, &konec, 10); // 10 - desitkova soustava

    // Zkontrolujeme, zda hodnota neni mimo rozsah
    if (konec[0] == '\0' && number >= 0 && number <= UINT_MAX) {
                // Prevod unsigned long int -> unsigned int
        return (unsigned int)number;
    } else {
        //fprintf(stderr, "readInt(): Integer too big!\n");
        readError = 7;
        return 7;
    }
}
//funckia pre nacitanie desatinnej hodnoty zo vstupu
double readDouble() {
    char *input = readLine();
    double number;
    char *konec;


    if(KA5(input, 0, strlen(input), NULL, 0) < 0 && KA4(input, 0, strlen(input), NULL, 0) < 0){
        readError = 1;
        return 7;
    }

    // Prevod string -> double
    number = strtod(input, &konec);
    if (konec[0] != '\0') {
        //fprintf(stderr, "readDouble(): Invalid number format!\n");
        readError = 1;
        return 7;
    }

    return number;
}
//gunkcia pre nacitanie retazca zo vstupu
char * readString() {
    return readLine();
}
//pomocna funkcia pre nacitavanie zo vstupu
char * readLine() {
    char *output;
    int character;
    size_t length = 0;
    size_t space = LINE_SIZE_INIT;
    output = malloc(sizeof(char) * space);

    if(!output) {
        return NULL;                    // Memory allocation error!
    }

    while (((character = fgetc(stdin)) != EOF) && character != '\n'){
        output[length++] = character;
        if(length >= space){
            space += LINE_SIZE_EXTEND;
            output = realloc(output, sizeof(char) * space);
            if (!output) {
                return NULL;            // Memory allocation error!
            }
        }
    }
    output[length++]='\0';
    return realloc(output, sizeof(char) * space);
}
//funkcia pre vypis na vystup
void print(char *s) {
    printf("%s", s);
}



/*
//TESTY PRO VESTAVENE FUNKCE
//void main() {
void ef_tests() {

    // Test pro sort() - Quick-Sort
    printf("Test: Quicksort\n");
    char arr[] = "asdioqwbfcyzywziuz";
    char *sorted;
    printf("%s\n", arr);
    sorted = sort(arr);
    printf("%s\n", sorted);


    // Test pro find() - Boyer-Moore
    int pozice;
    char str[] = "ahojkamojaksemas";
    char search[] = "kamo";

    printf("s=\"%s\"\n", str);

    char *result = substr(str, 4, 4);
    printf("substr(s, 4, 4): %s\n", result);

    int delka = length(str);
    printf("length(s): %d\n", delka);

    printf("search=\"%s\"\n", search);

    pozice = find(str, search);
    printf("find(s, search): %d\n", pozice);

    // Test funkci read* a print_*

    printf("Zadej string: ");
    char *s = readString();
    print_s(s);
    printf("\n");

    printf("Zadej int (nezaporny): ");
    unsigned int i = readInt();
    print_i(i);
    printf("\n");

    printf("Zadej double: ");
    double d = readDouble();
    print_d(d);
    printf("\n");

}
*/

#endif //EMBEDDED_FUNCTIONS_C
