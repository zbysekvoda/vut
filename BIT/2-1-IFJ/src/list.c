//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj

#ifndef LIST_C
#define LIST_C

#include "list.h"
#include "token.h"
#include "parser.h"
#include "expression.h"

void DLInitList(tDLList* L) {
/*
** Provede inicializaci seznamu L před jeho prvním použitím (tzn. žádná
** z následujících funkcí nebude volána nad neinicializovaným seznamem).
** Tato inicializace se nikdy nebude provádět nad již inicializovaným
** seznamem, a proto tuto možnost neošetřujte. Vždy předpokládejte,
** že neinicializované proměnné mají nedefinovanou hodnotu.
**/

    L->First = NULL;
    L->Act = NULL;
    L->Last = NULL;
}

int DLDisposeList(tDLList* L) {
/*
** Zruší všechny prvky seznamu L a uvede seznam do stavu, v jakém
** se nacházel po inicializaci. Rušené prvky seznamu budou korektně
** uvolněny voláním operace free. 
**/


    tDLElemPtr pom_p;

    while(L->First != NULL) {
        pom_p = L->First->rptr;
        free(L->First);
        /*if(L->First != NULL)
            return ERR_INTER;*/

        L->First = pom_p;
    }

    L->Act = NULL;
    L->Last = NULL;

    return ERR_OK;
}

int DLInsertFirst(tDLList* L, SToken* val) {
/*
** Vloží nový prvek na začátek seznamu L.
** V případě, že není dostatek paměti pro nový prvek při operaci malloc,
** volá funkci DLError().
**/

    tDLElemPtr novy_prvy = malloc(sizeof(struct tDLElem));
    if(novy_prvy == NULL){
        return ERR_INTER;
    }

    novy_prvy->token_data.type = val->type;
    novy_prvy->token_data.data = val->data;

    if(L->First != NULL){
        L->First->lptr = novy_prvy;
        novy_prvy->lptr = NULL;
        novy_prvy->rptr = L->First;
        L->First = novy_prvy;
    } else {
        L->First = novy_prvy;
        L->Last = novy_prvy;
        novy_prvy->lptr = NULL;
        novy_prvy->rptr = NULL;
    }

    return ERR_OK;
}

int DLInsertLast(tDLList* L, SToken* val) {
/*
** Vloží nový prvek na konec seznamu L (symetrická operace k DLInsertFirst).
** V případě, že není dostatek paměti pro nový prvek při operaci malloc,
** volá funkci DLError().
**/

    tDLElemPtr novy_posl = malloc(sizeof(struct tDLElem));
    if(novy_posl == NULL){
        return ERR_INTER;
    }

    novy_posl->token_data.type = val->type;
    novy_posl->token_data.data = val->data;

    if(L->First != NULL){
        L->Last->rptr = novy_posl;
        novy_posl->rptr = NULL;
        novy_posl->lptr = L->Last;
        L->Last = novy_posl;
    } else {
        L->First = novy_posl;
        L->Last = novy_posl;
        novy_posl->lptr = NULL;
        novy_posl->rptr = NULL;
    }

    return ERR_OK;

}

void DLFirst(tDLList* L) {
/*
** Nastaví aktivitu na první prvek seznamu L.
** Funkci implementujte jako jediný příkaz (nepočítáme-li return),
** aniž byste testovali, zda je seznam L prázdný.
**/

    L->Act = L->First;
}

void DLLast(tDLList* L) {
/*
** Nastaví aktivitu na poslední prvek seznamu L.
** Funkci implementujte jako jediný příkaz (nepočítáme-li return),
** aniž byste testovali, zda je seznam L prázdný.
**/

    L->Act = L->Last;
}

int DLCopyFirst(tDLList* L, SToken* val) {
/*
** Prostřednictvím parametru val vrátí hodnotu prvního prvku seznamu L.
** Pokud je seznam L prázdný, volá funkci DLError().
**/

    if(L->First == NULL){
        return ERR_INTER;
    }

    val->type = L->First->token_data.type;
    val->data = L->First->token_data.data;

    return ERR_OK;

}

int DLCopyLast(tDLList* L, SToken* val) {
/*
** Prostřednictvím parametru val vrátí hodnotu posledního prvku seznamu L.
** Pokud je seznam L prázdný, volá funkci DLError().
**/

    if(L->First == NULL){
        return ERR_INTER;
    }

    val->type = L->Last->token_data.type;
    val->data = L->Last->token_data.data;

    return ERR_OK;

}

void DLDeleteFirst(tDLList* L) {
/*
** Zruší první prvek seznamu L. Pokud byl první prvek aktivní, aktivita 
** se ztrácí. Pokud byl seznam L prázdný, nic se neděje.
**/

    tDLElemPtr delete_p;

    if(L->First != NULL){
        delete_p = L->First;

        if(L->First == L->Last){
            L->First = NULL;
            L->Last = NULL;
            L->Act = NULL;
        } else {
            if(L->First == L->Act)
                L->Act = NULL;

            L->First = delete_p->rptr;
            L->First->lptr = NULL;
        }

        free(delete_p);

    }
}

void DLDeleteLast(tDLList* L) {
/*
** Zruší poslední prvek seznamu L. Pokud byl poslední prvek aktivní,
** aktivita seznamu se ztrácí. Pokud byl seznam L prázdný, nic se neděje.
**/

    tDLElemPtr delete_l;

    if(L->Last != NULL){
        delete_l = L->Last;

        if(L->First == L->Last){
            L->First = NULL;
            L->Last = NULL;
            L->Act = NULL;
        } else {
            if(L->Last == L->Act)
                L->Act = NULL;

            L->Last = delete_l->lptr;
            L->Last->rptr = NULL;
        }

        free(delete_l);

    }
}

void DLPostDelete(tDLList* L) {
/*
** Zruší prvek seznamu L za aktivním prvkem.
** Pokud je seznam L neaktivní nebo pokud je aktivní prvek
** posledním prvkem seznamu, nic se neděje.
**/

    if((L->Act == NULL) || (L->Act == L->Last))
        return;

    tDLElemPtr pom_p = L->Act->rptr;

    L->Act->rptr = pom_p->rptr;

    if(pom_p == L->Last)
        L->Last = L->Act;

    else
        pom_p->rptr->lptr = L->Act;

    free(pom_p);

}

void DLPreDelete(tDLList* L) {
/*
** Zruší prvek před aktivním prvkem seznamu L .
** Pokud je seznam L neaktivní nebo pokud je aktivní prvek
** prvním prvkem seznamu, nic se neděje.
**/

    if((L->Act == NULL) || (L->Act == L->First)) //menene if
        return;

    tDLElemPtr pom_p = L->Act->lptr;

    L->Act->lptr = pom_p->lptr;

    if(pom_p == L->First)
        L->First = L->Act;

    else
        pom_p->lptr->rptr = L->Act;

    free(pom_p);

}


int DLCopy(tDLList* L, SToken* val) {
/*
** Prostřednictvím parametru val vrátí hodnotu aktivního prvku seznamu L.
** Pokud seznam L není aktivní, volá funkci DLError ().
**/

    if(L->Act == NULL){
        return ERR_INTER;
    }

    val->data = L->Act->token_data.data;
    val->type = L->Act->token_data.type;

    return ERR_OK;

}


void DLSucc(tDLList* L) {
/*
** Posune aktivitu na následující prvek seznamu L.
** Není-li seznam aktivní, nedělá nic.
** Všimněte si, že při aktivitě na posledním prvku se seznam stane neaktivním.
**/

    if(L->Act != NULL){
        if(L->Act == L->Last)
            L->Act = NULL;

        else
            L->Act = L->Act->rptr;

    }
}


int DLPreInsert(tDLList* L, SToken* val) {
/*
** Vloží prvek před aktivní prvek seznamu L.
** Pokud nebyl seznam L aktivní, nic se neděje.
** V případě, že není dostatek paměti pro nový prvek při operaci malloc,
** volá funkci DLError().
**/

    if(L->Act == NULL)
        return ERR_INTER;


    tDLElemPtr novy_p = malloc(sizeof(struct tDLElem));
    if(novy_p == NULL){
        return ERR_INTER;
    }

    novy_p->token_data.data = val->data;
    novy_p->token_data.type = val->type;
    novy_p->lptr = L->Act->lptr;
    novy_p->rptr = L->Act;
    L->Act->lptr = novy_p;

    if(L->Act == L->First)
        L->First = novy_p;

    return ERR_OK;
}


void DLPred(tDLList* L) {
/*
** Posune aktivitu na předchozí prvek seznamu L.
** Není-li seznam aktivní, nedělá nic.
** Všimněte si, že při aktivitě na prvním prvku se seznam stane neaktivním.
**/

    if(L->Act != NULL){
        if(L->Act == L->First)
            L->Act = NULL;

        else
            L->Act = L->Act->lptr;

    }
}

int DLActive(tDLList* L) {
/*
** Je-li seznam L aktivní, vrací nenulovou hodnotu, jinak vrací 0.
** Funkci je vhodné implementovat jedním příkazem return.
**/

    return (L->Act == NULL) ? 0 : 1;
}

void printTokenType(int i) {
    //printf("%s ", tokenTypes[i]);
}

void printTokenData(SToken f){
    //printf("[");
    switch (f.type){
        case simple_identifier:
            //printf("%s", (char*)f.data);
            break;
        case fully_qualified_identifier:
            //printf("%s", (char*)f.data);
            break;
        case num_i:
            //printf("%d", *(int *)f.data);
            break;
        case num_d:
            //printf("%f", *(double *)f.data);
            break;
        case comment:
            break;
        case semicolon:
            break;
        case bracket_left:
            //printf("(");
            break;
        case bracket_right:
            //printf(")");
            break;
        case bracket_open:
            //printf("{");
            break;
        case bracket_close:
            //printf("}");
            break;
        case comma:
            break;
        case string:
            //printf("%s", (char*)f.data);
            break;
        case builtInFunction:
            //printf("%s", builtInFunctions[*(unsigned char*)f.data]);
            break;
        case operator:
        case operator_equal:
        case operator_plus:
        case operator_minus:
        case operator_multiply:
        case operator_divide:
        case operator_less:
        case operator_greater:
        case operator_less_equal:
        case operator_greater_equal:
        case operator_relation:
        case operator_not_equal:
            //printf("%s", operators[*(unsigned char*)f.data]);
            break;
        case keyword:
        case keyword_data_type:
        case keyword_normal:
        case keyword_false:
        case keyword_true:
            //printf("%s", keywords[*(unsigned char*)f.data]);
            break;
        case undefined:
            break;
        case error:
            //printf("Chyba 1");
            break;
        case error99:
            //printf("Chyba 99");
            break;
        case end:
            break;
        case E:
            //printf("E - ");
            if(((EData)f.data)->type == Int){
                //printf("%d", *(int*)(((EData)f.data)->value));
            }
            else if(((EData)f.data)->type == Double){
                //printf("%g", *(double*)(((EData)f.data)->value));
            }
            else if(((EData)f.data)->type == String){
                //printf("%s", *(char**)(((EData)f.data)->value));
            }
            break;
        case DOLAR:
            //printf("$");
            break;
        case LLL:
            //printf("LLL");

        default:
            break;
    }

    //printf("]");
}

void printTokenInfo(SToken f){
    printTokenType((int)f.type);
    printTokenData(f);
}

int printList(tDLList* L) {
    SToken f;

    DLLast(L);

    do {
        f = L->Act->token_data;

        //printf("o Token type: \t ");
        printTokenInfo(f);
        //printf("\n");

        DLPred(L);
    } while(f.type != DOLAR);
    return 0;
}

#endif //LIST_C
