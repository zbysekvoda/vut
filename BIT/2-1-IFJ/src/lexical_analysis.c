//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj

#ifndef LEXICAL_ANALYSIS_C
#define LEXICAL_ANALYSIS_C

#include "token.h"
#include "lexical_analysis.h"

#include "memory_functions.c"
#include "lex_finit_state_machines.c"

/*
 * TODO isspace() -jaké to podporuje?
 * TODO nereprezentovat jednoduché tokeny jenom znakově?
 * TODO feature: detekce pomocí regulárních výrazů?
 * TODO vyřešit chybové stavy
 * TODO ošetřit rozsahy KA4 a KA5 - asi pomocí errno
*/

//slouží k chybového identifikaci tokenu podle návratových kódů KAx
TokenType getTokenTypeFromKAResult(long result) {
    if(result == -2){
        return error;
    }

    if(result == -3){
        return error99;
    }

    return error;
}

//přečte a vrátí jeden token ze vstupu
SToken getToken() {
    //Prochází všechny konečné automaty v pořadí KA11, KA10, KA8, KA7, KA9, KA5, KA4, KA6, KA3, KA12, KA2, KA1
    //pořadí je nutné dodržet, aby nebylo klíčové slovo vyhodnocené jako identifikátor, nebyla část desetinného literálu vyhodnocena jako celočíselný apod.
    //po každém testu KA je zkontrolována návratová hodnota, zda-li nenastala nějaká chyba
    //u KA, které identifikují lexém z nějakého výčku (například lexémy) je podle hodnoty předané parametrem ještě určen konkrétní typ tokenu -> KA akceptuje operator, podle hodnoty určíme např. operator_plus
    //na konci dojde k posunutí počitadla znaků o počet přečtených znaků

    long inputLength = strlen(input);
    long lastStop = lastStart; //index posledního znaku potenciálního tokenu
    static unsigned long lastLineNumber = 0;

    lastLineNumber = lineNumber;

    TokenType tokenType = undefined;
    void* tokenData = NULL;
    char c;

    //zbaví se přebytečných bílých znaků na začátku testovaného vstupu
    do {
        c = input[lastStart];
        lastStart++;
    } while(lastStart <= inputLength && isspace(c));
    lastStart--;

    /* START ANALÝZY */
    if(lastStop >= inputLength){
        tokenType = end;
    } else {
        long result = 0;
        char* resultString = NULL;
        int* value_int = NULL;
        double* value_double = NULL;
        unsigned char* p_id = NULL;

        if((result = KA11(input, lastStart, inputLength)) >= 0){
            tokenType = comma;
        } else if(result <= -2){
            tokenType = getTokenTypeFromKAResult(result);
        } else if((result = KA10(input, lastStart, inputLength, &p_id)) >= 0){
            if(*p_id == 0){
                tokenType = bracket_left;
            } else if(*p_id == 1) {
                tokenType = bracket_right;
            } else if(*p_id == 2) {
                tokenType = bracket_open;
            } else if(*p_id == 3){
                tokenType = bracket_close;
            }
        } else if(result <= -2){
            tokenType = getTokenTypeFromKAResult(result);
        } else if((result = KA8(input, lastStart, inputLength)) >= 0){
            tokenType = semicolon;
        } else if(result <= -2){
            tokenType = getTokenTypeFromKAResult(result);
        } else if((result = KA7(input, lastStart, inputLength)) >= 0){
            tokenType = comment;
        } else if(result <= -2){
            tokenType = getTokenTypeFromKAResult(result);
        } else if((result = KA9(input, lastStart, inputLength, &p_id, 1)) >= 0){
            if(*p_id == 0){
                tokenType = operator_relation;
            } else if(*p_id == 1) {
                tokenType = operator_plus;
            } else if(*p_id == 2) {
                tokenType = operator_minus;
            } else if(*p_id == 3) {
                tokenType = operator_multiply;
            } else if(*p_id == 4) {
                tokenType = operator_divide;
            } else if(*p_id == 5) {
                tokenType = operator_less;
            } else if(*p_id == 6) {
                tokenType = operator_greater;
            } else if(*p_id == 7) {
                tokenType = operator_less_equal;
            } else if(*p_id == 8) {
                tokenType = operator_greater_equal;
            } else if(*p_id == 9) {
                tokenType = operator_equal;
            } else if(*p_id == 10) {
                tokenType = operator_not_equal;
            }
            tokenData = (void*)p_id;
        } else if(result <= -2){
            tokenType = getTokenTypeFromKAResult(result);
        } else if((result = KA5(input, lastStart, inputLength, &value_double, 1)) >= 0){
            tokenType = num_d;
            tokenData = (void*)value_double;
        } else if(result <= -2){
            tokenType = getTokenTypeFromKAResult(result);
        } else if((result = KA4(input, lastStart, inputLength, &value_int, 1)) >= 0){
            tokenType = num_i;
            tokenData = (void*)value_int;
        } else if(result <= -2){
            tokenType = getTokenTypeFromKAResult(result);
        } else if((result = KA6(input, lastStart, inputLength, &resultString)) >= 0){
            tokenType = string;
            tokenData = (void*)resultString;
        } else if(result <= -2){
            tokenType = getTokenTypeFromKAResult(result);
        } else if((result = KA3(input, lastStart, inputLength, &p_id)) >= 0){
            if(*p_id <= 4){
                tokenType = keyword_data_type;
            } else if(*p_id == 11) {
                tokenType = keyword_false;
            } else if(*p_id == 15) {
                tokenType = keyword_true;
            } else {
                tokenType = keyword_normal;
            }
            tokenData = (void*)p_id;
        } else if(result <= -2){
            tokenType = getTokenTypeFromKAResult(result);
        } else if((result = KA12(input, lastStart, inputLength, &p_id)) >= 0){
            tokenType = builtInFunction;
            tokenData = p_id;
        } else if(result <= -2){
            tokenType = getTokenTypeFromKAResult(result);
        } else if((result = KA2(input, lastStart, inputLength, &resultString)) >= 0){
            tokenType = fully_qualified_identifier;
            tokenData = (void*)resultString;
        } else if(result <= -2){
            tokenType = getTokenTypeFromKAResult(result);
        } else if((result = KA1(input, lastStart, inputLength, 0, &resultString)) >= 0){
            tokenType = simple_identifier;
            tokenData = (void*)resultString;
        } else if(result <= -2){
            tokenType = getTokenTypeFromKAResult(result);
        } else {
            tokenType = error; //neznámý lexém
        }

        if(tokenType != error && tokenType != error99 && tokenType != undefined){
            lastStop = result;
        }
    }
    /* KONEC ANALÝZY */

    //zbaví se zbytečných bílých znaků na konci testovaného vstupu
    lastStop++;
    do {
        c = input[lastStop];
        lastStop++;
        if(c == '\n'){
            lineNumber++;
        }
    } while(lastStop <= inputLength && isspace(c));
    lastStop--;

    if(tokenType == end){
        lastStart = inputLength;
    } else {
        lastStart = lastStop;
    }

    //vytvoří token k vrácení
    SToken token = {tokenType, tokenData, lastLineNumber};
    return token;
}

void getToken_reset() {
    lastStart = 0;
}

#endif //LEXICAL_ANALYSIS_C
