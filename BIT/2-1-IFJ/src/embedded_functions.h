//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj

#ifndef EMBEDDED_FUNCTIONS_H
#define EMBEDDED_FUNCTIONS_H

#define LINE_SIZE_INIT 32
#define LINE_SIZE_EXTEND 16

#include <ctype.h>

char * substr(char *s, int i, int n);
int length(char *s);
int compare(char *s1, char *s2);

unsigned int readInt();
double readDouble();
char * readString();
char * readLine(); // Pomocna funkce pro cteni radku

void print(char *s);

void ef_tests(); // Testy vestavenych funkci

#endif
