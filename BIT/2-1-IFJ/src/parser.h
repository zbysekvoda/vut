//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj

#ifndef PARSER_H
#define PARSER_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include "token.h"
#include "lexical_analysis.h"
#include "ial.h"
//#include "ial.c"
#include "list.h"
#include "param_stack.c"
#include "embedded_functions.h"
#include "memory_functions.h"
#include "conditional_adress_stack.h"

//#include "Expression.h"

SToken getToken();

#define class_type char*

typedef enum 
{
    ERR_OK,
    ERR_LEX,
    ERR_SYN,
    ERR_SEM_UNDEF,
    ERR_SEM_TYPE,
    ERR_SEM = 6,
    ERR_READ,
    ERR_VAR,
    ERR_ZERO,
    ERR_OTHER,
    ERR_INTER = 99

} ERROR_TYPE;

#define LL 100
#define RR 101
#define EQ 102
#define ERR 103

int TokenTypeEqual(SToken s1, TokenType s2);
SToken readNextToken();
SToken readToken();
SToken getRealToken();
int breakIdentifier(char * FQID, char **first_part, char **second_part);
int StateID();
int StateIf();
int StateWhile();
int StateReturn();
int StateElse();
int StateCore();
int StateDeclaration();
int Start();
int StateStatic();
int StateCoreOne();
int StateDeclarationFunction();
int DecList();
int StateFunctionCall(char *, char *);
int StateFunctionCallBuildIn(char *name_func, char *class_f);
int StateFunction();
int StateExpression();
int StateClass();
int StateCoreFunc();
int StateDeclarationID();
int Parse();
SToken getTokenList();
int StateDeclarationFunctionOK();
int StateReturnVoid();
int CheckMain();

#endif 
