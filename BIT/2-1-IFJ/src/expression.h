//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj

#ifndef EXPRESSION_H
#define EXPRESSION_H

typedef struct sEData{
    void* value;
    paramType type;
} *EData;

typedef struct sTokenElement {
    SToken token;
    struct sTokenElement* bottom;
} * TokenStackElement;

typedef struct {
    TokenStackElement top;
} TokenStack;


int StateExpAnalysis();
SToken popFromTop(TokenStack* s);
void printTokenStack(TokenStack* s); //tiskne znaky na zásobníku po první $

TokenStack tokenStack;
#endif
