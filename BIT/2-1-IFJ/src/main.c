//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj

#include "token.h"
#include "memory_functions.h"
#include "conditional_adress_stack.h"
#include "embedded_functions.h"
#include "lex_finit_state_machines.h"
#include "lexical_analysis.h"
#include "list.h"
#include "main.h"
#include "memory_functions.h"
#include "param_stack.h"
#include "ial.h"
#include "expression.h"
#include "parser.h"
#include "tac.h"
#include "tac_memory_lists.h"
#include "tac_memory_stacks.h"
#include "tac_program.h"

#include "embedded_functions.c"
#include "conditional_adress_stack.c"
#include "memory_functions.c"
#include "tac_memory_lists.c"
#include "tac_memory_stacks.c"
#include "expression.c"
#include "parser.c"
#include "tac_program.c"

void printVarAdress() {
    //printf("\tIntegers\t\tDoubles\t\t\tStrings\n");
    //printf("1\t%p\t\t%p\t\t%p\n", reg_i1, reg_d1, reg_s1);
    //printf("2\t%p\t\t%p\t\t%p\n", reg_i2, reg_d2, reg_s2);
    //printf("Ret\t%p\t\t%p\t\t%p\n", ret_i, ret_d, ret_s);

    //printf("Types: \n");
    //printf("typeInt: %p\n", typeInt);
    //printf("typeDouble: %p\n", typeDouble);
    //printf("typeString: %p\n", typeString);
}

void printVarInfo() {
    //printf("--------------------------------------------------------Register dumb------------------------------------------------------------------------\n");

    //printf("\tIntegers\t\tDoubles\t\t\tStrings\n");
    //printf("1\t%d\t\t\t%g\t\t\t%s\n", *reg_i1, *reg_d1, *reg_s1);
    //printf("2\t%d\t\t\t%g\t\t\t%s\n", *reg_i2, *reg_d2, *reg_s2);
    //printf("Ret\t%d\t\t\t%g\t\t\t%s\n", *ret_i, *ret_d, *ret_s);
    //printVarAdress();
}

int GiveReturnValue()
{
    //vrátí buď hodnotu první chyby, nebo 0
    //vhyba může vzejít z parse(), nebo interpretProgram()

    int ret = Parse(); //parser
    //printf("Po parse: %d\n", ret);

    stackDelete_param(&ParamStack);

    int pom = DLDisposeList(&TokenList);
    if(pom == ERR_INTER){
        printf("Disp");
        return ERR_INTER;
    }

    if(ret != ERR_OK){
        return ret;
    }

    // INTERPRET RUN 
    //printf("=============================================================================================================================================\n");
    //printVarAdress();

    //printTACList(&functionDefinitions, 1);
    //printTACList(&program, 0);

    setActivityToFirst(&program);
    ret = interpretProgram(&program, &functionDefinitions, &functionReturnAdressesStack, &paramStack);

    //printTACParamStack(&paramStack);
    //printTACStack(&functionReturnAdressesStack);

    //printVarInfo();

    //printBin();
    //printf("=============================================================================================================================================\n");

    return ret;
}

int main(int argc, char** argv){

    if(argc < 2){
        return ERR_INTER;
    }

    //načte soubor ze vstupu
    FILE* fp = fopen(argv[1], "r");

    if(fp == NULL){
        return ERR_INTER;
    }

    fseek(fp, 0, SEEK_END);
    int inputLength = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    input = my_malloc((inputLength + 2) * sizeof(char));

    char red_c;
    int pos_counter = 0;

    //překopíruje obsah vstupního souboru do pomocného pole pro lexikální analýzu
    while((red_c = getc(fp)) != EOF){
        input[pos_counter++] = red_c;
    }
    input[pos_counter] = '\n';
    input[pos_counter+1] = '\0';

    fclose(fp);

   // printf("%d\n%s\n", pos_counter, input);

    /* INTERPRET DATA STRUCTURES */
    initTACList(&program);
    initTACList(&functionDefinitions);
    initFunctionReturnStack(&functionReturnAdressesStack);
    initTACParamStack(&paramStack);
    initTACParamStack(&paramOffsetStack);

    typeInt = (paramType*)mallocInt((int)Int);
    typeDouble = (paramType*)mallocInt((int)Double);
    typeString = (paramType*)mallocInt((int)String);

    reg_i1 = mallocInt(0);
    reg_i2 = mallocInt(0);
    reg_d1 = mallocDouble(0);
    reg_d2 = mallocDouble(0);
    reg_s1 = mallocString("original1");
    reg_s2 = mallocString("original2");
    ret_i = mallocInt(0);
    ret_d = mallocDouble(0);
    ret_s = mallocString("original3");
    /* END OF INTERPRET DATA STRUCTURES */

    T = hashTab_init(2000);
    if(T == NULL)
        return ERR_INTER;
   // printf("hashtabulka ok\n");

    int dec = BuiltInFuncDeclaration();
    if(dec != 0)
    {
      //  printf("chyba alokacie vstavanych funkcii\n");
        return ERR_INTER;
    }

    T_loc = hashTab_init_local(2000);
    if(T_loc == NULL)
        return ERR_INTER;
    //printf("local hash tabulka ok\n");

    DLInitList(&TokenList);
    //printf("tokenlist ok\n");

    stackInit_param(&ParamStack);

    int q = GiveReturnValue();

    emptyGarbage(); //vyčistí garbage collector

    hashTab_free(T);
    hashTab_free_local(T_loc);

    //vrátí pařičný kód chyby a vytiskne hlášku na chybový výstup
    switch(q) 
    {

        case ERR_OK:
            //printf("vsetko ok\n");
            return 0;

        case ERR_LEX:
            fprintf(stderr, "Lexikalna chyba!\n");
            return 1;

        case ERR_SYN:
            fprintf(stderr, "Syntakticka chyba!\n");
            return 2;

        case ERR_SEM_UNDEF:
            fprintf(stderr, "Semanticka chyba! Nedefinovana premenna alebo funkcia!\n");
            return 3;

        case ERR_SEM_TYPE:
            fprintf(stderr, "Semanticka chyba! Typova nekompabilita alebo nespravny pocet parametrov funkcii!\n");
            return 4;

        case ERR_SEM:
            fprintf(stderr, "Semanticka chyba!\n");
            return 6;

        case ERR_READ:
            fprintf(stderr, "Behova chyba pri nacitani ciselnej hodnoty zo vstupu!\n");
            return 7;

        case ERR_VAR:
            fprintf(stderr, "Citanie z neinicializovanej premennej!\n");
            return 8;

        case ERR_ZERO:
            fprintf(stderr, "Delenie nulou!\n");
            return 9;

        case ERR_OTHER:
            fprintf(stderr, "Behova chyba!\n");
            return 10;

        case ERR_INTER:
            fprintf(stderr, "Chyba neovplyvnena interpretom!\n");
            return 99;
    }
}
