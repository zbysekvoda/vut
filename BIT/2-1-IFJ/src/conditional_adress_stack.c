//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj

#ifndef LEXICALANALYSIS_CONDITIONAL_ADRESS_STACK_C
#define LEXICALANALYSIS_CONDITIONAL_ADRESS_STACK_C

#include "conditional_adress_stack.h"

void initConditionalAdressStack(ConditionalAdressStack* s) {
    s->top = NULL;
}

AdressStruct* createNewConditionalOnStack(ConditionalAdressStack* s) { //přidá trojici adres na stack
    AdressStruct* new = my_malloc(sizeof(struct sAdressStruct));

    new->if_addr = NULL;
    new->else_addr = NULL;
    new->end_addr = NULL;

    new->bottom = s->top;
    s->top = new;

    return new;
}

AdressStruct* readOffsetFromAddrStack(ConditionalAdressStack* s, int offset) {
    AdressStruct* pom = s->top;

    for(int i = 0; i < offset; i++) {
        if(pom != NULL){
            pom = pom->bottom;
        } else {
            //printf("NEDOSTATEK CONDITIONAL ADRES NA STACKU\n");
        }
    }

    return pom;
}

void popAdressStruct(ConditionalAdressStack* s){ //vyjme trojici z vrcholu stacku
    AdressStruct* pom = s->top;

    if(pom == NULL){
        //printf("NA CONDITIONAL STACKU NENI TROJICE K VYJMUTI!!!\n");
    }

    s->top = s->top->bottom;
}

#endif //LEXICALANALYSIS_CONDITIONAL_ADRESS_STACK_C
