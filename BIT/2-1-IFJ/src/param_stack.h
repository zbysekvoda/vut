//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj

#ifndef STACK_PARAM_H
#define STACK_PARAM_H

#include <stdio.h>
#include "ial.h"
#include "parser.h"

#define PARAM_STACK_SIZE 200


/* 
 * Hodnota MAX_STACK udává skutečnou velikost statického pole pro uložení
 * hodnot zásobníku. Při implementaci operací nad ADT zásobník však
 * předpokládejte, že velikost tohoto pole je pouze STACK_SIZE. Umožní to
 * jednoduše měnit velikost zásobníku v průběhu testování. Při implementaci
 * tedy hodnotu MAX_STACK vůbec nepoužívejte. 
 */

extern int solved;                      /* Indikuje, zda byla operace řešena. */
extern int err_flag;                   /* Indikuje, zda operace volala chybu. */

                                        /* Chybové kódy pro funkci stackError */
#define MAX_SERR    3                                   /* počet možných chyb */
#define SERR_INIT   1                                  /* chyba při stackInit */
#define SERR_PUSH   2                                  /* chyba při stackPush */
#define SERR_TOP    3                                   /* chyba při stackTop */

                             /* ADT zásobník implementovaný ve statickém poli */

typedef struct {                            /* pole pro uložení hodnot */
	int top; 
	FArg arr[PARAM_STACK_SIZE];                                 /* index prvku na vrcholu zásobníku */
} tStack;

                                  /* Hlavičky funkcí pro práci se zásobníkem. */
int stackInit_param ( tStack* s );
int stackEmpty_param ( const tStack* s );
int stackFull_param ( const tStack* s );
int stackTop_param ( const tStack* s, FArg* c );
void stackPop_param ( tStack* s );
int stackPush_param ( tStack* s, FArg* c );

#endif
