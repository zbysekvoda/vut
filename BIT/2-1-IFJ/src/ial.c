//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj

#include "ial.h"

int STACK_SIZE = MAX_STACK;
int solved;

void stackError ( int error_code ){
/*   ----------
** Vytiskne upozornění, že došlo k chybě při práci se zásobníkem.
**
** TUTO FUNKCI, PROSÍME, NEUPRAVUJTE!
*/
	static const char* SERR_STRINGS[MAX_SERR+1] = {"Unknown error","Stack error: INIT","Stack error: PUSH","Stack error: TOP"};
	if ( error_code <= 0 || error_code > MAX_SERR )
		error_code = 0;
	//printf ( "%s\n", SERR_STRINGS[error_code] );
	//err_flag = 1;
}

void stackInit ( tsStack* s ) {
/*   ---------
** Provede inicializaci zásobníku - nastaví vrchol zásobníku.
** Hodnoty ve statickém poli neupravujte - po inicializaci zásobníku
** jsou nedefinované.
**
** V případě, že funkce dostane jako parametr s == NULL,
** volejte funkci stackError(SERR_INIT). U ostatních funkcí pro zjednodušení
** předpokládejte, že tato situace nenastane. 
*/

	if (s == NULL) 
	{
		stackError(SERR_INIT);
		return;
	} 

	s->top = -1;
}

int stackEmpty ( const tsStack* s ) {
/*  ----------
** Vrací nenulovou hodnotu, pokud je zásobník prázdný, jinak vrací hodnotu 0.
** Funkci implementujte jako jediný příkaz. Vyvarujte se zejména konstrukce
** typu "if ( true ) b=true else b=false".
*/

	return (s->top == -1) ? 1 : 0;
}

int stackFull ( const tsStack* s ) {
/*  ---------
** Vrací nenulovou hodnotu, je-li zásobník plný, jinak vrací hodnotu 0.
** Dejte si zde pozor na častou programátorskou chybu "o jedničku"
** a dobře se zamyslete, kdy je zásobník plný, jestliže může obsahovat
** nejvýše STACK_SIZE prkvů a první prvek je vložen na pozici 0.
**
** Funkci implementujte jako jediný příkaz.
*/

	return (s->top == STACK_SIZE-1) ? 1 : 0;
}

void stackTop ( const tsStack* s, char* c ) {
/*   --------
** Vrací znak z vrcholu zásobníku prostřednictvím parametru c.
** Tato operace ale prvek z vrcholu zásobníku neodstraňuje.
** Volání operace Top při prázdném zásobníku je nekorektní
** a ošetřete ho voláním funkce stackError(SERR_TOP). 
**
** Pro ověření, zda je zásobník prázdný, použijte dříve definovanou
** funkci stackEmpty.
*/

	if ((stackEmpty(s)) != 0)
	{
		stackError(SERR_TOP);
		return;
	}
	
	*c = s->arr[s->top];

}


void stackPop ( tsStack* s ) {
/*   --------
** Odstraní prvek z vrcholu zásobníku. Pro ověření, zda je zásobník prázdný,
** použijte dříve definovanou funkci stackEmpty.
**
** Vyvolání operace Pop při prázdném zásobníku je sice podezřelé a může
** signalizovat chybu v algoritmu, ve kterém je zásobník použit, ale funkci
** pro ošetření chyby zde nevolejte (můžeme zásobník ponechat prázdný).
** Spíše než volání chyby by se zde hodilo vypsat varování, což však pro
** jednoduchost neděláme.
** 
*/

	if ((stackEmpty(s)) == 0)
		s->top--;
}


void stackPush ( tsStack* s, char c ) {
/*   ---------
** Vloží znak na vrchol zásobníku. Pokus o vložení prvku do plného zásobníku
** je nekorektní a ošetřete ho voláním procedury stackError(SERR_PUSH).
**
** Pro ověření, zda je zásobník plný, použijte dříve definovanou
** funkci stackFull.
*/

	if ((stackFull(s)) != 0) 
	{
		stackError(SERR_PUSH);
		return;
	}
	
	s->top++;
	s->arr[s->top] = c;
	
}

//hashovacia funckia
unsigned long hash_function(const char *name, unsigned long size)
{
    unsigned int h = 0;
    const unsigned char *p;

    for(p = (const unsigned char*)name; *p != '\0'; p++)
    {
        h = 65599*h + *p;
    }

    return (h % size);
}


//inicializacia hash tabulky statickej
hash_table *hashTab_init(unsigned long size)
{
	hash_table *table = malloc(sizeof(hash_table) + sizeof(hash_item)*size);
	if (table == NULL)
		return NULL;

	table->size = size;

	for (unsigned i = 0; i < size; i++)
		table->list[i] = NULL;

	return table;

}

//inicializacia lokalnej hash tabulky
hash_table_local *hashTab_init_local(unsigned size)
{
	hash_table_local *table = malloc(sizeof(hash_table_local) + sizeof(hash_item_local)*size);
	if (table == NULL)
		return NULL;

	table->size_local = size;

	for (unsigned i = 0; i < size; i++)
		table->list_local[i] = NULL;

    table->number_variable = 0;

	return table;

}

//vymazanie vsetkych prvkov hash tabulky statickej
int hashTab_clear(hash_table *table)
{
	if (table == NULL)
		return 10;

    hash_item *item;
    hash_item *tmp;
    unsigned count = 0;

    for (unsigned i = 0; i < table->size; i++)
    {
        item = table->list[i];

        while (item != NULL)
        {
            tmp = item;
            item = (hash_item*) tmp->next;
            free(tmp);
        }

        table->list[i] = NULL;
        count++;
    }

    if (count < table->size)
    	return 10;  

    return 0;
}

//vymazanie vsetkych prvkov hash tabulky lokalnej
int hashTab_clear_local(hash_table_local *table)
{
	if (table == NULL)
		return 10;

    //hash_item_local *item;
    //hash_item_local *tmp = NULL;
    unsigned count = 0;

    for (unsigned i = 0; i < table->size_local; i++)
    {
        //item = table->list_local[i];

        /*while (item != NULL)
        {
            tmp = item;
            item = tmp->next;
            free(tmp);
        }*/

        table->list_local[i] = NULL;
        count++;
        table->number_variable = 0;
    }

    if (count < table->size_local)
    	return 10;  

    return 0;
}

//uvolnenie pamati statickej
int hashTab_free(hash_table *table)
{
	int tmp;

    if((tmp = (hashTab_clear(table))) > 0)
    	return 10;

    free(table);

    return 0;
}

//uvolnenie pamati lokalnej
int hashTab_free_local(hash_table_local *table)
{
	int tmp;

    if((tmp = (hashTab_clear_local(table))) > 0)
    	return 10;

    table->number_variable = 0;

    free(table);

    return 0;
}

//vyhladanie a navrat daneho prvku v pripade najdenia - staticka
hash_item *hashTab_search (hash_table* table, const char *key, item_type wanted_type, char *wanted_class)
{

	if (table == NULL) 
		return NULL;

	hash_item *item = table->list[hash_function(key, table->size)];

	if(item == NULL)
    {
		//printf("item je null\n");
    }
	else
	{
		//printf("item nieje null idem hladat\n");

		while (item != NULL) 
		{

			if (((strcmp(item->key, key )) == 0) && ((strcmp(item->class_item, wanted_class)) == 0) && (item->type == wanted_type))
			{
				/*printf("nasiel som\n");
				printf("meno classu%s\n", item->class_item );
				printf("meno identifikatora %s\n",item->key );*/
				return item;
			}
			else 
				item = (hash_item*) item->next;
		}
	}
	

	//printf("nenasiel som\n");	
	return NULL;
}

//vyhladanie a navrat daneho prvku v pripade najdenia - lokalna
hash_item_local *hashTab_search_local (hash_table_local* table, const char *key )
{
	if (table == NULL) 
		return NULL;

	hash_item_local *item = table->list_local[hash_function(key, table->size_local)];

	while (item != NULL) 
	{
		if ((strcmp(item->local_key, key)) == 0)
		{
			//printf("nasiel som\n");
			return item;
		}

		else 
			item = (hash_item_local*) item->next;
	}

	//printf("nenasiel som\n");	
	return NULL;
}

//funkcia pre odstranenie prvku z tabulky - statickej
int hashTab_remove(hash_table *table, const char *key)
{
	int i = hash_function(key, table->size);
	hash_item *item = table->list[i];
	hash_item *tmp = item;

	while(item != NULL)
	{
		if(strcmp(item->key, key) == 0)
		{
			if(tmp == item)
				table->list[i] = (hash_item*) item->next;
			
			tmp->next = item->next;

			free(item);

			return 1;
		}
		else
		{
			tmp = item;
			item = (hash_item*) tmp->next;
		}
	}

	return 0;
}

//funkcia pre odstranenie prvku z tabulky - lokalnej
int hashTab_remove_local(hash_table_local *table, const char *key)
{
	int i = hash_function(key, table->size_local);
	hash_item_local *item = table->list_local[i];
	hash_item_local *tmp = item;

	while(item != NULL)
	{
		if(strcmp(item->local_key, key) == 0)
		{
			if(tmp == item)
				table->list_local[i] = item->next;
			
			tmp->next = item->next;

			free(item);

			return 1;
		}
		else
		{
			tmp = item;
			item = tmp->next;
		}
	}

	return 0;
}

//vlozenie prvku do tabulky symbolov - statickej
int hashTab_insert(hash_table *table, char *key, IDSymbolTable *IDsymbol, FSymbolTable *Fsymbol, item_type type, char *class_in)
{
	unsigned long i = hash_function(key, table->size);
	unsigned long size;

	//printf("som v hastab insert a idem insertovat !!!!!!!!!!!!\n");

    hash_item *item;
    item = table->list[i];
    hash_item *tmp;

    if (item == NULL)
    {
    	//printf("prazdna vetva, nic neprepisem iba overim ci zapisalo\n");

        item = malloc(sizeof(hash_item));

        if (item == NULL)
            return 99;

        table->list[i] = item;
        item->type = type;
        item->key = key;
        item->class_item = class_in;

       // printf("meno id alebo funkcie %s\n",item->key );
        if(type == type_id)
        {
        	item->struct_type.itemID = IDsymbol;
        /*	printf("class identifikatora %s\n",item->class_item );
        	printf("id type :%s\n",item->struct_type.itemID->type );
        	printf("initialized : %d\n", item->struct_type.itemID->initialized);*/
        }
        else
        {
        	//printf("pomocny vypis prveho argumentu meno :%s\n",Fsymbol->arguments[0].id_name );
        	item->struct_type.itemF = Fsymbol;
        	/*printf("class funkcie%s\n",item->class_item );
        	printf("func type :%s\n",item->struct_type.itemF->return_type );
        	for(int i = 0; i < item->struct_type.itemF->arg_num; i++)
        	{
        		printf("type %d parametru %s\n",i+1, item->struct_type.itemF->arguments[i].type);
				printf("meno %d parametru %s\n",i+1, item->struct_type.itemF->arguments[i].id_name);
        	}*/
        }

        item->next = NULL;

        return 0;
    }
    else
    {
        if (item == NULL)
            return 99;

    	//printf("preplo ma do druhej vetvy v hashinsert\n");
        while (item != NULL)
        {
        	//printf("cyklim pokial niesom null\n");
        		tmp = item;
                item = (hash_item*) item->next;
              //  printf("koniec funkcie\n");
        }

        item = malloc(sizeof(hash_item));

        item->type = type;
        item->key = key;
        item->class_item = class_in;
        tmp->next = item;

      //  printf("meno id alebo funkcie %s\n",item->key );

        if(type == type_id)
        {
        	item->struct_type.itemID = IDsymbol;
        	/*printf("class identifikatora %s\n",item->class_item );
        	printf("id type :%s\n",item->struct_type.itemID->type );
        	printf("initialized : %d\n", item->struct_type.itemID->initialized);*/
        }
        else
        {
        	item->struct_type.itemF = Fsymbol;
        	/*printf("class funkcie%s\n",item->class_item );
        	printf("func type :%s\n",item->struct_type.itemF->return_type );
        	for(int i = 0; i < item->struct_type.itemF->arg_num; i++)
        	{
        		printf("type %d parametru %s\n",i+1,item->struct_type.itemF->arguments[i].type);
				printf("meno %d parametru %s\n",i+1,item->struct_type.itemF->arguments[i].id_name);
        	}*/
        }
//printf("sem preslo\n");
        item->next = NULL;

        return 0;
    }
}

//vlozenie prvku do tabulky symbolov - lokalnej
int hashTab_insert_local(hash_table_local *table, char *key, char *type, void *value, unsigned init)
{
	unsigned i = hash_function(key, table->size_local);
	unsigned long size;

    hash_item_local *item = table->list_local[i];
    hash_item_local *tmp;

    if (item == NULL)
    {
        item = my_malloc(sizeof(hash_item_local)); //my_malloc
        if (item == NULL)
            return -2;

        if((strcmp(type, "int")) == 0)
        {
            if(value == NULL) 
                item->value_local = mallocInt(0);
            else
                item->value_local = value;
        }
        else if((strcmp(type, "double")) == 0)
        {
            if(value == NULL) 
                item->value_local = mallocDouble(0);
            else
                item->value_local = value;
        }
        else if((strcmp(type, "String")) == 0)
        {
            if(value == NULL) 
                item->value_local = mallocString(" ");
            else
                item->value_local = value;
        }


        table->list_local[i] = item;

        item->datatype_local = type;
        item->local_key = key;
        item->init_local = init;

        item->next = NULL;

        item->offset = table->number_variable;

        table->number_variable++;

        return 1;
    }
    else
    {	
    	item = my_malloc(sizeof(hash_item_local));	//my_malloc
        if (item == NULL)
            return -2;

        while (item != NULL)
        {
            if (strcmp(item->local_key, key) == 0)
            {
            //	printf("2rovnake\n");
                return -1;
            }
            
            else
            {
                tmp = item;
                item = item->next;
            }
        }

        tmp->next = item;

        item->datatype_local = type;
        item->local_key = key;
        item->value_local = value;
        item->init_local = init;

        item->next = NULL;

        item->offset = table->number_variable; 

        table->number_variable++;

        return 1;
    }
}

//funkcia pre radenie znakov v strigu podla asci hodnoty
char* sort(char *str) {
    int len = length(str);
    char *sorted = malloc(sizeof(len + 1));
    memcpy(sorted, str, len + 1);
    quicksort(sorted, 0, len - 1);
    return sorted;
}

//pomocna funkcia pre sort
void quicksort(char * item, int l, int r) {
    int i, j;
    partition(item, l, r, &i, &j);
    if (l < j) {
        quicksort(item, l, j);
    }
    if (i < r) {
        quicksort(item, i, r);
    }
}

//pomocna funkcia pre quicksort
void partition(char *item, int l, int r, int *i, int *j) {
    *i = l;
    *j = r;
    char pm = item[(l+r)/2];                // urceni pseudomedianu (pivot)
    char pom;
    do {
        while (item[*i] < pm) {
            (*i)++;
        }
        while (item[*j] > pm) {
            (*j)--;
        }
        if ((*i) <= (*j)) {
            pom = item[*i];
            item[*i] = item[*j];
            item[*j] = pom;
            (*i)++;
            (*j)--;
        }
    } while ((*i)<=(*j));
}

//funkcia pre hladanie podretazca v retazci
int find(char *s, char *search) {

    int pozice = 0;
    int s_len = length(s);
    int search_len = length(search);
    char *sub_str;
    int posuv = 0;
    int match = 0;
    int no_match = 0;

    sub_str = malloc(sizeof(char) * length(s));

    if (s_len < search_len) {           // hledany retezec "search" je delsi nez retezec "s"
        pozice = -1;
        return pozice;
    }

    if (search_len == 0) {              // prazdny retezec
        pozice = 0;
        return pozice;
    }

    if (compare(s, search) == 0) {      // retezce jsou stejne
        pozice = 0;
        return pozice;
    }

    // Boyer-Moore algorithm
    for (int i = 0; i < s_len; i++) {
        match = 0;
        for(int j = search_len - 1; j >= 0; j--) {
            if (j == search_len - 1) {
                sub_str = substr(s, pozice, search_len);
            }
            if (sub_str[j] == search[j]) {
                match++;
            } else {
                no_match = j;
                break;
            }
        }

        if (match == search_len) {          // nalezen retezec
            free(sub_str);
            return pozice;
        }

        for (int k = (no_match - 1); k >= 0; k--) {
            if (sub_str[no_match] == search[k]) {
                posuv = no_match - k;
            }
        }

        if ((pozice + search_len + posuv) >= s_len) {
            free(sub_str);
            return -1;
        }

        if (posuv == 0) {               // v retezci se neshoduji zadne znaky
            pozice += search_len;
        } else {
            pozice += posuv;            // nebyl nalezen dalsi znak ve zbytku retezce, ktere by se shodovaly
        }
        posuv = 0;
    }

    free(sub_str);

    return pozice;
}
