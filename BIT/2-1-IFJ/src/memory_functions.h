//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj

/* Implementace jednoduchého správce alokované paměti a pomocných funkcí pro alokování čísel a řetězců v paměti  */

#ifndef LEXICALANALYSIS_MEMORY_FUNCTIONS_H
#define LEXICALANALYSIS_MEMORY_FUNCTIONS_H

#include <string.h>
#include <stdio.h>

// struktura pro uchování informace o jednom alokovaném místě
typedef struct sGarbage {
    void* data;
    struct sGarbage* bottom;
} *Garbage;

// struktura seznamu alokovaných míst v paměti
typedef struct {
    Garbage top;
} Bin;

void initBin();
void addGarbage(void*);
void emptyGarbage();

void* my_malloc(size_t size);
int* mallocInt(int i);
double* mallocDouble(double d);
char** mallocString(char* s);

Bin bin;

#endif //LEXICALANALYSIS_MEMORY_FUNCTIONS_H
