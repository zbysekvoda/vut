//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj

#include "ial.c"
#include "ial.h"
#include "lexical_analysis.c"
#include "list.c"
#include "parser.c"

SToken c;
char table[8][8];
char rules[16][5] =  //pravidla pre vyrazy budou zapsány zrcadlově obráceně, aby se načítaný výraz nemusel obracet
        {
                {simple_identifier,          LLL},                //E -> id
                {fully_qualified_identifier, LLL},       //E -> id
                {string,                     LLL},                           //E -> const
                {num_i,                      LLL},                            //E -> const
                {num_d,                      LLL},                            //E -> const
                {bracket_right,              E,                      bracket_left, LLL},   //E -> (E)
                {E,                          operator_multiply,      E,            LLL},          //E -> E*E
                {E,                          operator_divide,        E,            LLL},            //E -> E/E
                {E,                          operator_plus,          E,            LLL},              //E -> E+E
                {E,                          operator_minus,         E,            LLL},             //E -> E-E
                {E,                          operator_less,          E,            LLL},              //E -> E<E
                {E,                          operator_greater,       E,            LLL},           //E -> E>E
                {E,                          operator_less_equal,    E,            LLL},        //E -> E<=E
                {E,                          operator_greater_equal, E,            LLL},     //E -> E>=E
                {E,                          operator_equal,         E,            LLL},          //E -> E==E
                {E,                          operator_not_equal,     E,            LLL},         //E -> E!=E
        };

//nasa verzia prikazu push na zasobnik
void addToTokenStack(TokenStack* s, SToken t) { // push
    TokenStackElement new = my_malloc(sizeof(struct sTokenElement));
    new->bottom = s->top;

    new->token.type = t.type;
    new->token.data = t.data;
    new->token.lineNumber = t.lineNumber;

    s->top = new;

    //printf("Pushing ");
    //printTokenInfo(new->token);
    //printf("\n");
}

//nasa verzia prikazu pop zo zasobniku
SToken popFromTop(TokenStack* s) { //pop první terminál
    TokenStackElement top = s->top;

    s->top = s->top->bottom;

    return top->token;
}

//nasa verzia prikazu top s moznostou precitat lubovolny terminal od vrcholu zasobniku
SToken readTerminalFromTop(TokenStack* s) { //read první terminál = top
    if(s->top->token.type == E){
        return s->top->bottom->token;
    }

    return s->top->token;
}

//nasa verzia prikazu top s moznostou precitat lubovolny token od vrcholu zasobniku
SToken readNthTokenFromTop(TokenStack* s, int n) { //read n-tý token odshora
    TokenStackElement p = s->top;

    for(int i = 0; i < n; i++) {
        p = p->bottom;
    }

    return p->token;
}

//inicializacia zasobniku
void initTokenStack(TokenStack* s) { //init stack + push $
    s->top = NULL;

    SToken dolar;
    dolar.type = DOLAR;
    addToTokenStack(s, dolar);
}

//pomocna funckia pre vypis zasobniku
void printTokenStack(TokenStack* s) { //tiskne znaky na zásobníku po první $
    TokenStackElement e = s->top;;

    //printf("----------------------------------Token stack----------------------------------\n");

    while(e->token.type != DOLAR) {
        printTokenData(e->token);

        e = e->bottom;
    }

    //printTokenData(e->token);
    //printf("\n");
    //printf("-------------------------------------------------------------------------------\n");
}

//pomocna funkcia pre nacitanie prvku zo zasobniku
SToken readNextNonEndingTokenFromInput(SToken actual, int* inputEnds, int* newStart) { //vrací vstup - při prvním čtení původní c, potom vstup, dokud nenásleduje ; nebo {
    if(*newStart == 1){
        *newStart = 0;
        return actual;
    }

    if(*inputEnds == 1){
        return actual;
    }

    SToken next = readNextToken();
    if(next.type == semicolon || next.type == bracket_open){
        *inputEnds = 1;
    }

    return readToken();
}

//nacitanie dalsieho tokenu
void moveInputToNextToken() { //posune se ve vstupu o jeden dál
    getTokenList();
}

//precedencna tabulka
void ExpressionTableInit(char table[8][8]) {
    (table)[0][0] = RR;
    (table)[0][1] = LL;
    (table)[0][2] = RR;
    (table)[0][3] = RR;
    (table)[0][4] = LL;
    (table)[0][5] = RR;
    (table)[0][6] = RR;
    (table)[0][7] = LL;

    (table)[1][0] = RR;
    (table)[1][1] = RR;
    (table)[1][2] = RR;
    (table)[1][3] = RR;
    (table)[1][4] = LL;
    (table)[1][5] = RR;
    (table)[1][6] = RR;
    (table)[1][7] = LL;

    (table)[2][0] = LL;
    (table)[2][1] = LL;
    (table)[2][2] = RR;
    (table)[2][3] = RR;
    (table)[2][4] = LL;
    (table)[2][5] = RR;
    (table)[2][6] = RR;
    (table)[2][7] = LL;

    (table)[3][0] = LL;
    (table)[3][1] = LL;
    (table)[3][2] = RR;
    (table)[3][3] = RR;
    (table)[3][4] = LL;
    (table)[3][5] = RR;
    (table)[3][6] = RR;
    (table)[3][7] = LL;

    (table)[4][0] = LL;
    (table)[4][1] = LL;
    (table)[4][2] = LL;
    (table)[4][3] = LL;
    (table)[4][4] = LL;
    (table)[4][5] = EQ;
    (table)[4][6] = ERR;
    (table)[4][7] = LL;

    (table)[5][0] = RR;
    (table)[5][1] = RR;
    (table)[5][2] = RR;
    (table)[5][3] = RR;
    (table)[5][4] = ERR; //err
    (table)[5][5] = RR;
    (table)[5][6] = RR;
    (table)[5][7] = RR; //err

    (table)[6][0] = LL;
    (table)[6][1] = LL;
    (table)[6][2] = LL;
    (table)[6][3] = LL;
    (table)[6][4] = LL;
    (table)[6][5] = ERR;
    (table)[6][6] = ERR;
    (table)[6][7] = LL;

    (table)[7][0] = RR;
    (table)[7][1] = RR;
    (table)[7][2] = RR;
    (table)[7][3] = RR;
    (table)[7][4] = ERR;
    (table)[7][5] = RR;
    (table)[7][6] = RR;
    (table)[7][7] = ERR;
}

//tabulka pre ziskanie indexu
int getTokenIndex(char t) { //vrátí index tokenu
    int i = -2;

    switch(t) {
        case operator_plus:
        case operator_minus:
            i = 0;
            break;

        case operator_multiply:
        case operator_divide:
            i = 1;
            break;

        case operator_less:
        case operator_greater:
        case operator_less_equal:
        case operator_greater_equal:
            i = 2;
            break;

        case operator_equal:
        case operator_not_equal:
            i = 3;
            break;

        case bracket_left:
            i = 4;
            break;

        case bracket_right:
            i = 5;
            break;

        case DOLAR:
        case bracket_open:
        case semicolon:
            i = 6;
            break;

        case simple_identifier:
        case fully_qualified_identifier:
        case num_i:
        case num_d:
        case string:
            i = 7;
            break;

        default:
            i = -1;
    }

    return i;
}

//funkcia pre zistenie nasledujuceho tokenu
void conditionalMoveInInput(int* firstPass, int* inputEnds) {
    if(*firstPass == 1){
        *firstPass = 0;
    } else if(*inputEnds == 0){
        moveInputToNextToken();
    }
}

//analyza vyrazov
int StateExpAnalysis() {
    //printf("======================= STARTING EXPRESSION ANALYSIS ==========================\n");
    ExpressionTableInit(table);

    SToken new;
    initTokenStack(&tokenStack);

    int a_i, b_i, top_i;
    int inputEnds = 0;
    int newStart = 1;
    int firstPass = 1;

    int indexInRule = 0;
    int foundInFirst = 0;
    int reduced = 0;

    SToken a;
    SToken b = c;

    SToken top;
    SToken second_top;

    SToken pop1, pop2, pop3;

    hash_item_local* item_loc;
    hash_item* item;
    char* first_p;
    char* second_p;
    int pom;
    int usedRelation = 0;
    int usedMath = 0;

    int counter = 0;
//zistovanie ci nede o volanie funkcie
    if(c.type == simple_identifier)
    {
        hash_item *item = hashTab_search(T, c.data, type_func, class_store);
        if(item != NULL)
        {
            char *func_call_name = c.data;
            printTokenInfo(c);
            //printf("\n");

            //printf("tu by som mal skocit\n");
            SToken q = readNextToken();
            if(q.type == bracket_left)
            {
                if((TokenTypeEqual((c = getTokenList()), error)) == 1)
                    return ERR_LEX;
                else if((TokenTypeEqual(c, error99)) == 1)
                    return ERR_INTER;

                if(c.type != bracket_left)
                    return ERR_SYN;

                argument_index = 0;

                int retV = StateFunctionCall(func_call_name, class_store);
                if(retV == ERR_SYN)
                    return ERR_SYN;
                else if(retV == ERR_LEX)
                    return ERR_LEX;
                else if(retV == ERR_INTER)
                    return ERR_INTER;
                else if(retV == ERR_SEM_UNDEF)
                    return ERR_SEM_UNDEF;
                else if(retV == ERR_SEM_TYPE)
                    return ERR_SEM_TYPE;
                else if(retV == ERR_SEM)
                    return ERR_SEM;
                else if(retV == ERR_VAR)
                    return ERR_VAR;
                else if(retV == ERR_OTHER)
                    return ERR_OTHER;
                else
                {
                    //printf("tu som skocil este\n");
                    addElement(&functionDefinitions, f_call, &(item->struct_type.itemF->def_adress), NULL, NULL);

                    new.type = E;
                    new.data = my_malloc(sizeof(struct sEData));

                    if((strcmp(item->struct_type.itemF->return_type, "int")) == 0)
                    {
                        ((EData)new.data)->type = Int;
                        ((EData)new.data)->value = mallocInt(0);
                        addElement(&functionDefinitions, assign_i, ret_i, NULL, ((EData)new.data)->value);
                    }
                    else if((strcmp(item->struct_type.itemF->return_type, "double")) == 0)
                    {
                        ((EData)new.data)->type = Double;
                        ((EData)new.data)->value = mallocDouble(0);
                        addElement(&functionDefinitions, assign_f, ret_d, NULL, ((EData)new.data)->value);
                    }
                    else if((strcmp(item->struct_type.itemF->return_type, "String")) == 0)
                    {
                        ((EData)new.data)->type = String;
                        ((EData)new.data)->value = mallocString("");
                        addElement(&functionDefinitions, assign_s, ret_s, NULL, ((EData)new.data)->value);
                    }
                    else
                        return ERR_SEM_TYPE;

                    addToTokenStack(&tokenStack, new);

                    if((TokenTypeEqual((c = getTokenList()), error)) == 1)
                        return ERR_LEX;
                    else if((TokenTypeEqual(c, error99)) == 1)
                        return ERR_INTER;

                    if(c.type != semicolon)
                        return ERR_SYN;
                    else
                        return ERR_OK;
                }
            }
        }
    }
    else if(c.type == fully_qualified_identifier)
    {
        char *first_p;
        char *second_p;
        int pom = breakIdentifier(c.data, &first_p, &second_p);
        if(pom == ERR_INTER)
            return ERR_INTER;

        hash_item *item = hashTab_search(T, second_p, type_func, first_p);
        if(item != NULL)
        {
            SToken q = readNextToken();
            if(q.type == bracket_left)
            {
                if((TokenTypeEqual((c = getTokenList()), error)) == 1)
                    return ERR_LEX;
                else if((TokenTypeEqual(c, error99)) == 1)
                    return ERR_INTER;

                if(c.type != bracket_left)
                    return ERR_SYN;

                argument_index = 0;

                int retV = StateFunctionCall(second_p, first_p);
                if(retV == ERR_SYN)
                    return ERR_SYN;
                else if(retV == ERR_LEX)
                    return ERR_LEX;
                else if(retV == ERR_INTER)
                    return ERR_INTER;
                else if(retV == ERR_SEM_UNDEF)
                    return ERR_SEM_UNDEF;
                else if(retV == ERR_SEM_TYPE)
                    return ERR_SEM_TYPE;
                else if(retV == ERR_SEM)
                    return ERR_SEM;
                else if(retV == ERR_VAR)
                    return ERR_VAR;
                else if(retV == ERR_OTHER)
                    return ERR_OTHER;
                 else
                {

                    addElement(&functionDefinitions, f_call, &(item->struct_type.itemF->def_adress), NULL, NULL);

                    new.type = E;
                    new.data = my_malloc(sizeof(struct sEData));

                    if((strcmp(item->struct_type.itemF->return_type, "int")) == 0)
                    {
                        ((EData)new.data)->type = Int;
                        ((EData)new.data)->value = mallocInt(0);
                        addElement(&functionDefinitions, assign_i, ret_i, NULL, ((EData)new.data)->value);
                    }
                    else if((strcmp(item->struct_type.itemF->return_type, "double")) == 0)
                    {
                        ((EData)new.data)->type = Double;
                        ((EData)new.data)->value = mallocDouble(0);
                        addElement(&functionDefinitions, assign_f, ret_d, NULL, ((EData)new.data)->value);
                    }
                    else if((strcmp(item->struct_type.itemF->return_type, "String")) == 0)
                    {
                        ((EData)new.data)->type = String;
                        ((EData)new.data)->value = mallocString("");
                        addElement(&functionDefinitions, assign_s, ret_s, NULL, ((EData)new.data)->value);
                    }
                    else
                        return ERR_SEM_TYPE;

                    addToTokenStack(&tokenStack, new);


                    if((TokenTypeEqual((c = getTokenList()), error)) == 1)
                        return ERR_LEX;
                    else if((TokenTypeEqual(c, error99)) == 1)
                        return ERR_INTER;

                    if(c.type != semicolon)
                        return ERR_SYN;
                    else
                        return ERR_OK;
                }
            }
        }  
    }

    do {
       //printf("-----------------------------NEXT ITERATION------------------------------------\n");
       printTokenStack(&tokenStack);

        a = readNthTokenFromTop(&tokenStack, 0); // nejvrchnější token
        b = readNextNonEndingTokenFromInput(b, &inputEnds, &newStart); //vstup

        top = readTerminalFromTop(&tokenStack); // nejvrchnější token různý od E

        b_i = getTokenIndex(b.type); //vstup
        a_i = getTokenIndex(a.type); //vrchol zasobniku
        top_i = getTokenIndex(top.type);

        //printf("> Na vstupu (b):\t %d\t", b_i);
        //printTokenInfo(b);
        //printf("\n");
        //printf("> Na vrcholu (a):\t %d\t", a_i);
        //printTokenInfo(a);
        //printf("\n");
        //printf("> Na vrcholu (top):\t %d\t", top_i);
        //printTokenInfo(top);
        //printf("\n");

        if(top_i == -1 || b_i == -1){
            return ERR_SYN;
        }
//ANALYZA VYRAZOV
        switch(table[top_i][b_i]) {
            case EQ:
                //printf("\t> Vetev EQ\n");

                addToTokenStack(&tokenStack, b);
                conditionalMoveInInput(&firstPass, &inputEnds);
                break;
            case LL:
            //vetva LL
                //printf("\t> Vetev LL\n");

                if(a.type == E){
                    pop1 = popFromTop(&tokenStack);
                    new.type = LLL;
                    addToTokenStack(&tokenStack, new);
                    new.type = E;
                    new.data = pop1.data;

                    addToTokenStack(&tokenStack, new);
                } else {
                    new.type = LLL;
                    addToTokenStack(&tokenStack, new);
                }

                if(b.type == simple_identifier){
                    item_loc = hashTab_search_local(T_loc, b.data);
                    if(item_loc == NULL){
                        if((item = hashTab_search(T, b.data, type_id, class_store)) == NULL)
                        {
                            return ERR_SEM_UNDEF;
                        }
                        else {
                            if(item->struct_type.itemID->initialized == 0)
                                return ERR_VAR;
                        }
                    }
                    else {
                        //printf("neinicializovana\n");
                        if(item_loc->init_local == 0)
                            return ERR_VAR;
                    }
                } 

                if(b.type == fully_qualified_identifier){
                    char* first_p;
                    char* second_p;
                    int pom = breakIdentifier(b.data, &first_p, &second_p);
                    if(pom == ERR_INTER)
                        return ERR_INTER;

                    hash_item* item;
                    if((item = hashTab_search(T, second_p, type_id, first_p)) == NULL)
                        return ERR_SEM_UNDEF;
                    else {
                        if(item->struct_type.itemID->initialized == 0)
                            return ERR_VAR;
                    }
                }

                addToTokenStack(&tokenStack, b);
                conditionalMoveInInput(&firstPass, &inputEnds);

                break;
            case RR:
            //vetva RR
                //printf("\t> Vetev RR\n");
                foundInFirst = 0;
                reduced = 0;

                for(int i = 0; i <= 4; i++) {
                    indexInRule = 0;

                    if(rules[i][indexInRule] == a.type){
                        pop1 = popFromTop(&tokenStack); // not E (s_id, fq_id, str, int, double, >
                        new.data = my_malloc(sizeof(struct sEData));
                        new.type = E;

                        switch(a.type) {
                            case simple_identifier:
                                item_loc = hashTab_search_local(T_loc, a.data);
                                if(item_loc == NULL){
                                    if((item = hashTab_search(T, a.data, type_id, class_store)) == NULL){
                                        return ERR_SEM_UNDEF;
                                    } else {
                                        //printf("som tu\n");
                                        if(strcmp(item->struct_type.itemID->type, "int") == 0){
                                            //printf("je to int\n");
                                            ((EData)new.data)->value = item->struct_type.itemID->value;
                                            ((EData)new.data)->type = Int;
                                        } else if(strcmp(item->struct_type.itemID->type, "double") == 0){
                                            ((EData)new.data)->value = item->struct_type.itemID->value;
                                            ((EData)new.data)->type = Double;
                                        } else if(strcmp(item->struct_type.itemID->type, "String") == 0){
                                            ((EData)new.data)->value = item->struct_type.itemID->value;
                                            ((EData)new.data)->type = String;
                                        }
                                    }
                                } else {

                                    //printf("%s \n\n\n", item_loc->datatype_local);
                                    if(strcmp(item_loc->datatype_local, "int") == 0){
                                        ((EData)new.data)->value = mallocInt(0);
                                        readFromLocal(item_loc, ((EData)new.data)->value, Int);
                                        ((EData)new.data)->type = Int;
                                    } else if(strcmp(item_loc->datatype_local, "double") == 0){
                                        ((EData)new.data)->value = mallocDouble(0);
                                        readFromLocal(item_loc, ((EData)new.data)->value, Double);
                                        ((EData)new.data)->type = Double;
                                    } else if(strcmp(item_loc->datatype_local, "String") == 0){
                                        ((EData)new.data)->value = mallocString("");
                                        readFromLocal(item_loc, ((EData)new.data)->value, String);
                                        ((EData)new.data)->type = String;
                                    }
                                }
                                break;
                            case fully_qualified_identifier:
                                pom = breakIdentifier(a.data, &first_p, &second_p);
                                if(pom == ERR_INTER)
                                    return ERR_INTER;

                                if((item = hashTab_search(T, second_p, type_id, first_p)) == NULL){
                                    return ERR_SEM_UNDEF;
                                } else {
                                    ((EData)new.data)->value = item->struct_type.itemID->value;
                                    if(strcmp(item->struct_type.itemID->type, "int") == 0){
                                        ((EData)new.data)->value = item->struct_type.itemID->value;
                                        ((EData)new.data)->type = Int;
                                    } else if(strcmp(item->struct_type.itemID->type, "double") == 0){
                                        ((EData)new.data)->value = item->struct_type.itemID->value;
                                        ((EData)new.data)->type = Double;
                                    } else if(strcmp(item->struct_type.itemID->type, "String") == 0){
                                        ((EData)new.data)->value = item->struct_type.itemID->value;
                                        ((EData)new.data)->type = String;
                                    }
                                }
                                break;
                            case num_i:
                                ((EData)new.data)->value = mallocInt(*(int*)a.data);
                                ((EData)new.data)->type = Int;
                                break;
                            case num_d:
                                ((EData)new.data)->value = mallocDouble(*(double*)a.data);
                                ((EData)new.data)->type = Double;
                                break;
                            case string:
                                ((EData)new.data)->value = mallocString((char*)a.data);
                                ((EData)new.data)->type = String;
                                break;
                        }

                        do {
                            a.type = readNthTokenFromTop(&tokenStack, 0).type;
                            indexInRule++;
                            if(rules[i][indexInRule] != a.type){
                                return ERR_SYN;
                            } else {
                                popFromTop(&tokenStack);
                            }
                        } while(a.type != LLL);


                        new.type = E;
                        addToTokenStack(&tokenStack, new);
                        foundInFirst = 1;
                        reduced = 1;
                    }

                    if(reduced == 1){
                        break;
                    }
                }

                if(foundInFirst == 0){
                    SToken pom1, pom2, pom3, pom4;
                    pom1 = readNthTokenFromTop(&tokenStack, 0);
                    pom2 = readNthTokenFromTop(&tokenStack, 1);
                    pom3 = readNthTokenFromTop(&tokenStack, 2);
                    pom4 = readNthTokenFromTop(&tokenStack, 3);

                    if(pom1.type == bracket_right && pom2.type == E && pom3.type == bracket_left && pom4.type == LLL){
                        new.type = E;
                        new.data = pom2.data;
                        new.type = pom2.type;

                        popFromTop(&tokenStack);
                        popFromTop(&tokenStack);
                        popFromTop(&tokenStack);
                        popFromTop(&tokenStack);

                        //printf("Novyyyyyy %d\n: ", new.type);
                        printTokenInfo(new);

                        addToTokenStack(&tokenStack, new);
                        foundInFirst = 1;
                    }
                }

                if(foundInFirst == 0){
                    pop3 = popFromTop(&tokenStack);
                    a = readNthTokenFromTop(&tokenStack, 0);

                    for(int i = 6; i <= 15; i++) {
                        indexInRule = 1;

                        if(rules[i][indexInRule] == a.type){
                            pop2 = popFromTop(&tokenStack); //tady je E
                            new.data = my_malloc(sizeof(struct sEData));

                            do {
                                indexInRule++;
                                if(rules[i][indexInRule] != readNthTokenFromTop(&tokenStack, 0).type){
                                    return ERR_SYN;
                                } else {
                                    if(indexInRule == 2){
                                        pop1 = popFromTop(&tokenStack);
                                    }
                                }
                            } while(readNthTokenFromTop(&tokenStack, 0).type != LLL);

                            switch(pop2.type) {
                                case operator_multiply:
                                    usedMath = 1;
                                    if(((EData)(pop1.data))->type == Int && ((EData)(pop3.data))->type == Int){
                                        ((EData)new.data)->type = Int;
                                        ((EData)new.data)->value = mallocInt(0);
                                        addElement(&functionDefinitions, m_mul_i, ((EData)(pop1.data))->value, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Double &&
                                              ((EData)(pop3.data))->type == Double){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, m_mul_f, ((EData)(pop1.data))->value, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Int &&
                                              ((EData)(pop3.data))->type == Double){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, c_itod, ((EData)(pop1.data))->value, NULL, reg_d1);
                                        addElement(&functionDefinitions, m_sum_f, ((EData)(pop3.data))->value, reg_d1, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Double &&
                                              ((EData)(pop3.data))->type == Int){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, c_itod, ((EData)(pop3.data))->value, NULL, reg_d1);
                                        addElement(&functionDefinitions, m_sum_f, ((EData)(pop1.data))->value, reg_d1, ((EData)new.data)->value);
                                    } else {
                                        return ERR_SEM;
                                    }
                                    break;
                                case operator_divide:
                                    usedMath = 1;
                                    if(((EData)(pop1.data))->type == Int && ((EData)(pop3.data))->type == Int){
                                        ((EData)new.data)->type = Int;
                                        ((EData)new.data)->value = mallocInt(0);
                                        addElement(&functionDefinitions, m_div_i, ((EData)(pop1.data))->value, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Double &&
                                              ((EData)(pop3.data))->type == Double){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, m_div_f, ((EData)(pop1.data))->value, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Int &&
                                              ((EData)(pop3.data))->type == Double){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, c_itod, ((EData)(pop1.data))->value, NULL, reg_d1);
                                        addElement(&functionDefinitions, m_div_f, ((EData)(pop3.data))->value, reg_d1, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Double &&
                                              ((EData)(pop3.data))->type == Int){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, c_itod, ((EData)(pop3.data))->value, NULL, reg_d1);
                                        addElement(&functionDefinitions, m_div_f, ((EData)(pop1.data))->value, reg_d1, ((EData)new.data)->value);
                                    } else {
                                        return ERR_SEM;
                                    }
                                    break;
                                case operator_plus:
                                    usedMath = 1;
                                    if(((EData)(pop1.data))->type == Int &&
                                       ((EData)(pop3.data))->type == Int){
                                        ((EData)new.data)->type = Int;
                                        ((EData)new.data)->value = mallocInt(0);
                                        addElement(&functionDefinitions, m_sum_i, ((EData)(pop1.data))->value, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Double &&
                                              ((EData)(pop3.data))->type == Double){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, m_sum_f, ((EData)(pop1.data))->value, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Int &&
                                              ((EData)(pop3.data))->type == Double){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, c_itod, ((EData)(pop1.data))->value, NULL, reg_d1);
                                        addElement(&functionDefinitions, m_sum_f, ((EData)(pop3.data))->value, reg_d1, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Double &&
                                              ((EData)(pop3.data))->type == Int){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, c_itod, ((EData)(pop3.data))->value, NULL, reg_d1);
                                        addElement(&functionDefinitions, m_sum_f, ((EData)(pop1.data))->value, reg_d1, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == String || ((EData)(pop3.data))->type == String){
                                        ((EData)new.data)->type = String;
                                        ((EData)new.data)->value = mallocString("");

                                        //první operand
                                        if(((EData)(pop3.data))->type == Int){
                                            addElement(&functionDefinitions, c_itos, ((EData)(pop3.data))->value, NULL, reg_s1);
                                        } else if(((EData)(pop3.data))->type == Double){
                                            addElement(&functionDefinitions, c_dtos, ((EData)(pop3.data))->value, NULL, reg_s1);
                                        } else if(((EData)(pop3.data))->type == String){
                                            addElement(&functionDefinitions, assign_s, ((EData)(pop3.data))->value, NULL, reg_s1);
                                        }

                                        //druhý operand
                                        if(((EData)(pop1.data))->type == Int){
                                            addElement(&functionDefinitions, c_itos, ((EData)(pop1.data))->value, NULL, reg_s2);
                                        } else if(((EData)(pop1.data))->type == Double){
                                            addElement(&functionDefinitions, c_dtos, ((EData)(pop1.data))->value, NULL, reg_s2);
                                        } else if(((EData)(pop1.data))->type == String){
                                            addElement(&functionDefinitions, assign_s, ((EData)(pop1.data))->value, NULL, reg_s2);
                                        }

                                        addElement(&functionDefinitions, s_concat, reg_s2, reg_s1, ((EData)new.data)->value);
                                    } else {
                                        return ERR_SEM;
                                    }

                                    break;
                                case operator_minus:
                                    usedMath = 1;
                                    if(((EData)(pop1.data))->type == Int && ((EData)(pop3.data))->type == Int){
                                        ((EData)new.data)->type = Int;
                                        ((EData)new.data)->value = mallocInt(0);
                                        addElement(&functionDefinitions, m_sub_i, ((EData)(pop1.data))->value, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Double &&
                                              ((EData)(pop3.data))->type == Double){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, m_sub_f, ((EData)(pop1.data))->value, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Int &&
                                              ((EData)(pop3.data))->type == Double){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, c_itod, ((EData)(pop1.data))->value, NULL, reg_d1);
                                        addElement(&functionDefinitions, m_sub_f, ((EData)(pop3.data))->value, reg_d1, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Double &&
                                              ((EData)(pop3.data))->type == Int){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, c_itod, ((EData)(pop3.data))->value, NULL, reg_d1);
                                        addElement(&functionDefinitions, m_sub_f, ((EData)(pop1.data))->value, reg_d1, ((EData)new.data)->value);
                                    } else {
                                        return ERR_SEM;
                                    }
                                    break;
                                case operator_less:
                                    usedRelation = 1;
                                    if(((EData)(pop1.data))->type == Int && ((EData)(pop3.data))->type == Int){
                                        ((EData)new.data)->type = Int;
                                        ((EData)new.data)->value = mallocInt(0);

                                        addElement(&functionDefinitions, cmp_ls_i, ((EData)(pop1.data))->value, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Double &&
                                              ((EData)(pop3.data))->type == Double){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, cmp_ls_d, ((EData)(pop1.data))->value, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Int &&
                                              ((EData)(pop3.data))->type == Double){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, c_itod, ((EData)(pop1.data))->value, NULL, reg_d1);
                                        addElement(&functionDefinitions, cmp_ls_d, reg_d1, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Double &&
                                              ((EData)(pop3.data))->type == Int){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, c_itod, ((EData)(pop3.data))->value, NULL, reg_d1);
                                        addElement(&functionDefinitions, cmp_ls_d, ((EData)(pop1.data))->value, reg_d1, ((EData)new.data)->value);
                                    } else {
                                        return ERR_SEM;
                                    }
                                    addElement(&functionDefinitions, assign_i, ((EData)new.data)->value, NULL, reg_i1);
                                    break;
                                case operator_greater:
                                    usedRelation = 1;
                                    if(((EData)(pop1.data))->type == Int && ((EData)(pop3.data))->type == Int){
                                        ((EData)new.data)->type = Int;
                                        ((EData)new.data)->value = mallocInt(0);
                                        addElement(&functionDefinitions, cmp_gr_i, ((EData)(pop1.data))->value, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Double &&
                                              ((EData)(pop3.data))->type == Double){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, cmp_gr_d, ((EData)(pop1.data))->value, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Int &&
                                              ((EData)(pop3.data))->type == Double){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, c_itod, ((EData)(pop1.data))->value, NULL, reg_d1);
                                        addElement(&functionDefinitions, cmp_gr_d, reg_d1, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Double &&
                                              ((EData)(pop3.data))->type == Int){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, c_itod, ((EData)(pop3.data))->value, NULL, reg_d1);
                                        addElement(&functionDefinitions, cmp_gr_d, ((EData)(pop1.data))->value, reg_d1, ((EData)new.data)->value);
                                    } else {
                                        return ERR_SEM;
                                    }
                                    addElement(&functionDefinitions, assign_i, ((EData)new.data)->value, NULL, reg_i1);
                                    break;
                                case operator_less_equal:
                                    usedRelation = 1;
                                    if(((EData)(pop1.data))->type == Int && ((EData)(pop3.data))->type == Int){
                                        ((EData)new.data)->type = Int;
                                        ((EData)new.data)->value = mallocInt(0);
                                        addElement(&functionDefinitions, cmp_leq_i, ((EData)(pop1.data))->value, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Double &&
                                              ((EData)(pop3.data))->type == Double){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, cmp_leq_d, ((EData)(pop1.data))->value, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Int &&
                                              ((EData)(pop3.data))->type == Double){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, c_itod, ((EData)(pop1.data))->value, NULL, reg_d1);
                                        addElement(&functionDefinitions, cmp_leq_d, reg_d1, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Double &&
                                              ((EData)(pop3.data))->type == Int){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, c_itod, ((EData)(pop3.data))->value, NULL, reg_d1);
                                        addElement(&functionDefinitions, cmp_leq_d, ((EData)(pop1.data))->value, reg_d1, ((EData)new.data)->value);
                                    } else {
                                        return ERR_SEM;
                                    }
                                    addElement(&functionDefinitions, assign_i, ((EData)new.data)->value, NULL, reg_i1);
                                    break;
                                case operator_greater_equal:
                                    usedRelation = 1;
                                    if(((EData)(pop1.data))->type == Int && ((EData)(pop3.data))->type == Int){
                                        ((EData)new.data)->type = Int;
                                        ((EData)new.data)->value = mallocInt(0);
                                        addElement(&functionDefinitions, cmp_geq_i, ((EData)(pop1.data))->value, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Double &&
                                              ((EData)(pop3.data))->type == Double){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, cmp_geq_d, ((EData)(pop1.data))->value, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Int &&
                                              ((EData)(pop3.data))->type == Double){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, c_itod, ((EData)(pop1.data))->value, NULL, reg_d1);
                                        addElement(&functionDefinitions, cmp_geq_d, reg_d1, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Double &&
                                              ((EData)(pop3.data))->type == Int){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, c_itod, ((EData)(pop3.data))->value, NULL, reg_d1);
                                        addElement(&functionDefinitions, cmp_geq_d, ((EData)(pop1.data))->value, reg_d1, ((EData)new.data)->value);
                                    } else {
                                        return ERR_SEM;
                                    }
                                    addElement(&functionDefinitions, assign_i, ((EData)new.data)->value, NULL, reg_i1);
                                    break;
                                case operator_equal:
                                    usedRelation = 1;
                                    if(((EData)(pop1.data))->type == Int && ((EData)(pop3.data))->type == Int){
                                        ((EData)new.data)->type = Int;
                                        ((EData)new.data)->value = mallocInt(0);
                                        addElement(&functionDefinitions, cmp_eq_i, ((EData)(pop1.data))->value, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Double &&
                                              ((EData)(pop3.data))->type == Double){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, cmp_eq_d, ((EData)(pop1.data))->value, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Int &&
                                              ((EData)(pop3.data))->type == Double){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, c_itod, ((EData)(pop1.data))->value, NULL, reg_d1);
                                        addElement(&functionDefinitions, cmp_eq_d, reg_d1, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Double &&
                                              ((EData)(pop3.data))->type == Int){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, c_itod, ((EData)(pop3.data))->value, NULL, reg_d1);
                                        addElement(&functionDefinitions, cmp_eq_d, ((EData)(pop1.data))->value, reg_d1, ((EData)new.data)->value);
                                    } else {
                                        return ERR_SEM;
                                    }
                                    addElement(&functionDefinitions, assign_i, ((EData)new.data)->value, NULL, reg_i1);
                                    break;
                                case operator_not_equal:
                                    usedRelation = 1;
                                    if(((EData)(pop1.data))->type == Int && ((EData)(pop3.data))->type == Int){
                                        ((EData)new.data)->type = Int;
                                        ((EData)new.data)->value = mallocInt(0);
                                        addElement(&functionDefinitions, cmp_neq_i, ((EData)(pop1.data))->value, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Double &&
                                              ((EData)(pop3.data))->type == Double){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, cmp_neq_d, ((EData)(pop1.data))->value, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Int &&
                                              ((EData)(pop3.data))->type == Double){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, c_itod, ((EData)(pop1.data))->value, NULL, reg_d1);
                                        addElement(&functionDefinitions, cmp_neq_d, reg_d1, ((EData)(pop3.data))->value, ((EData)new.data)->value);
                                    } else if(((EData)(pop1.data))->type == Double &&
                                              ((EData)(pop3.data))->type == Int){
                                        ((EData)new.data)->type = Double;
                                        ((EData)new.data)->value = mallocDouble(0);
                                        addElement(&functionDefinitions, c_itod, ((EData)(pop3.data))->value, NULL, reg_d1);
                                        addElement(&functionDefinitions, cmp_neq_d, ((EData)(pop1.data))->value, reg_d1, ((EData)new.data)->value);
                                    } else {
                                        return ERR_SEM;
                                    }
                                    addElement(&functionDefinitions, assign_i, ((EData)new.data)->value, NULL, reg_i1);
                                    break;
                            }

                            reduced = 1;
                            popFromTop(&tokenStack);
                            new.type = E;
                            addToTokenStack(&tokenStack, new);
                        }

                        if(reduced == 1){
                            break;
                        }
                    }
                }

                break;
            case ERR:
            //vetva ERR
                //printf("\t> Vetev ERR\n");
                return ERR_SYN;

                break;
            default:
                //printf("\t> Problem? \n");
                return ERR_SYN;
        }

        printTokenStack(&tokenStack);
    } while(!inputEnds || readTerminalFromTop(&tokenStack).type != DOLAR);

    c = b;

    //printf("======================= FINISHING EXPRESSION ANALYSIS =========================\n");
    //printTokenInfo(c);
    //printf("\n");

    //priradovanie vyslednej hodnoty pre ucely interpretacie

    c = getTokenList();

    if(relationOperation == 1 && usedRelation == 0){
        return ERR_SEM;
    }
    if(relationOperation == 1 && usedMath == 1){
        return ERR_SEM;
    }
    if(relationOperation == 0 && usedRelation == 1){
        return ERR_SEM;
    }

    top = readNthTokenFromTop(&tokenStack, 0);

    if(isVoid == 1) return ERR_OK;

    if(isReturnExp == 0 && relationOperation == 0 && (pom_token.type == simple_identifier || pom_token.type == fully_qualified_identifier)){
        item_loc = hashTab_search_local(T_loc, id_called_func);
        if(item_loc == NULL){
            if((item = hashTab_search(T, id_called_func, type_id, id_called_class)) == NULL){
                return ERR_SEM_UNDEF;
            } else {
                if(strcmp(item->struct_type.itemID->type, "int") == 0 && ((EData)top.data)->type == Int){
                    addElement(&functionDefinitions, assign_i, ((EData)top.data)->value, NULL, item->struct_type.itemID->value);
                } else if(strcmp(item->struct_type.itemID->type, "double") == 0 && ((EData)top.data)->type == Int){
                    addElement(&functionDefinitions, c_itod, ((EData)top.data)->value, NULL, item->struct_type.itemID->value);
                } else if(strcmp(item->struct_type.itemID->type, "double") == 0 && ((EData)top.data)->type == Double){
                    addElement(&functionDefinitions, assign_f, ((EData)top.data)->value, NULL, item->struct_type.itemID->value);
                } else if(strcmp(item->struct_type.itemID->type, "String") == 0 && ((EData)top.data)->type == String){
                    addElement(&functionDefinitions, assign_s, ((EData)top.data)->value, NULL, item->struct_type.itemID->value);
                }
                item->struct_type.itemID->initialized = 1;
            }
        } else {
            if(strcmp(item_loc->datatype_local, "int") == 0 && ((EData)top.data)->type == Int){
                writeToLocal(item_loc, ((EData)new.data)->value, Int);
            } else if(strcmp(item_loc->datatype_local, "double") == 0 && ((EData)top.data)->type == Int){
                writeToLocal(item_loc, ((EData)new.data)->value, Int);
            } else if(strcmp(item_loc->datatype_local, "double") == 0 && ((EData)top.data)->type == Double){
                writeToLocal(item_loc, ((EData)new.data)->value, Double);
            } else if(strcmp(item_loc->datatype_local, "String") == 0 && ((EData)top.data)->type == String){
                writeToLocal(item_loc, ((EData)new.data)->value, String);
            }
            item_loc->init_local = 1;   
        }
    }

    //printf("Vracim se z Expressions\n");

    return ERR_OK;
}
