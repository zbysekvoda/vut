//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj

/* Tento stack slouží k uchování adres pro skokové instrukce,
 * které jsou použity při generování instrukcí z konstrukcí if, else a while
 */

#ifndef LEXICALANALYSIS_CONDITIONAL_ADRESS_STACK_H
#define LEXICALANALYSIS_CONDITIONAL_ADRESS_STACK_H

#include "tac_memory_lists.h"

//struktura k uchování adres pro skoky, jsou zaplněny buď dvojice if_addr a else_addr, nebo jen while_addr. Nikdy nevznikne jiná kombinace
typedef struct sAdressStruct {
    TACListElement if_addr;
    TACListElement else_addr;
    TACListElement end_addr;
    TACListElement while_addr;
    struct sAdressStruct* bottom;
} AdressStruct;

//zásobník adres pro skokové instrukce
typedef struct sConditionalAdressStack {
    AdressStruct* top;
} ConditionalAdressStack;

void initConditionalAdressStack(ConditionalAdressStack* s);
AdressStruct* createNewConditionalOnStack(ConditionalAdressStack* s); //přidá trojici adres na stack
AdressStruct* readOffsetFromAddrStack(ConditionalAdressStack* s, int offset);
void popAdressStruct(ConditionalAdressStack* s); //vyjme trojici z vrcholu stacku

#endif //LEXICALANALYSIS_CONDITIONAL_ADRESS_STACK_H
