//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj
#include "tac_memory_stacks.h"

void initFunctionReturnStack(TACStack* S) {
    S->top = NULL;
    S->bottom = NULL;
}

//přidá na zásobník návratovou adresu z funkce
void pushReturnAdress(TACStack* S, TACListElement adress) {
    TACStackElement new = my_malloc(sizeof(struct sTACStaclElement));

    if(new != NULL){
        if(S->bottom == NULL){
            S->bottom = new;
        }

        new->bottom = S->top;
        new->adress = adress;

        S->top = new;
    }
}

//odebere a vrátí ze zásobníku návratovou adresu z funkce
TACListElement popReturnAdress(TACStack* S) {
    TACStackElement st = S->top;
    TACListElement adress = st->adress;

    S->top = st->bottom;
    if(st->bottom == NULL){
        S->bottom = NULL;
    }

    return adress;
}

//přečte návratovou adresu z vrcholu zásobníku
TACListElement readReturnAdressFromTop(TACStack* S) {
    return S->top == NULL ? NULL : S->top->adress;
}

void initTACParamStack(TACParamStack* S) {
    S->first = NULL;
    S->last  = NULL;
    S->elementsOnStack = 0;
}

//přidá parametr na zásobník
void pushParam(TACParamStack* S, void* val, paramType type) {
    TACParamStackElement new = my_malloc(sizeof(struct sTACParamStackElement));

    if(new != NULL){
        void* data;

        switch(type) {
            case Int:
                data = mallocInt(*(int*)val);
                break;
            case Double:
                data = mallocDouble(*(double*)val);
                break;
            case String:
                data = mallocString(*(char**)val);
                break;
        }
        new->data = data;
        new->type = type;
        new->before = S->last;
        new->next = NULL;

        if(S->last != NULL){
            S->last->next = new;
        }
        S->last = new;

        if(S->first == NULL){
            S->first = S->last;
        }

        (S->elementsOnStack)++;
    } else {
        //TODO - error99
    }
}

//vyjme parametr ze zásobníku
TACParamStackElement popParam(TACParamStack* S) {
    TACParamStackElement h = S->last;

    if(h != NULL){
        if(S->last == S->first){
            S->last = NULL;
            S->first = NULL;
        } else {
            if(h->before != NULL){
                h->before->next = NULL;
            }
            S->last = h->before;
        }

        (S->elementsOnStack)--;
    }

    return h;
}

//přečte prvek z vrcholu
TACParamStackElement readOffsetFromStack(TACParamStack* offsets, int offset) {
    TACParamStackElement h = offsets->last;

    for(int i = 0; i < offset; i++) {
        h = h == NULL ? NULL : h->before;
    }

    return h;
}

//při volání funkcí dochází k uchování informace o aktuálním stavu zásobníku parametrů a lokálních proměnných
//relativní offset proměnné v rámci funkce (pořadí, ve kterém byla deklarována) se ale liší od skutečného offsetu na seznamu parametrů
//tato funkce umí tento problém vyřešit díky přečtení informací ze zásobníku offsetů
TACParamStackElement readParam(TACParamStack* S, TACParamStack* offsets, int offset) {
    TACParamStackElement h = S->first;

    int fParamNO = *(int*)(readOffsetFromStack(&paramOffsetStack, 1)->data); // počet parametrů funkce
    int localNO = *(int*)(readOffsetFromStack(&paramOffsetStack, 0)->data); //počet prvků na stacku před voláním fce

    offset = localNO - fParamNO + offset;

    for(int i = 0; i < offset; i++) {
        h = h == NULL ? NULL : h->next;
    }

    return h;
}

//vytiskne zásobník návratových adres z funkcí
void printTACStack(TACStack* S) {
    int counter = 0;
    TACStackElement h = S->top;

    //printf("--------------------------------------------------------Return adresses stack----------------------------------------------------------------\n");

    if(h == NULL){
        //printf("Is EMPTY -> OK\n");
    }

    while(h != NULL) {
        //printf("%d:\t%p\n", counter, h->adress);

        counter++;
        h = h->bottom;
    }
}

//vytiskne zásobník parametrů
void printTACParamStack(TACParamStack* S) {
    int counter = 0;
    TACParamStackElement el = S->first;

    //printf("--------------------------------------------------------Params stack-------------------------------------------------------------------------\n");


    if(el == NULL){
        //printf("Is EMPTY -> OK\n");
    }

    while(el != NULL) {
        switch(el->type) {
            case Int:
                //printf("%d: \t%d", counter, *(int*)(el->data));
                break;
            case Double:
                //printf("%d: \t%f", counter, *(double*)(el->data));
                break;
            case String:
                //printf("%d: \t%s", counter, *(char**)(el->data));
                break;
        }

        if(counter == 0){
            //printf("\t <=== First");
        }

        //printf("\n");

        el = el->next;
        counter++;
    }
}