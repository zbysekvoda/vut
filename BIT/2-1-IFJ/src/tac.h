//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj

/* Tato část obsahuje instrukce navržené instrukční sady a definici pomocných struktur
 */

#ifndef LEXICALANALYSIS_TAC_H
#define LEXICALANALYSIS_TAC_H

#include <stdlib.h>

typedef enum {
    label,

    assign_i,
    assign_f,
    assign_s,

    m_sum_i,
    m_sub_i,
    m_mul_i,
    m_div_i,
    m_sum_f,
    m_sub_f,
    m_mul_f,
    m_div_f,

    s_concat,

    f_pushp,
    f_call,
    f_ret,
    f_readp,
    f_changep,

    j_a_jmp,
    j_a_eq_i,
    j_a_neq_i,

    b_readint,
    b_readdouble,
    b_readstring,
    b_print,
    b_length,
    b_substr,
    b_compare,
    b_find,
    b_sort,

    c_itod,
    c_itos,
    c_dtos,

    cmp_eq_i,
    cmp_neq_i,
    cmp_gr_i,
    cmp_ls_i,
    cmp_geq_i,
    cmp_leq_i,
    cmp_eq_d,
    cmp_neq_d,
    cmp_gr_d,
    cmp_ls_d,
    cmp_geq_d,
    cmp_leq_d
} TACInstruction;

char* TACInstructionName[] = {
        "label",

        "assign_i",
        "assign_f",
        "assign_s",

        "m_sum_i",
        "m_sub_i",
        "m_mul_i",
        "m_div_i",
        "m_sum_f",
        "m_sub_f",
        "m_mul_f",
        "m_div_f",

        "s_concat",

        "f_pushp",
        "f_call",
        "f_ret",
        "f_readp",
        "f_changep",

        "j_a_jmp",
        "j_a_eq_i",
        "j_a_neq_i",

        "b_readint",
        "b_readdouble",
        "b_readstring",
        "b_print",
        "b_length",
        "b_substr",
        "b_compare",
        "b_find",
        "b_sort",

        "c_itod",
        "c_itos",
        "c_dtos",

        "cmp_eq_i",
        "cmp_neq_i",
        "cmp_gr_i",
        "cmp_ls_i",
        "cmp_geq_i",
        "cmp_leq_i",
        "cmp_eq_d",
        "cmp_neq_d",
        "cmp_gr_d",
        "cmp_ls_d",
        "cmp_geq_d",
        "cmp_leq_d"
};

typedef enum {
    Int,
    Double,
    String
} paramType;

//struktura pro jednu instrukci v TAC
typedef struct sTAC {
    TACInstruction instruction;
    void* op1;
    void* op2;
    void* result;
} * TAC;

#endif //LEXICALANALYSIS_TAC_H
