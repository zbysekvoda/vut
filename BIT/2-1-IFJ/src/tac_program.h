//	*IMPLEMENTACE INTERPRETU IMPERATIVNIHO JAZYKA IFJ16*
//	      Spolecny projekt do predmetu IFJ a IAL
//
//	xradob00, Marketa Radoberska
//	xvodaz01, Zbysek Voda
//	xdubaj00, Ondrej Dubaj

/* Hlavní soubor interpretační části
 * čte jednu instrukci po druhé a provádí je
 */

#ifndef LEXICALANALYSIS_TAC_PROGRAM_H
#define LEXICALANALYSIS_TAC_PROGRAM_H

#include <stdio.h>

#include "tac.h"
#include "tac_memory_lists.h"
#include "tac_memory_stacks.h"
#include "memory_functions.h"
#include "parser.h"

int* reg_i1, * reg_i2, * ret_i;
double* reg_d1, * reg_d2, * ret_d;
char** reg_s1, ** reg_s2, ** ret_s;

TACListElement relativeJump(TACListElement el, int num);
void printInstructionLegend(TACInstruction instruction, int t);
ERROR_TYPE interpretNextElement(TACList* program, TACList* functions, TACStack* returnAdresses, TACParamStack* paramStack);
ERROR_TYPE interpretProgram(TACList* program, TACList* functions, TACStack* returnAdresses, TACParamStack* paramStack);

int readFromLocal(hash_item_local* variable, void *to, paramType toType);
int writeToLocal(hash_item_local* variable, void *from, paramType fromType);

TACList program;                            //program v TAC
TACList functionDefinitions;                //definice funkcí v TAC
TACStack functionReturnAdressesStack;       //stack pro návratové adresy z funkcí
TACParamStack paramStack;                   //stack pro parametry funkcí
TACParamStack paramOffsetStack;

paramType* typeInt;
paramType* typeDouble;
paramType* typeString;

#endif //LEXICALANALYSIS_TAC_PROGRAM_H
