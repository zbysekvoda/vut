#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>

#include "../../src/token.h"
#include "../../src/memory_functions.c"
#include "../../src/lexical_analysis/lex_main.h"
#include "../../src/lexical_analysis.c"
#include "lexical_analysis_tests.c"

int main(){
    test_getter_KA1();
    printf("---------------------------------------\n");
    test_getter_KA2();
    printf("---------------------------------------\n");
    test_getter_KA3();
    printf("---------------------------------------\n");
    test_getter_KA4();
    printf("---------------------------------------\n");
    test_getter_KA5();
    printf("---------------------------------------\n");
    test_getter_KA6();
    printf("---------------------------------------\n");
    test_getter_KA7();
    printf("---------------------------------------\n");
    test_getter_KA8();
    printf("---------------------------------------\n");
    test_getter_KA9();
    printf("---------------------------------------\n");
    test_getter_KA10();
    printf("---------------------------------------\n");
    test_getter_KA11();
    printf("---------------------------------------\n");
    test_getter_KA12();

}