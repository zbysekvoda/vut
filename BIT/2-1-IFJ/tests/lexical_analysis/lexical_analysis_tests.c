#define show 1
#define hide 0

char showResult = 1;

void assertEqual(long expected, long actual){
    if(expected == actual){
        if(showResult) printf("o-- Equal values \t(%ld == %ld)\n", expected, actual);
    }
    else{
        printf("!-- Unequal values \t(%ld != %ld)\n", expected, actual);
    }
}

void test_getter_KA1() {
    printf("Simple identifier \n");
    char* idName = NULL;
    showResult = hide;

    assertEqual(-1, KA1("123", 0, 3, 0, &idName));
    assertEqual(-1, KA1("1a", 0, 2, 0, &idName));
    assertEqual(-1, KA1("", 0, 0, 0, &idName));

    assertEqual(0, KA1("a ", 0, 2, 0, &idName));
    assertEqual(3, KA1("bbbb", 0, 4, 0, &idName));
    assertEqual(3, KA1("$$_b", 0, 4, 0, &idName));
    assertEqual(1, KA1("a3", 0, 2, 0, &idName));
    assertEqual(1, KA1("_a", 0, 2, 0, &idName));
    assertEqual(0, KA1("_", 0, 1, 0, &idName));
    assertEqual(0, KA1("$", 0, 1, 0, &idName));
    assertEqual(1, KA1("$a", 0, 2, 0, &idName));
}

void test_getter_KA2() {
    printf("Fully qualified identifier \n");
    char* idName = NULL;
    showResult = hide;

    assertEqual(-1 , KA2("", 0, 0, &idName));
    assertEqual(-1 , KA2("abc", 0, 3, &idName));
    assertEqual(-1 , KA2("1.abc", 0, 5, &idName));
    assertEqual(-1 , KA2("a.b.bc", 0, 6, &idName));

    assertEqual(2 , KA2("a.b", 0, 3, &idName));
    assertEqual(2 , KA2("a.b ", 0, 4, &idName));
    assertEqual(4 , KA2("_a.$b", 0, 5, &idName));
    assertEqual(2 , KA2("a.b+1 ", 0, 7, &idName));
}

void test_getter_KA3() {
    printf("Keyword \n");

    showResult = hide;
    unsigned char* kw_type = my_malloc(sizeof(unsigned char));

    assertEqual(-1, KA3("no_kw", 0, 5, &kw_type));
    assertEqual(-1, KA3("ifa", 0, 3, &kw_type));

    assertEqual(1, KA3("if ", 0, 3, &kw_type));
    assertEqual(1, KA3("if+", 0, 3, &kw_type));
    assertEqual(6, KA3("boolean", 0, 7, &kw_type));
    assertEqual(4, KA3("break", 0, 5, &kw_type));
    assertEqual(4, KA3("class", 0, 5, &kw_type));
    assertEqual(7, KA3("continue", 0, 8, &kw_type));
    assertEqual(1, KA3("do", 0, 2, &kw_type));
    assertEqual(5, KA3("double", 0, 6, &kw_type));
    assertEqual(3, KA3("else", 0, 4, &kw_type));
    assertEqual(4, KA3("false", 0, 5, &kw_type));
    assertEqual(2, KA3("for", 0, 3, &kw_type));
    assertEqual(1, KA3("if", 0, 2, &kw_type));
    assertEqual(2, KA3("int", 0, 3, &kw_type));
    assertEqual(5, KA3("return", 0, 6, &kw_type));
    assertEqual(5, KA3("String", 0, 6, &kw_type));
    assertEqual(5, KA3("static", 0, 6, &kw_type));
    assertEqual(3, KA3("true", 0, 4, &kw_type));
    assertEqual(3, KA3("void", 0, 4, &kw_type));
    assertEqual(4, KA3("while", 0, 5, &kw_type));
}

void test_getter_KA4() {
    printf("Whole number \n");
    showResult = hide;

    assertEqual(-1, KA4(".123", 0, 4, NULL, 0));
    assertEqual(2, KA4("123", 0, 3, NULL, 0));
    assertEqual(0, KA4("1.123", 0, 4, NULL, 0));
}

void test_getter_KA5() {
    printf("Float number \n");
    showResult = hide;

    assertEqual(-1, KA5(".a", 0, 2, NULL, 0));
    assertEqual(-1, KA5("1.", 0, 2, NULL, 0));
    assertEqual(-1, KA5("1e", 0, 2, NULL, 0));
    assertEqual(-1, KA5("1e+", 0, 3, NULL, 0));

    assertEqual(4, KA5("1.256", 0, 5, NULL, 0));
    assertEqual(2, KA5("1.1", 0, 3, NULL, 0));
    assertEqual(2, KA5("1e2", 0, 3, NULL, 0));
    assertEqual(5, KA5("1.2e10", 0, 6, NULL, 0));
    assertEqual(6, KA5("1.2e+10", 0, 7, NULL, 0));
    assertEqual(6, KA5("1.2e-10", 0, 7, NULL, 0));
    assertEqual(4, KA5("10e10", 0, 5, NULL, 0));
    assertEqual(5, KA5("10e+10", 0, 6, NULL, 0));
    assertEqual(5, KA5("10e-10", 0, 6, NULL, 0));
    assertEqual(5, KA5("10e-10 ", 0, 7, NULL, 0));
    assertEqual(5, KA5("10e-10+", 0, 7, NULL, 0));
}

void test_getter_KA6() {
    printf("String literal \n");
    showResult = hide;

    char* res = NULL;

    assertEqual(-1, KA6("123", 0, 3, &res));
    assertEqual(-1, KA6("xyz", 0, 2, &res));
    assertEqual(-2, KA6("\"\n\"", 0, 3, &res));

    assertEqual(1, KA6("\"\"", 0, 2, &res));
    assertEqual(1, KA6("\"\"", 0, 2, &res));
    assertEqual(1, KA6("\"\"", 0, 2, &res));
    assertEqual(1, KA6("\"\"", 0, 2, &res));
    assertEqual(5, KA6("\"ahoj\"", 0, 6, &res));
    assertEqual(3, KA6("\"\\t\"", 0, 3, &res));
    assertEqual(3, KA6("\"\\n\"", 0, 2, &res));
    assertEqual(3, KA6("\"\\\"\"", 0, 2, &res));
    assertEqual(5, KA6("\"\\037\"", 0, 2, &res));
    assertEqual(5, KA6("\"\\377\"", 0, 2, &res));
}

void test_getter_KA7() {
    printf("Komentar \n");
    showResult = hide;

    assertEqual(-1, KA7("", 0, 0));
    assertEqual(-1, KA7("ahoj", 0, 4));
    assertEqual(-1, KA7("1234", 0, 4));

    assertEqual(4, KA7("//ahoj", 0, 6));
    assertEqual(5, KA7("//ahoj\n  a", 0, 10));
    assertEqual(3, KA7("/**/ ababa", 0, 10));
    assertEqual(13, KA7("/* komentar */", 0, 14));
}

void test_getter_KA8() {
    printf("Strednik \n");
    showResult = hide;

    assertEqual(0, KA8(";", 0, 1));
    assertEqual(-1, KA8(".", 0, 1));
}

void test_getter_KA9() {
    printf("Operator \n");
    showResult = hide;

    unsigned char* p_id = NULL;

    assertEqual(-1, KA9("12456", 0, 5, &p_id, 0));
    assertEqual(-1, KA9("id", 0, 5, &p_id, 0));

    assertEqual(0, KA9("=", 0, 1, &p_id, 0));
    assertEqual(0, KA9("+", 0, 5, &p_id, 0));
    assertEqual(0, KA9("-", 0, 5, &p_id, 0));
    assertEqual(0, KA9("*", 0, 5, &p_id, 0));
    assertEqual(0, KA9("/", 0, 5, &p_id, 0));
    assertEqual(0, KA9("<", 0, 5, &p_id, 0));
    assertEqual(0, KA9(">", 0, 5, &p_id, 0));
    assertEqual(1, KA9("<=", 0, 2, &p_id, 0));
    assertEqual(1, KA9(">=", 0, 5, &p_id, 0));
    assertEqual(1, KA9("==", 0, 5, &p_id, 0));
    assertEqual(1, KA9("!=", 0, 5, &p_id, 0));
}

void test_getter_KA10() {
    printf("Zavorky \n");
    showResult = hide;

    unsigned char* p_id = NULL;

    assertEqual(-1, KA10("123", 0, 3, &p_id));
    assertEqual(-1, KA10(".", 0, 1, &p_id));

    assertEqual(0, KA10("(", 0, 1, &p_id));
    assertEqual(0, KA10(")", 0, 1, &p_id));
    assertEqual(0, KA10("{", 0, 1, &p_id));
    assertEqual(0, KA10("}", 0, 1, &p_id));
    assertEqual(0, KA10("}", 0, 2, &p_id));
}

void test_getter_KA11() {
    printf("Carka \n");
    showResult = hide;

    assertEqual(-1, KA11("123", 0, 3));
    assertEqual(-1, KA11("abc", 0, 3));

    assertEqual(0, KA11(",", 0, 1));
}

void test_getter_KA12() {
    printf("Vestavena funkce \n");
    showResult = hide;

    unsigned char* p_id = NULL;

    assertEqual(-1, KA12("-ifj16.sort",0,10, &p_id));

    assertEqual(12, KA12("ifj16.readInt",0,13, &p_id));
    assertEqual(15, KA12("ifj16.readDouble",0,16, &p_id));
    assertEqual(15, KA12("ifj16.readString",0,16, &p_id));
    assertEqual(10, KA12("ifj16.print",0,11, &p_id));
    assertEqual(11, KA12("ifj16.length",0,12, &p_id));
    assertEqual(11, KA12("ifj16.substr",0,12, &p_id));
    assertEqual(12, KA12("ifj16.compare",0,13, &p_id));
    assertEqual(9, KA12("ifj16.find",0,10, &p_id));
    assertEqual(9, KA12("ifj16.sort",0,10, &p_id));
    assertEqual(9, KA12("ifj16.sort+",0,11, &p_id));
}