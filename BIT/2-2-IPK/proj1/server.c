/* File: server.c
 * Author: Zbyšek Voda
 * Login: xvodaz01
 * Project: IPK 2018 - 1 - user info
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <signal.h>

#include "args.h"
#include "protocol.h"

#define PASSWD_PATH "/etc/passwd"
#define BUFSIZE 1024

// checks, whether _str_ starts with _prefix_
// when _prefixIsLogin_ == 1, prefix need to match exact login
uint8_t startsWith(char *str, char *prefix, uint8_t prefixIsLogin) {
    if (!prefix) return 1;

    size_t prefixLen = strlen(prefix);

    // if _prefix_ is longer than _str_, function is untrue
    // length needs to be one char longer, when prefix equals login, because folowing ':' is checked
    if (strlen(str) < prefixLen + prefixIsLogin) return 0;

    uint8_t isPrefix = (uint8_t) (strncmp(prefix, str, prefixLen) == 0);
    uint8_t loginMatches = (uint8_t) (prefixIsLogin ? str[prefixLen] == ':' : 1);

    return (uint8_t) (isPrefix && loginMatches);
}

// utility function
// prints program info to given output
void progDescription(FILE *output) {
    fprintf(output, "Run with ./ipk-server -p port\n");
}

// finds _userIdInfo_, _login_ and _homeDirectory_ in line from /etc/passwd
// it does not malloc memory for each string, but uses string parts from line
// replacing ':' with '\0' char causes _line_ to behave as multiple C strings
// by placing pointers to start of these strings, we don't need to malloc memory
int parseLine(char *line, char **login, char **userIdInfo, char **homeDirectory) {
    size_t len = strlen(line);

    if (len == 0) return -1;

    *login = line;

    // /etc/passwd line should contain six ':' chars
    int counter = 0;
    for (int i = 0; i < (int) len; i++) {
        // start of next string
        if (line[i] == ':') {
            if (counter == 3) {
                *userIdInfo = (line + i + 1);
            } else if (counter == 4) {
                *homeDirectory = (line + i + 1);
            }

            counter++;
            line[i] = '\0'; // replace ':' with '\0'
        }
    }

    return counter == 6 ? 0 : -1;
}

// gets _line_ from /etc/passwd, _type_ of request and _prefix_ of login (or exact login)
// sends response data to client
int processLine(int receivedSocket, p_type type, char *prefix, char *line) {
    // we need login exact match for -l request
    uint8_t prefixIsLogin = (uint8_t) (type == LIST_REQUEST ? 0 : 1);

    if (!startsWith(line, prefix, prefixIsLogin)) return 0;

    char *userIdInfo = NULL;
    char *homeDirectory = NULL;
    char *login = NULL;

    // try parse
    if (parseLine(line, &login, &userIdInfo, &homeDirectory) == -1) {
        return 0;
    }

    char *data = NULL;

    // select proper data for response
    if (type == LIST_REQUEST) {
        data = login;
    } else if (type == NAME_REQUEST) {
        data = userIdInfo;
    } else if (type == FOLDER_REQUEST) {
        data = homeDirectory;
    }

    size_t dataLen = strlen(data) + 1;
    size_t frameLen = sizeof(p_header) + dataLen;

    // sends matching line frames
    p_frame *responseFrame = malloc(frameLen);
    responseFrame->header.dl = (uint16_t) dataLen;
    responseFrame->header.found = FOUND;
    responseFrame->header.last = NOT_LAST;
    responseFrame->header.type = RESPONSE;

    memcpy(&(responseFrame->data), data, dataLen);

    write(receivedSocket, responseFrame, frameLen);

    free(responseFrame);

    return 1;
}

// gets request _frame_ and parses
int processPacket(int receivedSocket, p_frame *frame, FILE *file) {
    char *login = NULL;
    // if _frame_ contains some data
    if (frame->header.dl) {
        login = (char *) (frame->data);
    }

    p_type type = frame->header.type;

    size_t length;

    char *line = NULL;

    uint32_t counter = 0;
    // reads lines from /etc/passwd and processes them one by one
    // counts matching lines
    while (getline(&line, &length, file) != EOF) {
        if (processLine(receivedSocket, type, login, line)) {
            counter++;
        }
    }

    // send closing frame, with info, whether some matching record was found
    p_frame *responseFrame = malloc(sizeof(p_header));
    responseFrame->header.dl = 0;
    responseFrame->header.found = counter == 0 ? NOT_FOUND : FOUND;
    responseFrame->header.last = LAST;
    responseFrame->header.type = RESPONSE;

    write(receivedSocket, responseFrame, sizeof(p_header));

    free(responseFrame);

    rewind(file);

    return 0;
}

volatile uint8_t quit = 0;

void quitHandler(int unused) {
    (void)unused;

    quit = 1;
}

int main(int argc, char *argv[]) {
    arguments args = {NULL, NULL, -1, -1, -1, -1};
    int argParse = parseArgs(&args, argc, argv, 2, 2, "p:");

    struct sigaction actionOnQuitSignal;
    actionOnQuitSignal.sa_flags = 0;
    actionOnQuitSignal.sa_handler = quitHandler;
    sigemptyset( &actionOnQuitSignal.sa_mask );
    sigaction(SIGINT, &actionOnQuitSignal, NULL);

    if (argParse == -1) {
        fprintf(stderr, "Error occured while parsing arguments\n");
        progDescription(stderr);
        freeArgs(&args);
        return 1;
    }

    FILE *file = fopen(PASSWD_PATH, "r");
    if (!file) {
        fprintf(stderr, "Can't open file %s\n", PASSWD_PATH);
        freeArgs(&args);
        return 1;
    }

    int requestSocket;
    int received;
    struct sockaddr_in sa;
    struct sockaddr_in sa_client;

    socklen_t sa_client_len = sizeof(sa_client);
    requestSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (requestSocket < 0) {
        fprintf(stderr, "Error while creating socket");
        freeArgs(&args);
        return 1;
    }

    memset(&sa, 0, sizeof(sa));
    sa.sin_family = AF_INET;
    sa.sin_addr.s_addr = INADDR_ANY;
    sa.sin_port = htons(args.port);

    received = bind(requestSocket, (struct sockaddr *) &sa, sizeof(sa));
    if (received < 0) {
        fprintf(stderr, "Can't bind to port\n");
        close(requestSocket);
        freeArgs(&args);
        return 1;
    }

    int listenResult = listen(requestSocket, 1);
    if (listenResult < 0) {
        fprintf(stderr, "Can't listen to incomming packets");
        close(requestSocket);
        freeArgs(&args);
        return 1;
    }

    while (!quit) {
        int receivedSocket = accept(requestSocket, (struct sockaddr *) &sa_client, &sa_client_len);

        if (receivedSocket > 0) {
            char buffer[BUFSIZE];
            memset(buffer, 0, BUFSIZE);

            recv(receivedSocket, buffer, BUFSIZE, 0);

            p_frame *receivedFrame = (p_frame *) buffer;

            processPacket(receivedSocket, receivedFrame, file);
        }

        close(receivedSocket);
    }

    freeArgs(&args);

    return 0;
}
