/* File: args.h
 * Author: Zbyšek Voda
 * Login: xvodaz01
 * Project: IPK 2018 - 1 - user info
 */

#ifndef ARGS_H
#define ARGS_H

typedef struct {
    char* login;
    char* host;
    int32_t port;
    int8_t name;
    int8_t folder;
    int8_t list;
} arguments;

void argAlreadyExists(char code);
int parseArg(arguments *args, int code, char *val);
int parseArgs(arguments *args, int argc, char *argv[], int min, int max, char* argConf);
void printArgs(arguments *args);
void freeArgs(arguments *args);

#endif //ARGS_H
