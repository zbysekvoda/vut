/* File: args.c
 * Author: Zbyšek Voda
 * Login: xvodaz01
 * Project: IPK 2018 - 1 - user info
 */

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>

#include "args.h"

void argAlreadyExists(char code) {
    fprintf(stderr, "Switch %c is already set. Only one occurance is allowed.\n", code);
}

int parseArg(arguments *args, int code, char *val) {
    if (code == 'h') {
        if (args->host) {
            argAlreadyExists('h');
            return -1;
        }

        size_t size = strlen(val) + 1;

        args->host = malloc(size);
        if (args->host) {
            memcpy(args->host, val, size);
        }
    } else if (code == 'p') {
        if (args->port >= 0) {
            argAlreadyExists('p');
            return -1;
        }

        char *endptr = val;

        args->port = (int32_t) strtol(val, &endptr, 10);

        if (val == endptr) {
            fprintf(stderr, "Port value %s is not a number\n", val);
            return -1;
        }
    } else if (code == 'n') {
        if (args->name >= 0) {
            argAlreadyExists('n');
            return -1;
        }

        args->name = 1;
    } else if (code == 'f') {
        if (args->folder >= 0) {
            argAlreadyExists('f');
            return -1;
        }

        args->folder = 1;
    } else if (code == 'l') {
        if (args->list >= 0) {
            argAlreadyExists('l');
            return -1;
        }

        args->list = 1;
    }

    return 0;
}

int parseArgs(arguments *args, int argc, char *argv[], int min, int max, char* argConf) {
    int realMin = min+1;
    int realMax = max+1;

    if (argc < realMin) {
        fprintf(stderr, "Not enough arguments\n");

        return -1;
    }
    else if(argc > realMax) {
        fprintf(stderr, "Too many arguments\n");

        return -1;
    }

    // we don't want getopt to print error messages
    opterr = 0;

    int error = 0;

    int opt;
    while ((opt = getopt(argc, argv, argConf)) != -1 && !error) {
        if (opt == '?') {
            fprintf(stderr, "Unknown option %c", optopt);
            error = 1;
        } else {
            error = parseArg(args, opt, optarg);
        }
    }

    int argsLeft = argc - optind;
    if (argsLeft == 1) {
        size_t size = strlen(argv[optind]) + 1;

        args->login = malloc(size);
        if (args->login) {
            memcpy(args->login, argv[optind], size);
        }
    } else if (argsLeft > 1) {
        fprintf(stderr, "Too many arguments or bad combination\n");
        error = 1;
    }

    if (error) {
        if (args->host) {
            free(args->host);
        }

        if (args->login) {
            free(args->login);
        }

        return -1;
    }

    if (args->name == -1) args->name = 0;
    if (args->folder == -1) args->folder = 0;
    if (args->list == -1) args->list = 0;

    if(min > 2) {
        int sum = args->name + args->folder + args->list;

        if(sum == 0) {
            fprintf(stderr, "One of -n -f -l switches must be present\n");
            return -1;
        }

        if (sum > 1) {
            fprintf(stderr, "-n -f -l switches are exclusive\n");
            return -1;
        }

        if((args->name || args->folder) && !args->login) {
            fprintf(stderr, "Username needed\n");
            return -1;
        }
    }

    return 0;
}

void printArgs(arguments *args) {
    printf("============================\n");
    printf("Login:\t%s\n", args->login);
    printf("Host:\t%s\n", args->host);
    printf("Port:\t%d\n", args->port);
    printf("Name:\t%d\n", args->name);
    printf("Folder:\t%d\n", args->folder);
    printf("List:\t%d\n", args->list);
    printf("============================\n");
    printf("\n");
}

void freeArgs(arguments *args) {
    if(args->login) {
        free(args->login);
    }

    if(args->host) {
        free(args->host);
    }
}
