/* File: protocol.h
 * Author: Zbyšek Voda
 * Login: xvodaz01
 * Project: IPK 2018 - 1 - user info
 */

#ifndef PROTOCOL_H
#define PROTOCOL_H

typedef enum {
    RESPONSE,
    NAME_REQUEST,
    FOLDER_REQUEST,
    LIST_REQUEST
} p_type;

typedef enum {
    NOT_FOUND,
    FOUND
} p_found;

typedef enum {
    NOT_LAST,
    LAST
} p_last;

// protocol header
typedef struct {
    p_type type: 2;
    p_found found: 1;
    p_last last: 1;
    uint8_t filling: 4;
    uint16_t dl;
} p_header;

typedef struct {
    p_header header;
    uint8_t data[];
} p_frame;

#endif //PROTOCOL_H
