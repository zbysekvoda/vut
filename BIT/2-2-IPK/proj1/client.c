/* File: client.c
 * Author: Zbyšek Voda
 * Login: xvodaz01
 * Project: IPK 2018 - 1 - user info
 */

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include "args.h"
#include "protocol.h"

// utility function
// prints program info to given output
void progDescription(FILE *output) {
    fprintf(output, "Run with ./ipk-server -h host -p port [-n|-f|-l] login\n");
}

// waits until enough data is present on given _socket_
void waitForData(int socket, int len) {
    int bytesToRead;

    do {
        ioctl(socket, FIONREAD, &bytesToRead);
    } while(bytesToRead < len);
}

int main(int argc, char *argv[]) {

    // load arguments
    arguments args = {NULL, NULL, -1, -1, -1, -1};
    int argParse = parseArgs(&args, argc, argv, 4, 6, "p:h:nfl");

    if (argParse == -1) {
        fprintf(stderr, "Error occured while parsing arguments\n");
        progDescription(stderr);

        return 1;
    }

    // count space of packet to be send
    size_t ll = args.login ? strlen(args.login) + 1 : 0;
    size_t frameSize = sizeof(p_header) + ll;

    // create packet and fill proper fields
    p_frame *frame = calloc(1, frameSize);
    frame->header.dl = (uint16_t) ll;
    frame->header.last = LAST;
    frame->header.found = FOUND;

    if (args.folder) {
        frame->header.type = FOLDER_REQUEST;
    } else if (args.list) {
        frame->header.type = LIST_REQUEST;
    } else if (args.name) {
        frame->header.type = NAME_REQUEST;
    }

    memcpy(&(frame->data), args.login, ll);

    int requestSocket;
    struct hostent *server;
    struct sockaddr_in serverAddress;

    // get host host by IP or hostname
    server = gethostbyname(args.host);
    if (!server) {
        fprintf(stderr, "host does not exist\n");
        freeArgs(&args);
        return -1;
    }

    // fill _serverAddress_ structure
    memset(&serverAddress, 0, sizeof(serverAddress));
    serverAddress.sin_family = AF_INET;
    memcpy(&(serverAddress.sin_addr.s_addr), server->h_addr, server->h_length);
    serverAddress.sin_port = htons(args.port);

    // create socket, check proper creation
    requestSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (requestSocket <= 0) {
        fprintf(stderr, "Can't create socket");
        free(frame);
        freeArgs(&args);
        return 1;
    }

    // connect to server
    int conn = connect(requestSocket, (const struct sockaddr *) &serverAddress, sizeof(serverAddress));
    if (conn != 0) {
        fprintf(stderr, "Can't connect to server\n");
        free(frame);
        close(requestSocket);
        freeArgs(&args);
        return 1;
    }

    // send created frame with request data
    ssize_t sentBytes = write(requestSocket, frame, frameSize);
    if (sentBytes < 0) {
        fprintf(stderr, "Error occured while sending data");
        free(frame);
        close(requestSocket);
        freeArgs(&args);
        return 1;
    }

    free(frame);

    ssize_t receivedBytes;
    p_header header = {0, 0, 0, 0, 0};

    // receive response from server
    do {
        // wait until enough data is present in _requestSocket_
        waitForData(requestSocket, (int)sizeof(p_header));

        memset(&header, 0, sizeof(p_header));
        receivedBytes = read(requestSocket, &header, sizeof(p_header));

        if (receivedBytes == 0) {
            break;
        } else if (receivedBytes < 0) {
            fprintf(stderr, "Error occured while receiving data\n");
            close(requestSocket);
            freeArgs(&args);
            return 1;
        }

        // if record wasn't found, print error message
        if (header.found == NOT_FOUND) {
            fprintf(stderr, "Can't find record");
            if(args.login) {
                fprintf(stderr, " for %s", args.login);
            }
            fprintf(stderr, "\n");
            break;
        }

        if (header.dl > 0) {
            waitForData(requestSocket, header.dl);

            uint8_t *data = calloc(1, header.dl);
            if (!data) {
                fprintf(stderr, "Error occured on data calloc\n");
                close(requestSocket);
                freeArgs(&args);
                return 1;
            }

            receivedBytes = read(requestSocket, data, header.dl);

            if (receivedBytes == 0) {
                free(data);
                break;
            } else if (receivedBytes < 0) {
                fprintf(stderr, "Error occured while receiving data");
                free(data);
                close(requestSocket);
                freeArgs(&args);
                return 1;
            }

            printf("%s\n", data);

            free(data);
        }

    } while(!header.last); // if incomming header was last message, break cycle

    // close socket and free allocated memory
    close(requestSocket);
    freeArgs(&args);

    return 0;
}
