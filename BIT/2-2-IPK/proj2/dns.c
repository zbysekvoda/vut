#include "dns.h"

char* recordTypes[] = {
    "A",
    "AAAA",
    "NS",
    "PTR",
    "CNAME"
};

char *strToDnsName(char *name) {
    size_t size = strlen(name) + 2;

    char *new = malloc(size);
    if (!new) {
        return NULL;
    }

    char counter = -1;
    for (int i = (int) (size - 1); i >= 0; i--) {
        if (i == 0) {
            new[i] = counter;
        } else if (name[i - 1] == '.') {
            new[i] = counter;
            counter = 0;
        } else {
            new[i] = name[i - 1];
            counter++;
        }
    }

    return new;
}
