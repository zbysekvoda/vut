#ifndef IPK18_1_DNS_H
#define IPK18_1_DNS_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>

enum qr_t {
    QR_QUERY = 0,
    QR_RESPONSE = 1
};

enum opcode_t {
    OP_QUERY = 0,
    OP_IQUERY = 1,
    OP_STATUS = 2,
    OP_NOTIFY = 4,
    OP_UPDATE = 5
};

enum aa_t {
    AA_AUTHORITY = 0,
    AA_NOAUTHORITY = 1
};

enum tc_t {
    TC_NOTTRUNC = 0,
    TC_TRUNC = 1
};

enum rd_t {
    RD_NO = 0,
    RD_YES = 1
};

enum ra_t {
    RA_NOTAVAIL = 0,
    RA_AVAIL = 1
};

enum ad_t {
    AD_NOTAUTH = 0,
    AD_AUTH = 1
};

enum cd_t {
    CD_DISABLED = 0,
    CD_ENABLED = 1
};

enum rcode_t {
    RCODE_OK = 0,
    RCODE_FORMAT_ERR = 1,
    RCODE_SERVER_FAIL = 2,
    RCODE_NAME_ERR = 3,
    RCODE_NOT_IMPLEMENTED = 4,
    RCODE_REFUSED = 5,
    RCODE_YXDOMAIN = 6,
    RCODE_YXRRSET = 7,
    RCODE_NXRRSET = 8,
    RCODE_NOTAUTH = 9,
    RCODE_NOT_ZONE = 10
};

struct dns_header {
    uint16_t ID;

    enum rd_t RD:           1;
    enum tc_t TC:           1;
    enum aa_t AA:           1;
    enum opcode_t OPCODE:   4;
    enum qr_t QR:           1;
    enum rcode_t RCODE:     4;
    enum cd_t CD:           1;
    enum ad_t AD:           1;
    uint8_t Z:              1;
    enum ra_t RA:           1;

    uint16_t TotalQues; // questions count
    uint16_t TotalAnsw; // answers count
    uint16_t TotalAuth; // authority count
    uint16_t TotalAddi; // aditional counts
};

enum questionType {
    QT_A = 1,
    QT_NS = 2,
    QT_CNAME = 5,
    QT_PTR = 12,
    QT_AAAA = 28
};

enum questionClass {
    QC_RES = 0,
    QC_IN = 1,
    QC_CH = 3,
    QC_HS = 4
};

struct dns_question_info {
    uint16_t questionType;
    uint16_t questionClass;
};

struct dns_answer_info {
    uint16_t answerType;
    uint16_t answerClass;
    uint32_t ttl;
    uint16_t datalen;
};

#define DNS_HEADER_SIZE sizeof(struct dns_header)
#define DNS_QUESTION_INFO_SIZE sizeof(struct dns_question_info)
#define DNS_ANSWER_INFO_SIZE 10

char *recordTypes[5];

char *strToDnsName(char *name);

#endif //IPK18_1_DNS_H
