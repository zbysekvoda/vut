/* File: args.c
 * Author: Zbyšek Voda
 * Login: xvodaz01
* Project: IPK 2018 - 2
 */

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>

#include "args.h"

enum questionType argToQuestionType(enum recordType in) {
    switch(in) {
        case A:
            return QT_A;
        case AAAA:
            return QT_AAAA;
        case NS:
            return QT_NS;
        case PTR:
            return QT_PTR;
        case CNAME:
            return QT_CNAME;
        default:
            break;
    }

    return QT_A;
}

enum recordType questionToArgType(enum questionType in) {
    switch(in) {
        case QT_A:
            return A;
        case QT_AAAA:
            return AAAA;
        case QT_NS:
            return NS;
        case QT_PTR:
            return PTR;
        case QT_CNAME:
            return CNAME;
        default:
            break;
    }

    return A;
}

void argAlreadyExists(char code) {
    fprintf(stderr, "Switch %c is already set. Only one occurance is allowed.\n", code);
}

int parseArg(arguments *args, int code, char *val) {
    if (code == 'h') {
        if (args->help) {
            argAlreadyExists('h');
            return -1;
        }

        args->help = 1;
        return 0;
    }

    if(code == 's') {
        if (args->server) {
            argAlreadyExists('s');
            return -1;
        }

        size_t size = strlen(val) + 1;

        args->server = malloc(size);
        if (args->server) {
            memcpy(args->server, val, size);
        }
        return 0;
    }

    if(code == 'T') {
        if(args->timeout >= 0) {
            argAlreadyExists('T');
            return -1;
        }

        char *endptr = val;
        args->timeout = (int32_t) strtol(val, &endptr, 10);
        if (val == endptr) {
            fprintf(stderr, "Timeout [%s] is not a number\n", val);
            return -1;
        }

        return 0;
    }

    if(code == 't') {
        if(args->recordType != UNSET_REC) {
            argAlreadyExists('t');
            return -1;
        }

        for(int i = 0; i < 5; i++) {
            if(strcmp(val, recordTypes[i]) == 0) {
                args->recordType = (enum recordType)i;
                break;
            }
        }

        if(args->recordType == UNSET_REC) {
            fprintf(stderr, "Unknown record type [%s]\n", val);
            return -1;
        }

        return 0;
    }

    if(code == 'i') {
        if(args->requestType != UNSET_REQ) {
            argAlreadyExists('i');
            return -1;
        }

        args->requestType = ITERATIVE;
        return 0;
    }

    return 0;
}

int parseArgs(arguments *args, int argc, char *argv[], char *argConf) {
    // we don't want getopt to print error messages
    opterr = 0;

    int error = 0;

    int opt;
    while ((opt = getopt(argc, argv, argConf)) != -1 && !error) {
        if (opt == '?') {
            fprintf(stderr, "Unknown option %c", optopt);
            error = 1;
        } else {
            error = parseArg(args, opt, optarg);
        }
    }

    int argsLeft = argc - optind;
    if (argsLeft == 1) {
        size_t size = strlen(argv[optind]) + 1;

        args->domainName = malloc(size);
        if (args->domainName) {
            memcpy(args->domainName, argv[optind], size);
        }
    } else if (argsLeft > 1) {
        fprintf(stderr, "Too many arguments or bad combination\n");
        error = 1;
    }

    if (error) {
        if (args->domainName) {
            free(args->domainName);
        }

        if (args->server) {
            free(args->server);
        }

        return -1;
    }

    if (args->timeout == -1) args->timeout = 5;
    if (args->recordType == UNSET_REC) args->recordType = A;
    if (args->requestType == UNSET_REQ) args->requestType = RECURSIVE;

    if(args->domainName == NULL) {
        fprintf(stderr, "Domain name is required\n");
        return -1;
    }

    if(args->server == NULL) {
        fprintf(stderr, "DNS server IP is required\n");
        return -1;
    }

    return 0;
}

void printArgs(arguments *args) {
    printf("============================\n");
    printf("DNS IP:\t\t%s\n", args->server);
    printf("Name:\t\t%s\n", args->domainName);
    printf("Timeout:\t%d\n", args->timeout);
    printf("Req. type:\t%s\n", args->requestType == ITERATIVE ? "Iterative" : "Recursive");
    printf("Rec. type:\t%s (%d -> %d)\n", recordTypes[args->recordType], args->recordType, argToQuestionType(args->recordType));
    printf("Help:\t\t%d\n", args->help);
    printf("============================\n");
    printf("\n");
}

void freeArgs(arguments *args) {
    if (args->domainName) {
        free(args->domainName);
    }

    if (args->server) {
        free(args->server);
    }
}
