/* File: args.h
 * Author: Zbyšek Voda
 * Login: xvodaz01
 * Project: IPK 2018 - 2
 */

#ifndef ARGS_H
#define ARGS_H

#include "dns.h"

enum recordType {
    A = 0,
    AAAA = 1,
    NS = 2,
    PTR = 3,
    CNAME = 4,
    UNSET_REC
};

enum requestType {
    RECURSIVE,
    ITERATIVE,
    UNSET_REQ
};

typedef struct {
    char* domainName;
    char* server;
    int32_t timeout;
    enum recordType recordType;
    enum requestType requestType;
    int8_t help;

} arguments;

void argAlreadyExists(char code);
int parseArg(arguments *args, int code, char *val);
int parseArgs(arguments *args, int argc, char *argv[], char* argConf);
void printArgs(arguments *args);
void freeArgs(arguments *args);
enum questionType argToQuestionType(enum recordType in);
enum recordType questionToArgType(enum questionType in);

#endif //ARGS_H
