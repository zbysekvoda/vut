/* File: client.c
 * Author: Zbyšek Voda
 * Login: xvodaz01
* Project: IPK 2018 - 2
 */

/* ZDROJE
 * https://gist.github.com/fffaraz/9d9170b57791c28ccda9255b48315168
 * ukázkové příklady k ISA
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include "args.h"
#include "dns.h"

// utility function
// prints program info to given output
void progDescription(FILE *output) {
    fprintf(output, "Run with ./ipk-lookup [-h] -s server [-T timeout] [-t type] [-i] name\n");
}

int sendDnsRequest(int serverSocket, struct sockaddr_in *server, arguments *args) {
    struct dns_header dns = {
        .ID = (uint16_t) htons(getpid()),

        .QR = QR_QUERY,
        .OPCODE = args->recordType == PTR ? OP_IQUERY : OP_QUERY,
        .AA = AA_AUTHORITY,
        .TC = TC_NOTTRUNC,
        .RD = (args->requestType == RECURSIVE ? RD_YES : RD_NO),
        .RA = RA_NOTAVAIL,
        .Z = 0,
        .AD = AD_NOTAUTH,
        .CD = CD_DISABLED,
        .RCODE = (enum rcode_t) 0,

        .TotalQues = htons(1),
        .TotalAnsw = 0,
        .TotalAuth = 0,
        .TotalAddi = 0
    };

    char *questionName = strToDnsName(args->domainName);

    struct dns_question_info questionInfo = {
        .questionType = (enum questionType) htons(argToQuestionType(args->recordType)),
        .questionClass = (enum questionClass) htons(QC_IN)
    };

    size_t nameSize = strlen(questionName) + 1;

    size_t frameLen = DNS_HEADER_SIZE + nameSize + DNS_QUESTION_INFO_SIZE;
    uint8_t *frame = malloc(frameLen);
    if (frame == NULL) {
        fprintf(stderr, "Problem\n");
        return 1;
    }

    memcpy(frame, &dns, sizeof(dns));
    memcpy(frame + DNS_HEADER_SIZE, questionName, nameSize);
    memcpy(frame + DNS_HEADER_SIZE + nameSize, &questionInfo, DNS_QUESTION_INFO_SIZE);

    ssize_t sendRes = sendto(serverSocket, frame, frameLen, 0, (struct sockaddr *) server, sizeof(struct sockaddr_in));
    if (sendRes == -1) {
        fprintf(stderr, "Can't send data to server\n");
        return 1;
    }

    free(frame);

    return 0;
}

int main(int argc, char *argv[]) {

    // load arguments
    arguments args = {NULL, NULL, -1, UNSET_REC, UNSET_REQ, 0};
    int argParse = parseArgs(&args, argc, argv, "s:T:t:ih");

    if (argParse == -1) {
        fprintf(stderr, "Error occured while parsing arguments\n");
        progDescription(stderr);

        return 1;
    }

    if(args.help) {
        progDescription(stdout);
        return 0;
    }

    int serverSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (serverSocket == -1) {
        fprintf(stderr, "Can't create socket.\n");
    }

    struct sockaddr_in server;

    memset(&server, 0, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_port = htons(53);
    server.sin_addr.s_addr = inet_addr(args.server);

    ////////////////////////////////////////////////
    // Send request
    ////////////////////////////////////////////////
    sendDnsRequest(serverSocket, &server, &args);


    ////////////////////////////////////////////////////////////
    // RECEIVE
    ////////////////////////////////////////////////////////////

    char buf[65536];
    int size = sizeof(struct sockaddr_in);
    ssize_t recvRes = recvfrom(serverSocket, (char *) buf, 65536, 0, (struct sockaddr *) &server, (socklen_t *) &size);
    if (recvRes == -1) {
        fprintf(stderr, "Receiving failed\n");
    }

    struct dns_header *response = (struct dns_header *) buf;

    int questions = ntohs(response->TotalQues);
    int answers = ntohs(response->TotalAnsw);

    uint8_t *responseBody = (uint8_t *) &buf[DNS_HEADER_SIZE];

    for (int i = 0; i < questions; i++) {
        int cnt = 0;

        while (responseBody[cnt++]);
        responseBody += cnt;
        responseBody += DNS_QUESTION_INFO_SIZE;
    }

    for (int i = 0; i < answers; i++) {
        char *name = NULL;
        int nameLen = 0;

        if (responseBody[0] == 0xC0 && responseBody[1] == 0x0C) {
            name = args.domainName;
            nameLen = 2;
        } else {
            //name =
            //nameLen =
        }

        responseBody += nameLen;

        struct dns_answer_info *info = (struct dns_answer_info *) (responseBody);
        int datalen = ntohs(info->datalen);

        responseBody += DNS_ANSWER_INFO_SIZE;

        enum questionType answerType = (enum questionType) ntohs(info->answerType);

        char *addr = NULL;

        switch (answerType) {
            case QT_A:
                if (datalen == 4) {
                    char *a = malloc(INET_ADDRSTRLEN);
                    if (a == NULL) {
                        return 1;
                    };

                    addr = (char *) inet_ntop(AF_INET, responseBody, a, INET_ADDRSTRLEN);
                    printf("%s. IN %s %s\n", name, recordTypes[questionToArgType(answerType)], addr);
                } else {
                    fprintf(stderr, "Corrupted data\n");
                    return 1;
                }

                break;
            case QT_AAAA:
                if (datalen == 16) {
                    char *a = malloc(INET6_ADDRSTRLEN);
                    if (a == NULL) {
                        return 1;
                    };

                    addr = (char *) inet_ntop(AF_INET6, responseBody, a, INET6_ADDRSTRLEN);
                    printf("%s. IN %s %s\n", name, recordTypes[questionToArgType(answerType)], addr);
                } else {
                    fprintf(stderr, "Corrupted data\n");
                    return 1;
                }

                break;
            default:
                break;
        }

        if(addr) {
            free(addr);
        }
    }

    freeArgs(&args);
    return 0;
}

