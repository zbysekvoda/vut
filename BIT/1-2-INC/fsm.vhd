-- fsm.vhd: Finite State Machine
-- Author(s): 
--
library ieee;
use ieee.std_logic_1164.all;

-- ----------------------------------------------------------------------------
--                        Entity declaration
-- ----------------------------------------------------------------------------
entity fsm is
port(
   CLK         : in  std_logic;
   RESET       : in  std_logic;

   -- Input signals
   KEY         : in  std_logic_vector(15 downto 0);
   CNT_OF      : in  std_logic;

   -- Output signals
   FSM_CNT_CE  : out std_logic;
   FSM_MX_MEM  : out std_logic;
   FSM_MX_LCD  : out std_logic;
   FSM_LCD_WR  : out std_logic;
   FSM_LCD_CLR : out std_logic
);
end entity fsm;
-- ----------------------------------------------------------------------------
--                      Architecture declaration
-- ----------------------------------------------------------------------------
architecture behavioral of fsm is
   type t_state is 
      (ST0, 
       ST1,
       ST2a, ST2b,
       ST3a, ST3b, 
       ST4a, ST4b, 
       ST5a, ST5b, 
       ST6a, ST6b, 
       ST7a, ST7b, 
       ST8a, ST8b, 
       ST9a, ST9b, 
       ST10, 
       ST_faulty,
       PRINT_OK_MESSAGE, 
       PRINT_F_MESSAGE, 
       FINISH);
   signal present_state, next_state: t_state;
begin
-- -------------------------------------------------------
sync_logic : process(RESET, CLK)
begin
   if (RESET = '1') then
      present_state <= ST0;
   elsif (CLK'event AND CLK = '1') then
      present_state <= next_state;
   end if;
end process sync_logic;

-- -------------------------------------------------------
next_state_logic : process(present_state, KEY, CNT_OF)
begin
   next_state <= present_state;
   case (present_state) is
      when ST0 =>
         if (KEY(15) = '1') then
            next_state <= PRINT_F_MESSAGE;
         elsif (KEY(1) = '1') then
            next_state <= ST1;
         elsif KEY = "0000000000000000" then

         else 
            next_state <= ST_faulty;
         end if;
      
      when ST1 =>
         if (KEY(15) = '1') then
            next_state <= PRINT_F_MESSAGE;
         elsif KEY(2) = '1' then
            next_state <= ST2a;
         elsif KEY(4) = '1' then
            next_state <= ST2b;
         elsif KEY = "0000000000000000" then
         
         else
            next_state <= ST_faulty;
         end if;

      when ST2a =>
         if (KEY(15) = '1') then
            next_state <= PRINT_F_MESSAGE;
         elsif KEY(8) = '1' then
            next_state <= ST3a;
         elsif KEY = "0000000000000000" then
         
         else
            next_state <= ST_faulty;
         end if;

      when ST2b =>
         if (KEY(15) = '1') then
            next_state <= PRINT_F_MESSAGE;
         elsif KEY(0) = '1' then
            next_state <= ST3b;
         elsif KEY = "0000000000000000" then
         
         else
            next_state <= ST_faulty;
         end if;

      when ST3a =>
         if (KEY(15) = '1') then
            next_state <= PRINT_F_MESSAGE;
         elsif KEY(1) = '1' then
            next_state <= ST4a;
         elsif KEY = "0000000000000000" then
         
         else
            next_state <= ST_faulty;
         end if;

      when ST3b =>
         if (KEY(15) = '1') then
            next_state <= PRINT_F_MESSAGE;
         elsif KEY(9) = '1' then
            next_state <= ST4b;
         elsif KEY = "0000000000000000" then
         
         else
            next_state <= ST_faulty;
         end if;

      when ST4a =>
         if (KEY(15) = '1') then
            next_state <= PRINT_F_MESSAGE;
         elsif KEY(2) = '1' then
            next_state <= ST5a;
         elsif KEY = "0000000000000000" then
         
         else
            next_state <= ST_faulty;
         end if;

      when ST4b =>
         if (KEY(15) = '1') then
            next_state <= PRINT_F_MESSAGE;
         elsif KEY(3) = '1' then
            next_state <= ST5b;
         elsif KEY = "0000000000000000" then
         
         else
            next_state <= ST_faulty;
         end if;

      when ST5a =>
         if (KEY(15) = '1') then
            next_state <= PRINT_F_MESSAGE;
         elsif KEY(1) = '1' then
            next_state <= ST6a;
         elsif KEY = "0000000000000000" then
         
         else
            next_state <= ST_faulty;
         end if;

      when ST5b =>
         if (KEY(15) = '1') then
            next_state <= PRINT_F_MESSAGE;
         elsif KEY(3) = '1' then
            next_state <= ST6b;
         elsif KEY = "0000000000000000" then
         
         else
            next_state <= ST_faulty;
         end if;

      when ST6a =>
         if (KEY(15) = '1') then
            next_state <= PRINT_F_MESSAGE;
         elsif KEY(5) = '1' then
            next_state <= ST7a;
         elsif KEY = "0000000000000000" then
         
         else
            next_state <= ST_faulty;
         end if;

      when ST6b =>
         if (KEY(15) = '1') then
            next_state <= PRINT_F_MESSAGE;
         elsif KEY(6) = '1' then
            next_state <= ST7b;
         elsif KEY = "0000000000000000" then
         
         else
            next_state <= ST_faulty;
         end if;

      when ST7a =>
         if (KEY(15) = '1') then
            next_state <= PRINT_F_MESSAGE;
         elsif KEY(1) = '1' then
            next_state <= ST8a;
         elsif KEY = "0000000000000000" then
         
         else
            next_state <= ST_faulty;
         end if;

      when ST7b =>
         if (KEY(15) = '1') then
            next_state <= PRINT_F_MESSAGE;
         elsif KEY(6) = '1' then
            next_state <= ST8b;
         elsif KEY = "0000000000000000" then
         
         else
            next_state <= ST_faulty;
         end if;

      when ST8a =>
         if (KEY(15) = '1') then
            next_state <= PRINT_F_MESSAGE;
         elsif KEY(7) = '1' then
            next_state <= ST9a;
         elsif KEY = "0000000000000000" then
         
         else
            next_state <= ST_faulty;
         end if;

      when ST8b =>
         if (KEY(15) = '1') then
            next_state <= PRINT_F_MESSAGE;
         elsif KEY(8) = '1' then
            next_state <= ST9b;
         elsif KEY = "0000000000000000" then
         
         else
            next_state <= ST_faulty;
         end if;

      when ST9a =>
         if (KEY(15) = '1') then
            next_state <= PRINT_F_MESSAGE;
         elsif KEY(0) = '1' then
            next_state <= ST10;
         elsif KEY = "0000000000000000" then
         
         else
            next_state <= ST_faulty;
         end if;

      when ST9b =>
         if (KEY(15) = '1') then
            next_state <= PRINT_F_MESSAGE;
         elsif KEY(8) = '1' then
            next_state <= ST10;
         elsif KEY = "0000000000000000" then
         
         else
            next_state <= ST_faulty;
         end if;

      when ST10 =>
         if (KEY(15) = '1') then
            next_state <= PRINT_OK_MESSAGE;
         elsif KEY = "0000000000000000" then
         
         else
            next_state <= ST_faulty;
         end if;

      when ST_faulty =>
         if (KEY(15) = '1') then
            next_state <= PRINT_F_MESSAGE;
         end if;

      when PRINT_F_MESSAGE =>
         if (CNT_OF = '1') then
            next_state <= FINISH;
         end if;

      when PRINT_OK_MESSAGE =>
         if (CNT_OF = '1') then
            next_state <= FINISH;
         end if;

      when FINISH =>
         if (KEY(15) = '1') then
            next_state <= ST0; 
         end if;
   end case;
end process next_state_logic;

-- -------------------------------------------------------
output_logic : process(present_state, KEY)
begin
   FSM_CNT_CE     <= '0';
   FSM_MX_MEM     <= '0';
   FSM_MX_LCD     <= '0';
   FSM_LCD_WR     <= '0';
   FSM_LCD_CLR    <= '0';

   case (present_state) is
      when PRINT_F_MESSAGE =>
         FSM_CNT_CE     <= '1';
         FSM_MX_LCD     <= '1';
         FSM_MX_MEM     <= '0';
         FSM_LCD_WR     <= '1';
      when PRINT_OK_MESSAGE =>
         FSM_CNT_CE     <= '1';
         FSM_MX_LCD     <= '1';
         FSM_MX_MEM     <= '1';
         FSM_LCD_WR     <= '1';  
      when FINISH =>
         if (KEY(15) = '1') then
            FSM_LCD_CLR    <= '1';
         end if;
      when others =>
         if (KEY(14 downto 0) /= "000000000000000") then
            FSM_LCD_WR     <= '1';
         end if;
         if (KEY(15) = '1') then
            FSM_LCD_CLR    <= '1';
         end if;
   end case;
end process output_logic;

end architecture behavioral;

