/*
    project: ISA, packet analyzer, 2017/2018
    author: Zbyšek Voda, xvodaz01, BIT3
 */

#ifndef ISA_PROJ_PACKETS_H
#define ISA_PROJ_PACKETS_H

#include <stdint.h>
#include <inttypes.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/ethernet.h>
#include <netinet/if_ether.h>

#ifdef __linux__
#include <netinet/ether.h>
#endif

#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netinet/ip6.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>

#include <pcap.h>

#include "args.h"

#define MAC_SIZE 6
#define IPV4_SIZE 6
#define VLAN_TAG_SIZE 4
#define ETHERTYPE_SIZE 2
#define ICMP_SIZE 8

#define ICMPV4_PROTOCOL_CODE 1

// IPv6
#define TCP_PROTOCOL_CODE 6
#define UDP_PROTOCOL_CODE 17
#define ICMPV6_PROTOCOL_CODE 58
#define NO_NXT_HDR_CODE 59
#define HOP_BY_HOP_CODE 0
#define DST_OPTS_CODE 60
#define ROUTING_CODE 43
#define FRAGMENT_CODE 44
#define MOBILITY_CODE 135


#define PCP_SHIFT 13
#define PCP_MASK 7
#define DEI_SHIFT 12
#define DEI_MASK 1
#define VID_MASK 0xFFF


#ifndef ETHERTYPE_SIZE
#define ETHERTYPE_SIZE 2
#endif

#ifndef ETHERTYPE_VLAN
#define ETHERTYPE_VLAN 0x8100
#endif

#ifndef ETHERTYPE_VLAN_QINQ
#define ETHERTYPE_VLAN_QINQ 0x88A8
#endif

#ifndef ETHERTYPE_IPV6
#define ETHERTYPE_IPV6		0x86dd
#endif

#ifndef ETHERTYPE_IPV4
#define ETHERTYPE_IPV4 0x0800
#endif

#define BIG_INT 131056

#ifndef TH_FIN
#define TH_FIN 0x01
#endif

#ifndef TH_SYN
#define TH_SYN 0x02
#endif

#ifndef TH_RST
#define TH_RST 0x04
#endif

#ifndef TH_PUSH
#define TH_PUSH 0x08
#endif

#ifndef TH_ACK
#define TH_ACK 0x10
#endif

#ifndef TH_URG
#define TH_URG 0x20
#endif

#ifndef TH_ECE
#define TH_ECE 0x40
#endif

#ifndef TH_CWR
#define TH_CWR 0x80
#endif



// META structs for packet list construction

typedef enum {
    Tethernet_header,
    Tvlan_header,
    Tipv4_header,
    Tipv6_header,
    Ttcp_header,
    Tudp_header,
    Ticmpv4_header,
    Ticmpv6_header,
    raw_data,
    numbering,
    info,
    aggr_info,
    aggr_srcipv4,
    aggr_dstipv4,
    aggr_srcipv6,
    aggr_dstipv6,
    aggr_srcmac,
    aggr_dstmac,
    aggr_srcport,
    aggr_dstport
} packet_part_type;

typedef struct packet_part_handle {
    packet_part_type type;
    size_t size; // packet size
    void *data;
    struct packet_part_handle *next;
} packet_part_handle;

typedef struct packet_handle {
    struct packet_handle *next;
    struct packet_handle *prev;
    packet_part_handle *parts;
} packet_handle;

typedef struct {
    packet_handle *first;
    packet_handle *last;
} packet_list;

packet_list packetList;

// actual packet parts (ethernet frame, VLAN, ...)
typedef struct {
    long long timestamp;
    uint32_t length;
    uint32_t packets;
} packet_info;

typedef uint32_t packet_numbering;

typedef struct {
    uint8_t destinationMAC[MAC_SIZE];
    uint8_t sourceMAC[MAC_SIZE];
} ethernet_macs;

typedef struct {
    uint16_t TPID;
    uint8_t PCP;
    uint8_t DEI;
    uint16_t VID;
} vlan_frame;

void packetHandler(uint8_t *userData, const struct pcap_pkthdr *pkthdr, const uint8_t *packet);

void initPacketList(packet_list *packetList);

char createPacket(packet_list *packetList, const struct pcap_pkthdr *pkthdr);

char addDataToPacket(packet_handle *packetHandle, packet_part_type type, size_t size, void *data);

char addDataToLastPacket(packet_list *list, packet_part_type type, size_t size, void *data);

void printPacketList(packet_list *packetList, int32_t limit);

void removeLastPacket(packet_list *packetList);

void reassemblePackets(packet_list *packetList);

void finishPacketParsing(packet_list *packetList);

void freePacket(packet_handle *packetHandle);

void freePacketList(packet_list *packetList);

void aggregatePackets(packet_list *packetList, TAggr_key aggregationKey);

void addNumbersToPacketList(packet_list *packetList);

void sortPacketList(packet_list *packetList, TSort_Key sortKey);

void appendPacket(packet_list *packetList, packet_handle *packetHandle);

#endif //ISA_PROJ_PACKETS_H
