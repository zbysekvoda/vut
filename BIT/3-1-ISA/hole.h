/*
    project: ISA, packet analyzer, 2017/2018
    author: Zbyšek Voda, xvodaz01, BIT3
 */

#ifndef ISA_HOLE_H
#define ISA_HOLE_H

#include <stdint.h>
#include <inttypes.h>

#include <stdlib.h>
#include <stdio.h>


typedef struct hole_descriptor {
    uint32_t first;
    uint32_t last;
    struct hole_descriptor *prev;
    struct hole_descriptor *next;
} hole_descriptor;

typedef struct {
    hole_descriptor *first;
    hole_descriptor *last;
} hole_list;

void initHoleList(hole_list *list);

void freeHoleList(hole_list *list);

hole_descriptor *getFirstHole(hole_list *list);

hole_descriptor *getLastHole(hole_list *list);

void addHole(hole_list *list, uint32_t first, uint32_t last);

void fillHole(hole_list *list, uint32_t first, uint32_t last);

#endif //ISA_HOLE_H
