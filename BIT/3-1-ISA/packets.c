/*
    project: ISA, packet analyzer, 2017/2018
    author: Zbyšek Voda, xvodaz01, BIT3
 */

#include "packets.h"
#include "packets_global.h"
#include "hole.h"
#include "frame_processor.h"

// main packet parser used as callback for pcap_loop function
void packetHandler(uint8_t *userData, const struct pcap_pkthdr *pkthdr, const uint8_t *packet) {
    // tmp structures for packet processing
    packet_list *packetList = (packet_list *) userData;
    uint8_t *nextDataPointer = (uint8_t *) packet;
    unsigned long remainingPackets = pkthdr->len;

    // creates new packet header on packet list
    createPacket(packetList, pkthdr);

    // get parts of ethernet frame
    ethernet_macs *ethernetFrame = getData(&nextDataPointer, sizeof(ethernet_macs), &remainingPackets, 0);
    addDataToLastPacket(packetList, Tethernet_header, sizeof(ethernet_macs), ethernetFrame);

    // check, if VLAN (8100) tag is present, if so process all of them
    uint16_t next2B = ntohs(*((uint16_t *) getData(&nextDataPointer, ETHERTYPE_SIZE, &remainingPackets, 1)));
    while (next2B == ETHERTYPE_VLAN || next2B == ETHERTYPE_VLAN_QINQ) {
        processVlanFrame(packetList, &nextDataPointer, &remainingPackets);
        next2B = ntohs(*((uint16_t *) getData(&nextDataPointer, ETHERTYPE_SIZE, &remainingPackets, 1)));
    }

    // here comes actual ethertype field (after all VLAN tags have been processed)
    uint16_t ethertype = ntohs(*(uint16_t *) getData(&nextDataPointer, ETHERTYPE_SIZE, &remainingPackets, 0));

    // now check, if we know found ethernet type code
    if (ethertype == ETHERTYPE_IPV4 || ethertype == ETHERTYPE_IPV6) {
        // if we know, process it depending of IP version
        if (ethertype == ETHERTYPE_IPV4) {
            processIPv4Frame(packetList, &nextDataPointer, &remainingPackets);

        } else if (ethertype == ETHERTYPE_IPV6) {
            processIPv6Frame(packetList, &nextDataPointer, &remainingPackets);

        }

        // finaly, add all unprocessed parts of higher layers to packet, they can be used
        addDataToLastPacket(packetList, raw_data, remainingPackets, nextDataPointer);
    } else {
        // if we don't know ethertype, print error message and delete created packet header with its parts
        fprintf(stderr, "Error: Unknown ethernet type 0x%04x\n", ethertype);
        removeLastPacket(packetList);
    }
}

// checks equality of two byt strings of given length
uint8_t bytesEqual(const uint8_t *a, const uint8_t *b, uint8_t num) {
    if (!a || !b) return 0;

    for (uint8_t i = 0; i < num; i++) {
        if (a[i] != b[i]) return 0;
    }

    return 1;
}

// returns string of icmp message identified by message type and and code
char *getIcmpMessage(icmp_message *messages, int messagesNo, int type, char code) {
    for (int i = 0; i < messagesNo; i++) {
        icmp_message message = messages[i];

        if (message.type == type) {
            if (code == -1) {
                return message.message;
            } else {
                if (code < message.codesNo) return message.codes[(unsigned char) code];
                else return NULL;
            }
        }
    }

    return NULL;
}

// initialize packet list
void initPacketList(packet_list *packetList) {
    if (!packetList) return;

    packetList->first = NULL;
    packetList->last = NULL;
}

// creates header for packet and adds it to packet list
char createPacket(packet_list *packetList, const struct pcap_pkthdr *pkthdr) {
    if (!packetList) return 0;

    packet_handle *new = malloc(sizeof(packet_handle));
    if (!new || !packetList) return 0;

    new->next = NULL;
    new->prev = packetList->last;
    new->parts = NULL;

    if (pkthdr) {
        packet_info packetInfo;
        packetInfo.timestamp = pkthdr->ts.tv_sec * 1000000 + pkthdr->ts.tv_usec;
        packetInfo.length = pkthdr->len;
        packetInfo.packets = 1;

        addDataToPacket(new, info, sizeof(packet_info), &packetInfo);
    }

    appendPacket(packetList, new);

    return 1;
}

// appends packet to end of list
void appendPacket(packet_list *packetList, packet_handle *packetHandle) {
    if (!packetList || !packetHandle) return;

    if (!packetList->first) {
        packetList->first = packetHandle;
        packetList->last = packetHandle;
    } else {
        packetList->last->next = packetHandle;
        packetList->last = packetHandle;
    }
}

// adds parts to packet
char addDataToPacket(packet_handle *packetHandle, packet_part_type type, size_t size, void *data) {
    if (!packetHandle) return 0;
    if (!data) return 0;

    packet_part_handle *newPart = malloc(sizeof(packet_part_handle));
    if (!newPart) return 0;

    void *newData = malloc(size);
    if (!newData) {
        free(newPart);
        return 0;
    }

    memcpy(newData, data, size);
    newPart->type = type;
    newPart->size = size;
    newPart->data = newData;
    newPart->next = NULL;

    if (!packetHandle->parts) {
        packetHandle->parts = newPart;
    } else {
        packet_part_handle *lastPart = packetHandle->parts;

        while (lastPart->next) {
            lastPart = lastPart->next;
        }

        lastPart->next = newPart;
    }

    return 1;
}

// adds parts to last packet in list
char addDataToLastPacket(packet_list *list, packet_part_type type, size_t size, void *data) {
    if (!data) return 0;
    if (!list->last) return 0;

    addDataToPacket(list->last, type, size, data);

    return 1;
}

void printMac(const struct ether_addr *mac, FILE *output) {
    fprintf(
        output,
        "%02x:%02x:%02x:%02x:%02x:%02x ",
        mac->ether_addr_octet[0],
        mac->ether_addr_octet[1],
        mac->ether_addr_octet[2],
        mac->ether_addr_octet[3],
        mac->ether_addr_octet[4],
        mac->ether_addr_octet[5]
    );
}

void printPacketPart(packet_part_handle *partHandle, FILE *output) {
    if (!partHandle) return;

    // TMP structures
    ethernet_macs *ethernetFrame;
    struct ip *ipv4Frame;
    struct ip6_hdr *ipv6Frame;
    vlan_frame *vlanFrame;
    packet_info *packetInfo;
    struct tcphdr *tcpFrame;
    struct udphdr *udpFrame;
    char ipv6[INET6_ADDRSTRLEN];
    char ipv4[INET_ADDRSTRLEN];
    struct icmp *icmpFrame;
    packet_info *aggrMetrics;
    char *pchar;

    if(output == stderr) {
        fprintf(stderr, "\t");
    }

    switch (partHandle->type) {
        case numbering:
            fprintf(output, "%u: ", *(uint32_t *) (partHandle->data));

            break;
        case info:
            packetInfo = partHandle->data;
            fprintf(output, "%llu ", packetInfo->timestamp); // timestamp
            fprintf(output, "%u ", packetInfo->length);
            break;
        case Tethernet_header:
            ethernetFrame = partHandle->data;

            fprintf(output, "| Ethernet: ");
            printMac((const struct ether_addr *) ethernetFrame->sourceMAC, output);
            printMac((const struct ether_addr *) ethernetFrame->destinationMAC, output);

            break;
        case Tvlan_header:
            vlanFrame = partHandle->data;

            fprintf(output, "%u ", vlanFrame->VID);

            break;
        case Tipv4_header:
            ipv4Frame = partHandle->data;

            fprintf(output, "| IPv4: ");
            inet_ntop(AF_INET, &(ipv4Frame->ip_src), ipv4, INET_ADDRSTRLEN);
            fprintf(output, "%s ", ipv4);
            inet_ntop(AF_INET, &(ipv4Frame->ip_dst), ipv4, INET_ADDRSTRLEN);
            fprintf(output, "%s ", ipv4);
            fprintf(output, "%u ", ipv4Frame->ip_ttl);

            break;
        case Ttcp_header:
            tcpFrame = partHandle->data;

            fprintf(output, "| TCP: ");
            fprintf(output, "%u ", ntohs(tcpFrame->th_sport));
            fprintf(output, "%u ", ntohs(tcpFrame->th_dport));
            fprintf(output, "%u ", ntohl(tcpFrame->th_seq));
            fprintf(output, "%u ", ntohl(tcpFrame->th_ack));

            fprintf(output, "%c%c%c%c%c%c%c%c ",
                   tcpFrame->th_flags & TH_CWR ? 'C' : '.',
                   tcpFrame->th_flags & TH_ECE ? 'E' : '.',
                   tcpFrame->th_flags & TH_URG ? 'U' : '.',
                   tcpFrame->th_flags & TH_ACK ? 'A' : '.',
                   tcpFrame->th_flags & TH_PUSH ? 'P' : '.',
                   tcpFrame->th_flags & TH_RST ? 'R' : '.',
                   tcpFrame->th_flags & TH_SYN ? 'S' : '.',
                   tcpFrame->th_flags & TH_FIN ? 'F' : '.'
            );

            break;
        case Tudp_header:
            udpFrame = partHandle->data;

            fprintf(output, "| UDP: ");
            fprintf(output, "%u ", ntohs(udpFrame->uh_sport));
            fprintf(output, "%u ", ntohs(udpFrame->uh_dport));

            break;
        case Tipv6_header:
            ipv6Frame = partHandle->data;

            fprintf(output, "| IPv6: ");

            inet_ntop(AF_INET6, &ipv6Frame->ip6_src, ipv6, INET6_ADDRSTRLEN);
            fprintf(output, "%s ", ipv6);
            inet_ntop(AF_INET6, &ipv6Frame->ip6_dst, ipv6, INET6_ADDRSTRLEN);
            fprintf(output, "%s ", ipv6);

            fprintf(output, "%u ", ipv6Frame->ip6_ctlun.ip6_un1.ip6_un1_hlim);

            break;
        case Ticmpv4_header:
            icmpFrame = partHandle->data;

            fprintf(output, "| ICMPv4: ");
            fprintf(output, "%u ", icmpFrame->icmp_type);
            fprintf(output, "%u ", icmpFrame->icmp_code);

            pchar = getIcmpMessage(icmpv4Messages, ICMPV4_MESSAGES_NO, icmpFrame->icmp_type, -1);
            if (pchar) {
                fprintf(output, "%s ", pchar);

                pchar = getIcmpMessage(icmpv4Messages, ICMPV4_MESSAGES_NO, icmpFrame->icmp_type,
                                       icmpFrame->icmp_code);
                if (pchar) {
                    fprintf(output, "%s ", pchar);
                }
            }

            break;
        case Ticmpv6_header:
            icmpFrame = partHandle->data;

            fprintf(output, "| ICMPv6: ");
            fprintf(output, "%u ", icmpFrame->icmp_type);
            fprintf(output, "%u ", icmpFrame->icmp_code);

            pchar = getIcmpMessage(icmpv6Messages, ICMPV6_MESSAGES_NO, icmpFrame->icmp_type, -1);
            if (pchar) {
                fprintf(output, "%s ", pchar);

                pchar = getIcmpMessage(icmpv6Messages, ICMPV6_MESSAGES_NO, icmpFrame->icmp_type,
                                       icmpFrame->icmp_code);
                if (pchar) {
                    fprintf(output, "%s ", pchar);
                }
            }

            break;
        case raw_data:
            fprintf(output, "| %zuB of raw parts", partHandle->size);

            break;
        case aggr_srcmac:
        case aggr_dstmac:
            fprintf(output, "%s: ", ether_ntoa(partHandle->data));

            break;
        case aggr_srcipv4:
        case aggr_dstipv4:
            inet_ntop(AF_INET, partHandle->data, ipv4, INET_ADDRSTRLEN);
            fprintf(output, "%s: ", ipv4);
            break;
        case aggr_srcipv6:
        case aggr_dstipv6:
            inet_ntop(AF_INET6, partHandle->data, ipv6, INET6_ADDRSTRLEN);
            fprintf(output, "%s: ", ipv6);
            break;
        case aggr_srcport:
        case aggr_dstport:
            fprintf(output, "%u: ", ntohs(*(uint16_t *) partHandle->data));
            break;
        case aggr_info:
            aggrMetrics = partHandle->data;

            fprintf(output, "%u ", aggrMetrics->packets);
            fprintf(output, "%u ", aggrMetrics->length);

            break;
        default:
            break;
    }

    if(output == stderr) {
        fprintf(stderr, "\n");
    }
}

// prints packet parts
void printPacket(packet_handle *packetHandle) {
    if (!packetHandle) return;

    // iterate through packet parts and print its content
    packet_part_handle *partHandle = packetHandle->parts;
    while (partHandle) {
        printPacketPart(partHandle, stdout);
        partHandle = partHandle->next;
    }

    printf("\n");
}

// print packet list
void printPacketList(packet_list *packetList, int32_t limit) {
    if (!packetList) return;
    if (!packetList->first) return;

    int32_t counter = 0;

    // iterate through packets in list and print them
    packet_handle *packetHandle = packetList->first;
    while (packetHandle) {
        if (limit >= 0 && counter >= limit) {
            break;
        }

        printPacket(packetHandle);

        counter++;
        packetHandle = packetHandle->next;
    }
}

// remove last packet in list
void removeLastPacket(packet_list *packetList) {
    if (!packetList) return;
    if (!packetList->last) return;

    packet_handle *deleted = packetList->last;
    if (packetList->first == packetList->last) {
        packetList->first = NULL;
    } else {
        deleted->prev->next = NULL;
    }

    packetList->last = deleted->prev;

    freePacket(deleted);
}

// returns part of given type in packet
packet_part_handle *findPacketPart(packet_handle *packetHandle, packet_part_type partType) {
    if (!packetHandle) return NULL;

    packet_part_handle *partHandle = packetHandle->parts;

    while (partHandle) {
        if (partHandle->type == partType) {
            return partHandle;
        }

        partHandle = partHandle->next;
    }

    return NULL;
}

// check, if packet matches given criteria
uint8_t packetMatches(packet_handle *packetHandle, uint8_t *srcMac, uint8_t *dstMac, uint8_t *srcIp, uint8_t *dstIp,
                      uint8_t protocolId, uint16_t identification) {
    uint8_t macsEquals = 0;
    uint8_t ipsEquals = 0;
    uint8_t protocolIdEquals = 0;
    uint8_t identificationEquals = 0;

    packet_part_handle *partHandle;
    ethernet_macs *ethernetFrame;
    struct ip *ipv4Frame;

    partHandle = findPacketPart(packetHandle, Tethernet_header);
    if (partHandle) {
        ethernetFrame = partHandle->data;
        macsEquals = (uint8_t) (bytesEqual(ethernetFrame->sourceMAC, srcMac, 6) &&
                                bytesEqual(ethernetFrame->destinationMAC, dstMac, 6));
    }

    // is used in reassembly function for IPv4 packets, so we dont need to add IPv6 version
    partHandle = findPacketPart(packetHandle, Tipv4_header);
    if (partHandle) {
        ipv4Frame = partHandle->data;
        ipsEquals = (uint8_t) (bytesEqual((uint8_t *) &((ipv4Frame->ip_src.s_addr)), srcIp, 4) &&
                               bytesEqual((uint8_t *) &((ipv4Frame->ip_dst.s_addr)), dstIp, 4));

        protocolIdEquals = (uint8_t) (protocolId == ipv4Frame->ip_p);
        identificationEquals = (uint8_t) (identification == ipv4Frame->ip_id);
    }

    return (uint8_t) (macsEquals && ipsEquals && protocolIdEquals && identificationEquals);
}

// free packet header and its data
void freePacket(packet_handle *packetHandle) {
    packet_part_handle *partHandle = packetHandle->parts;

    while (partHandle) {
        void *next = partHandle->next;

        free(partHandle->data);
        free(partHandle);

        partHandle = next;
    }

    free(packetHandle);
}

// free packet list and all data it contains
void freePacketList(packet_list *packetList) {
    packet_handle *packetHandle = packetList->first;

    while (packetHandle) {
        packet_handle *next = packetHandle->next;

        freePacket(packetHandle);
        packetHandle = next;
    }
}

// reassembles all fragmented IPv4 packets, removes fragments from list and replaces them by one whole packet
void reassemblePackets(packet_list *packetList) {
    if (!packetList) return;
    if (!packetList->first) return;

    // packet identifiers
    uint8_t srcMac[6];
    uint8_t dstMac[6];
    uint8_t srcIp[4];
    uint8_t dstIp[4];
    uint8_t protocolId = 0;
    uint16_t identification = 0;
    packet_handle *firstPacket = NULL;

    uint8_t fragmentsFound;

    // while no fragmented packets remains in list, repeat
    do {
        fragmentsFound = 0;

        // find first fragmented packet and copy fields, which identify it (srcmac, dstmac, srcip, dstip, protocolId, identification)
        packet_handle *packetHandle = packetList->first;
        while (packetHandle && !fragmentsFound) { // procházím packet list
            packet_part_handle *partHandle;
            ethernet_macs *ethernetFrame;
            struct ip *ipv4Frame;

            partHandle = findPacketPart(packetHandle, Tipv4_header);
            if (partHandle) {
                ipv4Frame = partHandle->data;

                uint16_t frag = ntohs(ipv4Frame->ip_off);
                uint8_t frag_mf = (uint8_t) ((frag & IP_MF) >> 13);
                uint16_t frag_off = (uint16_t) (frag & IP_OFFMASK);

                if (frag_off != 0 || frag_mf == 1) { // nalezen fragmentovaný packet
                    fragmentsFound = 1;

                    memcpy(srcIp, &(ipv4Frame->ip_src.s_addr), 4);
                    memcpy(dstIp, &(ipv4Frame->ip_dst.s_addr), 4);
                    protocolId = ipv4Frame->ip_p;
                    identification = ipv4Frame->ip_id;
                    firstPacket = packetHandle;

                    partHandle = findPacketPart(packetHandle, Tethernet_header);
                    if (partHandle) {
                        ethernetFrame = partHandle->data;

                        memcpy(srcMac, ethernetFrame->sourceMAC, 6);
                        memcpy(dstMac, ethernetFrame->destinationMAC, 6);
                    }
                }
            }

            packetHandle = packetHandle->next;
        }

        // fragmented packet have been found in this itereation
        if (fragmentsFound) {
            hole_list holeList;
            initHoleList(&holeList);
            addHole(&holeList, 0, BIG_INT);

            // malloc sufficient space for reassembled packet (size if chosen from mac packet size)
            uint8_t *reassembledPacket = malloc(BIG_INT);
            uint32_t reassembledPacketSize = 0;
            if (!reassembledPacket) {
                return;
            }

            // iterate through packet list and reassemble final packet from its fragments
            packet_part_handle *partHandle;
            packetHandle = packetList->first;
            packet_handle *nextPacketHandle = NULL;

            while (packetHandle) { // procházím packet list
                nextPacketHandle = packetHandle->next;
                struct ip *ipv4Frame;

                // is part of fragmented packet found before?
                if (packetMatches(packetHandle, srcMac, dstMac, srcIp, dstIp, protocolId, identification)) {
                    partHandle = findPacketPart(packetHandle, Tipv4_header);
                    if (partHandle) {
                        ipv4Frame = partHandle->data;

                        uint16_t frag = ntohs(ipv4Frame->ip_off);
                        uint8_t frag_mf = (uint8_t) ((frag & IP_MF) >> 13);
                        uint16_t frag_off = (uint16_t) (frag & IP_OFFMASK);

                        uint32_t byteOffset = (uint32_t) (frag_off * 8);

                        partHandle = findPacketPart(packetHandle, raw_data);
                        if (partHandle) {
                            memcpy(reassembledPacket + byteOffset, partHandle->data, partHandle->size);
                            uint32_t lastByte = (uint32_t) (byteOffset + partHandle->size - 1);
                            fillHole(&holeList, byteOffset, lastByte);

                            // if this is last fragment
                            if (!frag_mf) {
                                reassembledPacketSize = lastByte + 1;
                                fillHole(&holeList, reassembledPacketSize, BIG_INT);

                                // realloc reassembled packet to its size
                                uint8_t *newSizePacket = realloc(reassembledPacket, reassembledPacketSize);
                                if (newSizePacket) {
                                    reassembledPacket = newSizePacket;
                                } else {
                                    free(reassembledPacket);
                                    return;
                                }
                            }

                            // remove packet from list, if it is not the first found fragment (will be reused)
                            if (packetHandle != firstPacket) {

                                if (packetList->first == packetHandle) {
                                    packetList->first = packetHandle->next;
                                } else {
                                    packetHandle->prev->next = packetHandle->next;
                                }

                                if (packetList->last == packetHandle) {
                                    packetList->last = packetHandle->prev;
                                } else {
                                    packetHandle->next->prev = packetHandle->prev;
                                }

                                freePacket(packetHandle);
                            }
                        }
                    }
                }

                packetHandle = nextPacketHandle;
            }

            // replace raw data packet part with reassembled packet
            partHandle = findPacketPart(firstPacket, raw_data);
            if (partHandle) {
                free(partHandle->data);
                partHandle->data = reassembledPacket;
                partHandle->size = reassembledPacketSize;

                partHandle = findPacketPart(firstPacket, Tipv4_header);
                if (partHandle) {
                    ((struct ip *) (partHandle->data))->ip_off = 0;
                }
            }

            // packet wasn't fully reassembled
            if(holeList.first) {
                fprintf(stderr, "ERROR: Not enough fragments to reassemble whole packet with identification:\n");
                printPacketPart(findPacketPart(firstPacket, Tethernet_header), stderr);
                printPacketPart(findPacketPart(firstPacket, Tipv4_header), stderr);
                fprintf(stderr, "\t| Protocol ID: %u\n", protocolId);
                fprintf(stderr, "\t| Identification: %u\n", identification);

                if (packetList->first == firstPacket) {
                    packetList->first = firstPacket->next;
                    if (packetList->first) {
                        packetList->first->prev = NULL;
                    }
                } else {
                    firstPacket->prev->next = firstPacket->next;
                }

                if (packetList->last == firstPacket) {
                    packetList->last = firstPacket->prev;
                    if (packetList->last) {
                        packetList->last->next = NULL;
                    }
                } else {
                    firstPacket->next->prev = firstPacket->prev;
                }
            }

            freeHoleList(&holeList);
        }

    } while (fragmentsFound);
}

// finish parsing of all layers higher than IP layer
void finishPacketParsing(packet_list *packetList) {
    if (!packetList) return;
    if (!packetList->first) return;

    packet_handle *packetHandle = packetList->first;
    while (packetHandle) {
        packet_part_handle *ipV4Handle = findPacketPart(packetHandle, Tipv4_header);
        packet_part_handle *ipV6Handle = findPacketPart(packetHandle, Tipv6_header);
        packet_part_handle *rawDataHandle = findPacketPart(packetHandle, raw_data);

        uint8_t *tmp = NULL;
        unsigned long remainingPackets = 0;
        if (rawDataHandle && (ipV4Handle || ipV6Handle)) {
            tmp = rawDataHandle->data;
            remainingPackets = rawDataHandle->size;

            packet_part_handle *ipPartHandle = NULL;
            uint8_t protocolID = 0;

            if (ipV4Handle) {
                ipPartHandle = ipV4Handle;
                protocolID = ((struct ip *) ipV4Handle->data)->ip_p;
            } else {
                ipPartHandle = ipV6Handle;
                protocolID = *((uint8_t *) (ipV6Handle->data) + 6);
            }

            ipPartHandle->next = NULL;

            switch (protocolID) {
                case ICMPV4_PROTOCOL_CODE:
                    processICMPFrame(packetHandle, &tmp, &remainingPackets, Ticmpv4_header);
                    break;
                case ICMPV6_PROTOCOL_CODE:
                    processICMPFrame(packetHandle, &tmp, &remainingPackets, Ticmpv6_header);
                    break;
                case UDP_PROTOCOL_CODE:
                    processUDPFrame(packetHandle, &tmp, &remainingPackets);
                    break;
                case TCP_PROTOCOL_CODE:
                    processTCPFrame(packetHandle, &tmp, &remainingPackets);
                    break;
                default:
                    fprintf(stderr, "unknown protocolID/nextHeader code %u\n", protocolID);
                    break;
            }

            free(rawDataHandle->data);
            free(rawDataHandle);
        }

        packetHandle = packetHandle->next;
    }
}

// adds part to packet with its number
void addNumbersToPacketList(packet_list *packetList) {
    if (!packetList) return;
    if (!packetList->first) return;

    uint32_t counter = 0;

    packet_handle *packetHandle = packetList->first;
    while (packetHandle) {
        counter++;

        packet_part_handle *newPart = malloc(sizeof(packet_part_handle));
        if (!newPart) break;

        void *newData = malloc(sizeof(packet_numbering));
        if (!newData) {
            free(newPart);
            break;
        }

        memcpy(newData, &counter, sizeof(packet_numbering));
        newPart->type = numbering;
        newPart->size = sizeof(packet_numbering);
        newPart->data = newData;
        newPart->next = packetHandle->parts;

        packetHandle->parts = newPart;

        packetHandle = packetHandle->next;
    }
}

// returns packet with part of given value
packet_handle *findPacketWithPartValue(packet_list *packetList, packet_part_type partType, uint8_t *value,
                                       uint8_t dataLength) {
    if (!packetList) return NULL;
    if (!packetList->first) return NULL;

    packet_handle *packetHandle = packetList->first;
    packet_part_handle *partHandle = NULL;

    while (packetHandle) {
        partHandle = findPacketPart(packetHandle, partType);
        if (partHandle && bytesEqual(value, partHandle->data, dataLength)) {
            return packetHandle;
        }

        packetHandle = packetHandle->next;
    }

    return NULL;
}

// creates new list with aggregated packets data
void aggregatePackets(packet_list *packetList, TAggr_key aggregationKey) {
    if (aggregationKey == ak_undefined) {
        return;
    }

    packet_list aggregatedList;
    initPacketList(&aggregatedList);

    packet_handle *packetHandle = packetList->first;
    packet_handle *foundPacketHandle = NULL;
    ethernet_macs *ethernetFrame;
    struct ip *ipv4Frame;
    struct ip6_hdr *ipv6Frame;
    struct tcphdr *tcpFrame;

    packet_part_handle *packetInfoHandle;
    packet_part_handle *aggrPacketInfoHandle;
    packet_info *packetInfo;
    packet_info *aggrPacketInfo;
    packet_info newAggrPacketInfo;

    while (packetHandle) {
        packet_part_handle *partHandle = NULL;
        uint8_t *dataSource = NULL;
        uint8_t dataSize = 0;
        packet_part_type targetPart = aggr_srcmac;

        switch (aggregationKey) {
            case srcmac:
            case dstmac:
                partHandle = findPacketPart(packetHandle, Tethernet_header);
                if (partHandle) {
                    ethernetFrame = partHandle->data;
                    targetPart = aggregationKey == srcmac ? aggr_srcmac : aggr_dstmac;
                    dataSource = (aggregationKey == srcmac ? (uint8_t *) (ethernetFrame->sourceMAC)
                                                           : (uint8_t *) (ethernetFrame->destinationMAC));
                    dataSize = 6;
                }

                break;
            case srcip:
            case dstip:
                partHandle = findPacketPart(packetHandle, Tipv6_header);
                if (partHandle) { // IPv6
                    ipv6Frame = partHandle->data;

                    targetPart = aggregationKey == srcip ? aggr_srcipv6 : aggr_dstipv6;
                    dataSource = (aggregationKey == srcip ? (uint8_t *) &(ipv6Frame->ip6_src)
                                                          : (uint8_t *) &(ipv6Frame->ip6_dst));
                    dataSize = 16;
                } else {
                    partHandle = findPacketPart(packetHandle, Tipv4_header);
                    if (partHandle) { // IPv4
                        ipv4Frame = partHandle->data;

                        targetPart = aggregationKey == srcip ? aggr_srcipv4 : aggr_dstipv4;
                        dataSource = (aggregationKey == srcip ? (uint8_t *) &(ipv4Frame->ip_src.s_addr)
                                                              : (uint8_t *) &(ipv4Frame->ip_dst.s_addr));
                        dataSize = 4;
                    }
                }
                break;
            case srcport:
            case dstport:
                partHandle = findPacketPart(packetHandle, Ttcp_header);
                if (partHandle) {
                    tcpFrame = partHandle->data;

                    targetPart = aggregationKey == srcport ? aggr_srcport : aggr_dstport;
                    dataSource = (aggregationKey == srcport ? (uint8_t *) &(tcpFrame->th_sport)
                                                            : (uint8_t *) &(tcpFrame->th_dport));
                    dataSize = 2;
                }

                break;
            default:
                break;
        }

        if (dataSource && partHandle) {
            // look for packet with aggregated key val
            foundPacketHandle = findPacketWithPartValue(
                &aggregatedList,
                targetPart,
                dataSource,
                dataSize
            );

            if (foundPacketHandle) { // packet was found - just update statistics
                aggrPacketInfoHandle = findPacketPart(foundPacketHandle, aggr_info);
                packetInfoHandle = findPacketPart(packetHandle, info);

                if (aggrPacketInfoHandle && packetInfoHandle) {
                    aggrPacketInfo = (packet_info *) aggrPacketInfoHandle->data;
                    packetInfo = packetInfoHandle->data;

                    aggrPacketInfo->length += packetInfo->length;
                    aggrPacketInfo->packets += 1;
                }
            } else { // not found - create new aggregation list entry
                createPacket(&aggregatedList, NULL);

                addDataToLastPacket(
                    &aggregatedList,
                    targetPart,
                    dataSize,
                    dataSource
                );

                packetInfoHandle = findPacketPart(packetHandle, info);
                if (packetInfoHandle) {
                    packetInfo = packetInfoHandle->data;

                    newAggrPacketInfo.packets = 1;
                    newAggrPacketInfo.length = packetInfo->length;

                    addDataToLastPacket(&aggregatedList, aggr_info, sizeof(packet_info),
                                        &newAggrPacketInfo);
                }
            }
        }

        packetHandle = packetHandle->next;
    }

    freePacketList(packetList);

    packetList->first = aggregatedList.first;
    packetList->last = aggregatedList.last;
}

// returns packet handle of packet with biggest sortKey value, also removes it drom packetList
packet_handle *popMaxPacket(packet_list *packetList, TSort_Key sortKey) {
    uint32_t maxVal = 0;

    packet_handle *packetHandle = packetList->first;
    packet_handle *maxPacketHandle = packetHandle;
    packet_info *packetInfo = NULL;
    while (packetHandle) {
        packet_part_handle *partHandle = findPacketPart(packetHandle, info);
        partHandle = partHandle ? partHandle : findPacketPart(packetHandle, aggr_info);
        if (partHandle) {
            packetInfo = partHandle->data;

            if (sortKey == packets || sortKey == bytes) {
                uint8_t isNewBigger = 0;

                if (sortKey == packets) {
                    isNewBigger = (uint8_t) (packetInfo->packets > maxVal);
                    maxVal = isNewBigger ? packetInfo->packets : maxVal;
                } else if (sortKey == bytes) {
                    isNewBigger = (uint8_t) (packetInfo->length > maxVal);
                    maxVal = isNewBigger ? packetInfo->length : maxVal;
                }

                maxPacketHandle = isNewBigger ? packetHandle : maxPacketHandle;
            }
        }

        packetHandle = packetHandle->next;
    }

    if (packetList->first == maxPacketHandle) {
        packetList->first = maxPacketHandle->next;
        if (packetList->first) {
            packetList->first->prev = NULL;
        }
    } else {
        maxPacketHandle->prev->next = maxPacketHandle->next;
    }

    if (packetList->last == maxPacketHandle) {
        packetList->last = maxPacketHandle->prev;
        if (packetList->last) {
            packetList->last->next = NULL;
        }
    } else {
        maxPacketHandle->next->prev = maxPacketHandle->prev;
    }

    maxPacketHandle->prev = NULL;
    maxPacketHandle->next = NULL;

    return maxPacketHandle;
}

// sort packet list - all we need to do is to sort list with packet headers
void sortPacketList(packet_list *packetList, TSort_Key sortKey) {
    if (!packetList) return;
    if (!packetList->first) return;

    packet_list sortedList;
    initPacketList(&sortedList);

    packet_handle *maxPacketHandle = NULL;
    while (packetList->first) {
        maxPacketHandle = popMaxPacket(packetList, sortKey);
        appendPacket(&sortedList, maxPacketHandle);
    }

    freePacketList(packetList);
    packetList->first = sortedList.first;
    packetList->last = sortedList.last;
}








