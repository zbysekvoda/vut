/*
    project: ISA, packet analyzer, 2017/2018
    author: Zbyšek Voda, xvodaz01, BIT3
 */

#ifndef ISA_PROJ_PACKETS_GLOBAL_H
#define ISA_PROJ_PACKETS_GLOBAL_H

#define ICMPV4_MESSAGES_NO 11
#define ICMPV6_MESSAGES_NO 6
#define MAX_CODE_MESSAGES 7


// structure to store ICMP messages
typedef struct {
    int type;
    char *message;
    char codesNo;
    char *codes[MAX_CODE_MESSAGES];
} icmp_message;

// array with ICMPv4 messages
icmp_message icmpv4Messages[] = {
    {
        0,
        "echo reply",
        0,
        {NULL}
    },
    {
        3,
        "destination unreachable",
        6,
        {"net unreachable",                    "host unreachable",                "protocol unreachable",                                   "port unreachable", "fragmentation needed and DF set", "source route failed"}
    },
    {
        4,
        "source quench",
        0,
        {NULL}
    },
    {
        5,
        "redirect",
        4,
        {"redirect datagrams for the network", "redirect datagrams for the host", "redirect datagrams for the type of service and network", "redirect datagrams for the type of service and host"}
    },
    {
        8,
        "echo",
        0,
        {NULL}
    },
    {
        11,
        "time exceeded",
        2,
        {"time to live exceeded in transit",   "fragment reassembly time exceeded"}
    },
    {
        12,
        "parameter problem",
        1,
        {"pointer indicates the error"}
    },
    {
        13,
        "timestamp",
        0,
        {NULL}
    },
    {
        14,
        "timestamp reply",
        0,
        {NULL}
    },
    {
        15,
        "information request",
        0,
        {NULL}
    },
    {
        16,
        "information reply",
        0,
        {NULL}
    }
};

// array with ICMPv6 messages
icmp_message icmpv6Messages[] = {
    {
        1,
        "destination unreachable",
        7,
        {
            "no route to destination",
            "communication with destination administratively prohibited",
            "beyond scope of source address",
            "address unreachable",
            "port unreachable",
            "source address failed ingress/egress policy",
            "reject route to destination"
        }
    },
    {
        2,
        "packet too big",
        0,
        {NULL}
    },
    {
        3,
        "time exceeded",
        2,
        {
            "hop limit exceeded in transit",
            "fragment reassembly time exceeded"
        }
    },
    {
        4,
        "parameter problem",
        3,
        {
            "erroneous header field encountered",
            "unrecognized Next Header type encountered",
            "unrecognized ipv6 option encountered"
        }
    },
    {
        128,
        "echo request",
        0,
        {NULL}
    },
    {
        129,
        "echo reply",
        0,
        {NULL}
    }
};


#endif //ISA_PROJ_PACKETS_GLOBAL_H
