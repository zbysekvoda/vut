/*
    project: ISA, packet analyzer, 2017/2018
    author: Zbyšek Voda, xvodaz01, BIT3
 */

#include "hole.h"

// initialise hole list
void initHoleList(hole_list *list) {
    if (!list) return;

    list->first = NULL;
    list->last = NULL;
}

// free hole list
void freeHoleList(hole_list *list) {
    if (!list) return;

    hole_descriptor *h = list->first;

    while (h) {
        list->first = h->next;
        free(h);
        h = list->first;
    }

    list->first = NULL;
    list->last = NULL;
}

// returns first hole in list
hole_descriptor *getFirstHole(hole_list *list) {
    if (!list) return NULL;

    return list->first;
}

// returns last hole in list
hole_descriptor *getLastHole(hole_list *list) {
    if (!list) return NULL;

    return list->last;
}

// adds hole to the end of list
void addHole(hole_list *list, uint32_t first, uint32_t last) {
    if (!list) return;

    hole_descriptor *new = malloc(sizeof(hole_descriptor));
    if (!new) return;

    new->first = first;
    new->last = last;
    new->next = NULL;
    new->prev = NULL;

    hole_descriptor *lastHole = list->last;

    if (!lastHole) {
        list->first = new;
    } else {
        lastHole->next = new;
        new->prev = lastHole;
    }

    list->last = new;
}

// fills hole(s) between given limits (included)
void fillHole(hole_list *list, uint32_t first, uint32_t last) {
    if (!list) return;
    if (!list->first || !list->last) return;

    if (first >= last) {
        return;
    }

    hole_descriptor *start = list->first;
    hole_descriptor *stop = list->last;

    // find hole containing first boundary
    while (start && !(first >= start->first && first <= start->last)) {
        start = start->next;
    }

    // find hole containing last boundary
    while (stop && !(last >= stop->first && last <= stop->last)) {
        stop = stop->prev;
    }

    if (last < list->first->first || first > list->last->last) {
        return;
    }

    // this happens, when limit first is not in hole, but somewhere in existing parts
    // if it happens, find nearest suitable hole
    if (!start) {
        start = list->first;
        while (start && first > start->last) {
            start = start->next;
        }

        if (!start) {
            return;
        }

        first = start->first;
    }

    // this happens, when limit last is not in hole, but somewhere in existing parts
    // if it happens, find nearest suitable hole
    if (!stop) {
        stop = list->last;
        while (stop && last < stop->first) {
            stop = stop->prev;
        }

        if (!stop) {
            return;
        }

        last = stop->last;
    }

    if (start > stop) {
        return;
    }

    // delete all holes between start and stop hole (excuded)
    if (start != stop) {
        hole_descriptor *h = start->next;

        while (h != stop) {
            h = h->next;
            free(h->prev);
        }

        start->next = stop;
        stop->prev = start;
    }

    // if i am deleting one or more whole holes
    if (first == start->first && last == stop->last) {
        // is start hole first hole in list?
        if (list->first == start) {
            list->first = stop->next;
            if (list->first) {
                list->first->prev = NULL;
            }
        } else {
            start->prev->next = stop->next;
        }

        // is stop hole last hole in list?
        if (list->last == stop) {
            list->last = start->prev;
            if (list->last) {
                list->last->next = NULL;
            }
        } else {
            stop->next->prev = start->prev;
        }

        free(start);
        if (start != stop) {
            free(stop);
        }
    }
    else if (start == stop) { // if first and last limit are in same hole
        // we can now have three different situations
        // 1) first limit is the same as start of hole, last limit is different
        // 2) last limit is the same as end of hole, first limit are different
        // 3) both limits are different -> in this case wi will end with two new holes
        //      - we can reuse old hole and malloc new one

        if (first == start->first && last != stop->last) {
            start->first = (uint32_t) (last + 1);
        } else if (first != start->first && last == stop->last) {
            stop->last = (uint32_t) (first - 1);
        } else if (first != start->first && last != stop->last) {
            uint32_t memLast = start->last;
            start->last = (uint32_t) (first - 1);

            hole_descriptor *new = malloc(sizeof(hole_descriptor));
            if (!new) return;

            new->first = (uint32_t) (last + 1);
            new->last = memLast;
            new->prev = start;
            new->next = start->next;

            if (start->next) {
                start->next->prev = new;
            }
            start->next = new;

            if (list->last == stop) {
                list->last = new;
            }
        }
    }
    else { // start != stop
        // the situation repeat as previously described, but for two different holes

        if (first == start->first && last != stop->last) {
            if (list->first == start) { // první díra v seznamu
                list->first = start->next;
                list->first->prev = NULL;
            } else {
                start->prev->next = stop;
            }

            stop->prev = start->prev;
            stop->first = (uint32_t) (last + 1);

            free(start);
        } else if (first != start->first && last == stop->last) {
            if (list->last == stop) { // poslední díra v seznamu
                list->last = stop->prev;
                list->last->next = NULL;
            } else {
                stop->next->prev = start;
            }

            start->next = stop->next;
            start->last = (uint32_t) (first - 1);

            free(stop);
        } else if (first != start->first && last != stop->last) {
            start->last = (uint32_t) (first - 1);
            stop->first = (uint32_t) (last + 1);
        }
    }
}
