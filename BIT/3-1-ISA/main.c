/*
    project: ISA, packet analyzer, 2017/2018
    author: Zbyšek Voda, xvodaz01, BIT3
 */

#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <pcap.h>
#include <pcap/pcap.h>

#ifndef PCAP_ERRBUF_SIZE
#define PCAP_ERRBUF_SIZE (256)
#endif

#include "args.h"
#include "packets.h"

int main(int argc, char **argv) {
    // start with arguments processing
    Arguments arguments;
    initArgs(&arguments);
    processArgs(argc, argv, &arguments);

    // initialize list for packet store
    packet_list packetList;
    initPacketList(&packetList);

    // process every given file
    uint8_t filterValid = 1;
    for (int i = 0; i < arguments.inputFilesNumber; i++) {
        char *fileName = arguments.inputFiles[i];

        struct bpf_program filter;
        char errbuf[PCAP_ERRBUF_SIZE];
        pcap_t *file = pcap_open_offline(fileName, errbuf);

        if (file == NULL) {
            fprintf(stderr, "Error: can't open input file %s\n", fileName);
            continue;
        }

        // if filter is present, compile filter and set pcap library to use it
        if (filterValid && arguments.filterExpression) {
            if (pcap_compile(file, &filter, arguments.filterExpression, 0, PCAP_NETMASK_UNKNOWN) == -1) {
                fprintf(stderr, "ERROR: Bad filter: %s\n", pcap_geterr(file));
                filterValid = 0;
            } else if (pcap_setfilter(file, &filter) == -1) {
                fprintf(stderr, "ERROR: Can't set filter: %s\n", pcap_geterr(file));
                filterValid = 0;
            }
        }

        // start actual file processing using packetHandler callback
        if (pcap_loop(file, 0, packetHandler, (uint8_t *) &packetList) < 0) {
            fprintf(stderr, "Error: Reading of %s failed\n", fileName);
            continue;
        }

        pcap_close(file);
    }

    // check packet list for fragmented packets and reassemble them eventually
    reassemblePackets(&packetList);

    // we can finish packet parsing after reassembly
    finishPacketParsing(&packetList);

    // aggregate packets
    aggregatePackets(&packetList, arguments.aggrKey);

    // if aggregation is not used, add indexes to packets
    if (arguments.aggrKey == ak_undefined) {
        addNumbersToPacketList(&packetList);
    }

    // sort packets (sorting by packets for unagregated packets is without efect)
    if (arguments.sortKey == bytes || (arguments.sortKey == packets && arguments.aggrKey != ak_undefined)) {
        sortPacketList(&packetList, arguments.sortKey);
    }

    printPacketList(&packetList, arguments.limit);

    freePacketList(&packetList);

    return 0;
}
