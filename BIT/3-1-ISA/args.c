/*
    project: ISA, packet analyzer, 2017/2018
    author: Zbyšek Voda, xvodaz01, BIT3
 */

#include "args.h"

unsigned char sort_key_num = 2;

char *aggr_key_strings[] = {
    "srcmac",
    "dstmac",
    "srcip",
    "dstip",
    "srcport",
    "dstport"
};

char *sort_key_strings[] = {
    "packets",
    "bytes"
};

unsigned char aggr_key_num = 6;

// initialise arguments structure
void initArgs(Arguments *args) {
    args->help = 0;
    args->aggrKey = ak_undefined;
    args->sortKey = sk_undefined;
    args->limit = -1;
    args->filterExpression = NULL;
    args->inputFiles = NULL;
    args->inputFilesNumber = 0;
}

// compares given strings for -a and -s arguments, returns it index in predefined arrays if exists
int findArgCode(Argument arg, char *val) {
    if (arg == aggr_key) {
        for (int i = 0; i < aggr_key_num; i++) {
            if (strcmp(val, aggr_key_strings[i]) == 0) return i;
        }
    } else if (arg == sort_key) {
        for (int i = 0; i < sort_key_num; i++) {
            if (strcmp(val, sort_key_strings[i]) == 0) return i;
        }
    }

    return UNKNOWN_VAL;
}

// searches for argument occurance in arguments array
// returns its string value or empty string, if its argument without value (-h)
char *findArgVal(int argc, char **argv, char *name, unsigned char hasValue, int *lastParamIndex) {
    for (int i = argc - 1; i > 0; i--) {
        if (strcmp(name, argv[i]) == 0) {
            if (!hasValue) {
                *lastParamIndex = *lastParamIndex < i ? i : *lastParamIndex;
                return ""; // is truthy
            } else if (i < argc - 1) {
                *lastParamIndex = *lastParamIndex < i + 1 ? i + 1 : *lastParamIndex;
                return argv[i + 1];
            }
        }
    }

    return NULL;
}

// checks, if -s and -a arguments have known value
void checkKnownArgs(Arguments *args) {
    if (args->aggrKey == ak_unknown) {
        fprintf(stderr, "Error: Invalid aggregation key\n");
        exit(0);
    }

    if (args->sortKey == sk_unknown) {
        fprintf(stderr, "Error: Invalid sort key\n");
        exit(0);
    }
}

// checks filename validity
char checkFilenames(int from, int to, char **args) {
    for(int i = from; i <= to; i++){
        if(args[i] && args[i][0] == '-'){
            fprintf(stderr, "Invalid argument or file name %s\n", args[i]);
            exit(0);
        }
    }

    return 1;
}

// parses given arguments array, fills arguments strucure used in program
void processArgs(int argc, char **argv, Arguments *args) {
    int lastParamIndex = 0;

    char *aggrKeyStr = findArgVal(argc, argv, "-a", 1, &lastParamIndex);
    char *sortKeyStr = findArgVal(argc, argv, "-s", 1, &lastParamIndex);
    char *limitStr = findArgVal(argc, argv, "-l", 1, &lastParamIndex);

    args->help = (char)(findArgVal(argc, argv, "-h", 0, &lastParamIndex) ? 1 : 0);
    if(args->help){
        printf("== Isashark ==\nIsashark is tool for offline packet analysis and is able to process pcap files. It supports main protocols from TCP/IP family.\nThese protocols are: \n\t- ethernet protocol (including IEEE 802.1Q headers)\n\t- IPv4 protocol\n\t- IPv6 protocol (including extension headers)\n\t- ICMP protocols v4 and v6\n\t- TCP protocol\n\t- UDP protocol\n\n== Usage ==\nisashark [-h] [-a aggr-key] [-s sort-key] [-l limit] [-f filter-expression] file …\n\n-h\tshow help and exit\n\n-a\tset aggregation by given key\n\tsupported key values are:\n\t\tsrcmac\taggregate by source MAC address\n\t\tdstmac\taggregate by destination MAC address\n\t\tsrcip\taggregate by source IP address\n\t\tdstip\taggregate by destination IP address\n\t\tsrcport\taggregate by source port\n\t\tdstport\taggregate by destination port\n\taggregation by ports have effect only for packets with port provided (ICMP eg. don't have port)\n\t\n-s\tset sorting by given key\n\tsorting is always in descending order\n\tsupported key values are\n\t\tpackets\tsort by packet numbers (will have effect only for aggregated packets)\n\t\tbytes \tsort by packet size or size sum for aggregated packets\n\n-l\tmaximum number of packets to be printed out\n\n-f\tpacket filter selection\n\tonly packets which match filter are processed\n\tfor more filter syntax info see man pcap-filter(7)\n\nfile... \tjeden, nebo více vstupních souborů ve formátu pcap\n\n== Usage examples ==\nsingle file analysis\n\tisashark file\n\nmultiple files analysis\n\tisashark file1 file2 ...\n\nlimit packet output\n\tisashark -l 3 file \n\naggregate by source IP\n\tisashark -a srcip file\n\nsort by packet size\n\tisashark -s bytes file\n\naggregate by source IP, sort by packets number\n\tisashark -a srcip -s packets file\n\nuse filter - print only packets from IPv6 address 2001:db8::1\n\tisashark -f \"src host 2001:db8::1\" file\n");
        exit(0);
    }

    args->aggrKey = aggrKeyStr ? (TAggr_key) findArgCode(aggr_key, aggrKeyStr) : ak_undefined;
    args->sortKey = sortKeyStr ? (TSort_Key) findArgCode(sort_key, sortKeyStr) : sk_undefined;
    args->limit = limitStr ? (int32_t) strtol(limitStr, NULL, 10) : -1;
    args->filterExpression = findArgVal(argc, argv, "-f", 1, &lastParamIndex);

    checkKnownArgs(args);
    if(checkFilenames(lastParamIndex + 1, argc - 1, argv)){
        args->inputFiles = &argv[lastParamIndex + 1];
        args->inputFilesNumber = argc - lastParamIndex - 1;

        if(args->inputFilesNumber <= 0){
            fprintf(stderr, "Error: Enter input files\n");
            exit(0);
        }
    }
}
