/*
    project: ISA, packet analyzer, 2017/2018
    author: Zbyšek Voda, xvodaz01, BIT3
 */

#ifndef ISA_PROJ_ARGS_H
#define ISA_PROJ_ARGS_H

#define UNKNOWN_VAL 98
#define UNDEFINED_VAL 99

#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

typedef enum {
    help,
    aggr_key,
    sort_key,
    limit,
    filterExpression
} Argument;

typedef enum {
    srcmac,
    dstmac,
    srcip,
    dstip,
    srcport,
    dstport,
    ak_unknown = UNKNOWN_VAL,
    ak_undefined = UNDEFINED_VAL
} TAggr_key;

typedef enum {
    packets,
    bytes,
    sk_unknown = UNKNOWN_VAL,
    sk_undefined = UNDEFINED_VAL
} TSort_Key;


typedef struct {
    int8_t help;
    TAggr_key aggrKey;
    TSort_Key sortKey;
    int32_t limit;
    char *filterExpression;
    char **inputFiles;
    int32_t inputFilesNumber;
} Arguments;


int findArgCode(Argument arg, char *val);

char *findArgVal(int argc, char **argv, char *name, unsigned char hasValue, int *lastParamIndex);

void checkKnownArgs(Arguments *args);

char checkFilenames(int from, int to, char **args);

void processArgs(int argc, char **argv, Arguments *args);

void initArgs(Arguments *args);

#endif //ISA_PROJ_ARGS_H
