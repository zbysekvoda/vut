/*
    project: ISA, packet analyzer, 2017/2018
    author: Zbyšek Voda, xvodaz01, BIT3
 */

#include "packets.h"
#include "frame_processor.h"

// base function for packet parsing
// it returns pointer to demanded parts and decrements remainingBytes counter, also moves pointer to source parts by given length
// this function protects reading from non-authorised addresses
// it can be used for lookup too, which means, that it just checks parts boundaries, return demanded pointer and not decrements counter nor moves parts pointer
void *getData(uint8_t **data, unsigned n, unsigned long *remainingBytes, char lookup) {
    if (*remainingBytes < n) {
        fprintf(stderr, "DEBUG Error: Not enough bytes to read (%u needed, %lu remains)\n", n, *remainingBytes);
        exit(0);
    }

    void *out = *data;

    if (!lookup) {
        *data = *data + n;
        *remainingBytes -= n;
    }

    return out;
}

// reads vlan frame, extracts useful parts and adds them to last packet of packet list
char processVlanFrame(packet_list *packetList, uint8_t **nextDataPointer, unsigned long *remainingBytes) {
    uint8_t *vlanTags = getData(nextDataPointer, VLAN_TAG_SIZE, remainingBytes, 0);
    uint16_t secondHalf = ntohs(*(uint16_t *) (vlanTags + 2));

    vlan_frame vlanFrame;
    vlanFrame.TPID = ntohs(*(uint16_t *) vlanTags);
    vlanFrame.PCP = (uint8_t) ((secondHalf >> PCP_SHIFT) & PCP_MASK);
    vlanFrame.DEI = (uint8_t) ((secondHalf >> DEI_SHIFT) & DEI_MASK);
    vlanFrame.VID = (uint8_t) (secondHalf & VID_MASK);

    return addDataToLastPacket(packetList, Tvlan_header, sizeof(vlan_frame), &vlanFrame);
}

// reads IPv4 frame and adds it to last packet of packet list
char processIPv4Frame(packet_list *packetList, uint8_t **nextDataPointer, unsigned long *remainingBytes) {
    struct ip *ipFrame = getData(nextDataPointer, sizeof(struct ip), remainingBytes, 1);

    // skip BYTES of ipv4 header
    getData(nextDataPointer, ipFrame->ip_hl * 4, remainingBytes, 0);

    return addDataToLastPacket(packetList, Tipv4_header, sizeof(struct ip), ipFrame);
}

// reads TCP frame and adds it to the end of given packet
char processTCPFrame(packet_handle *packetHandle, uint8_t **nextDataPointer, unsigned long *remainingBytes) {
    struct tcphdr *tcpFrame = getData(nextDataPointer, sizeof(struct tcphdr), remainingBytes, 0);
    return addDataToPacket(packetHandle, Ttcp_header, sizeof(struct tcphdr), tcpFrame);
}

// reads UDP frame and adds it to the end of given packet
char processUDPFrame(packet_handle *packetHandle, uint8_t **nextDataPointer, unsigned long *remainingBytes) {
    struct udphdr *udpFrame = getData(nextDataPointer, sizeof(struct udphdr), remainingBytes, 0);
    return addDataToPacket(packetHandle, Tudp_header, sizeof(struct udphdr), udpFrame);
}

// skips all IPv6 extension headers in remaining parts
char skipIPv6ExtensionHeaders(uint8_t **nextDataPointer, unsigned long *remainingBytes, uint8_t *nextHeader) {
    (void) nextDataPointer;
    (void) remainingBytes;

    while (*nextHeader != NO_NXT_HDR_CODE && *nextHeader != TCP_PROTOCOL_CODE && *nextHeader != UDP_PROTOCOL_CODE &&
           *nextHeader != ICMPV6_PROTOCOL_CODE && *remainingBytes) {

        unsigned int length = 0;

        switch (*nextHeader) {
            case HOP_BY_HOP_CODE:
            case DST_OPTS_CODE:
            case ROUTING_CODE:
                length = 16;
                break;
            case FRAGMENT_CODE:
                length = 8;
                break;
            case MOBILITY_CODE:
                length = *((uint8_t *) getData(nextDataPointer, 2, remainingBytes, 1) + 1);
                length += 1;
                length *= 8;

                break;
            default:
                length = 2;
                break;
        }

        *nextHeader = *(uint8_t *) getData(nextDataPointer, length, remainingBytes, 0);
    }

    return 1;
}

// reads IPv6 frame and  adds it to last packet of packet list
char processIPv6Frame(packet_list *packetList, uint8_t **nextDataPointer, unsigned long *remainingBytes) {
    uint8_t nextHeader;
    struct ip6_hdr *ipFrame = getData(nextDataPointer, sizeof(struct ip6_hdr), remainingBytes, 0);

    nextHeader = ipFrame->ip6_ctlun.ip6_un1.ip6_un1_nxt;

    skipIPv6ExtensionHeaders(nextDataPointer, remainingBytes, &nextHeader);

    *((uint8_t *) ipFrame + 6) = nextHeader;

    return addDataToLastPacket(packetList, Tipv6_header, sizeof(struct ip6_hdr), ipFrame);
}

// reads icmp and adds it to given packet
char processICMPFrame(packet_handle *packetHandle, uint8_t **nextDataPointer, unsigned long *remainingBytes, packet_part_type icmpType) {
    struct icmp *icmpFrame = getData(nextDataPointer, ICMP_SIZE, remainingBytes, 0);

    return addDataToPacket(packetHandle, icmpType, ICMP_SIZE, icmpFrame);
}
