/*
    project: ISA, packet analyzer, 2017/2018
    author: Zbyšek Voda, xvodaz01, BIT3
 */

#ifndef ISA_FRAME_PROCESSOR_H
#define ISA_FRAME_PROCESSOR_H

#include <stdint.h>
#include <inttypes.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>

#include <netinet/ip6.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <arpa/inet.h>

void *getData(uint8_t **data, unsigned n, unsigned long *remainingPackets, char lookup);

char processVlanFrame(packet_list *packetList, uint8_t **nextDataPointer, unsigned long *remainingPackets);

char processIPv4Frame(packet_list *packetList, uint8_t **nextDataPointer, unsigned long *remainingPackets);

char processTCPFrame(packet_handle *packetHandle, uint8_t **nextDataPointer, unsigned long *remainingPackets);

char processUDPFrame(packet_handle *packetHandle, uint8_t **nextDataPointer, unsigned long *remainingPackets);

char skipIPv6ExtensionHeaders(uint8_t **nextDataPointer, unsigned long *remainingPackets, uint8_t *nextHeader);

char processIPv6Frame(packet_list *packetList, uint8_t **nextDataPointer, unsigned long *remainingPackets);

char processICMPFrame(packet_handle *packetHandle, uint8_t **nextDataPointer, unsigned long *remainingBytes, packet_part_type icmpType);

#endif //ISA_FRAME_PROCESSOR_H
