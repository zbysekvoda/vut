//
//  Settings.swift
//  IZA
//
//  Created by Zbyšek Voda on 06.05.18.
//  Copyright © 2018 Zbyšek Voda. All rights reserved.
//

import UIKit

class Settings {
    var signal: [String:Int]?
    var measurement: [String:Int]?
    var equipment: [String:Int]?
    
    init(from: String, signal: Any, measurement: Any, equipment: Any) {
        if(from == "structure") {
            let _signal = signal as! [SettingsItem]
            let _measurement = measurement as! [SettingsItem]
            let _equipment = equipment as! [SettingsItem]
            
            self.signal = [
                "type": _signal[0].numValue,
                "freqFrom": _signal[1].numValue,
                "freqTo": _signal[2].numValue,
                "freqStep": _signal[3].numValue
            ]
            
            self.measurement = [
                "repeat": _measurement[0].numValue,
                "period": _measurement[1].numValue
            ]
            
            self.equipment = [
                "electrodes": _equipment[0].numValue,
                "dish": _equipment[1].numValue
            ]
            
        }
        else if(from == "dictionary") {
            let _signal = signal as? [String: Int]
            let _measurement = measurement as? [String: Int]
            let _equipment = equipment as? [String: Int]
            
            self.signal = Util.unwrapOptionalDictionaryOrCreate(dict: _signal)
            self.measurement = Util.unwrapOptionalDictionaryOrCreate(dict: _measurement)
            self.equipment = Util.unwrapOptionalDictionaryOrCreate(dict: _equipment)
        }
    }
}
