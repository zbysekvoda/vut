//
//  SettingsItem.swift
//  IZA
//
//  Created by Zbyšek Voda on 06.05.18.
//  Copyright © 2018 Zbyšek Voda. All rights reserved.
//

import Foundation

enum SettingsItemType {
    case Text
    case Number
    case Select
}

class SettingsItem {
    var label: String
    var numValue: Int
    var stringValue: String
    var inputType: SettingsItemType
    var values: [String]?
    
    convenience init(_ label: String, _ value: Any, _ type: SettingsItemType) {
        self.init(label, value, type, nil)
    }
    
    init(_ label: String, _ value: Any, _ type: SettingsItemType, _ values: [String]?) {
        if(type == .Text) {
            self.numValue = 0
            self.stringValue = value as! String
        }
        else {
            self.numValue = value as! Int
            self.stringValue = ""
        }
        
        self.label = label
        self.inputType = type
        self.values = values
    }
}
