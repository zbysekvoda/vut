//
//  Util.swift
//  IZA
//
//  Created by Zbyšek Voda on 06.05.18.
//  Copyright © 2018 Zbyšek Voda. All rights reserved.
//

import Foundation

class Util {
    static func unwrapOptionalDictionaryOrCreate<T1,T2>(dict: [T1:T2]?) -> [T1:T2] {
        if let d = dict {
            return d
        }
        
        return [T1:T2]()
    }
    
    static func toJSON(dict: [String: Int]?) -> String {
        if let _dict = dict {
            var out = "{"
            
            for (i, entry) in _dict.enumerated() {
                out += "\"\(entry.key)\": \(entry.value)"
                if i < _dict.count - 1 {
                    out += ", "
                }
            }
            
            out += "}"
            
            return out
        }
        else {
            return "{}"
        }
    }
    
    static func toJSON(array: [Double]?) -> String {
        var str: String = ""
        
        if let _a = array {
            let inner = _a.map{ String($0) };
            str = inner.joined(separator: ", ")
        }
        
        return "[\(str)]"
    }
    
    static func toJSON(array: [[Double]]?) -> String {
        var str: String = ""
        
        if let _a = array {
            str = _a
                .map( { "\t" + self.toJSON(array: $0) } )
                .joined(separator: ",\n")
        }
        
        return "[\n\(str)\n]"
    }
    
    static func jsonToArray(_ data: Data?) -> [[String:AnyObject]]? {
        if let jsonData = data {
            do {
                let z = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)
                return z as? [[String:AnyObject]]
            }
            catch {
                return nil
            }
        }
        
        return nil
    }
    
    static func jsonToDict(_ data: Data?) -> [String:AnyObject]? {
        if let jsonData = data {
            do {
                let z = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)
                
                return z as? [String:AnyObject]
            }
            catch {
                return nil
            }
        }
        
        return nil
    }
    
    static func getPoints(_ a: AnyObject) -> [[Double]] {
        guard let x = a as? [AnyObject] else {
            return [[]]
        }
        
        return x.map({_vector in
            guard let vector = _vector as? [String: AnyObject] else {
                return []
            }
            
            var out = [Double]()
            
            let keys = Array(vector.keys).sorted()
            
            for key in keys {
                guard let obj = vector[key] as? [String: AnyObject] else {
                    return []
                }
                
                let _parsed = Double(obj["|Z|"] as! String)
                
                guard let parsed = _parsed else {
                    return []
                }
                
                out.append(parsed)
            }
            
            return out
        })
    }
}
