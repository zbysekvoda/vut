//
//  Measurement.swift
//  IZA
//
//  Created by Zbyšek Voda on 06.05.18.
//  Copyright © 2018 Zbyšek Voda. All rights reserved.
//

import UIKit

class Measurement {
    var name: String
    var settings: Settings
    var data: [[Double]]?
    var timestamp: Int
    
    init(name: String, timestamp: Int, settings: Settings, data: [[Double]]?) {
        self.name = name
        self.settings = settings
        self.data = data
        self.timestamp = timestamp
    }
    
    init(name: String, timestamp: Int, settings: Settings) {
        self.name = name
        self.settings = settings
        self.timestamp = timestamp
    }
    
    init(structure: [[SettingsItem]], timestamp: Int?) {
        self.name = structure[0][0].stringValue
        self.settings = Settings(from: "structure", signal: structure[1], measurement: structure[2], equipment: structure[3])
        self.data = nil
        
        if let ts = timestamp {
            self.timestamp = ts
        }
        else {
            self.timestamp = 0
        }
    }
    
    func toJson() -> String {
        return """
        {
            "name": "\(name)",
            "timestamp": \(timestamp),
            "signal": \(Util.toJSON(dict: settings.signal)),
            "measurement": \(Util.toJSON(dict: settings.measurement)),
            "equipment": \(Util.toJSON(dict: settings.equipment)),
            "data": \(Util.toJSON(array: data))
        }
        """
    }
}
