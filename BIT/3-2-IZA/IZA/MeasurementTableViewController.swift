//
//  MeasurementTableViewController.swift
//  IZA
//
//  Created by Zbyšek Voda on 06.05.18.
//  Copyright © 2018 Zbyšek Voda. All rights reserved.
//

import UIKit

class MeasurementTableViewController: UITableViewController {
    
    @IBOutlet weak var textInput: UITextField!
    
    var defaultSettings = [
        [""],
        [0, 5000, 6000, 1000],
        [1, 1],
        [2, 10]
    ]
    
    var name = SettingsItem("Name", "", SettingsItemType.Text)

    var type = SettingsItem("Measure", 0, SettingsItemType.Select, ["Range", "Point"]) // 0 = range, 1 = point
    var freqFrom = SettingsItem("Freqency from [Hz]", 0, SettingsItemType.Number)
    var freqTo = SettingsItem("Freqency to [Hz]", 0, SettingsItemType.Number)
    var freqStep = SettingsItem("Freqency step [Hz]", 0, SettingsItemType.Number)
    
    var aqRepeat = SettingsItem("Repeat [x]", 0, SettingsItemType.Number)
    var aqPeriod = SettingsItem("Period [s]", 0, SettingsItemType.Number)
    
    var eqElectrodes = SettingsItem("Electrodes", 0, SettingsItemType.Select, ["2", "4"]) // 2/4
    var eqDish = SettingsItem("Dish diameter [mm]", 0, SettingsItemType.Number)
    
    var structure = [[SettingsItem]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.structure = [
            [self.name],
            [self.type, self.freqFrom, self.freqTo, self.freqStep],
            [self.aqRepeat, self.aqPeriod],
            [self.eqElectrodes, self.eqDish]
        ]
        
        setDefaultSettings()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            if self.type.numValue == 0 {
                return 4
            }
            
            return 2
        }
        
        if section == 2 {
            if self.aqRepeat.numValue == 1 {
                return 1
            }
            
            return 2
        }
        
        return structure[section].count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Table view cells are reused and should be dequeued using a cell identifier.
        
        let settings = self.structure[indexPath.section][indexPath.row]
        
        let cell: UITableViewCell
        if settings.inputType == .Number || settings.inputType == .Text {
            let _cell = tableView.dequeueReusableCell(withIdentifier: "InputTableViewCell", for: indexPath) as! InputTableViewCell
            
            _cell.initWith(settingsObject: settings)
            _cell.setCallback(callback: update)
            
            cell = _cell
        }
        else {
            let _cell = tableView.dequeueReusableCell(withIdentifier: "SelectTableViewCell", for: indexPath) as! SelectTableViewCell
            
            _cell.initWith(settingsObject: settings)
            _cell.setCallback(callback: update)
        
            cell = _cell
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "General"
        case 1:
            return "Signal"
        case 2:
            return "Data collection"
        case 3:
            return "Equipment"
        default:
            return ""
        }
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        print(segue)
        
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        print("ABCD")
    }
 
    
    //MARK: UI
    @IBAction func startNewMeasurement(_ sender: UIBarButtonItem) {
        
        let timestamp = Int(Date().timeIntervalSince1970) * 1000
        let measurement = Measurement(structure: self.structure, timestamp: timestamp)
        let tbvc = tabBarController as! MainViewController
        tbvc.measurements.insert(measurement, at: 0)
        
        print(measurement.toJson())
        
        let url = URL(string: "http://mylinkit.local:1337/api/result/" + String(timestamp))!
        let urlSession = URLSession.shared
        var postRequest = URLRequest(url: url)
        postRequest.httpMethod = "POST"
        postRequest.httpBody = measurement.toJson().data(using: .utf8)
        postRequest.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        let task = urlSession.dataTask(with: postRequest as URLRequest, completionHandler: { data, response, error in
            guard error == nil else { return }
            //guard let data = data else { return }
        })
        task.resume()
        
        setDefaultSettings()
        tbvc.selectedIndex = 0
    }
    
    func update () {
        tableView.reloadData()
    }
    
    //MARK: Private methods
    private func setDefaultSettings() {
        for (x, _) in self.structure.enumerated() {
            for (y, obj) in self.structure[x].enumerated() {
                if(obj.inputType == .Text) {
                    obj.stringValue = self.defaultSettings[x][y] as! String
                }
                else {
                    obj.numValue = self.defaultSettings[x][y] as! Int
                }
            }
        }
        
        tableView.reloadData()
    }
}
