//
//  MainViewController.swift
//  IZA
//
//  Created by Zbyšek Voda on 06.05.18.
//  Copyright © 2018 Zbyšek Voda. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController {
    /*
    var m1 = Measurement(
        name: "Sweep",
        settings: Settings(
            from: "dictionary",
            signal: [
                "type": 0,
                "freqFrom": 5000,
                "freqTo": 11000,
                "freqStep": 1000
            ],
            measurement: [
                "repeat": 2,
                "period": 10
            ],
            equipment: [
                "electrodes": 2,
                "dish": 4
            ]
        ),
        data: [
            [1, 3, 0, 4, 5, 2, 20],
            [10, 30, 20, 11, 13, 18, 10]
        ]
    )
    
    var m2 = Measurement(
        name: "Sweep",
        settings: Settings(
            from: "dictionary",
            signal: [
                "type": 1,
                "freqFrom": 5000,
                "freqTo": 0,
                "freqStep": 0
            ],
            measurement: [
                "repeat": 5,
                "period": 10
            ],
            equipment: [
                "electrodes": 2,
                "dish": 4
            ]
        ),
        data: [
            [1],
            [3],
            [6],
            [2],
            [4]
        ]
    )

    */
    var measurements = [Measurement]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        //measurements.append(m1)
        //measurements.append(m2)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
