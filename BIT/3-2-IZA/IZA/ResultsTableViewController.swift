//
//  ResultsTableViewController.swift
//  IZA
//
//  Created by Zbyšek Voda on 06.05.18.
//  Copyright © 2018 Zbyšek Voda. All rights reserved.
//

import UIKit

class ResultsTableViewController: UITableViewController, UINavigationControllerDelegate {
    var results = [Measurement]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        DispatchQueue.global().async {
            // https://github.com/dwyl/learn-apple-watch-development/issues/34
            let url = URL(string: "http://mylinkit.local:1337/api/results")!
            let urlSession = URLSession.shared
            let getRequest = URLRequest(url: url)
            
            let task = urlSession.dataTask(with: getRequest as URLRequest, completionHandler: { data, response, error in
                guard error == nil else { return }
                guard let data = data else { return }
                
                var measurements = [Measurement]()
                
                if let measurementsRawArray = Util.jsonToArray(data) {
                    measurementsRawArray.forEach({measurement in
                        if measurement["calibration"] == nil {
                            if let _signal = measurement["signal"],
                                let _measurement = measurement["measurement"],
                                let _equipment = measurement["equipment"],
                                let _timestamp = measurement["timestamp"],
                                let _timestampString = _timestamp as? Int,
                                let _name = measurement["name"],
                                let _nameString = _name as? String {
                                
                                let settings = Settings(from:"dictionary", signal: _signal, measurement: _measurement, equipment: _equipment)
                                let newMeasurement = Measurement(name: _nameString, timestamp: _timestampString, settings: settings);
                                
                                measurements.append(newMeasurement)
                            }
                        }
                    });
                    
                    DispatchQueue.main.async {
                        self.results = measurements
                        self.tableView.reloadData()
                    }
                }
            })
            task.resume()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.results.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResultsTableViewCell", for: indexPath) as! ResultsTableViewCell

        cell.initWith(measurement: self.results[indexPath.row])
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if (segue.identifier ?? "") == "ShowResult" {
            guard let resultView = segue.destination as? ResultViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            
            guard let selectedMeasurement = sender as? ResultsTableViewCell else {
                fatalError("Unexpected sender")
            }
            
            guard let indexPath = tableView.indexPath(for: selectedMeasurement) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            resultView.measurementObj = self.results[indexPath.row]
        }
    }
}
