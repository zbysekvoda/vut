//
//  ResultsTableViewCell.swift
//  IZA
//
//  Created by Zbyšek Voda on 06.05.18.
//  Copyright © 2018 Zbyšek Voda. All rights reserved.
//

import UIKit

class ResultsTableViewCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var frequency: UILabel!
    @IBOutlet weak var measurement: UILabel!
    @IBOutlet weak var electrodes: UILabel!
    @IBOutlet weak var dish: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func initWith(measurement measurementObject: Measurement) {
        let _settings = measurementObject.settings
        
        guard _settings.signal != nil else {
            return
        }
        
        guard _settings.measurement != nil else {
            return
        }
        
        guard _settings.equipment != nil else {
            return
        }
        
        name.text = measurementObject.name
        
        let _signal = _settings.signal!
        let freqFrom = String(describing: _signal["freqFrom"] ?? 0)
        
        if(_signal["type"] == 0) {
            let freqTo = String(describing: _signal["freqTo"] ?? 0)
            let freqStep = String(describing: _signal["freqStep"] ?? 0)
            
            frequency.text = "\(freqFrom) - \(freqTo) Hz, step \(freqStep) Hz"
        }
        else {
            frequency.text = "\(freqFrom) Hz"
        }
        
        let _measurement = _settings.measurement!
        
        if _measurement["repeat"]! <= 1 {
            measurement.text = "Once"
        }
        else {
            let rep = String(describing: _measurement["repeat"] ?? 0)
            let per = String(describing: _measurement["period"] ?? 0)
            
            measurement.text = "Repeat \(rep)x, period: \(per) s"
        }
        
        let _equipment = _settings.equipment!
        let _dish = String(describing: _equipment["dish"] ?? 0)
        dish.text = "\(_dish) mm"
        
        let _electrodes = String(describing: _equipment["electrodes"] ?? 0)
        electrodes.text = "\(_electrodes) electrodes"
    }
}
