//
//  SelectTableViewCell.swift
//  IZA
//
//  Created by Zbyšek Voda on 06.05.18.
//  Copyright © 2018 Zbyšek Voda. All rights reserved.
//

import UIKit

class SelectTableViewCell: UITableViewCell {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var selection: UISegmentedControl!
    
    var initialised = false
    
    var settings: SettingsItem?
    var changeCallback: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        selection.removeAllSegments()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initWith(settingsObject: SettingsItem) {
        if(initialised) {
            return
        }
        
        label.text = settingsObject.label
        
        self.settings = settingsObject
        
        for (i, s) in settingsObject.values!.enumerated() {
            selection.insertSegment(withTitle: s, at: i, animated: false)
        }
        
        selection.selectedSegmentIndex = 0
        initialised = true
    }
    
    func setCallback(callback: @escaping () -> Void) {
        self.changeCallback = callback
    }
    
    @IBAction func selectionChanged(_ sender: UISegmentedControl) {
        guard let _settings = settings else { return }
        guard let values = _settings.values else { return }
        
        let index = selection.selectedSegmentIndex;
        let str = values[index]
        
        print(str)
        
        if let val = Int(str) {
            _settings.numValue = val
        }
        else {
            _settings.numValue = selection.selectedSegmentIndex
        }
        
        print(_settings.numValue)
        
        self.changeCallback!()
    }
}
