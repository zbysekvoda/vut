//
//  InputTableViewCell.swift
//  IZA
//
//  Created by Zbyšek Voda on 06.05.18.
//  Copyright © 2018 Zbyšek Voda. All rights reserved.
//

import UIKit

class InputTableViewCell: UITableViewCell, UITextFieldDelegate {
    @IBOutlet private weak var label: UILabel!
    @IBOutlet private weak var textInput: UITextField!
    
    var settings: SettingsItem?
    var changeCallback: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        textInput.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func initWith(settingsObject: SettingsItem) {
        self.settings = settingsObject
        
        label.text = settingsObject.label
        
        if(settingsObject.inputType == .Number) {
            textInput.text = String(settingsObject.numValue)
        }
        else {
            textInput.text = settingsObject.stringValue
        }
    }
    
    func setCallback(callback: @escaping () -> Void) {
        self.changeCallback = callback
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let settings = self.settings {
            if(settings.inputType == .Number) {
                if let str = textField.text {
                    if let val = Int(str) {
                        settings.numValue = val
                    }
                }
            }
            else {
                if let str = textField.text {
                    settings.stringValue = str
                }
            }
        }
        
        self.changeCallback!()
    }
}
