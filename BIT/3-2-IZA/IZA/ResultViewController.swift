//
//  ResultViewController.swift
//  IZA
//
//  Created by Zbyšek Voda on 07.05.18.
//  Copyright © 2018 Zbyšek Voda. All rights reserved.
//

import UIKit
import SwiftCharts

class ResultViewController: UIViewController {
    @IBOutlet weak var frequency: UILabel!
    @IBOutlet weak var measurement: UILabel!
    @IBOutlet weak var electrodes: UILabel!
    @IBOutlet weak var dish: UILabel!
    @IBOutlet weak var name: UILabel!
    
    var measurementObj: Measurement?
    var chart: LineChart?;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //print(measurementObj!.toJson())
        
        DispatchQueue.global().async {
            guard let _ms = self.measurementObj else { return }
            
            let url = URL(string: "http://mylinkit.local:1337/api/result/" + String(_ms.timestamp))!
            let urlSession = URLSession.shared
            let getRequest = URLRequest(url: url)
            
            let task = urlSession.dataTask(with: getRequest as URLRequest, completionHandler: { data, response, error in
                guard error == nil else { return }
                guard let data = data else { return }
                
                if let measurementsRawArray = Util.jsonToDict(data) {
                    if let _signal = measurementsRawArray["signal"],
                        let _measurement = measurementsRawArray["measurement"],
                        let _equipment = measurementsRawArray["equipment"],
                        let _timestamp = measurementsRawArray["timestamp"],
                        let _timestampString = _timestamp as? Int,
                        let _name = measurementsRawArray["name"],
                        let _nameString = _name as? String,
                        let _vectors = measurementsRawArray["vectors"]{
                        
                        let settings = Settings(from:"dictionary", signal: _signal, measurement: _measurement, equipment: _equipment)
                        let data = Util.getPoints(_vectors)
                        let measurement = Measurement(name: _nameString, timestamp: _timestampString, settings: settings, data: data);
                        
                        DispatchQueue.main.async {
                            self.measurementObj = measurement
                            self.displayView()
                        }
                    }
                }
            })
            task.resume()
        }
        
        displayView()
    }
    
    func displayView() {
        if let _measurement = measurementObj {
            let screenSize = UIScreen.main.bounds
            let screenWidth = screenSize.width
            
            let _settings = _measurement.settings
            
            guard _settings.signal != nil else {
                return
            }
            
            guard _settings.measurement != nil else {
                return
            }
            
            guard _settings.equipment != nil else {
                return
            }
            
            name.text = _measurement.name
            
            let _signal = _settings.signal!
            let freqFrom = String(describing: _signal["freqFrom"] ?? 0)
            
            if(_signal["type"] == 0) {
                let freqTo = String(describing: _signal["freqTo"] ?? 0)
                let freqStep = String(describing: _signal["freqStep"] ?? 0)
                
                frequency.text = "\(freqFrom) - \(freqTo) Hz\nstep \(freqStep) Hz"
            }
            else {
                frequency.text = "\(freqFrom) Hz"
            }
            
            let _measurementSet = _settings.measurement!
            
            if _measurementSet["repeat"]! <= 1 {
                measurement.text = "Once"
            }
            else {
                let rep = String(describing: _measurementSet["repeat"] ?? 0)
                let per = String(describing: _measurementSet["period"] ?? 0)
                
                measurement.text = "Repeat \(rep)x, period: \(per) s"
            }
            
            let _equipment = _settings.equipment!
            let _dish = String(describing: _equipment["dish"] ?? 0)
            dish.text = "\(_dish) mm"
            
            let _electrodes = String(describing: _equipment["electrodes"] ?? 0)
            electrodes.text = "\(_electrodes) electrodes"
            
            guard let _data = _measurement.data else { return }
            guard let a = _settings.signal else { return }
            guard var _from = a["freqFrom"] else { return }
            guard var _to = a["freqTo"] else { return }
            guard var _step = a["freqStep"] else { return }
            guard let _type = a["type"] else { return }
            
            func doubleAToPoints(p: (offset: Int, element: Double)) -> (Double, Double) {
                var x: Double = 0;
                
                if(_type == 1) {
                    _from = 0
                    _step = 1
                }
                
                x = Double(_from) + Double(p.offset) * Double(_step)
                
                return (x, p.element)
            }
            
            func vectToChartVect(vector: [Double]) -> (chartPoints: [(Double, Double)], color: UIColor) {
                return (
                    chartPoints: vector.enumerated().map(doubleAToPoints),
                    color: UIColor.red
                )
            }
            
            func findMax (_ a: [Double]) -> Double {
                var max: Double = -Double.infinity;
                
                for x in a {
                    if x > max {
                        max = x
                    }
                }
                
                return max
            }
            
            func findMin (_ a: [Double]) -> Double {
                var min: Double = Double.infinity;
                
                for x in a {
                    if x < min {
                        min = x
                    }
                }
                
                return min
            }
            
            func totalMin (_ a: [[Double]]) -> Double {
                return findMin(a.map(findMin))
            }
            
            func totalMax (_ a: [[Double]]) -> Double {
                return findMax(a.map(findMax))
            }
            
            func transformData (type: Int, data: [[Double]]) -> [(chartPoints: [(Double, Double)], color: UIColor)] {
                var out: [[Double]] = data
                
                if(type == 1) {
                    let a = data.map({$0[0]})
                    out = [a]
                }
                
                return out.map(vectToChartVect)
            }
            
            var to: Double = 0;
            var from: Double = 0;
            var step: Double = 0;
            
            if(a["type"] == 0) {
                from = Double(_from) - step
                to = Double(_to) + step
            }
            else {
                from = Double(0)
                to = Double(_data.count)
            }
            
            let _min = floor(totalMin(_data))
            let _max = ceil(totalMax(_data))
            let _diff = _max - _min
            let min = _min - _diff/10
            let max = _max + _diff/5
            let by = ceil((max - min) / 10)
            
            step = ceil((to - from) / 5)
            
            let chartConfig = ChartConfigXY(
                xAxisConfig: ChartAxisConfig(from: from, to: to, by: step),
                yAxisConfig: ChartAxisConfig(from: min, to: max, by: by)
            )
            
            let frame = CGRect(x: 10, y: 80, width: screenWidth - 30, height: 380)
            
            let data = transformData(type: a["type"]!, data: _data)
            
            let chart = LineChart(
                frame: frame,
                chartConfig: chartConfig,
                xTitle: "X axis",
                yTitle: "Y axis",
                lines: data
            )
            
            self.view.addSubview(chart.view)
            self.chart = chart;
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
