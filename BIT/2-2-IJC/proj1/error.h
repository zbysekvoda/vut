// error.h
// Řešení IJC-DU1 2), 26.3.2017
// Autor: Zbyšek Voda, FIT, xvodaz01
// Přeloženo: gcc 5.4.0

#ifndef IJC_ERROR_H
#define IJC_ERROR_H

void warning_msg(const char *fmt, ...);

void error_msg(const char *fmt, ...);

#endif //IJC_ERROR_H
