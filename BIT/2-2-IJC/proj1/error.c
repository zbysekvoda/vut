// error.c
// Řešení IJC-DU1 2), 26.3.2017
// Autor: Zbyšek Voda, FIT, xvodaz01
// Přeloženo: gcc 5.4.0

#include "error.h"
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

// prints message fmt to stderr using values from l
void print_msg(const char *fmt, va_list l){
    fprintf(stderr, "CHYBA: ");
    vfprintf(stderr, fmt, l);

    va_end(l);
}

void warning_msg(const char *fmt, ...){
    va_list va_params;
    va_start(va_params, fmt);

    print_msg(fmt, va_params);
}

void error_msg(const char *fmt, ...){
    va_list va_params;
    va_start(va_params, fmt);

    print_msg(fmt, va_params);

    exit(1);
}
