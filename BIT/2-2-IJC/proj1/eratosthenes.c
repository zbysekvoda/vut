// eratosthenes.c
// Řešení IJC-DU1 1), 26.3.2017
// Autor: Zbyšek Voda, FIT, xvodaz01
// Přeloženo: gcc 5.4.0

#include <stdio.h>
#include <math.h>

#include "bit_array.h"
#include "eratosthenes.h"

void Eratosthenes(bit_array_t pole){
    // sets zero and one bits to one (these are not prime numbers)
    ba_set_bit(pole, 0, 1);
    ba_set_bit(pole, 1, 1);

    unsigned long index = 2;
    unsigned long size = pole[0] * sizeof(unsigned long) * CHAR_BIT;
    double limit = sqrt(size); // counts the limit for eratisthenes

    do {
        while(ba_get_bit(pole, index)){ // finds next zero bit
            index++;
        }

        for(unsigned long i = 2 * index; i < size; i += index){
            ba_set_bit(pole, i, 1);
        }

        index++;
    } while (index <= limit);
}
