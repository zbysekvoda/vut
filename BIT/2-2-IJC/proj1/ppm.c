// ppm.c
// Řešení IJC-DU1 2), 26.3.2017
// Autor: Zbyšek Voda, FIT, xvodaz01
// Přeloženo: gcc 5.4.0

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include "ppm.h"
#include "error.h"

// checks, if file continues with string needle followed by whitespace
char readStringWithWS(FILE *file, const char *needle) {
    for (unsigned char i = 0; i < strlen(needle); i++) {
        if (getc(file) != needle[i]) return 0;
    }

    if (!isspace(getc(file))) return 0;

    return 1;
}

// reads number from file
int readNumber(FILE *file) {
    char buf[NUMBER_BUFFER];
    memset(buf, 0, NUMBER_BUFFER);

    unsigned char counter = 0;

    int c;
    do {
        c = getc(file);

        if (!isspace(c)) {
            buf[counter] = c;
        } else {
            break;
        }

        counter++;
    } while (counter < NUMBER_BUFFER - 1);

    if (counter == NUMBER_BUFFER - 1) return 0;

    return atoi(buf);
}

// reads image to structure ppm
struct ppm *ppm_read(const char *filename) {
    FILE * file;
    int x = 0;
    int y = 0;
    size_t expectedSize = 0;

    // open file
    file = fopen(filename, "r");
    if (!file)
        error_msg("Soubor %s nenalezen.\n", filename);

    // search for string "P6" on its beginning
    if (!readStringWithWS(file, "P6"))
        error_msg("Retezec \"P6\" specifikujici verzi ppm nenealezen.\n", filename);

    // read x size
    x = readNumber(file);
    if (x == 0)
        error_msg("Cislo specifikujici velikost v ose x nenalezeno.\n", filename);

    // check max x size
    if (x > X_LIMIT) {
        warning_msg("Souradnice X je vetsi, nez povolena (maximum je %d)\n", filename, X_LIMIT);
        return NULL;
    }

    // read y size
    y = readNumber(file);
    if (y == 0)
        error_msg("Cislo specifikujici velikost v ose Y nenalezeno.\n", filename);

    // check max x size
    if (y > Y_LIMIT) {
        warning_msg("Souradnice Y je vetsi, nez povolena (maximum je %d)\n", filename, Y_LIMIT);
        return NULL;
    }

    // search for string "255" in file
    if (!readStringWithWS(file, "255"))
        error_msg("Retezec \"255\" specifikujici typ dat nenalezen.\n", filename);

    // counts expected size of image
    expectedSize = sizeof(struct ppm) + x * y * 3 * (sizeof(char));

    // create structure for ppm image
    struct ppm *image = malloc(expectedSize);
    if(!image){
        warning_msg("Malloc error for image structure.\n", filename);
        return NULL;
    }

    image->xsize = x;
    image->ysize = y;

    for(unsigned i = 0; i < expectedSize; i++){
        int c = getc(file);

        if(c == EOF)
            break;

        image->data[i] = c;
    }

    return image;
}

// write structure ppm to file filename
int ppm_write(struct ppm *p, const char *filename) {
    FILE * file;
    char buf[NUMBER_BUFFER];
    memset(buf, 0, NUMBER_BUFFER);

    file = fopen(filename, "w");
    if(!file){
        warning_msg("Problem pri zapisu souboru %s.\n", filename);
        return -1;
    }

    fputs("P6 ", file);
    sprintf(buf, "%u ", p->xsize);
    fputs(buf, file);
    memset(buf, 0, NUMBER_BUFFER);
    sprintf(buf, "%u ", p->ysize);
    fputs(buf, file);
    fputs("255\n", file);

    for(unsigned long i = 0; i < p->xsize * p-> ysize * 3; i++){
        fputc(p->data[i], file);
    }
    fputc(EOF, file);

    return 0;
}
