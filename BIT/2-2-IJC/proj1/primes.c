// primes.c
// Řešení IJC-DU1 1), 26.3.2017
// Autor: Zbyšek Voda, FIT, xvodaz01
// Přeloženo: gcc 5.4.0

#include <stdio.h>

#include "bit_array.h"
#include "eratosthenes.h"

#define ARRAY_SIZE 303000000L

int main() {
    static ba_create(ba, ARRAY_SIZE);
    Eratosthenes(ba);

    // creates array for ten primes less than 303000000
    unsigned long primes[10];
    unsigned long index = ARRAY_SIZE - 1;

    unsigned long counter = 0;
    while(counter < 10){
        if(ba_get_bit(ba, index) == 0){
            counter++;
            primes[10 - counter] = index;
        }

        index--;
    }

    for(int i = 0; i < 10; i++){
        printf("%lu\n", primes[i]);
    }

    return 0;
}
