// bit_array.h
// Řešení IJC-DU1 1), 26.3.2017
// Autor: Zbyšek Voda, FIT, xvodaz01
// Přeloženo: gcc 5.4.0

#ifndef IJC_BIT_ARRAY_H
#define IJC_BIT_ARRAY_H

#include <limits.h>
#include "error.h"


typedef unsigned long bit_array_t[];

// returns number of bits used by unsigned long
#define BITS_IN_UL (sizeof(unsigned long) * CHAR_BIT)

// returns bit mask for one bit on position index (from left)
#define BIT_MASK(sizeBytes, index) (1UL << (((sizeBytes) * CHAR_BIT) - ((index) % ((sizeBytes) * CHAR_BIT)) - 1))

// returns bit on position index in bit array name
#define BA_GET_BIT_(name, index) \
    ((name[ (index) / (sizeof(name[0]) * CHAR_BIT) + 1] & BIT_MASK(sizeof(*name), index)) == 0 ? 0 : 1)

// sets bit to value val on position index in bit array name
#define BA_SET_BIT_(name, index, val) \
    do { \
        if(val == 0) (name[(index) / (sizeof(name[0]) * CHAR_BIT) + 1]) &= ~BIT_MASK(sizeof(*name), index); \
        else (name[(index) / (sizeof(name[0]) * CHAR_BIT) + 1]) |= BIT_MASK(sizeof(*name), index); \
    } while(0)

// counts how many unsigned longs will be needed ho host size bytes
#define UL_USED(size) (size / BITS_IN_UL) + ((size % BITS_IN_UL) > 0 ? 1 : 0)

// creates bit array name of size size+1 (because first element is used to hold data about array size)
#define ba_create(name, size) \
    unsigned long name[UL_USED(size) + 1] = {UL_USED((size)), 0L, }

#ifdef USE_INLINE /////////////////////////////////////////////////////////////////////////////////////////////

inline unsigned long ba_size(bit_array_t name){
    return name[0] * BITS_IN_UL;
}

inline void ba_set_bit(bit_array_t name, unsigned long index, int val){
    if(index < ba_size(name))
        BA_SET_BIT_((name), (index), (val));
    else
        error_msg("Index %lu mimo rozsah 0..%lu", (unsigned long)(index), (unsigned long)ba_size(name));
}

inline int ba_get_bit(bit_array_t name, unsigned long index){
    if(index >= ba_size(name))
        error_msg("Index %lu mimo rozsah 0..%lu", (unsigned long)(index), (unsigned long)ba_size(name));

    return BA_GET_BIT_(name, index);
}

#else /////////////////////////////////////////////////////////////////////////////////////////////////////////

#define ba_size(name) \
    (name[0] * BITS_IN_UL)

#define ba_set_bit(name, index, val) \
    do{ \
        if((index) >= 0 && (index) < ba_size(name)) BA_SET_BIT_((name), (index), (val)); \
        else error_msg("Index %lu mimo rozsah 0..%lu", (unsigned long)(index), (unsigned long)ba_size(name)); \
    } while(0)

#define ba_get_bit(name, index) \
     (((index) >= 0 && (index) < ba_size(name)) == 0 ? \
        error_msg("Index %lu mimo rozsah 0..%lu", (unsigned long)(index), (unsigned long)ba_size(name)), 0 : \
        (BA_GET_BIT_(name, index)))



#endif //USE_INLINE ////////////////////////////////////////////////////////////////////////////////////////////

#endif //IJC_BIT_ARRAY_H
