// ppm.h
// Řešení IJC-DU1 2), 26.3.2017
// Autor: Zbyšek Voda, FIT, xvodaz01
// Přeloženo: gcc 5.4.0

#ifndef IJC_PPM_H
#define IJC_PPM_H

#define NUMBER_BUFFER 11
#define X_LIMIT 1000
#define Y_LIMIT 1000

struct ppm {
    unsigned xsize;
    unsigned ysize;
    char data[];
};

struct ppm * ppm_read(const char * filename);
int ppm_write(struct ppm *p, const char * filename);

#endif //IJC_PPM_H
