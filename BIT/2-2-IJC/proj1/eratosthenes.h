// eratosthenes.h
// Řešení IJC-DU1 1), 26.3.2017
// Autor: Zbyšek Voda, FIT, xvodaz01
// Přeloženo: gcc 5.4.0

#ifndef IJC_ERATOSTHENES_H
#define IJC_ERATOSTHENES_H

#include "bit_array.h"

void Eratosthenes(bit_array_t pole);

#endif //IJC_ERATOSTHENES_H
