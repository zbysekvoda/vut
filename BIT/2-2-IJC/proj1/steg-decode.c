// steg-decode.c
// Řešení IJC-DU1 2), 26.3.2017
// Autor: Zbyšek Voda, FIT, xvodaz01
// Přeloženo: gcc 5.4.0

#include <stdio.h>
#include <stdlib.h>

#include "error.h"
#include "ppm.h"
#include "bit_array.h"
#include "eratosthenes.h"

// counts prime numbers in bit array ba, which are less than treshold
unsigned countPrimesLessThan(bit_array_t ba, unsigned treshold){
    unsigned i = 0;
    unsigned primesCouter = 0;

    while(i < treshold){
        if(ba_get_bit(ba, i) == 0) primesCouter++;
        i++;
    }

    return primesCouter;
}

int main(int argc, char *argv[]){
    if(argc != 2){
        warning_msg("Input file name of image.\n");
        return 0;
    }

    // create bit array and find prime numbers
    ba_create(ba, X_LIMIT * Y_LIMIT * 3);
    Eratosthenes(ba);

    // load image to structure image
    struct ppm *image = ppm_read(argv[1]);
    if(!image){
        return 0;
    }

    unsigned long bytesInImage = image->xsize * image->ysize * 3; // how many bytes is there in image
    unsigned long noOfPrimes = countPrimesLessThan(ba, bytesInImage); // number of prime indexed bytes in image

    char *message = malloc(noOfPrimes + 1); // creates space big enough to host every byte of message

    unsigned long primeBytesLoaded = 0;
    unsigned long bytesLoaded = 0;
    unsigned char bitsAddedInByte = 0;

    while(bytesLoaded < bytesInImage){ // while there are bytes to be red
        if(!ba_get_bit(ba, bytesLoaded)){ // value of bytesLoaded is prime number
            char bit = image->data[bytesLoaded] & 1; // LSb of actual byte
            unsigned long bitPosition = primeBytesLoaded - 2 * bitsAddedInByte + CHAR_BIT - 1; // counts actual bit position downway (7,6,5,4,3,2,1,0,15,14,13,12,...)

            BA_SET_BIT_(message, bitPosition, bit); // sets bit on

            primeBytesLoaded++;
            bitsAddedInByte = (bitsAddedInByte + 1) % CHAR_BIT; // keeps counting position of added bit in new byte

            // if we have formed whole byte, test, if this bite is is equal to \0 - end if it is
            if(bitsAddedInByte == 0 && message[primeBytesLoaded / 8] == '\0')
                break;
        }

        bytesLoaded++;
    }

    // prints loaded hidden message (first element of array holds information about array size, so we must skip it)
    printf("%s\n", message + 1);

    free(image);
    free(message);
}
