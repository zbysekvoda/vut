/*
 IJC 2016/17
 Project:   DU2

 Author:    Zbysek Voda, xvodaz01
 Faculty:   FIT
 Part:      2
 Compiler:  GCC 5.4.0 on Merlin
 Date:      26.4.2017
*/

#include "htab.h"

// finds or adds item with given key
htab_listitem *htab_lookup_add(htab *t, char *key) {
    unsigned int index;
    htab_listitem *found = NULL;
    htab_listitem *lastExistingItemOnIndex = NULL;

    if (t == NULL || key == NULL) return NULL;

    index = hash_function(key) % t->arr_size;
    found = search_on_index_add(t->data[index], key, &lastExistingItemOnIndex);

    if (found == NULL) {
        found = malloc(sizeof(htab_listitem));
        if (found == NULL) {
            return NULL;
        }

        found->data = 0;
        found->key = malloc((strlen(key) + 1) * sizeof(char));
        if (found->key == NULL) {
            free(found);
            return NULL;
        }

        strcpy(found->key, key);
        found->next = NULL;

        if (lastExistingItemOnIndex == NULL) {
            t->data[index] = found;
        } else {
            lastExistingItemOnIndex->next = found;
        }

        t->n++;
    }

    return found;
}
