/*
 IJC 2016/17
 Project:   DU2

 Author:    Zbysek Voda, xvodaz01
 Faculty:   FIT
 Part:      1a
 Compiler:  GCC 5.4.0 on Merlin
 Date:      26.4.2017
*/

/* tail <in.txt
 * tail -n 20 <in.txt
 * tail in.txt
 * tail -n 20 in.txt
 */


#define _GNU_SOURCE
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define DEFAULT_N_LINES 10

// Parses arguments from argv. Tests, if file exists, ...
int loadParams(int argc, char *argv[], int *n, FILE **file) {
    int err = 0; // 0 = OK, 1 = argumens error, 2 = file error
    int fileIndex = 0;

    if (argc == 1) {
        *n = DEFAULT_N_LINES;
        *file = stdin;
    } else if (argc == 2) {
        if (argv[1][0] == '-') {
            err = 1;
        } else {
            fileIndex = 1;
        }
        *n = DEFAULT_N_LINES;
    } else if (argc == 3) {
        if (strcmp(argv[1], "-n") != 0) {
            err = 1;
        } else {
            *n = atoi(argv[2]);
            if (errno != 0) {
                err = 1;
            }
        }
        *file = stdin;
    } else if (argc == 4) {
        if (strcmp(argv[1], "-n") == 0) {
            *n = atoi(argv[2]);
            fileIndex = 3;
        }
        else if (strcmp(argv[2], "-n") == 0) {
            *n = atoi(argv[3]);
            fileIndex = 1;
        }

        if (errno != 0) {
            err = 1;
        }
    }

    if (err != 0 && errno == 0) {
        fprintf(stderr, "Possible tail run combinations:\n");
        fprintf(stderr, "\ttail <in.txt\n");
        fprintf(stderr, "\ttail -n 20 <in.txt\n");
        fprintf(stderr, "\ttail in.txt\n");
        fprintf(stderr, "\ttail -n 20 in.txt\n");
        return err;
    }

    if(err == 0 && *n < 0){
        fprintf(stderr, "-n must be bigger or equal to 0\n");
        err = 1;
        return err;
    }


    if(fileIndex != 0){
        if(access(argv[fileIndex], F_OK ) == -1) {
            err = 2;
        }
        else {
            *file = fopen(argv[fileIndex], "r");
            if (*file == NULL) {
                err = 2;
            }
        }
    }

    return err;
}

// Allocates space for pointer array where loaded lines will be stored.
char **createArrayOfPointers(int n){
    char **out = malloc(n * sizeof(char *));
    if(out == NULL) return NULL;

    for(int i = 0; i < n; i++){
        out[i] = NULL;
    }

    return out;
}

// Reads file line by line and stores pointers to these lines to array lines
int loadFileToLines(FILE* file, char *lines[], int n, int *posOfStart){
    int actual = 0;

    size_t len = 0;
    char *linep = NULL;

    while((getline(&linep, &len, file)) != -1){
        if(lines[actual] != NULL){
            free(lines[actual]);
            lines[actual] = NULL;
        }

        lines[actual] = linep;
        linep = NULL;
        actual = (actual + 1) % n;
    }

    *posOfStart = actual;
    free(linep);

    return 0;
}

// Prints array of lines to stdout
void printLines(char ** lines, int n, int posOfStart){
    for(int i = posOfStart; i < posOfStart + n; i++){
        if(lines[i % n] == NULL) continue;
        printf("%s", lines[i % n]);
    }
}

// Frees allocated space for lines from file and array lines itself
void freeLines(char **lines, int n){
    for(int i = 0; i < n; i++){
        free(lines[i]);
    }
    free(lines);
}

int main(int argc, char *argv[]) {
    int n = 0;
    FILE *file = NULL;

    errno = 0;
    int status = loadParams(argc, argv, &n, &file);
    if(errno != 0 || status != 0) goto error_lbl;

    char **lines = createArrayOfPointers(n);
    if(lines == NULL) goto error_lbl_with_close;

    int posOfStart = 0;
    loadFileToLines(file, lines, n, &posOfStart);

    printLines(lines, n, posOfStart);
    freeLines(lines, n);

    fclose(file);
    return 0;

    error_lbl_with_close:
        fclose(file);
    error_lbl:

    if(errno != 0){
        perror("ERROR");
    }
    return errno;
}
