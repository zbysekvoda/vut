/*
 IJC 2016/17
 Project:   DU2

 Author:    Zbysek Voda, xvodaz01
 Faculty:   FIT
 Part:      2
 Compiler:  GCC 5.4.0 on Merlin
 Date:      26.4.2017
*/

#ifndef IJC2_IO_H
#define IJC2_IO_H

#include <stdio.h>
#include <ctype.h>

#define MAX_WORD_LEN 256

int get_word(char *s, int max, FILE *f);

#endif //IJC2_IO_H
