/*
 IJC 2016/17
 Project:   DU2

 Author:    Zbysek Voda, xvodaz01
 Faculty:   FIT
 Part:      2
 Compiler:  GCC 5.4.0 on Merlin
 Date:      26.4.2017
*/

#include "io.h"

// reads one word from given file
int get_word(char *s, int max, FILE *f) {
    if (f == NULL || s == NULL) return EOF;

    static int lengthWarningPrinted = 0;
    int i = 0;
    char c;

    int wordFound = 0;
    while (!wordFound) {
        i = 0;
        while (i < max - 1 && (c = fgetc(f)) != EOF && !isspace(c)) {
            s[i++] = c;
        }
        s[i] = '\0';

        if(i != 0 || c == EOF){
            wordFound = 1;
        }
    }

    if (i == max - 1) {
        if(lengthWarningPrinted == 0){
            fprintf(stderr, "WARNING: Max word length is %d\n", MAX_WORD_LEN - 1);
            lengthWarningPrinted = 1;
        }
        while (!isspace(fgetc(f)));
    }

    return c == EOF ? EOF : i;
}
