/*
 IJC 2016/17
 Project:   DU2

 Author:    Zbysek Voda, xvodaz01
 Faculty:   FIT
 Part:      2
 Compiler:  GCC 5.4.0 on Merlin
 Date:      26.4.2017
*/

#include "htab.h"

// moves one table to new of given size
htab *htab_move(unsigned int newsize, htab *t2) {
    htab_listitem *h = NULL;
    htab *t = htab_init(newsize);
    if (t == NULL) return NULL;

    if (t2 == NULL) return NULL;

    for (unsigned int i = 0; i < t2->arr_size; i++) {
        h = t2->data[i];

        while (h != NULL) {
            htab_listitem *new = htab_lookup_add(t, h->key);
            if(new == NULL){
                htab_free(t);
                return NULL;
            }

            new->data = h->data;

            h = h->next;
            t2->n--;
        }

        t2->data[i] = NULL;
    }

    htab_clear(t2);

    return t;
}
