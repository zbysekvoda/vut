/*
 IJC 2016/17
 Project:   DU2

 Author:    Zbysek Voda, xvodaz01
 Faculty:   FIT
 Part:      2
 Compiler:  GCC 5.4.0 on Merlin
 Date:      26.4.2017
*/

#include "htab.h"

// searchs for key occurance in linear list
htab_listitem *search_on_index_add(htab_listitem *item, char *key, htab_listitem **lastExistingItemOnIndex) {
    htab_listitem *h = item;
    *lastExistingItemOnIndex = NULL;

    while (h != NULL && strcmp(h->key, key) != 0) {
        *lastExistingItemOnIndex = h;
        h = h->next;
    }

    return h;
}
