/*
 IJC 2016/17
 Project:   DU2

 Author:    Zbysek Voda, xvodaz01
 Faculty:   FIT
 Part:      2
 Compiler:  GCC 5.4.0 on Merlin
 Date:      26.4.2017
*/

#include "htab.h"

// finds item in hash table
htab_listitem *htab_find(htab *t, char *key) {
    unsigned int index;
    htab_listitem *lastExistingItemOnIndex = NULL;
    htab_listitem *found = NULL;

    if (t == NULL || key == NULL) return NULL;

    index = hash_function(key) % t->arr_size;
    found = search_on_index_add(t->data[index], key, &lastExistingItemOnIndex);

    return found;
}
