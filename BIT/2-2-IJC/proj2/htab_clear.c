/*
 IJC 2016/17
 Project:   DU2

 Author:    Zbysek Voda, xvodaz01
 Faculty:   FIT
 Part:      2
 Compiler:  GCC 5.4.0 on Merlin
 Date:      26.4.2017
*/

#include "htab.h"

// frees memory used by items in hash table
void htab_clear(htab *t) {
    htab_listitem *h = NULL;

    if (t == NULL) return;

    for (unsigned int i = 0; i < t->arr_size; i++) {
        h = t->data[i];

        while (h != NULL) {
            htab_listitem *next = h->next;

            free(h->key);
            free(h);

            h = next;

            t->n--;
        }

        t->data[i] = NULL;
    }
}
