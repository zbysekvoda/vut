/*
 IJC 2016/17
 Project:   DU2

 Author:    Zbysek Voda, xvodaz01
 Faculty:   FIT
 Part:      2
 Compiler:  GCC 5.4.0 on Merlin
 Date:      26.4.2017
*/

#include "htab.h"

// frees space occupied by hash_table
void htab_free(htab *t) {
    if (t == NULL) return;

    htab_clear(t);
    free(t);
}
