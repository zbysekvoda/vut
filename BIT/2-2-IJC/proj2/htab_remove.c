/*
 IJC 2016/17
 Project:   DU2

 Author:    Zbysek Voda, xvodaz01
 Faculty:   FIT
 Part:      2
 Compiler:  GCC 5.4.0 on Merlin
 Date:      26.4.2017
*/

#include "htab.h"

// removes item of given key from hash tab
bool htab_remove(htab *t, char *key) {
    htab_listitem *before = NULL;
    htab_listitem *h = NULL;
    unsigned int index;

    if (t == NULL || key == NULL) return NULL;

    index = hash_function(key) % t->arr_size;
    h = search_on_index_add(t->data[index], key, &before);

    if (h == NULL) {
        return false;
    }

    if (before == NULL) {
        t->data[index] = h->next;
    } else {
        before->next = h->next;
    }

    free(h->key);
    free(h);

    t->n--;

    return true;
}
