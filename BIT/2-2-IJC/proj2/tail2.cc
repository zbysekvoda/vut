/*
 IJC 2016/17
 Project:   DU2

 Author:    Zbysek Voda, xvodaz01
 Faculty:   FIT
 Part:      1b
 Compiler:  GCC 5.4.0 on Merlin
 Date:      26.4.2017
*/

#include <iostream>
#include <fstream>
#include <string>
#include <queue>

#include <stdio.h>
#include <string.h>

using namespace std;

#define DEFAULT_N_LINES 10

// Parses arguments from argv. Tests, if file exists, ...
int loadParams(int argc, char *argv[], int &n, string &file) {
    int err = 0; // 0 = OK, 1 = argumens error, 2 = file error
    int fileIndex = 0;

    if (argc == 1) {
        n = DEFAULT_N_LINES;
    } else if (argc == 2) {
        if (argv[1][0] == '-') {
            err = 1;
        } else {
            fileIndex = 1;
        }
        n = DEFAULT_N_LINES;
    } else if (argc == 3) {
        if (strcmp(argv[1], "-n") != 0) {
            err = 1;
        } else {
            n = atoi(argv[2]);
            if (errno != 0) {
                err = 1;
            }
        }
    } else if (argc == 4) {
        if (strcmp(argv[1], "-n") == 0) {
            n = atoi(argv[2]);
            fileIndex = 3;
        } else if (strcmp(argv[2], "-n") == 0) {
            n = atoi(argv[3]);
            fileIndex = 1;
        }

        if (errno != 0) {
            err = 1;
        }
    }

    if (err != 0 && errno == 0) {
        cerr << "Possible tail run combinations:\n";
        cerr << "\ttail <in.txt\n";
        cerr << "\ttail -n 20 <in.txt\n";
        cerr << "\ttail in.txt\n";
        cerr << "\ttail -n 20 in.txt\n";
        return err;
    }

    if (err == 0 && n < 0) {
        fprintf(stderr, "-n must be bigger or equal to 0\n");
        err = 1;
        return err;
    }


    if (fileIndex != 0) {
        file = string(argv[fileIndex]);
    } else {
        file = "";
    }

    return err;
}

int main(int argc, char *argv[]) {
    int n = 0;
    string fileName;

    std::queue <string> lines;

    int status = loadParams(argc, argv, n, fileName);
    if (status == 0) {
        if (fileName == "") { //cteni ze stdin
            string line = "";

            status = 1;
            while (!cin.eof()) {
                getline(cin, line);

                lines.push(line);
                if (lines.size() > (unsigned int)n+1) {
                    lines.pop();
                }

                line.erase();
            }
        } else { // cteni z file
            string line = "";
            std::fstream fs;
            fs.open (fileName, std::fstream::in);

            if(fs.fail()){
                cerr << "File could not be opened\n";
                return 0;
            }

            status = 1;
            while (!fs.eof()) {
                getline(fs, line);

                lines.push(line);
                if (lines.size() > (unsigned int)n+1) {
                    lines.pop();
                }

                line.erase();
            }

            fs.close();
        }

        if(lines.back() == "\0"){
            while (lines.size() > 1) {
                cout << lines.front() << "\n";
                lines.pop();
            }
        }
        else {
            lines.pop();
            while (lines.size() > 0) {
                cout << lines.front() << "\n";
                lines.pop();
            }
        }

    }
}