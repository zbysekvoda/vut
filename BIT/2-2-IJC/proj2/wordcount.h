/*
 IJC 2016/17
 Project:   DU2

 Author:    Zbysek Voda, xvodaz01
 Faculty:   FIT
 Part:      2
 Compiler:  GCC 5.4.0 on Merlin
 Date:      26.4.2017
*/

#ifndef IJC2_WORDCOUNT_H
#define IJC2_WORDCOUNT_H

/*
    Teoretically, for hash maps, the bigger the array is, the beter
    (of course, array bigger than max value of its index - UINT_MAX
    in this case - does not make sense).

    I chose value 1023, because of practical reasons. It is still not
    big chunk of memory, yet offers good spread factor for tested text
    inputs.

    Some statististics:
        - input - Jan Neruda: Balady a romance
            - 2716 unique words
            - average items per index: 2.65494
            - variance 0.987921
            - min items per index: 0
            - max items per index: 10
*/
#define HASH_ARRAY_SIZE 1023

#endif //IJC2_WORDCOUNT_H
