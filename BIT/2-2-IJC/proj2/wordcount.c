/*
 IJC 2016/17
 Project:   DU2

 Author:    Zbysek Voda, xvodaz01
 Faculty:   FIT
 Part:      2
 Compiler:  GCC 5.4.0 on Merlin
*/

#include "wordcount.h"

#include "htab.h"
#include "io.h"

// function used for print all elements using htab_foreach
void print(const char *key, unsigned int *val) {
    printf("%s\t%d\n", key, *val);
}

int main() {
    htab *tab = htab_init(HASH_ARRAY_SIZE);
    htab_listitem *found = NULL;

    int counter = 0;
    int res = 0;

    char buf[MAX_WORD_LEN];
    while ((res = get_word(buf, MAX_WORD_LEN, stdin)) != EOF) {
        if(res == 0) continue;

        counter++;

        found = htab_lookup_add(tab, buf);
        if(found) {
            found->data++;
        }
    }

    htab_foreach(tab, print);

    htab_free(tab);
    fclose(stdin);
}
