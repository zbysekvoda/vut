/*
 IJC 2016/17
 Project:   DU2

 Author:    Zbysek Voda, xvodaz01
 Faculty:   FIT
 Part:      2
 Compiler:  GCC 5.4.0 on Merlin
 Date:      26.4.2017
*/

#include "htab.h"

// returns number of objects in hash table
size_t htab_size(htab *t) {
    if (t == NULL) return 0;

    return t->n;
}
