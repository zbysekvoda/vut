/*
 IJC 2016/17
 Project:   DU2

 Author:    Zbysek Voda, xvodaz01
 Faculty:   FIT
 Part:      2
 Compiler:  GCC 5.4.0 on Merlin
 Date:      26.4.2017
*/

#include "htab.h"

// applies function f to every item in hash table
void htab_foreach(htab *t, void (*f)(const char*, unsigned int*)){
    htab_listitem *h = NULL;

    if(t == NULL || f == NULL) return;

    for (unsigned int i = 0; i < t->arr_size; i++) {
        h = t->data[i];

        while (h != NULL) {
            (*f)(h->key, &h->data);

            h = h->next;
        }
    }
}
