/*
 IJC 2016/17
 Project:   DU2

 Author:    Zbysek Voda, xvodaz01
 Faculty:   FIT
 Part:      2
 Compiler:  GCC 5.4.0 on Merlin
 Date:      26.4.2017
*/

#ifndef IJC2_HTAB_H
#define IJC2_HTAB_H

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

typedef struct htab_listitem {
    char *key;
    unsigned int data;
    struct htab_listitem *next;
} htab_listitem;

typedef struct {
    size_t arr_size;
    size_t n;
    htab_listitem *data[];
} htab;

htab *htab_init(unsigned int size);

htab *htab_move(unsigned int newsize, htab *t2);

size_t htab_size(htab *t);

size_t htab_bucket_count(htab *t);

htab_listitem *htab_lookup_add(htab *t, char *key);

htab_listitem *htab_find(htab *t, char *key);

void htab_foreach(htab *t, void (*f)(const char*, unsigned int*));

bool htab_remove(htab *t, char *key);

void htab_clear(htab *t);

void htab_free(htab *t);



unsigned int hash_function(const char *str);

htab_listitem *search_on_index_add(htab_listitem *item, char *key, htab_listitem **lastExistingItemOnIndex);

#endif //IJC2_HTAB_H
