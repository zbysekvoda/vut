/*
 IJC 2016/17
 Project:   DU2

 Author:    Zbysek Voda, xvodaz01
 Faculty:   FIT
 Part:      2
 Compiler:  GCC 5.4.0 on Merlin
 Date:      26.4.2017
*/

#include "htab.h"

// creates hash table (including memory allocation etc.)
htab *htab_init(unsigned int size) {
    htab *new = malloc(sizeof(htab) + size * sizeof(htab_listitem*));
    if (new == NULL) return NULL;

    new->arr_size = size;
    new->n = 0;
    for (unsigned int i = 0; i < size; i++) {
        new->data[i] = NULL;
    }

    return new;
}

