# Angular 1 Devstack #

## Requirements 
- [git](http://git-scm.com/downloads)
- [nodeJS](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/)
- [Java Development Kit (JDK)](http://www.oracle.com/technetwork/java/javase/downloads/index.html ) - for protractor

## Installation

Install dependencies

```shell
yarn install
```

or just 

```shell
yarn
```

note: npm is also working

Run

```shell
gulp devel
```

Build files to /dist

```shell
yarn build
```