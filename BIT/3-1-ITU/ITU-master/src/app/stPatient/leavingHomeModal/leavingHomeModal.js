angular.module('lekovka.patient.leavingHomeModal', [])
  .component('leavingHomeModal', {
    templateUrl: 'app/stPatient/leavingHomeModal/leavingHomeModal.html',
    bindings: {
      resolve: '<',
      close: '&',
      dismiss: '&',
      patient: '<'
    },
    controller: function (drugService, dateTimeService) {
      this.timeSelection = {
        PERIOD: false,
        DATE: false
      };

      this.times = [1, 2, 4, 6, 8, 10, 12];

      this.leave = () => {
        this.close({});
      };

      this.periodSelected = (time) => {
        this.leavePeriod = time;
      };

      this.timeSelected = (time) => {
        this.returnTime = time;
      };

      this.timeClicked = () => {
        this.leavePeriod = undefined;
        this.timeSelection = {
          PERIOD: false,
          DATE: true
        };
      };

      this.periodClicked = () => {
        this.returnTime = undefined;
        this.timeSelection = {
          PERIOD: true,
          DATE: false
        };
      };

      this.countDrugs = () => {
        if(this.timeSelection.PERIOD !== this.timeSelection.DATE) {
          if(this.timeSelection.PERIOD){
            this.futureLength = (this.leavePeriod) * 60;
          }
          else {
            this.futureLength = dateTimeService.getTimeDiffInMinutes(Date.now(), this.returnTime.getTime());
          }

          this.pills = drugService.getDrugsInMinutes(this.resolve.patient, this.futureLength);

          this.showDrugs = true;
        }
      };
    }
  });
