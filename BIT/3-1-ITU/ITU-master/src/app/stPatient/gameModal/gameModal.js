angular.module('lekovka.patient.gameModal', [])
  .component('gameModal', {
    templateUrl: 'app/stPatient/gameModal/gameModal.html',
    bindings: {
      resolve: '<',
      close: '&',
      dismiss: '&'
    },
    controller: function () {
      this.$onInit = () => {

      };

      this.leave = () => this.close({});
    }
  });
