angular.module('lekovka.patient', [])

  .config(function ($stateProvider) {
    $stateProvider
      .state('patient', {
        url: '/patient/:uuid',
        component: 'stPatient'
      });
  })

  .component('stPatient', {
    templateUrl: 'app/stPatient/stPatient.html',
    controller: StPatientController
  });

function StPatientController($uibModal, drugService, userService, $stateParams, drugTicker) {
  this.$onInit = function () {
    let loggedUser = userService.getCurrentUser();
    this.patient = loggedUser.id === $stateParams.uuid ? loggedUser : null;

    this.pills = drugService.getDrugsInMinutes(this.patient, 4 * 60);
  };

  this.leavingHome = () => {
    $uibModal.open({
      component: 'leavingHomeModal',
      size: 'lg',
      resolve: {
        patient: () => this.patient
      }
    });
  };

  this.playGame = () => {
    $uibModal.open({
      component: 'gameModal',
      size: 'lg',
      resolve: {

      }
    });
  };

  this.showDrugbox = () => {
    $uibModal.open({
      component: 'drugboxModal',
      size: 'lg',
      resolve: {
        patient: () => this.patient
      }
    });
  };

  this.takePills = () => {
    let nextPills = drugService.getNextDrugs(this.patient);
    drugTicker.takePills(nextPills);
  };

  this.fillDrugbox = () => {
    drugTicker.fillDrugbox(this.patient);
  };
}
