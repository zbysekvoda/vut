angular.module('lekovka.patient.takePillModal', [])
  .component('takePillModal', {
    templateUrl: 'app/stPatient/takePillModal/takePillModal.html',
    bindings: {
      resolve: '<',
      close: '&',
      dismiss: '&',
      pills: '<'
    },
    controller: function (drugService) {
      this.$onInit = function () {
        this.pills = this.resolve.pills;
      };

      this.leave = () => {
        this.close({$value: this.selected});
      };
    }
  });
