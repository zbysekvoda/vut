angular.module('lekovka.patient.fillDrugboxModal', [])
  .component('fillDrugboxModal', {
    templateUrl: 'app/stPatient/fillDrugboxModal/fillDrugboxModal.html',
    bindings: {
      resolve: '<',
      close: '&',
      dismiss: '&',
      patient: '<'
    },
    controller: function (drugService) {
      this.$onInit = function () {
        this.patient = this.resolve.patient;
      };

      this.leave = () => {
        this.close({});
      };
    }
  });
