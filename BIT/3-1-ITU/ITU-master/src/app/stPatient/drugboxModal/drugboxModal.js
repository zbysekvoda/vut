angular.module('lekovka.patient.drugboxModal', [])
  .component('drugboxModal', {
    templateUrl: 'app/stPatient/drugboxModal/drugboxModal.html',
    bindings: {
      resolve: '<',
      close: '&',
      dismiss: '&'
    },
    controller: function () {
      this.$onInit = () => {
        this.patient = this.resolve.patient;
      };

      this.leave = () => this.close({});
    }
  });
