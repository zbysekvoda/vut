angular.module('lekovka.configuration', [])
/**
 * check if state have defaultSubstate: 'name' property, and if yes, then redirect hte state to this defaultSubstate.
 * Alternatively it could be used to temporally, or permanently redirect to different state.
 */
  .run(($transitions) => {

    const criteria = {
      to: function (state) {
        return state.defaultSubstate != null;
      }
    };

    $transitions.onBefore(criteria, (trans) => {
      const substate = trans.to().defaultSubstate;
      return trans.router.stateService.target(substate);
    });
  });
