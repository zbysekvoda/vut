angular.module('lekovka.admin.editDoctorModal', [])
  .component('editDoctorModal', {
    templateUrl: 'app/stAdmin/editDoctorModal/editDoctorModal.html',
    bindings: {
      resolve: '<',
      close: '&',
      dismiss: '&',
      doctor: '<'
    },
    controller: function () {
      this.$onInit = () => {
        this.doctor = this.resolve.doctor;
      };

      this.leave = () => {
        this.close({$value: this.doctor});
      };
    }
  });
