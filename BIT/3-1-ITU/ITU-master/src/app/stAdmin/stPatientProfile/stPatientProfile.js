angular.module('lekovka.editPatient', [])

  .config(function ($stateProvider) {
    $stateProvider
      .state('admin.patient', {
        url: '/patient/:uuid',
        component: 'stPatientProfile'
      });
  })

  .component('stPatientProfile', {
    templateUrl: 'app/stAdmin/stPatientProfile/stPatientProfile.html',
    controller: StAdminController
  });

function StAdminController($stateParams, $uibModal, userService, drugService, imageService, dateTimeService, DRUG_PERIOD_CZECH_NAMES, DRUG_PERIOD_NAMES, DRUGBOX_TYPES, DAYS, CZECH_DAYS) {
  // img z https://thenounproject.com/term/grandpa/

  this.getAge = function () {
    return dateTimeService.getAge(this.patient.birthDate);
  };

  this.$onInit = function () {
    this.periodCzechNames = DRUG_PERIOD_CZECH_NAMES;
    this.periodNames = DRUG_PERIOD_NAMES;
    this.patient = userService.getPatient($stateParams.uuid);
    this.patientImageUrl = imageService.getPatientImageUrl(this.patient);

    this.drugboxTypes = Object.values(DRUGBOX_TYPES).map(type => {
      return {
        id: type.id,
        name: type.name
      };
    });

    this.periodNameObjects = Object.keys(DRUG_PERIOD_CZECH_NAMES).map(period => {
      return {
        id: period,
        name: DRUG_PERIOD_CZECH_NAMES[period]
      };
    });

    this.medications = new Array(24 * 12)
      .fill({})
      .map((_, index) => {
        let val = dateTimeService.minutesFromDayBeginningToTime(index * 5);

        return {
          id: val,
          name: val
        };
      });

    this.dayObjects = Object.keys(DAYS).map(day => {
      return {
        id: DAYS[day],
        name: CZECH_DAYS[day]
      };
    });

    this.drugTypes = drugService.drugs;

    this.reload();
  };

  this.reload = () => {
    this.drugs = drugService.getDrugsForUser(this.patient).sort((a, b) => dateTimeService.minutesFromTimeString(a.medication) - dateTimeService.minutesFromTimeString(b.medication));
  };

  this.toggleEditSimple = function (el) {
    this.setEdit([el], !this.edit[el]);
  };

  this.toggleEdit = function() {
    let val = !R.path(arguments, this.edit);

    this.setEdit(arguments, val);
  };

  this.setEdit = function(path, val) {
    this.edit = R.assocPath(path, val, this.edit);
  };


  this.setPatientImage = () => {
    let image = $uibModal.open({
      component: 'setImageModal',
      size: 'lg',
      resolve: {
        selected: () => this.patient.image
      }
    })
      .result
      .then((selected) => {
        userService.setPatientImage(this.patient, selected);
        this.patientImageUrl = imageService.getPatientImageUrl(this.patient);
      });
  };

  this.addDrugs = () => {
    drugService.addEmptyDrug(this.patient);
    this.reload();
  };

  this.deleteDrug = (index) => {
    drugService.deleteDrug(this.patient, index);
    this.reload();
  };

  this.getDrugData = (drug) => drugService.getDrugInfo(drug);
}
