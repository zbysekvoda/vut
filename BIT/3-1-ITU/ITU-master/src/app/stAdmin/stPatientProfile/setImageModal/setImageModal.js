angular.module('lekovka.editPatient.setImageModal', [])
  .component('setImageModal', {
    templateUrl: 'app/stAdmin/stPatientProfile/setImageModal/setImageModal.html',
    bindings: {
      resolve: '<',
      close: '&',
      dismiss: '&'
    },
    controller: function (imageService) {
      this.$onInit = function () {
        this.images = imageService.getAllImages();
        this.selected = this.resolve.selected;
      };

      this.select = (image) => {
        this.selected = image.id;
      };

      this.leave = () => {
        this.close({$value: this.selected});
      };
    }
  });
