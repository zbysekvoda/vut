angular.module('lekovka.admin', [])

  .config(function ($stateProvider) {
    $stateProvider
      .state('admin', {
        abstract: true,
        url: '/admin',

      })
      .state('admin.view', {
        url: '',
        component: 'stAdmin'
      });
  })

  .component('stAdmin', {
    templateUrl: 'app/stAdmin/stAdmin.html',
    controller: StAdminController
  });

function StAdminController($state, userService, drugService, dateTimeService) {
  this.$onInit = function () {
    this.currentUser = userService.getCurrentUser();

    this.patients = userService.getUsersPatients(this.currentUser);
  };

  this.addPatient = () => {
    let patient = userService.addPatient();
    drugService.createUserDrugData(patient);

    $state.go('admin.patient', {uuid: patient.id});
  };

  this.editPatient = (patient) => $state.go('admin.patient', {uuid: patient.id});

  this.removePatient = (patient) => {
    userService.removeUser(patient);

    this.patients = userService.getUsersPatients(this.currentUser);
  };

  this.getAge = function (patient) {
    return dateTimeService.getAge(patient.birthDate);
  };
}
