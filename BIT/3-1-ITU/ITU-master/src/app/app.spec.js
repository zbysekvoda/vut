describe('application', function() {

  beforeEach(module('lekovka'));

  it('should have defined module', function() {
    expect(angular.module('lekovka')).toBeDefined();
  });

  it('should use ES6 syntax', function() {
    expect((function(a = 2) {
      return a;
    })()).toBe(2);
  });

});
