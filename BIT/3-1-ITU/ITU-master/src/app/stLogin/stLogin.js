angular.module('lekovka.login', [])

  .config(function ($stateProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        component: 'stLogin'
      });
  })

  .component('stLogin', {
    templateUrl: 'app/stLogin/stLogin.html',
    controller: StLoginController
  });

function StLoginController($state, $uibModal, userService, USER_ROLES) {
  // login window from https://bootsnipp.com/snippets/featured/clean-modal-login-form
  this.$onInit = () => {
    let currentUser = userService.getCurrentUser();

    if (currentUser) {
      if (currentUser.role === USER_ROLES.ADMIN) {
        $state.go('admin.view');
      }
      else {
        $state.go('patient', {uuid: currentUser.id});
      }
    }
  };

  this.login = (username, password) => {
    let user = userService.login(username, password);
    this.badCredentials = !user;

    if (user) {
      if (user.role === USER_ROLES.ADMIN) {
        $state.go('admin.view');
      }
      else {
        $state.go('patient', {uuid: user.id});
      }
    }
  };

  this.newUser = () => {
    let created = userService.addDoctor();

    let out = $uibModal.open({
      component: 'editDoctorModal',
      size: 'lg',
      resolve: {
        doctor: () => created
      }
    }).result;

    out.then((user) => userService.updateUser(user));
  };

  this.loginMockUser = () => {
    this.login('karelbrbla', 'karelbrbla');
  };

  this.loginMockAdmin = () => {
    this.login('josefsuchy', 'josefsuchy');
  };
}
