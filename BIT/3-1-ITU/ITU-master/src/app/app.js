angular.module('lekovka', [
  'ngAnimate',
  'ngCookie',
  'ui.bootstrap',
  'ui.router',
  'cm.common.utils',
  'cm.common.constants',
  'angular-uuid',

  'lekovka.editableText',
  'lekovka.editableSelect',
  'lekovka.radioRow',

  'lekovka.dateTimeService',
  'lekovka.configuration',
  'lekovka.userService',
  'lekovka.drugService',
  'lekovka.drugTicker',
  'lekovka.imageService',
  'lekovka.userRoles',

  'lekovka.login',
  'lekovka.admin',
  'lekovka.admin.editDoctorModal',
  'lekovka.editPatient',
  'lekovka.editPatient.setImageModal',
  'lekovka.drugbox',
  'lekovka.drug',
  'lekovka.drugImage',
  'lekovka.patient',
  'lekovka.patient.drugTimeline',
  'lekovka.patient.drugTimetable',
  'lekovka.patient.doctorInfo',
  'lekovka.patient.drugboxModal',
  'lekovka.patient.gameModal',
  'lekovka.patient.takePillModal',
  'lekovka.patient.fillDrugboxModal',
  'lekovka.patient.leavingHomeModal',
])

  .config(function ($urlRouterProvider, userServiceProvider) {
    $urlRouterProvider.otherwise('/login');

    let userService = userServiceProvider.$get();

    document.getElementById('logout-button').onclick = userService.logout;
    document.getElementById('edit-profile-button').onclick = userService.editProfile;
  });

