let drugFaces = {
  OVAL: 'oval',
  CIRCLE: 'circle',
  PILL: 'pill',
  SHOT: 'shot',
};

let drugImages = {
  [drugFaces.OVAL]: 'images/drugs/OVAL.svg',
  [drugFaces.CIRCLE]: 'images/drugs/CIRCLE.svg',
  [drugFaces.PILL]: 'images/drugs/PILL.svg',
  [drugFaces.SHOT]: 'images/drugs/SHOT.svg'
};

let drugboxTypes = {
  SINGLE: {
    id: 1,
    name: 'Jednoduchá',
    limits: [
      {from: 0, to: 24 * 60, inclusive: true}
    ],
    labels: [
      'Celý den'
    ]
  },
  DOUBLE: {
    id: 2,
    name: 'Dvojitá',
    limits: [
      {from: 0, to: 12 * 60, inclusive: false},
      {from: 12 * 60, to: 24 * 60, inclusive: true}
    ],
    labels: [
      'Dopoledne',
      'Odpoledne\na\nVečer'
    ]
  },
  TRIPPLE: {
    id: 3,
    name: 'Trojitá',
    limits: [
      {from: 0, to: 12 * 60, inclusive: false},
      {from: 12 * 60, to: 17 * 60, inclusive: false},
      {from: 17 * 60, to: 24 * 60, inclusive: true}
    ],
    labels: [
      'Dopoledne',
      'Odpoledne',
      'Večer'
    ]
  },
  QUADRUPLE: {
    id: 4,
    name: 'Čtyřnásobná',
    limits: [
      {from: 0, to: 8 * 60, inclusive: false},
      {from: 8 * 60, to: 14 * 60, inclusive: false},
      {from: 14 * 60, to: 19 * 60, inclusive: false},
      {from: 19 * 60, to: 24 * 60, inclusive: true}
    ],
    labels: [
      'Ráno',
      'Dopoledne',
      'Odpoledne',
      'Večer'
    ]
  },
  QUINTUPLE: {
    id: 5,
    name: 'Pětinásobná',
    limits: [
      {from: 0, to: 8 * 60, inclusive: false},
      {from: 8 * 60, to: 12 * 60, inclusive: false},
      {from: 12 * 60, to: 16 * 60, inclusive: false},
      {from: 16 * 60, to: 20 * 60, inclusive: false},
      {from: 20 * 60, to: 24 * 60, inclusive: true}
    ],
    labels: [
      'Ráno',
      'Dopoledne',
      'Odpoledne',
      'Večer',
      'Noc'
    ]
  }
};

let drugPeriodNames = {
  DAILY: 'daily',
  WEEKLY: 'weekly'
};

let drugPeriodCzechNames = {
  [drugPeriodNames.DAILY]: 'denně',
  [drugPeriodNames.WEEKLY]: 'týdně'
};

// period in minutes
let drugPeriods = {
  [drugPeriodNames.DAILY]: 24 * 60,
  [drugPeriodNames.WEEKLY]: 7 * 24 * 60
};


angular.module('lekovka.drugService')
  .constant('DRUG_FACES', drugFaces)
  .constant('DRUGBOX_TYPES', drugboxTypes)
  .constant('DRUG_PERIOD_NAMES', drugPeriodNames)
  .constant('DRUG_PERIOD_CZECH_NAMES', drugPeriodCzechNames)
  .constant('DRUG_IMAGES', drugImages)
  .constant('DRUG_PERIODS', drugPeriods);
