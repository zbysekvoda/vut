angular.module('lekovka.imageService', [])

  .service('imageService', function () {
    this.images = [
      {
        url: 'images/grandpa1.svg',
        id: '7f22d46d-84b6-4583-aba6-33eb41973aa6'
      },
      {
        url: 'images/grandpa2.svg',
        id: 'cb965fb5-c50c-4c11-9e16-e5581552af5d'
      },
      {
        url: 'images/grandpa3.svg',
        id: '5dc53c1b-06d3-4bb5-89c9-6d68349cf296'
      },
      {
        url: 'images/grandpa4.svg',
        id: '0680d68a-0e58-42f9-96da-b8499441a358'
      },
      {
        url: 'images/grandpa5.svg',
        id: '9b320106-d520-4e9a-bd7d-28e32f9e4fde'
      }
    ];

    this.getPatientImageUrl = (patient) => {
      let imageObj = this.images.find(image => image.id === patient.image);
      return imageObj ? imageObj.url : '';
    };

    this.getAllImages = () => this.images;
  });