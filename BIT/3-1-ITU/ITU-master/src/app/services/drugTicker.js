angular.module('lekovka.drugTicker', [])

  .service('drugTicker', function ($uibModal) {
    this.init = (drugs) => {

    };

    this.takePills = (pills) => {
      $uibModal.open({
        component: 'takePillModal',
        size: 'lg',
        resolve: {
          pills: () => pills
        }
      });
    };

    this.fillDrugbox = (patient) => {
      $uibModal.open({
        component: 'fillDrugboxModal',
        size: 'lg',
        resolve: {
          patient: () => patient
        }
      });
    };
  });