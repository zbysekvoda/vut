angular.module('lekovka.userService', [])

  .service('userService', function ($state, $uibModal, cookies, USER_ROLES, uuid) {
    this.registeredUsers = [
      {
        id: '349a8631-56be-488d-92c3-3f5bce015c47',
        name: 'Karel',
        surname: 'Brbla',
        username: 'karelbrbla',
        password: 'karelbrbla',
        birthDate: (new Date(1941, 11, 24, 0, 0, 0)),
        role: USER_ROLES.PATIENT,
        adminId: '6b19a8b6-4121-4e19-b3df-716542dc8c1f',
        image: 'cb965fb5-c50c-4c11-9e16-e5581552af5d',
        drugboxType: 5
      },
      {
        id: '349a8631-56be-488d-92c3-3f5bce015c48',
        name: 'Jan',
        surname: 'Brbla',
        username: 'janbrbla',
        password: 'janbrbla',
        birthDate: (new Date(1972, 11, 11, 0, 0, 0)),
        role: USER_ROLES.PATIENT,
        adminId: '6b19a8b6-4121-4e19-b3df-716542dc8c1f',
        image: '7f22d46d-84b6-4583-aba6-33eb41973aa6',
        drugboxType: 3
      },
      {
        id: '6b19a8b6-4121-4e19-b3df-716542dc8c1f',
        name: 'Josef',
        surname: 'Suchý',
        username: 'josefsuchy',
        password: 'josefsuchy',
        role: USER_ROLES.ADMIN,
        doctor: true,
        contact: {
          address: 'Javořická 25, Brno',
          phone: '234543212',
          email: 'some@email.com'
        }
      }
    ];

    this.getAllRegisteredUsers = () => {
      return this.registeredUsers;
    };

    this.addUser = (user) => {
      this.registeredUsers.push(user);
    };

    this.getCurrentUser = () => {
      this.currentUser = cookies.get('logged-user');

      let found = this.getAllRegisteredUsers().filter(user => user.id === this.currentUser);

      return found ? found[0] : null;
    };

    this.getUsersPatients = (user) => {
      if(user.role !== USER_ROLES.ADMIN){
        console.log('Not admin');
        return null;
      }

      return this.getAllRegisteredUsers().filter(patient => patient.adminId === user.id);
    };

    this.getPatient = (uuid) => {
      let found = this.getAllRegisteredUsers().filter(user => user.id === uuid && user.role === USER_ROLES.PATIENT);

      return found ? found[0] : null;
    };

    this.addPatient = () => {
      this.currentUser = cookies.get('logged-user');

      let patient = {
        id: uuid.v4(),
        name: '',
        surname: '',
        birthDate: '',
        role: USER_ROLES.PATIENT,
        adminId: this.currentUser,
        image: ''
      };

      this.addUser(patient);

      return patient;
    };

    this.addDoctor = () => {
      let doctor = {
        id: uuid.v4(),
        name: '',
        surname: '',
        birthDate: '',
        role: USER_ROLES.ADMIN,
        doctor: true,
        image: '',
        contact: {
          address: '',
          phone: '',
          email: ''
        }
      };

      this.addUser(doctor);
      this.currentUser = doctor.id;
      cookies.set('logged-user', this.currentUser);

      return doctor;
    };

    this.setPatientImage = (patient, image) => patient.image = image;

    this.findUserBy = (attr, val) => {
      return this.getAllRegisteredUsers().find(user => user[attr] === val);
    };

    this.login = (username, password) => {
      let user = this.findUserBy('username', username);

      if(!user || user.password !== password) {
        return undefined;
      }

      this.currentUser = user.id;
      cookies.set('logged-user', this.currentUser);
      return user;
    };

    this.getDoctorInfo = (id) => {
      let found = this.findUserBy('id', id);

      return found && found.role === USER_ROLES.ADMIN ? found : {};
    };

    this.removeUser = (user) => {
      let userIndex = this.getAllRegisteredUsers().findIndex(regUser => regUser.id === user.id);


      this.registeredUsers = R.dissocPath([userIndex], this.getAllRegisteredUsers());
    };

    this.updateUser = (user) => {
      let index = this.getAllRegisteredUsers().findIndex(u => u.id === user.id);

      if(!user.username){
        user.username = user.name.toLowerCase() + user.surname.toLowerCase();
      }
      if(!user.password){
        user.password = user.name.toLowerCase() + user.surname.toLowerCase();
      }

      this.registeredUsers = R.update(index, user, this.getAllRegisteredUsers());
    };

    this.logout = () => {
      cookies.set('logged-user', undefined);
      $state.go('login');
    };

    this.editProfile = () => {
      let out = $uibModal.open({
        component: 'editDoctorModal',
        size: 'lg',
        resolve: {
          doctor: this.getCurrentUser
        }
      }).result;

      out.then((user) => this.updateUser(user));
    };
  });