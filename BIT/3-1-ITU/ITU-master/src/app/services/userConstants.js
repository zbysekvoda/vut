let userRoles = {
  ADMIN: 'admin',
  PATIENT: 'patient'
};

angular.module('lekovka.userRoles', [])
  .constant('USER_ROLES', userRoles);