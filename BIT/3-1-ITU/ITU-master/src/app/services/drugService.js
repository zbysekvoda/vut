angular.module('lekovka.drugService', [])

  .service('drugService', function ($rootScope, USER_ROLES, DRUG_FACES, DRUG_PERIOD_NAMES, DRUG_PERIOD_CZECH_NAMES, DAYS, DRUGBOX_TYPES, DAYS_VALUES, dateTimeService) {
    this.drugs = [
      {
        id: 1,
        name: 'Paralen',
        face: DRUG_FACES.OVAL,
        color: 'violet'
      },
      {
        id: 2,
        name: 'Aspirin',
        face: DRUG_FACES.PILL,
        color: 'blue'
      },
      {
        id: 3,
        name: 'Epidural',
        face: DRUG_FACES.SHOT,
        color: 'red'
      },
      {
        id: 4,
        name: 'Celaskon',
        face: DRUG_FACES.CIRCLE,
        color: '#ff7500'
      },
      {
        id: 5,
        name: 'Hedelix',
        face: DRUG_FACES.PILL,
        color: '#00e2ff'
      }
    ];

    this.userDrugs = {
      '349a8631-56be-488d-92c3-3f5bce015c47': [
        {
          id: 1,
          drug: 1,
          medication: '01:00',
          repetition: DRUG_PERIOD_NAMES.DAILY
        },
        {
          id: 2,
          drug: 2,
          medication: '02:30',
          repetition: DRUG_PERIOD_NAMES.DAILY
        },
        {
          id: 3,
          drug: 2,
          medication: '03:55',
          repetition: DRUG_PERIOD_NAMES.DAILY
        },
        {
          id: 4,
          drug: 1,
          medication: '10:00',
          repetition: DRUG_PERIOD_NAMES.DAILY
        },
        {
          id: 5,
          drug: 1,
          medication: '10:30',
          repetition: DRUG_PERIOD_NAMES.DAILY
        },
        {
          id: 6,
          drug: 3,
          medication: '10:30',
          repetition: DRUG_PERIOD_NAMES.DAILY
        },
        {
          id: 7,
          drug: 5,
          medication: '10:40',
          repetition: DRUG_PERIOD_NAMES.DAILY
        },
        {
          id: 8,
          drug: 1,
          medication: '11:00',
          repetition: DRUG_PERIOD_NAMES.DAILY
        },
        {
          id: 9,
          drug: 2,
          medication: '12:00',
          counter: 3,
          repetition: DRUG_PERIOD_NAMES.DAILY
        },
        {
          id: 10,
          drug: 1,
          medication: '11:50',
          repetition: DRUG_PERIOD_NAMES.DAILY
        },
        {
          id: 14,
          drug: 4,
          medication: '13:00',
          repetition: DRUG_PERIOD_NAMES.DAILY
        },
        {
          id: 13,
          drug: 4,
          medication: '14:00',
          repetition: DRUG_PERIOD_NAMES.DAILY
        },
        {
          id: 11,
          drug: 1,
          medication: '18:00',
          repetition: DRUG_PERIOD_NAMES.DAILY
        },
        {
          id: 12,
          drug: 4,
          medication: '23:05',
          repetition: DRUG_PERIOD_NAMES.WEEKLY,
          day: DAYS.MONDAY
        },
      ],
      '349a8631-56be-488d-92c3-3f5bce015c48': [
        {
          id: 1,
          drug: 1,
          medication: '10:00',
          repetition: DRUG_PERIOD_NAMES.DAILY
        },
        {
          id: 2,
          drug: 1,
          medication: '18:00',
          repetition: DRUG_PERIOD_NAMES.DAILY
        }
      ]
    };

    this.getDrugsForUser = (user) => {
      return this.userDrugs[user.id];
    };

    this.getDrugInfo = (drug) => {
      return this.drugs.find(d => d.id === drug.drug);
    };

    this.createUserDrugData = (user) => {
      this.userDrugs[user.id] = this.userDrugs[user.id] || [];
    };

    let getDrugsWithTimeInMinutes = (drugs) => {
      return drugs.map((drug, index) => {
        let times = drug.medication.split(':');
        let hours = parseInt(times[0]);
        let minutes = parseInt(times[1]);

        let out = {
          minutes: hours * 60 + minutes,
          index: index
        };

        if (drug.repetition === DRUG_PERIOD_NAMES.WEEKLY) {
          out.day = drug.day;
        }

        if (drug.counter) {
          out.counter = drug.counter;
        }

        return out;
      });
    };

    this.getDrugsInMinutes = (patient, minutes) => {
      let todaysBeginning = dateTimeService.dayBeginningFromTimestamp(Date.now());
      let minutesFromDayBeginning = dateTimeService.minutesFromDayBeginning(todaysBeginning, Date.now());

      let drugs = this.getDrugsForUser(patient);
      let endMinutes = minutesFromDayBeginning + minutes;

      let drugsWithTimeInMinutes = getDrugsWithTimeInMinutes(drugs);

      let out = [];


      let drugMatches = (from, to, minutes, day) => minutes >= from && minutes <= to && dateTimeService.isToday(day);

      if (endMinutes > 24 * 60) { // přes půlnoc
        out = drugsWithTimeInMinutes.reduce(
          (acc, drug) => drugMatches(minutesFromDayBeginning, 24 * 60, drug.minutes, drug.day) ? R.append(drug, acc) : acc,
          out
        );
        out = drugsWithTimeInMinutes.reduce(
          (acc, drug) => drugMatches(0, endMinutes % (24 * 60), drug.minutes, drug.day) ? R.append(drug, acc) : acc,
          out
        );
      }
      else {
        out = drugsWithTimeInMinutes.reduce(
          (acc, drug) => drugMatches(minutesFromDayBeginning, endMinutes, drug.minutes, drug.day) ? R.append(drug, acc) : acc,
          out
        );
      }

      return out
        .map(drug => Object.assign({}, drugs[drug.index], {minutes: drug.minutes}))
        .sort((a, b) => a.minutes - b.minutes);
    };

    this.getDrugsInDayPart = (limit, drugs, drugsWithTimeInMinutes) => {
      let out = [];

      for (let minutes = limit.from; limit.inclusive ? minutes <= limit.to : minutes < limit.to; minutes++) {
        drugsWithTimeInMinutes.forEach(drug => {

          if (minutes % dateTimeService.minutesInDay === drug.minutes) {
            let outDrug = drugs[drug.index];
            outDrug.minutes = drug.minutes;

            out.push(outDrug);
          }
        });
      }

      return out;
    };

    this.getDrugbox = (patient, filterUsedPills) => {
      let drugs = this.getDrugsForUser(patient);

      let drugsWithTimeInMinutes = getDrugsWithTimeInMinutes(drugs);

      let drugbox = Object.values(DRUGBOX_TYPES).find(type => type.id === patient.drugboxType);

      if(!drugbox) {
        return [];
      }

      let dailyMedication = drugbox.limits
        .map(limit => {
          return {
            limits: limit,
            drugs: this.getDrugsInDayPart(limit, drugs, drugsWithTimeInMinutes)
          };
        });

      // todo remove used medications
      let week = new Array(7)
        .fill(dailyMedication)
        .map((day, dayIndex) => day.map(part => {
          let out = {
            limits: part.limits
          };

          out.drugs = part.drugs
            .filter(drug => {
              let isMedicatedToday = drug.repetition === DRUG_PERIOD_NAMES.DAILY || (drug.repetition === DRUG_PERIOD_NAMES.WEEKLY && drug.day === DAYS_VALUES[dayIndex]);

              if (isMedicatedToday && angular.isDefined(drug.counter)) {
                drug.counter--;
                if (drug.counter < 0) return false;
              }

              return isMedicatedToday;
            });


          return out;
        }));

      // filter passed drugs
      if (filterUsedPills) {
        week = week.map((day, dayIndex) =>
          day.map(part => {
            return {
              limits: part.limits,
              drugs: part.drugs.filter(drug => {
                if ((new Date).getDay() > dayIndex + 1) {
                  return false;
                }
                else if ((new Date).getDay() === dayIndex + 1 && drug.minutes < dateTimeService.minutesFromTodayBeginning(Date.now())) {
                  return false;
                }

                return true;
              })
            };
          })
        );
      }

      let parts = drugbox.limits.length;
      let transposedWeek = [];

      for (let part = 0; part < parts; part++) {
        transposedWeek[part] = [];

        for (let day = 0; day < 7; day++) {
          transposedWeek[part][day] = week[day][part];
        }
      }

      return transposedWeek;
    };

    this.getLabelsForDayPart = (patient, index) => {
      let drugbox = Object.values(DRUGBOX_TYPES).find(type => type.id === patient.drugboxType);

      let label = drugbox.labels[index];
      let limits = drugbox.limits[index];

      return {
        label: label,
        from: dateTimeService.minutesFromDayBeginningToTime(limits.from),
        to: dateTimeService.minutesFromDayBeginningToTime(limits.to)
      };
    };

    this.getCzechNameForPeriod = period => DRUG_PERIOD_CZECH_NAMES[period];

    this.getNextDrugs = (patient) => {
      let drugsInDay = this.getDrugsInMinutes(patient, 24 * 60);

      let nextDrug = drugsInDay[0];

      return drugsInDay.filter(drug => Math.abs(drug.minutes - nextDrug.minutes) <= 5);
    };

    this.addEmptyDrug = (patient) => {
      this.userDrugs[patient.id].push({
        id: Math.max(this.userDrugs[patient.id].map(drug => drug.id)) + 1,
        drug: 0,
        medication: '23:55',
        repetition: DRUG_PERIOD_NAMES.DAILY
      });

      $rootScope.$broadcast('drugs-changed');
    };

    this.deleteDrug = (patient, index) => {
      this.userDrugs[patient.id] = R.dissocPath([index], this.userDrugs[patient.id]);

      $rootScope.$broadcast('drugs-changed');
    };

    this.updateDrug = (patient, drug) => {
      let index = this.userDrugs[patient.id].findIndex(d => d.id === drug.id);
      this.userDrugs[patient.id] = R.update(index, drug, this.userDrugs[patient.id]);

      $rootScope.$broadcast('drugs-changed');
    };
  });