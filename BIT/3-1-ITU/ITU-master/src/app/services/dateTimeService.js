let days = {
  MONDAY: 'monday',
  TUESDAY: 'tuesday',
  WEDNESDAY: 'wednesday',
  THURSDAY: 'thursday',
  FRIDAY: 'firday',
  SATURDAY: 'saturday',
  SUNDAY: 'sunday'
};

let czechDays = {
  MONDAY: 'pondělí',
  TUESDAY: 'úterý',
  WEDNESDAY: 'středa',
  THURSDAY: 'čtvrtek',
  FRIDAY: 'pátek',
  SATURDAY: 'sobota',
  SUNDAY: 'neděle'
};

angular.module('lekovka.dateTimeService', [])

  .constant('DAYS', days)
  .constant('CZECH_DAYS', czechDays)
  .constant('DAYS_VALUES', Object.values(days))

  .service('dateTimeService', function (DAYS_VALUES) {
    this.msInDay = 24 * 60 * 60 * 1000;
    this.msInMinute = 60 * 1000; // ms in minute
    this.minutesInDay = 24 * 60;

    this.addZeroIfNeeded = (num) => num <= 9 ? '0' + num : num;

    this.dayBeginningFromTimestamp = (ts) => {
      return Math.floor(ts / this.msInDay) * 24 * 60 - 60;
    };

    this.minutesFromTodayBeginning = (ts) => {
      return Math.floor(ts / this.msInMinute) - this.dayBeginningFromTimestamp(Date.now());
    };

    this.minutesFromDayBeginning = (beginning, ts) => {
      return Math.floor(ts / this.msInMinute) - beginning;
    };

    this.getAge = function(ts) {
      let diff = Date.now() - ts;

      return Math.abs((new Date(diff)).getUTCFullYear() - 1970);
    };

    this.getDate = function(ts) {
      let date = new Date(ts);

      return `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()}`;
    };

    this.minutesFromDayBeginningToTime = (minTs) => {
      let minutes = minTs % 60;
      let hours = (minTs - minutes) / 60;

      return `${this.addZeroIfNeeded(hours)}:${this.addZeroIfNeeded(minutes)}`;
    };

    this.getTimeDiffInMinutes = (ts1, ts2) => {
      if(ts1 >= ts2) return 0;

      let diff = ts2 - ts1;

      return Math.ceil(diff / (60 * 1000));
    };

    this.isToday = (day) => {
      if(!day) return true;

      let todayNo = (new Date).getDay();
      let today = DAYS_VALUES[todayNo - 1];

      return day === today;
    };

    this.minutesFromTimeString = (string) => {
      let parts = string.split(':');

      return parseInt(parts[0]) * 60 + parseInt(parts[1]);
    };
  })
  ;