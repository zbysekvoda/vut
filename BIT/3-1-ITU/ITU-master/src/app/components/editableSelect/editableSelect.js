angular.module('lekovka.editableSelect', [])

  .component('editableSelect', {
    templateUrl: 'app/components/editableSelect/editableSelect.html',
    bindings: {
      title: '<',
      model: '=',
      options: '<',
      changedCallback: '&'
    },
    controller: EditableSelectController
  });

function EditableSelectController($scope, userService) {
  this.$onInit = function() {
    this.val = this.model;
    this.editted = false;
  };

  this.toggleEdit = function() {
    if(this.editted) {
      let number = parseInt(this.val);

      this.model = isNaN(number) || number.toFixed(0).length !== this.val.length ? this.val : number;

      if(this.changedCallback) {
        this.changedCallback();
      }
    }

    this.editted = !this.editted;
  };

  this.getLabel = (id) => {
    let res = this.options.find(option => option.id === id);

    return res ? res.name : '';
  };
}