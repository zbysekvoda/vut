angular.module('lekovka.editableText', [])

  .component('editableText', {
    templateUrl: 'app/components/editableText/editableText.html',
    bindings: {
      title: '<',
      type: '@',
      model: '='
    },
    controller: EditableTextController
  });

function EditableTextController() {
  this.$onInit = function() {
    this.editted = false;

    this.val = this.model;
  };

  this.toggleEdit = function() {
    if(this.editted) {
      this.model = this.val;
    }

    this.editted = !this.editted;
  };

  this.asterixs = (text) => {
    return new Array(text.length).fill('*').join('');
  };
}