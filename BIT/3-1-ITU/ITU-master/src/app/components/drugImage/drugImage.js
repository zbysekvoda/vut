angular.module('lekovka.drugImage', [])
  .component('drugImage', {
    templateUrl: 'app/components/drugImage/drugImage.html',
    bindings: {
      face: '<',
      color: '<'
    },
    controller: DrugImageController
  });

function DrugImageController(DRUG_FACES, drugService) {
  this.$onInit = function () {
    this.faces = DRUG_FACES;
  };
}