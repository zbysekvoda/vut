angular.module('lekovka.patient.doctorInfo', [])

  .component('doctorInfo', {
    templateUrl: 'app/components/doctorInfo/doctorInfo.html',
    bindings: {
      doctorId: '<'
    },
    controller: DoctorInfoController
  });

function DoctorInfoController(userService) {
  this.$onInit = function() {
    this.doctor = userService.getDoctorInfo(this.doctorId);
    this.contact = this.doctor.contact;
  };
}