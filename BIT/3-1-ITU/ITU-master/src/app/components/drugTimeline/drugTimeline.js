let stringifyTimeNum = (time) => {
  return time < 10 ? '0' + time : time;
};

angular.module('lekovka.patient.drugTimeline', [])

  .component('drugTimeline', {
    templateUrl: 'app/components/drugTimeline/drugTimeline.html',
    bindings: {
      pills: '<'
    },
    controller: DrugTimelineController
  });

function DrugTimelineController(drugService) {
  this.$onInit = function() {
    let canvas = document.getElementById('my-canvas');
    let timeline = canvas.parentNode;
    canvas.width = timeline.offsetWidth;
    canvas.height = timeline.offsetHeight;

    drawTimeline(drugService, canvas, this.pills, 4);
  };
}

function getMinutes(timeString) {
  let times = timeString.split(':');
  return parseInt(times[0]) * 60 + parseInt(times[1]);
}

function drawTimeline(drugService, canvas, drugs, futureLength) {
  let drugTypes = R.uniq(drugs.map(drug => drugService.getDrugInfo(drug).id));

  let context = canvas.getContext('2d');

  let timeAreaHeight = 50;
  let drugAreaWidth = 50;

  let plotArea = {
    width: canvas.width,
    height: canvas.height - timeAreaHeight
  };

  // main lines
  let noOfLines = drugTypes.length;
  let verticalStep = plotArea.height / noOfLines;
  let verticalOffset = verticalStep / 2;

  for(let i = 0; i < noOfLines; i++){
    let y = verticalOffset + i * verticalStep;

    context.moveTo(0, y);
    context.lineTo(canvas.width, y);
    context.stroke();
  }

  // time steps
  let startTime = Math.floor(Date.now() / (60 * 60 * 1000)) * 60 * 60 * 1000;
  let noOfTimes = futureLength + 1;
  let horizontalStep = plotArea.width / noOfTimes;
  let horizontalOffset = horizontalStep / 2;
  let timeSteps = new Array(noOfTimes)
    .fill('')
    .map((step, index) => {
      let ts = startTime + index * 60 * 60 * 1000;
      let date = new Date(ts);

      return stringifyTimeNum(date.getHours()) + ':' + stringifyTimeNum(date.getMinutes());
    });

  context.font = '15px Arial';
  for(let x = 0; x < noOfTimes; x++) {
    context.moveTo(x * horizontalStep + horizontalOffset, canvas.height);
    context.lineTo(x * horizontalStep + horizontalOffset, canvas.height - 10);
    context.stroke();

    context.fillText(timeSteps[x],x * horizontalStep + horizontalOffset - 20, canvas.height - 20);
  }

  let startingTimeInMinutes = getMinutes(timeSteps[0]);
  drugs.forEach(drug => {
    let drugData = drugService.getDrugInfo(drug);

    let diff = getMinutes(drug.medication) - startingTimeInMinutes;
    diff = diff < 0 ? diff + (24 * 60) : diff;
    let ratio = diff / (60 * (futureLength + 1));

    let x = horizontalOffset + plotArea.width * ratio;
    let y = verticalOffset + drugTypes.findIndex(type => type === drugData.id) * verticalStep;

    context.beginPath();
    context.fillStyle = drugData.color;
    context.arc(x, y, 10, 0, 2*Math.PI);
    context.fill();
  });

  context.fillStyle = 'black';
}