angular.module('lekovka.drug', [])
  .component('drug', {
    templateUrl: 'app/components/drug/drug.html',
    bindings: {
      drugData: '<'
    },
    controller: DrugController
  });

function DrugController($scope, DRUG_FACES, drugService) {
  this.$onInit = function () {
    this.faces = DRUG_FACES;

    this.reload();

    $scope.$watch('$ctrl.drugData.drug', this.reload);
  };

  this.reload = () => {
    this.drugInfo = drugService.getDrugInfo(this.drugData);
  };
}