angular.module('lekovka.drugbox', [])
  .component('drugbox', {
    templateUrl: 'app/components/drugbox/drugbox.html',
    bindings: {
      patient: '<',
      filterUsedPills: '<'
    },
    controller: DrugboxController
  });

function DrugboxController($interval, $scope, drugService) {
  this.$onInit = function () {
    this.displayedDetail = undefined;

    $scope.$watch('$ctrl.patient.drugboxType', this.reload);

    $interval(this.reload, 1000);
  };

  this.reload = () => {
    this.drugbox = drugService.getDrugbox(this.patient, this.filterUsedPills);
  };

  this.leave = () => {
    this.close({$value: this.selected});
  };

  this.getDaypartInfo = index => drugService.getLabelsForDayPart(this.patient, index);
  this.getDaypartLabelFor = index => this.getDaypartInfo(index).label;
  this.getDaypartFrom = index => this.getDaypartInfo(index).from;
  this.getDaypartTo = index => this.getDaypartInfo(index).to;

  this.getNameForPeriod = drugService.getCzechNameForPeriod;

  this.showDetail = (drug) => {
    this.displayedDetail = drug;
  };
}