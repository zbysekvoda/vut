angular.module('lekovka.radioRow', [])

  .component('radioRow', {
    templateUrl: 'app/components/radioRow/radioRow.html',
    bindings: {
      options: '<',
      onUpdate: '&'
    },
    controller: RadioRowController
  });

function RadioRowController() {
  this.$onInit = function() {
    this.selected = undefined;
  };

  this.buttonSelected = (time) => {
    this.selected = time;
    this.onUpdate()(time);
  };
}
