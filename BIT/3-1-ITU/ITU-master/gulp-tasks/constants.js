const gulp = require('gulp');
const del = require('del');
const ngConstant = require('gulp-ng-constant');
const argv = require('yargs');

const environment = argv.argv.env || 'testing';

exports.cleanConstants = function () {
  return del('dependencies/es5/constants');
};

exports.createConstants = function () {
  return gulp.src('gulp-tasks/constants/' + environment + '.json')
    .pipe(ngConstant({
      name: 'cm.common.constants',
      wrap: true
    }))
    .pipe(gulp.dest('dependencies/es5/constants'));
};
