/**
 * IOS - Project 2
 * \author Zbyšek Voda
 * \version 0.1
 * \date 1.5.2016
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <time.h>
#include <limits.h>

#define shmSIZE sizeof(sem_t)
#define semNAME "proj2-sem"
#define newMem() mmap(NULL, shmSIZE, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0)

int errorHandler(int error) {
	if (error == 1){
		fprintf(stderr, "Unexpected number of arguments, faulty arguments values or bad relation between them \n");
	}
	else if(error == 2){
		fprintf(stderr, "System call error \n");
	}

	return error;
}

char checkArguments(int P, int C, int PT, int RT){
	int success = 1;

	if(P <= 0) //checks if P is valid
		success = 0;

	if(C <= 0 || P <= C || P % C != 0) //checks if C is valid
		success = 0;

	if(PT < 0 || PT > 5000) //checks if PT is valid
		success = 0;

	if(RT < 0 || RT > 5000) //checks if RT is valid
		success = 0;

	return success;
}

void setSharedMem(int *mem, sem_t *triggeringSemaphor, int val){ //counter val is implicit first on line
	sem_wait(triggeringSemaphor);
	*mem = val;
	sem_post(triggeringSemaphor);
}

void manipulateSharedMem(int *mem, sem_t *triggeringSemaphor, int manipulation){
	setSharedMem(mem, triggeringSemaphor, *mem+manipulation);
}

int printLog(/*FILE *log, */int *counter, sem_t *sem_counter, char *s, int i, int d){
	int result = 0;
	result += sem_wait(sem_counter);

	FILE *log = fopen("proj2.log", "a+");

	fprintf(log, "%d \t: ", *counter);
	fprintf(log, s, i, d);

	printf("%d \t: ", *counter);
	printf(s, i, d);

	fclose(log);

	(*counter)++;
	result += sem_post(sem_counter);

	return result == 0 ? 1 : 0;
}

sem_t *createAndInitSemaphor(int *errorCounter){
	sem_t *sem_inner = newMem();
	*errorCounter += sem_init(sem_inner, 1, 0);

	return sem_inner;
}

int main(int argc, char *argv[]){
	setbuf(stdout, NULL); /*< Clears buffer for stdout */
	setbuf(stderr, NULL); /*< Clears buffer for stderr */

	if(argc != 5){
		return errorHandler(1); //faulty number of arguments
	}

	// loads program arguments
	int P = atoi(argv[1]);	/*< Variable representing number of passengers*/
	int C = atoi(argv[2]); 	/*< Variable representing capacity of carriage*/
	int PT = atoi(argv[3]);	/*< Variable representing maximum waiting time for generating new process */
	int RT = atoi(argv[4]);	/*< Variable representing maximum time for one carriage drive*/

	if(!checkArguments(P, C, PT, RT)){
		return errorHandler(1); // exits program, when arguments are faulty
	}

	int *shm_messageCounter = (int*)newMem(); /*< Shared counter of messages printed to log file or stdout*/
	int *shm_passengersInCartCounter = (int*)newMem(); /*< Shared counter of passangers in cart */
	int *shm_processCounter = (int*)newMem(); /*< Shared counter of running passanger processes */
	*shm_messageCounter = 1;
	*shm_passengersInCartCounter = 0;
	*shm_processCounter = 1;

	int *shm_cartPID = (int*)newMem(); /*< Shared variable for cartPID */
	int *shm_makerPID = (int*)newMem(); /*< Shared variable for makerPID */

	//checks, if all shared variables have been properly initialised
	if(shm_messageCounter == NULL || shm_passengersInCartCounter == NULL || shm_processCounter == NULL || shm_cartPID == NULL || shm_makerPID == NULL){
		return errorHandler(2);
	}

	//let's initialise semaphores
	int initStatus = 0; /*< If error in semaphor ocures, counter will be incremented */

	//PID variables locks
	sem_t *sem_cartPIDlock = createAndInitSemaphor(&initStatus); /*< Lock for shm_cartPID shared variable */
	sem_t *sem_makerPIDlock = createAndInitSemaphor(&initStatus); /*< Lock for shm_makerPID shared variable */

	//semaphors used as counters locks
	sem_t *sem_messageCounterManipulated = createAndInitSemaphor(&initStatus); /*< Stores, whether message counter is beeing manipulated*/
	sem_t *sem_passengerCounterManipulated = createAndInitSemaphor(&initStatus); /*< Stores, whether passengers counter is beeing manipulated*/
	sem_t *sem_processCounterManipulated =  createAndInitSemaphor(&initStatus); /*< Stores, whether process counter is beeing manipulated */

	//cart load and unload
	sem_t *sem_cartLoad = createAndInitSemaphor(&initStatus); /*< Is cart ready to be loaded? */
	sem_t *sem_cartUnloaded = createAndInitSemaphor(&initStatus); /*< Is cart unloaded? */

	//cart controll
	sem_t *sem_start = createAndInitSemaphor(&initStatus); /*< Can cart process start working? */
	sem_t *sem_stop = createAndInitSemaphor(&initStatus); /*< Has cart process stopped working? */

	//passanger board and unboard
	sem_t *sem_passBoard = createAndInitSemaphor(&initStatus); /*< Cart is ready for next passanger board */
	sem_t *sem_passBoarded = createAndInitSemaphor(&initStatus); /*< Passanger has boarded */
	sem_t *sem_passUnboard = createAndInitSemaphor(&initStatus); /*< Cart is ready for next passanger  unboard */
	sem_t *sem_passUnboarded = createAndInitSemaphor(&initStatus); /*< Passanger has unboarded */

	sem_t *sem_killChild = createAndInitSemaphor(&initStatus); /*< Passanger waits on this semaphor before exit */
	sem_t *sem_childKilled = createAndInitSemaphor(&initStatus); /*< The last words of passanger before passing away */

	if(initStatus){
		return errorHandler(2); // if error ocured while initialising semaphors, program ends with error
	}

	srand(time(0));

	//clear the logging file
	FILE *log = fopen("proj2.log", "w+");
	if(log == NULL){
		return errorHandler(2); //if error occures, program exits, with error
	}
	fclose(log);

	// lets start forking
	pid_t pid = fork();
	if(pid < 0){
		return errorHandler(2); //if error occures while forking, program exits with error
	}

	if(pid == 0){ //cart process
		setSharedMem(shm_cartPID, sem_cartPIDlock, getpid()); // saves PID of cart proces to shared mem shm_cartPID

		sem_wait(sem_start); //waits for start permittion

		printLog(shm_messageCounter, sem_messageCounterManipulated, "C %d \t : started \n", 1, INT_MIN);

		//one iteration for one cart ride
		for(int i = 0; i < P/C; i++){
			sem_post(sem_cartLoad); //tells others, that cart is ready to start
			printLog(shm_messageCounter, sem_messageCounterManipulated, "C %d \t : load \n", 1, INT_MIN);

			for(int j = 0; j < C; j++){
				sem_post(sem_passBoard); //cart is ready for new passenger
				sem_wait(sem_passBoarded); //new passenger has arrived
			}

			printLog(shm_messageCounter, sem_messageCounterManipulated, "C %d \t : run \n", 1, INT_MIN);

			if(RT != 0){
				usleep((rand() % RT)*1000); //waits for given ms
			}

			printLog(shm_messageCounter, sem_messageCounterManipulated, "C %d \t : unload \n", 1, INT_MIN);

			for(int j = 0; j < C; j++) {
				sem_post(sem_passUnboard); //cart needs one passenger to unboard
				sem_wait(sem_passUnboarded); //one passenger has unboarded
			}

			sem_wait(sem_cartUnloaded); //waits till cart has been unboarded
		}
		printLog(shm_messageCounter, sem_messageCounterManipulated, "C %d \t : finished \n", 1, INT_MIN);

		sem_post(sem_stop); //cart has stopped, all drives have been driven

		exit(0);
	}
	else{
		pid = fork();
		if(pid < 0){
			return errorHandler(2); //if error occures, program will be exited
		}

		if(pid == 0) { //passenger maker process
			setSharedMem(shm_makerPID, sem_makerPIDlock, getpid()); //saves PID of passanger maker process to shared mem shm_makerPID

			for(int i = 1; i <= P/C; i++){
				sem_wait(sem_cartLoad); // waits till cart can be loaded

				setSharedMem(shm_passengersInCartCounter, sem_passengerCounterManipulated, 0); // sets passengers counter to zero

				for(int j = 1; j <= C; j++){
					if(PT !=  0){
						usleep((rand() % PT)*1000); //waits for given ms
					}

					pid = fork();
					if(pid < 0){
						return errorHandler(2); //if error occures, program will be exited
					}

					if(pid == 0){ //passanger process
						manipulateSharedMem(shm_passengersInCartCounter, sem_passengerCounterManipulated, 1); // increments shared passenger counter
						manipulateSharedMem(shm_processCounter, sem_processCounterManipulated, 1); // increments shared process counter

						int I = *shm_processCounter; //makes value local for time of process creation

						printLog(shm_messageCounter, sem_messageCounterManipulated, "P %d \t : started \n", I, INT_MIN);
						sem_wait(sem_passBoard); // waits till passanger can be loaded

						printLog(shm_messageCounter, sem_messageCounterManipulated, "P %d \t : board \n", I, INT_MIN);

						sem_wait(sem_passengerCounterManipulated);
						if(*shm_passengersInCartCounter == C){
							printLog(shm_messageCounter, sem_messageCounterManipulated, "P %d \t : board last\n", I, INT_MIN);
						}
						else{
							printLog(shm_messageCounter, sem_messageCounterManipulated, "P %d \t : board order %d\n", I, *shm_passengersInCartCounter);
						}
						sem_post(sem_passengerCounterManipulated);

						sem_post(sem_passBoarded); //sends, that passenger has been boarded

						sem_wait(sem_passUnboard); //waits till passenger can be unboarded

						printLog(shm_messageCounter, sem_messageCounterManipulated, "P %d \t : unboard\n", I, INT_MIN);

						manipulateSharedMem(shm_passengersInCartCounter, sem_passengerCounterManipulated, -1); //decrements shared passenger counter
						sem_wait(sem_passengerCounterManipulated);
						if(*shm_passengersInCartCounter == 0){
							printLog(shm_messageCounter, sem_messageCounterManipulated, "P %d \t : unboard last\n", I, INT_MIN);
						}
						else{
							printLog(shm_messageCounter, sem_messageCounterManipulated, "P %d \t : unboard order %d\n", I, C - *shm_passengersInCartCounter);
						}
						sem_post(sem_passengerCounterManipulated);

						sem_post(sem_passUnboarded); //sends, that passanger has unboarded

						sem_wait(sem_killChild); //waits till passanger can die
						printLog(shm_messageCounter, sem_messageCounterManipulated, "P %d \t : finished\n", I, INT_MIN);
						sem_post(sem_childKilled); //sends, that passanger has passed away

						exit(0);
					}
				}

				sem_post(sem_cartUnloaded); //sends, that cart has been unloaded
			}

			exit(0);
		}
		else{ //main process
			//sends all triggers
			sem_post(sem_start);
			sem_post(sem_messageCounterManipulated);
			sem_post(sem_passengerCounterManipulated);
			sem_post(sem_processCounterManipulated);
			sem_post(sem_cartPIDlock);
			sem_post(sem_makerPIDlock);


			sem_wait(sem_stop); //waits on cart process end

			for(int i = 0; i < P; i++){ //kills all living passangers
				sem_post(sem_killChild);
				sem_wait(sem_childKilled);
			}

			waitpid(*shm_cartPID, NULL, 0);
			waitpid(*shm_makerPID, NULL, 0);
		}
	}

	//unmap all used memories and semaphors
	munmap(sem_messageCounterManipulated, shmSIZE);
	munmap(sem_passengerCounterManipulated, shmSIZE);
	munmap(sem_cartLoad, shmSIZE);
	munmap(sem_passBoard, shmSIZE);
	munmap(sem_start, shmSIZE);
	munmap(sem_stop, shmSIZE);
	munmap(sem_passBoarded, shmSIZE);
	munmap(sem_cartUnloaded, shmSIZE);
	munmap(sem_passUnboard, shmSIZE);
	munmap(sem_passUnboarded, shmSIZE);
	munmap(sem_cartPIDlock, shmSIZE);
	munmap(sem_makerPIDlock, shmSIZE);
	munmap(sem_killChild, shmSIZE);
	munmap(sem_childKilled, shmSIZE);

	munmap(shm_cartPID, shmSIZE);
	munmap(shm_makerPID, shmSIZE);
	munmap(shm_messageCounter, shmSIZE);
	munmap(shm_passengersInCartCounter, shmSIZE);
	munmap(shm_processCounter, shmSIZE);

	//finally, program ends
	return 0;
}