% 1 - nacteni souboru
	%nacte soubor do sloupcoveho vektoru a prevede jej na radkovy vektor
	[x, Fs] = audioread('xvodaz01.wav'); x = x'; 

	%pocet vzorku:
	length(x) = 16000
	%Vzorkovaci frekvence: 		
	Fs = 16000
	%D�lka v sekund�ch:
	length(x)/Fs = 1

% 2 - Spektrum sign�lu pomoc� DFT
	nf = 2048;
	X = spectrogram(x, nf);
	f = Fs/2 * linspace(0, 1, nf/2+1);
	modules = abs(X(1:nf/2 + 1));
	plot(f, modules)
	title('Spektrum sign�lu')
	xlabel('f (Hz)')
	ylabel('abs(X)');
	
% 3 - maximum modulu spektra
	[m, i] = max(modules); %i = index maxim�ln� hodnoty
	fmax =  i / length(X) * Fs/2	%=398.0488
	
% 4 - filtr - pou�ita ��st funkce ukazmito z uk�zky 3
	a = [1	    0.2289  0.4662];
	b = [0.2324 -0.4112 0.2324];
	
	N = 32; 
	n = 0:N-1; 
	imp = [1 zeros(1,N-1)]; %jednotkov� impuly
	h = filter(b, a, imp); 
	H = freqz(b,a,256); f=(0:255) / 256 * Fs / 2; 
	p = roots(a); 
	
	if (isempty(p) | abs(p) < 1) 
 		disp('Je stabilni')
	else
  		disp('Neni stabilni')
	end

	zplane (b,a); 

	%Je stabiln�

% 5 - Kmito�tov� charakteristika
	plot (f,abs(H)); grid; xlabel('f'); ylabel('|H(f)|');

	%horn� propust

% 6 - filtrovani signalu
	y = filter(b, a, x);
	
	nf = 2048;
	Y = spectrogram(y, nf);
	f = Fs/2 * linspace(0, 1, nf/2+1);
	modules = abs(Y(1:nf/2 + 1));
	plot(f, modules)
	title('Spektrum filtrovaneho sign�lu')
	xlabel('f (Hz)')
	ylabel('abs(Y)');

% 7 - maximum filtrovan�ho sign�lu
	[fm, fi] = max(modules);
	fmax =  fi / length(Y) * Fs/2	%=5658.5 Hz

% 8 - detekce obd�ln�k� - 20ms, 2kHz
	%20ms = 1/50s
	%20ms = 16000/50 = 320 vzorku

	%vytvoޒm si sign�l tvaru [h h h h -h -h -h -h] -> [1 1 1 1 -1 -1 -1 -1] a hled�m, na jak�m indexu je nejv�t�  absolutn� hodnota korelace (i antikorelace je shoda, ale naopak)

	nf = 256;
	f = Fs/2 * linspace(0, 1, nf/2+1);

	needle = reshape(repmat([1 1 1 1 -1 -1 -1 -1], 40, 1)',320,1);

	[val, shift] = xcorr(x, needle);
	val = val(16000:length(val));
	
	%stem(1:length(val), val)

	[m_val, m_index] = max(abs(val));
	block = x(m_index: m_index+319);
	
	Xb = spectrogram(block, nf);
	modules = abs(Xb(1:nf/2 + 1));
	plot(f, modules)

	%primichany signal zacina na 9398
	%zacina v case 0.5874s 

% 9 - autokorelacni koeficienty
	function y = my_autocorr_inner(x, k)
    		sum = 0;
    		if k >= 0
       			for i = 1 : 1 : 16000 - k
           	 	sum = sum + (x(i) * x(i + k));
        	end
    		else
        		for i = -k + 1 : 1 : 16000
        	    		sum = sum + (x(i) * x(i + k));
        		end
    		end
    
    		y = 1 / length(x) * sum;
	end
	
	function y = my_autocorr(x, k)
    		y = [];

    		for i = k
        		y = [y my_autocorr_inner(x, i)];
    		end
	end

	R = my_autocorr(x, -50:50);
	stem(-50:50, R);

% 10 - R[10]
	R[10] = R(51 + 10) = -0.0055

% 11 - �asov� odhad sdru�en� funkce hustoty rozd�len� pravd�podobnosti
	shift = 10

	xmin = min(x);
	xmax = max(x);
	[h,p,r] = hist2opt(x(1:16000-shift), x(1+shift:16000), linspace(xmin, xmax, 50));
	imagesc (x,x,p); axis xy; colorbar; xlabel('x2'); ylabel('x1');	

% 12 - Spr�vn� sdru�en� funkce rozd�len� pravd�podobnosti? 
	ano (ov��uje funkce hist2opt)
% 13 - Hodnota R[10]
	r = -0.0055
	V�sledky jsou shodn�










