#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
#include "simlib.h"

#define AVG_SPEED 8.11
#define DISTANCE_KOEF 1.5
#define CAR 80000
#define WEEK 10080
#define INCREASE 15/60
#define MINUTE 1
#define HOME 101
#define FREE 10

using namespace std;

struct Container {
	int amount;
	int current;
};

double distance_table[102][102];
int cluster_table[101][11];
Container containers[461];
bool autoActive;
double totalDistance;

void createTable(double table[102][102])
{
	ifstream table_file ( "table.csv");
	string value;

	for(int i = 1; i <= 101; i++)
	{
		for(int j = 1; j <= 101; j++)
		{
			getline ( table_file, value, ',' );
			table[i][j] = stod (value);
		}
	}
}

void createContainerAmount(Container containers[461])
{
	ifstream table_file ( "data.csv");
	string value;

	for(int i = 1; i <= 460; i++)
	{
		getline ( table_file, value, '\n' );
		containers[i].amount = stoi (value);
		containers[i].current = rand() % 75 + 1;

	}
}

void printTable(double table[102][102])
{
	for(int i = 1; i <= 101; i++)
	{
		printf("[%d] -> \n{ \n",i);
		for(int j = 1; j <= 101; j++)
		{
			printf("\t[%d] -> %.14lf \n",j, table[i][j] );
		}
		printf("} \n");
	}
}

void printClusterTable(int table[101][11])
{
	for(int i = 1; i <= 100; i++)
	{
		printf("[%d] -> \n{ \n",i);
		for(int j = 1; j <= 11; j++)
		{
			printf("\t[%d] -> %d \n",j, table[i][j] );
		}
		printf("} \n");
	}
}

void createClusterTable(int cluster_table[101][11])
{
	ifstream table_file ( "output_table.txt");
	string value;

	for(int i = 1; i <= 100; i++)
	{
		int j = 1;
		while(1)
		{
			getline ( table_file, value, ',' );
			if(value.compare(";") == 0)
				break;

			cluster_table[i][j] = stoi (value);
			j++;
		}
	}
}

int isOver(int cluster, int percent)
{
	for(int i = 1; i <= 11; i++)
	{
		if(cluster_table[cluster][i] == 0)
			break;

		if(containers[cluster_table[cluster][i]].current >= percent)
		{
			return cluster_table[cluster][i];
		}
	}

	return -1;
}

void findFullCluster(int percent, int cluster, int kontajner)
{
	int tmp;

	for(int i = 1; i <= 100; i++)
	{
		tmp = isOver(i, percent);
		if(tmp != -1)
		{
			cluster = i;
			kontajner = tmp;
			return;
		}
	}

	cluster = 0;
	kontajner = 0;

}

double countTime(int cluster1, int cluster2)
{
	double distance = distance_table[cluster1][cluster2] * DISTANCE_KOEF;
	totalDistance += distance;
	cout << totalDistance << " som v CountTime a zvysujem prejdenu vzdialenost\n";
	return (((distance*1000)/AVG_SPEED)/60);
}

int findCluster(int container)
{
	for (int i = 1; i <= 100; i++)
	{
		for(int j = 1; j <= 11; j++)
		{
			if(cluster_table[i][j] == container)
				return i;
		}
	}
}

class Auto : public Process
{
	private:
		int target;
		int position;
		int percent_low;
		int percent_high;
		int kontajner;
		int Korba;

	public:
		Auto(int target, int percent_low, int percent_high, int kontajner): Process()
		{
			this->target = target;
			this->percent_low = percent_low;
			this->percent_high = percent_high;
			this->kontajner = kontajner;
			this->Korba = CAR;
			this->position = HOME;
		}


	void Behavior()
	{
		int tmp_kontajner, tmp_cluster, next;

	cesta:
		Wait(countTime(target, position));
		position = target;

	vysyp:
		totalDistance += 0.05* DISTANCE_KOEF;
		cout << "vysypam a zvysujem totaldistance" << totalDistance << endl;
		int objem = containers[kontajner].amount * (containers[kontajner].current/100);
		if(objem < Korba)
		{
			cout << Korba << " hodnota korby\n";
			containers[kontajner].current = 0;
			Korba -= objem;
			Wait(MINUTE);
		}
		else
		{
			goto navrat;
		}

		next = isOver(position, percent_low);
		if(next != -1)
		{
			kontajner = next;
			goto vysyp;
		}

		findFullCluster(percent_high, tmp_cluster, tmp_kontajner);
		if(tmp_cluster != 0)
		{
			target = tmp_cluster;
			kontajner = tmp_kontajner;
			goto cesta;
		}

	navrat:
		target = HOME;
		Wait(countTime(target, position));
		position = target;

		Wait(FREE);
		Korba = CAR;
		autoActive = false;

	}
};

class Kontajner : public Process
{
	private:
		int percent_low = 0;
		int percent_high = 0;

	public:
		Kontajner(int percent_low, int percent_high):Process()
		{
			this->percent_low = percent_low;
			this->percent_high = percent_high;
		}

	void Behavior()
	{
		int number = rand() % 460 + 1;
		cout << "zvysujem kontajner cislo " << number << endl;
		containers[number].current++;

		if((containers[number].current > this->percent_high) && (autoActive == false))
		{
			cout << "kontajner cislo " << number << "plny, idem vysypat" << endl;
			autoActive = true;
			(new Auto(findCluster(number), percent_low, percent_high, number))->Activate();
		}

		//~Auto();
	}
};

class Generator : public Event
{
	private:
		int percent_low = 0;
		int percent_high = 0;

	public:
		Generator(int percent_low, int percent_high)
		{
			this->percent_low = percent_low;
			this->percent_high = percent_high;
		}

	void Behavior()
	{
		cout << ".....generujem....";
		(new Kontajner(this->percent_low, this->percent_high))->Activate();
		Activate(Time + Exponential(INCREASE));
	}
};

int main(int argc, char* argv[])
{
	if(argc != 3)
	{
		cout << "zle parametre" << endl;
		return 0;
	}
	createTable(distance_table);
	//printTable(table);
	createContainerAmount(containers);
	createClusterTable(cluster_table);
	//printClusterTable(cluster_table);

	autoActive = false;
	totalDistance = 0.00;

	Init(0, WEEK);
	cout << "zacinam generovat" << endl;
	(new Generator(atoi(argv[1]), atoi(argv[2])))->Activate();

	Run();

	return 0;
}
