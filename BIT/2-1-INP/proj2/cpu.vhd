-- cpu.vhd: Simple 8-bit CPU (BrainLove interpreter)
-- Copyright (C) 2016 Brno University of Technology,
--                    Faculty of Information Technology
-- Author(s): Zbysek Voda, xvodaz01
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ----------------------------------------------------------------------------
--                        Entity declaration
-- ----------------------------------------------------------------------------
entity cpu is
 port (
   CLK   : in std_logic;  -- hodinovy signal
   RESET : in std_logic;  -- asynchronni reset procesoru
   EN    : in std_logic;  -- povoleni cinnosti procesoru
 
   -- synchronni pamet ROM
   CODE_ADDR : out std_logic_vector(11 downto 0); -- adresa do pameti
   CODE_DATA : in std_logic_vector(7 downto 0);   -- CODE_DATA <- rom[CODE_ADDR] pokud CODE_EN='1'
   CODE_EN   : out std_logic;                     -- povoleni cinnosti
   
   -- synchronni pamet RAM
   DATA_ADDR  : out std_logic_vector(9 downto 0); -- adresa do pameti
   DATA_WDATA : out std_logic_vector(7 downto 0); -- mem[DATA_ADDR] <- DATA_WDATA pokud DATA_EN='1'
   DATA_RDATA : in std_logic_vector(7 downto 0);  -- DATA_RDATA <- ram[DATA_ADDR] pokud DATA_EN='1'
   DATA_RDWR  : out std_logic;                    -- cteni (1) / zapis (0)
   DATA_EN    : out std_logic;                    -- povoleni cinnosti
   
   -- vstupni port
   IN_DATA   : in std_logic_vector(7 downto 0);   -- IN_DATA <- stav klavesnice pokud IN_VLD='1' a IN_REQ='1'
   IN_VLD    : in std_logic;                      -- data platna
   IN_REQ    : out std_logic;                     -- pozadavek na vstup data
   
   -- vystupni port
   OUT_DATA : out  std_logic_vector(7 downto 0);  -- zapisovana data
   OUT_BUSY : in std_logic;                       -- LCD je zaneprazdnen (1), nelze zapisovat
   OUT_WE   : out std_logic                       -- LCD <- OUT_DATA pokud OUT_WE='1' a OUT_BUSY='0'
 );
end cpu;


-- ----------------------------------------------------------------------------
--                      Architecture declaration
-- ----------------------------------------------------------------------------
architecture behavioral of cpu is

signal pc_inc: std_logic := '0';
signal pc_dec: std_logic := '0';
signal pc: std_logic_vector(11 downto 0) := (others => '0');

signal cnt_inc: std_logic := '0';
signal cnt_dec: std_logic := '0';
signal cnt: std_logic_vector(7 downto 0) := (others => '0'); 

signal tmp_id: std_logic := '0';
signal tmp_out: std_logic_vector(7 downto 0) := (others => '0');

signal ptr_inc: std_logic := '0';
signal ptr_dec: std_logic := '0';
signal ptr: std_logic_vector(9 downto 0) := (others => '0');

signal mux_sel: std_logic_vector(1 downto 0) := (others => '0');

type Instruction is (
   PtrPlus, 
   PtrMinus, 
   DataPlus, 
   DataMinus, 
   DataPrint, 
   DataRead, 
   JumpStart, 
   JumpStop, 
   ChangeTmp, 
   ReadTmp, 
   Halt, 
   Unknown
);
signal actualInstruction: Instruction;

type State is (
   S_Fetch,
   S_Decode, 
   S_PtrPlus,
   S_PtrMinus,
   S_DataPlus_load, S_DataPlus_write,
   S_DataMinus_load, S_DataMinus_write,
   S_DataPrint_load, S_DataPrint_print,
   S_DataRead,
   S_JumpStart_1, S_JumpStart_2, S_JumpStart_3, S_JumpStart_4,
   S_JumpStop_1, S_JumpStop_2, S_JumpStop_3, S_JumpStop_4, S_JumpStop_5,
   S_ChangeTmp_read, S_ChangeTmp_write,
   S_ReadTmp,
   S_Halt,
   S_Unknown
);
signal currentState: State := S_Fetch;
signal nextState: State := S_Fetch;

begin
PC_process: process (CLK, RESET, pc_inc, pc_dec)
begin
   if RESET = '1' then
      pc <= (others => '0');
   elsif rising_edge(CLK) then
      if pc_inc = '1' then
         pc <= pc + 1;
      elsif pc_dec = '1' then
         pc <= pc - 1;
      end if;
   end if;
end process;

CNT_process: process (CLK, RESET, cnt_inc, cnt_dec)
begin
   if RESET = '1' then
      cnt <= (others => '0');
   elsif rising_edge(CLK) then
      if cnt_inc = '1' then
         cnt <= cnt + 1;
      elsif cnt_dec = '1' then
         cnt <= cnt - 1;
      end if;
   end if;
end process;

TMP_process: process (CLK, RESET, tmp_id)
begin
   if RESET = '1' then
      tmp_out <= (others => '0');
   elsif rising_edge(CLK) then
      if tmp_id = '1' then
         tmp_out <= DATA_RDATA;
      end if;
   end if;
end process;

PTR_process: process (CLK, RESET, ptr_inc, ptr_dec)
begin
   if RESET = '1' then
      ptr <= (others => '0');
   elsif rising_edge(CLK) then
      if ptr_inc = '1' then
         ptr <= ptr + 1;
      elsif ptr_dec = '1' then
         ptr <= ptr - 1;
      end if;
   end if;
end process;

STATESHIFT_process: process (CLK, EN, RESET, nextState)
begin
   if RESET = '1' then 
      currentState <= S_Fetch;
   elsif EN = '1' and rising_edge(CLK) then
      currentState <= nextState;
   end if;
end process;

IREG_process: process (CODE_DATA)
begin
  case CODE_DATA is
    when x"3E" => actualInstruction <= PtrPlus;
    when x"3C" => actualInstruction <= PtrMinus;
    when x"2B" => actualInstruction <= DataPlus;
    when x"2D" => actualInstruction <= DataMinus;
    when x"5B" => actualInstruction <= JumpStart;
    when x"5D" => actualInstruction <= JumpStop;
    when x"2E" => actualInstruction <= DataPrint;
    when x"2C" => actualInstruction <= DataRead;
    when x"24" => actualInstruction <= ChangeTmp;
    when x"21" => actualInstruction <= ReadTmp;
    when x"00" => actualInstruction <= Halt;
    when others => actualInstruction <= Unknown;
  end case;
end process;  

MUX_process: process (mux_sel, IN_DATA, tmp_out, DATA_RDATA)
begin
  case mux_sel is 
    when "00" => DATA_WDATA <= IN_DATA;
    when "01" => DATA_WDATA <= tmp_out;
    when "10" => DATA_WDATA <= DATA_RDATA - 1;
    when "11" => DATA_WDATA <= DATA_RDATA + 1;
    when others =>
  end case;
end process;

CODE_ADDR <= pc;
DATA_ADDR <= ptr;
OUT_DATA <= DATA_RDATA;

FSM: process (nextState, currentState, OUT_BUSY, IN_VLD, IN_DATA, DATA_RDATA, CODE_DATA, cnt, actualInstruction) 
begin
  pc_inc <= '0';
  pc_dec <= '0';
  cnt_inc <= '0';
  cnt_dec <= '0';
  tmp_id <= '0';
  ptr_inc <= '0';
  ptr_dec <= '0';
  mux_sel <= "00";

  CODE_EN <= '0';
  DATA_RDWR <= '0';
  DATA_EN <= '0';
  IN_REQ <= '0';
  OUT_WE <= '0';

  nextState <= S_Fetch;

  case currentState is
    when S_Fetch =>
      CODE_EN <= '1';
      nextState <= S_Decode;
        
    when S_Decode =>
      case actualInstruction is
        when PtrPlus => nextState <= S_PtrPlus;
        when PtrMinus => nextState <= S_PtrMinus;
        when DataPlus => nextState <= S_DataPlus_load;
        when DataMinus => nextState <= S_DataMinus_load;
        when DataPrint => nextState <= S_DataPrint_load;
        when DataRead => nextState <= S_DataRead;
        when JumpStart => nextState <= S_JumpStart_1;
        when JumpStop => nextState <= S_JumpStop_1;
        when ChangeTmp => nextState <= S_ChangeTmp_read;
        when ReadTmp => nextState <= S_ReadTmp;
        when Halt => nextState <= S_Halt;
        when Unknown => nextState <= S_Unknown;
      end case;

    when S_PtrPlus => -- >
      ptr_inc <= '1';
      pc_inc <= '1';

    when S_PtrMinus => -- <
      ptr_dec <= '1';
      pc_inc <= '1';
        
    when S_DataPlus_load => -- +
      DATA_EN <= '1';
      DATA_RDWR <= '1';
      nextState <= S_DataPlus_write;
    when S_DataPlus_write => 
      DATA_EN <= '1';
      DATA_RDWR <= '0';
      mux_sel <= "11";
      pc_inc <= '1';
          
    when S_DataMinus_load => -- -
      DATA_EN <= '1';
      DATA_RDWR <= '1';
      nextState <= S_DataMinus_write;
    when S_DataMinus_write => 
      DATA_EN <= '1'; 
      DATA_RDWR <= '0';
      mux_sel <= "10";
      pc_inc <= '1';

    when S_DataPrint_load => -- .
      nextState <= S_DataPrint_load;
      if OUT_BUSY = '0' then
        DATA_EN <= '1';
        DATA_RDWR <= '1';
        nextState <= S_DataPrint_print;
      end if;
    when S_DataPrint_print =>
      OUT_WE <= '1';
      pc_inc <= '1';

    when S_DataRead => -- ,
      IN_REQ <= '1';
      if IN_VLD = '1' then
        mux_sel <= "00";
        DATA_EN <= '1';
        DATA_RDWR <= '0';
        pc_inc <= '1';
      else
        nextState <= S_DataRead;
      end if;

    when S_JumpStart_1 => -- [
      DATA_EN <= '1';
      DATA_RDWR <= '1';
      pc_inc <= '1';
      nextState <= S_JumpStart_2;
    when S_JumpStart_2 => 
      if DATA_RDATA = "00000000" then
        nextState <= S_JumpStart_3;
        cnt_inc <= '1';
      end if;
    when S_JumpStart_3 =>
      if cnt /= "00000000" then
        CODE_EN <= '1';
        nextState <= S_JumpStart_4;
      end if;
    when S_JumpStart_4 => 
      if actualInstruction = JumpStart then
        cnt_inc <= '1';
      elsif actualInstruction = JumpStop then
        cnt_dec <= '1';
      end if;
      pc_inc <= '1';
      nextState <= S_JumpStart_3;

    when S_JumpStop_1 => -- ]
      DATA_EN <= '1';
      DATA_RDWR <= '1';
      nextState <= S_JumpStop_2;
    when S_JumpStop_2 => 
      if DATA_RDATA = "00000000" then
        pc_inc <= '1';
      else
        pc_dec <= '1';
        cnt_inc <= '1';
        nextState <= S_JumpStop_3;
      end if;
    when S_JumpStop_3 => 
      if cnt /= "00000000" then
        CODE_EN <= '1';
        nextState <= S_JumpStop_4;
      end if;
    when S_JumpStop_4 => 
      if actualInstruction = JumpStart then
        cnt_dec <= '1';
      elsif actualInstruction = JumpStop then
        cnt_inc <= '1';
      end if;
      nextState <= S_JumpStop_5;
    when S_JumpStop_5 => 
      if cnt = "00000000" then
        pc_inc <= '1';
      else 
        pc_dec <= '1';
        nextState <= S_JumpStop_3;
      end if;

    when S_ChangeTmp_read => -- $
      DATA_EN <= '1';
      DATA_RDWR <= '1';
      nextState <= S_ChangeTmp_write;
    when S_ChangeTmp_write => 
      tmp_id <= '1';
      pc_inc <= '1';

    when S_ReadTmp => -- !
      DATA_EN <= '1';
      DATA_RDWR <= '0';
      mux_sel <= "01";
      pc_inc <= '1';
    
    when S_Halt => -- null
      nextState <= S_Halt;
    
    when S_Unknown =>
      pc_inc <= '1';
  end case;
end process;

end behavioral; 