library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity ledc8x8 is
port ( -- Sem doplnte popis rozhrani obvodu.
     RESET: in std_logic;
     SMCLK: in std_logic;

     ROW: out std_logic_vector(7 downto 0);
     LED: out std_logic_vector(0 to 7)
);
end ledc8x8;

architecture main of ledc8x8 is
    type MATRIX is array (7 downto 0) of std_logic_vector(7 downto 0);
    type INICIALS is array (0 to 1) of MATRIX;
    constant I: INICIALS :=
    (
        (
            "11111111",
            "11111111",
            "00001111",
            "00011110",
            "00111100",
            "11110000",
            "11111111",
            "11111111"
        ),
        (
            "11000011",
            "11000011",
            "11000011",
            "11100111",
            "01100110",
            "01100110",
            "00111100",
            "00011000"
        )
    );

    signal ticks_counter: std_logic_vector(23 downto 0)  := (others => '0');
    signal ROW_inner:     std_logic_vector(7 downto 0)   := (others => '0');

    signal letter:        std_logic                      := '0';
    signal ce_shift:      std_logic                      := '0';    
begin
    --------------------------------------------------
    ----------------SYNCHRONOUS PART------------------

    couter: process(SMCLK, RESET)
    begin
        if RESET = '1' then
            ticks_counter <= (others => '0');
        elsif rising_edge(SMCLK) then
            ticks_counter <= ticks_counter + 1;
        end if;
    end process; --counter

    shift_register8: process(SMCLK, RESET)
    begin
        if RESET = '1' then
            ROW_inner <= (7 => '1', others => '0');
        elsif rising_edge(SMCLK) then
            if ce_shift = '1' then
                ROW_inner <= ROW_inner(0) & ROW_inner(7 downto 1);
            end if;
        end if;
    end process;--8bit_shift_register

    ----------------------------------------------------
    ----------------COMBINATIONAL PART------------------

    letter <= ticks_counter(23);

    --ce_shift is active one tick every 256 ticks
    with ticks_counter(7 downto 0) select
        ce_shift <= 
            '1' when "11111111",
            '0' when others;

    ROW <= ROW_inner;

    with letter & ROW_inner select
        LED <= 
            -- letter = '0'
            not I(0)(0) when "010000000",
            not I(0)(1) when "001000000",
            not I(0)(2) when "000100000",
            not I(0)(3) when "000010000",
            not I(0)(4) when "000001000",
            not I(0)(5) when "000000100",
            not I(0)(6) when "000000010",
            not I(0)(7) when "000000001",
            -- letter = '1'
            not I(1)(0) when "110000000",
            not I(1)(1) when "101000000",
            not I(1)(2) when "100100000",
            not I(1)(3) when "100010000",
            not I(1)(4) when "100001000",
            not I(1)(5) when "100000100",
            not I(1)(6) when "100000010",
            not I(1)(7) when "100000001",
            "11111111"  when others;
end main;