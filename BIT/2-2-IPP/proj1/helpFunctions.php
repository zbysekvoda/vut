<?php

function separateByCommas($str)
{
    if ($str === null) return null;

    $inAppos = false;
    $out = array();
    $actual = "";
    $len = strlen($str);
    $i = 0;

    while ($i < $len) {
        if ($str[$i] === chr(39)) $inAppos = !$inAppos;
        if ($str[$i] === ',' AND !$inAppos) {
            $out[] = $actual;
            $actual = "";
        } else {
            $actual .= $str[$i];
        }

        $i++;
    }

    $out[] = $actual;

    if(count($out) === 1 AND $out[0] === ""){
        return array();
    }

    return $out;
}

function prettyPrintFSM($FSM)
{
    array_map(function ($el, $key) {
        echo "$key:\t";

        if ($key !== "Sig") echo "\t";
        if ($key === "s") echo $el . "\n";
        else if ($key === "R") {
            echo "{" . join(array_map(function ($rule) {
                    return $rule[0] . $rule[1] . "->" . $rule[2];
                }, $el), ", ") . "}\n";
        } else echo "{" . join($el, ", ") . "}\n";
    }, $FSM, array_keys($FSM));
}

function isOKOrBad($part, $bool)
{
    echo $part;
    echo $bool == true ? "OK\n" : "BAD\n";
}

/* Funkce parsující parametry příkazové řádky
 *
 */

//vrátí parametry příkazové řádky jako hash mapu - dvojice odpovídajících přepínačů jsou indexovány na stejný index
//dochází také k ke kontrole duplicitních přepínačů
function getParams($argv)
{
    $allowedParams = array(
        "--help" => "help",
        "--input" => "input",
        "--output" => "output",

        "--no-epsilon-rules" => "no-epsilon",
        "-e" => "no-epsilon",
        "--determinization" => "determinization",
        "-d" => "determinization",
        "--case-insensitive" => "insensitive",
        "-i" => "insensitive"
    );

    $out = array();

    foreach ($argv as $index => $val) {
        if ($index === 0) continue;

        $parts = explode("=", $val);
        if ($parts[0] !== null) {
            $paramKey = $allowedParams[$parts[0]];
            if ($paramKey === null) exit(1);
            if ($out[$paramKey] !== null) exit(1);

            $out[$paramKey] = $parts[1] !== null ? $parts[1] : false;
        }
    }

    return $out;
}

?>