<?php
//zkontroluje, jestli je FSM uzavřený do kulatých závorek
function isFSMInParenthesis($FSM)
{
    return $FSM[0] === "(" AND $FSM[count($FSM) - 1] === ")";
}

//funkce, která vrací funkci testující, zda je funkce $f pravdivá na všech prvcích předaného pole (první parametr)
function isTrueForAllElements($f)
{
    return function ($set, $extra1 = null, $extra2 = null) use ($f) {
        return array_reduce($set, function ($acc, $el) use ($f, $extra1, $extra2) {
            return $acc AND $f($el, $extra1, $extra2);
        }, true);
    };
}

//zkontroluje syntaktickou správnost stavu
function isStateSyntacticallyValid($state)
{
    $s = str_split($state);
    $s_len = count($s);

    if ($s[0] === "_" OR $s[$s_len - 1] === "_") return false;
    if (!ctype_alpha($s[0])) return false;

    return array_reduce($s, function ($acc, $char) {
        return $acc AND (ctype_alnum($char) OR $char === "_");
    }, true);
}

//zkontroluje syntaktickou správnost symbolu
function isSymbolSyntacticallyValid($state)
{
    $s = str_split($state);
    $s_len = count($s);

    return ($s[0] === chr(39) AND $s[$s_len - 1] === chr(39));
}

//zkontroluje syntaktickou správnost pravidla
function isRuleSyntacticallyValid($rule)
{
    return isStateSyntacticallyValid($rule[0]) AND isSymbolSyntacticallyValid($rule[1]) AND isStateSyntacticallyValid($rule[2]);
}

//vytvoření syntaktických kontrol pomocí funkce vracející jiné funkce
$areAllStatesSyntacticallyValid = isTrueForAllElements('isStateSyntacticallyValid');
$areAllSymbolsSyntacticallyValid = isTrueForAllElements('isSymbolSyntacticallyValid');
$areAllRulesSyntacticallyValid = isTrueForAllElements('isRuleSyntacticallyValid');


//zkontroluje sémantickou správnost symbolu
function isSigmaSemanticallyValid($sig)
{
    return strlen(join($sig, "")) !== 0;
}

//zkontroluje sémantickou správnost stavu
function isStartingStateSemanticallyValid($s, $Q)
{
    return in_array($s, $Q);
}

//zkontroluje sémantickou správnost pravidla
function isRuleSemanticallyValid($rule, $Q, $Sig)
{
    return in_array($rule[0], $Q) AND (in_array($rule[1], $Sig) OR $rule[1] === "''") AND in_array($rule[2], $Q);
}

//vytvoření sémantických kontrol pomocí funkce vracející jiné funkce
$areAllRulesSemanticallyValid = isTrueForAllElements('isRuleSemanticallyValid');
$areAllFinalStatesSemanticallyValid = isTrueForAllElements('in_array');
?>