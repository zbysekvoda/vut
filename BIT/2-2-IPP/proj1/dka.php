<?php
require "FSMParsingFunctions.php";
require "FSMCheckupFunctions.php";
require "FSMDeterminisationFunctions.php";
require "FSMFormatter.php";


//načte parametry příkazové řádky
$params = getParams($argv);

if ($params["help"] !== null) {
    if (count($params) !== 1) exit(1);

    $file = fopen('php://stdout', 'w') or die(3);

    $out = "Script dka.php provadi determinizaci zadaneho konecneho automatu, odstraneni eps-prechodu, popripade pouze normalizace.\n
Dostupne prepinace:
--help
    vypise na standardni vystup napovedu skriptu (nenacita zadny vstup), kterou lze prevzit ze zadani 
    (lze odstranit diakritiku, pripadne prelozit do anglictiny dle zvoleneho jazyka dokumentace). 
    Tento parametr nelze kombinovat s zadnym dalsim parametrem, jinak skript ukoncete s chybou.
--input=filename
    zadany vstupni soubor filename muze byt zadan relativni cestou nebo absolutni cestou; 
    v pripade, ze by nazev ci cesta obsahovala mezeru, tak musi byt uvedena cela cesta 
    i se jmenem souboru v uvozovkach; chybi-li tento parametr, je uvazovan standardni vstup.
--output=filename
    zadany vystupni soubor filename muze byt zadan relativni cestou nebo absolutni cestou; v pripade, 
    ze by nazev ci cesta obsahovala mezeru, tak musi byt uvedena cesta i se jmenem souboru v uvozovkach;
    chybi-li tento parametr, je vystup presmerovan na standardni vystup. Existuje-li jiz vystupni soubor, 
    bude bez varovani prepsan, jinak bude vytvoren novy soubor.
-e, --no-epsilon
    rules pro pouhe odstraneni ε - pravidel vstupniho konecneho automatu . Parametr nelze kombinovat
    s parametrem - d(resp . --determinization).
-d, --determinization
    provede determinizaci bez generovani nedostupnych stavu(viz algo - ritmus IFJ,
    4. prednaska, snimek 24 / 36). Parametr nelze kombinovat s parametrem - e(resp . --no-epsilon - rules).
-i, --case-insensitive
    nebude bran ohled na velikost znaku pri porovnavani symbolu ci stavu(tj . a = A, ahoj = AhOj nebo A b = a B);
    ve vystupu potom budou vsechna velka pismena prevedena na mala.\n";

    fputs($file, $out);
    fclose($file);

    exit(0);
}

$in = "";

//načtení vstupu
if ($params["input"] !== null) {
    if ($params["input"] === false) exit(2);
    if (!file_exists($params["input"])) exit(2);
    if (filetype($params["input"]) !== "file") exit(2);
    if (mime_content_type($params["input"]) !== "text/plain") exit(2);
    $file = fopen($params["input"], "r") or die(2);

    do {
        $in .= fgetc($file);
    } while (!feof($file));

    fclose($file);
} else {
    $file = fopen('php://stdin', 'r') or die(2);

    while (FALSE !== ($line = fgets($file))) {
        $in .= $line;
    }

    fclose($file);
}

if ($params["insensitive"] !== null) {
    $in = strtolower($in);
}

//odstraní nepotřebné mezery z textové reprezentace FSM
$FSMData = cleanSpacesFromFSMTextualRepresentation($in);
if (!isFSMInParenthesis($FSMData)) exit(40); // je FSM obalené () ?

//získá hash mapu s jendotlivými částmi FSM (Q, Sig, R, s, F)
$FSM = getFSMParts($FSMData);
if ($FSM === null) exit(40);

//zkontroluje syntaktickou správnost částí FSM
if (!$areAllStatesSyntacticallyValid($FSM["Q"])) exit(40);
if (!$areAllSymbolsSyntacticallyValid($FSM["Sig"])) exit(40);
if (!$areAllRulesSyntacticallyValid($FSM["R"])) exit (40);

//zkontroluje sémantickou správnost částí FSM
if (!isSigmaSemanticallyValid($FSM["Sig"])) exit(41);
if (!$areAllRulesSemanticallyValid($FSM["R"], $FSM["Q"], $FSM["Sig"])) exit(41);
if (!isStartingStateSemanticallyValid($FSM["s"], $FSM["Q"])) exit(41);
if (!$areAllFinalStatesSemanticallyValid($FSM["F"], $FSM["Q"])) exit(41);

if ($params["no-epsilon"] !== null OR $params["determinization"] !== null) {
    //nesmí být oba přepínače --no-epsilon-rules a --determinisation najednou
    if ($params["no-epsilon"] !== null AND $params["determinization"] !== null) exit(1);

    //odstraní z FSM eps-přechody
    $FSMWithoutEPS = removeEpsTransitions($FSM);

    if ($params["no-epsilon"] !== null) { //pokud byl zadán přepínač --no-epsilon-rules
        $out = formatFSMForOutput($FSMWithoutEPS);
    } else if ($params["determinization"] !== null) { //pokud byl zadán přepínač --determinization
        //provede determinizaci FSM
        $FSMDeterministic = determinise($FSMWithoutEPS);
        $out = formatFSMForOutput($FSMDeterministic);
    }
} else {
    $out = formatFSMForOutput($FSM);
}

//vypíše na patřičný výstup normalizovaný FSM dle přepínačů
if ($params["output"] !== null) {
    if ($params["output"] === false) exit(3);
    $file = fopen($params["output"], "w") or die(3);

    if (filetype($params["output"]) !== "file") exit(3);
} else {
    $file = fopen('php://stdout', 'w') or die(3);
}

fputs($file, $out);
fclose($file);
?>