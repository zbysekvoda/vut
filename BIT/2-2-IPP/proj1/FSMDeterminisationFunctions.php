<?php

//pomocná funkce, která vkládá prvky pole $add do pole $arr, pokud je toto pole ještě neobsahuje (zabraňuje duplicitám)
function arrayMergeUnique($arr, $add)
{
    if (is_array($add)) {
        return array_reduce($add, function ($acc, $el) {
            if (!in_array($el, $acc)) array_push($acc, $el);

            return $acc;
        }, $arr);
    }

    if (!in_array($add, $arr)) array_push($arr, $add);
    return $arr;
}

//kontroluje, zda je průnik prázdná množina
function isIntersectionEmpty($A, $B)
{
    return array_reduce($A, function ($acc, $el) use ($B) {
        return $acc AND !in_array($el, $B);
    }, true);
}

//vrátí množinu stavů, kam se ze stavů z monžiny M můžu dostat bez přečtení symbolu
function getEpsTranslatesFromRuleSet($M, $R)
{
    return array_reduce($R, function ($acc, $rule) use ($M) {
        if ($rule[1] === "''" AND in_array($rule[0], $M)) {
            return arrayMergeUnique($acc, $rule[2]);
        }
        return $acc;
    }, array());
}

//vrátí eps uzávěr
function getEpsClosure($p, $R)
{
    $Q_old = array($p);

    while (true) {
        $Q_new = getEpsTranslatesFromRuleSet($Q_old, $R);
        $Q_new = arrayMergeUnique($Q_old, $Q_new);

        if ($Q_old === $Q_new) break;
        $Q_old = $Q_new;
    }

    return $Q_old;
}

//pomocná funkce pro odstranění eps přechodů, vrací pravidla, která budou přidána do nové množiny pravidel
function getRulesToAdd($p, $R)
{
    $epsClosure = getEpsClosure($p, $R);

    return array_reduce($epsClosure, function ($acc, $p_new) use ($R, $p) {
        return arrayMergeUnique(
            $acc,
            array_reduce($R, function ($acc, $rule) use ($p, $p_new) {
                if ($rule[0] === $p_new AND $rule[1] !== "''") {
                    return arrayMergeUnique($acc, array(array($p, $rule[1], $rule[2])));
                }
                return $acc;
            }, array())
        );
    }, array());
}

//odstraní eps přechpdy z FSM
function removeEpsTransitions($FSM)
{
    $Q = $FSM["Q"];
    $R = $FSM["R"];
    $F = $FSM["F"];

    $R_new = array_reduce($Q, function ($acc, $p) use ($R) {
        $added = getRulesToAdd($p, $R);
        return arrayMergeUnique($acc, $added);
    }, array());

    $F_new = array_reduce($Q, function ($acc, $p) use ($R, $F) {
        $epsClosure = getEpsClosure($p, $R);

        if (!isIntersectionEmpty($epsClosure, $F)) return arrayMergeUnique($acc, $p);
        return $acc;
    }, array());

    return array(
        "Q" => $Q,
        "Sig" => $FSM["Sig"],
        "R" => $R_new,
        "s" => $FSM["s"],
        "F" => $F_new
    );
}

//pomocná funkce pro determinizaci, vrací novou množinu Q''
function getQaa($Q_a, $R, $a)
{
    return array_reduce($R, function ($acc, $rule) use ($Q_a, $a) {
        if (!in_array($rule[0], $Q_a)) return $acc;
        if ($rule[1] !== $a) return $acc;

        return arrayMergeUnique($acc, $rule[2]);
    }, array());
}

//vrátí stav vzniklý spojením seřazeného pole $S podtržítkem
function joinStatesInArray($S)
{
    natsort($S);
    return join($S, "_");
}

//provede spojení stavů na všech prvcích pole $A
function joinStatesInArrays($A)
{
    return array_map(function ($stateArray) {
        return joinStatesInArray($stateArray);
    }, $A);
}

//spojí stavy ve stavech v pravidle $rule
function joinStatesInRule($rule)
{
    natsort($rule[0]);
    natsort($rule[2]);

    return array(
        joinStatesInArray($rule[0]),
        $rule[1],
        joinStatesInArray($rule[2])
    );
}

//provede spojení stavů ve všech pravidlech v poli $R
function joinStatesInRules($R)
{
    return array_map(function ($rule) {
        return joinStatesInRule($rule);
    }, $R);
}

//provede determinizaci podle zadaného algoritmu
function determinise($FSM)
{
    $Q_d = array();
    $Sig_d = $FSM["Sig"];
    $R_d = array();
    $s_d = array($FSM["s"]);
    $F_d = array();

    $Q_new = array($s_d);

    $Q_v = array(); //visited states

    do {
        $Q_a = array_pop($Q_new);
        $Q_d = arrayMergeUnique($Q_d, array($Q_a));

        $Q_v = arrayMergeUnique($Q_v, array($Q_a));

        foreach ($Sig_d as $a) {
            $Q_aa = getQaa($Q_a, $FSM["R"], $a);

            if (count($Q_aa) !== 0) {
                $R_d = arrayMergeUnique($R_d, array(array($Q_a, $a, $Q_aa)));
            }

            if (!in_array($Q_aa, arrayMergeUnique($Q_d, array(array())))) {
                if (!in_array($Q_aa, $Q_v)) {
                    $Q_new = arrayMergeUnique($Q_new, array($Q_aa));
                }
            }
        }

        if (!isIntersectionEmpty($Q_a, $FSM["F"])) {
            $F_d = arrayMergeUnique($F_d, array($Q_a));
        }
    } while (count($Q_new) !== 0);

    $Q_d = joinStatesInArrays($Q_d);
    $R_d = joinStatesInRules($R_d);
    $F_d = joinStatesInArrays($F_d);
    $s_d = joinStatesInArray($s_d);

    return array(
        "Q" => array_unique($Q_d, SORT_REGULAR),
        "Sig" => array_unique($Sig_d, SORT_REGULAR),
        "R" => array_unique($R_d, SORT_REGULAR),
        "s" => $s_d,
        "F" => array_unique($F_d, SORT_REGULAR)
    );
}

?>