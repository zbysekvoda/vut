<?php
//normalizuje FSM pro výstup
function formatFSMForOutput($FSM)
{
    sort($FSM["Q"]);
    sort($FSM["Sig"]);
    sort($FSM["F"]);

    //řazení podle složeného klíče
    usort($FSM["R"], function($a, $b){
        $first = strcmp($a[0], $b[0]);
        if($first !== 0) return $first;

        $second = strcmp($a[1], $b[1]);
        if($second !== 0) return $second;

        return strcmp($a[2], $b[2]);
    });

    $out =
        "(\n" .
        "{" .
        join($FSM["Q"], ", ") .
        "},\n" .
        "{" .
        join($FSM["Sig"], ", ") .
        "},\n" .
        "{\n" .
        join(array_map(function($rule){
            return $rule[0]." ".$rule[1]." -> ".$rule[2];
        }, $FSM["R"]), ",\n") .
        (count($FSM["R"]) !== 0 ? "\n" : "").
        "},\n" .
        $FSM["s"] .
        ",\n" .
        "{" .
        join($FSM["F"], ", ") .
        "}\n" .
        ")\n";

    return $out;
}


?>