<?php
require "helpFunctions.php";

//odstraní nepotřebné mezery z textové reprezentace FSM
function cleanSpacesFromFSMTextualRepresentation($input)
{
    $in = str_split($input);

    $withoutComments = array();
    $inComment = false;
    $canIgnoreSpace = true;
    for ($i = 0; $i < count($in); $i++) {
        $c = $in[$i];

        if ($c === "#" AND $canIgnoreSpace) { //začátek komentáře
            $inComment = true;
            continue;
        }

        if ($inComment === true AND in_array($c, array("\n", "\r", chr(26)))) {
            $inComment = false;
            continue;
        }

        if (in_array($c, array(" ", "\r", "\n", "\t")) > 0 AND $canIgnoreSpace) {
            continue;
        }

        if ($c === chr(39)) {
            $canIgnoreSpace = !$canIgnoreSpace;
        }

        if (!$inComment) {
            $withoutComments[] = $c;
        }
    }

    return $withoutComments;
}

//vrátí text uzavřený v nejbližších {} (část FSM)
function getTextInBrackets($data, $start, $len)
{
    $out = "";
    $i = $start;

    while ($data[$i++] !== "{" AND $i < $len) ;
    while ($data[$i] !== "}" AND $i < $len) {
        $out .= $data[$i];
        $i++;
    }

    return $i === $len ? null : array("endIndex" => $i, "data" => $out);
}

//vrátí písmeno následující po čárce (pro počáteční stav)
function getLetterAfterComma($data, $start, $len)
{
    $out = "";
    $i = $start;

    while ($data[$i++] !== "," AND $i < $len) ;
    while ($data[$i] !== "," AND $i < $len) {

        $out .= $data[$i];
        $i++;
    }

    return $i === $len ? null : array("endIndex" => $i, "data" => $out);
}

//vrátí FSM jako hash mapu indexovanou jednotlivými částmi FSM (Q, Sig, R, s, F)
function getFSMParts($data)
{
    $i = 0;
    $len = count($data);

    $parsed["Q"] = getTextInBrackets($data, $i, $len);
    $parsed["Sig"] = getTextInBrackets($data, $parsed["Q"]["endIndex"], $len);
    $parsed["R"] = getTextInBrackets($data, $parsed["Sig"]["endIndex"], $len);
    $parsed["s"] = getLetterAfterComma($data, $parsed["R"]["endIndex"], $len);
    $parsed["F"] = getTextInBrackets($data, $parsed["s"]["endIndex"], $len);

    $out = array(
        "Q" => $parsed["Q"] === null ? array() : $parsed["Q"]["data"],
        "Sig" => $parsed["Sig"] === null ? array() : $parsed["Sig"]["data"],
        "R" => $parsed["R"] === null ? array() : $parsed["R"]["data"],
        "s" => $parsed["s"] === null ? "" : $parsed["s"]["data"],
        "F" => $parsed["F"] === null ? array() : $parsed["F"]["data"]
    );

    $out = array(
        "Q" => array_unique(explode(",", $out["Q"])),
        "Sig" => array_unique(separateByCommas($out["Sig"])),
        "R" => array_unique(separateByCommas($out["R"])),
        "s" => $out["s"],
        "F" => array_unique(separateByCommas($out["F"]))
    );


    $out["R"] = array_map(function ($rule) {
        $apostrophesCount = substr_count($rule, chr(39));
        if ($apostrophesCount !== 2 AND $apostrophesCount !== 4) return null;

        $a = explode("->", $rule);
        $stop = $a[1];

        $start = "";
        $i = 0;
        while ($a[0][$i] !== chr(39)) {
            $start .= $a[0][$i];

            $i++;
        }
        $sym = substr($a[0], $i, strlen($a[0]));

        // $pom2[0] - state from
        // $pom2[1] - symbol without apostrophes
        // $pom1[1] - state to

        return array($start, $sym, $stop);
    }, $out["R"]);

    $out["R"] = $out["R"][0] === null ? array() : $out["R"];

    return $out;
}

?>