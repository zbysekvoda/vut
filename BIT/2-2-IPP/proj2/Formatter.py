import os
import re
import sys

from Tag import Tag, TagGroup
from Expression import Expression
from TagExpPair import TagExpPair


class Formatter:
    def __init__(self, formatFile):
        if formatFile == None or not os.path.exists(formatFile):
            self.__formatPairs = None
        else:
            with open(formatFile, "r") as f:
                formatPairs = [TagExpPair(x.strip()) for x in f.readlines()]
            self.__formatPairs = formatPairs
        self.__tmpOut = ""

    def __updateTmpOut(self, new):
        old = self.__tmpOut
        posNew = 0
        posOld = 0

        out = ""

        while (posNew < len(new) or posOld < len(old)):
            if (posNew < len(new) and posOld < len(old)): # oboje mají dostatek znaků
                if posNew < len(new) and new[posNew] == "<":
                    while new[posNew] != ">":
                        out += new[posNew]
                        posNew += 1
                    out += new[posNew]
                    posNew += 1
                elif posOld < len(old) and old[posOld] == "<":
                    while old[posOld] != ">":
                        out += old[posOld]
                        posOld += 1
                    out += old[posOld]
                    posOld += 1
                elif old[posOld] == new[posNew]:
                    out += old[posOld]
                    posOld += 1
                    posNew += 1
            elif posNew < len(new):
                out += new[posNew]
                posNew += 1
            elif posOld < len(old):
                out += old[posOld]
                posOld += 1

        self.__tmpOut = out

    def __formatVersioned(self, text, br):
        for exp in self.__formatPairs:
            newVersion = exp.apply(text)
            self.__updateTmpOut(newVersion)

        return self.__tmpOut

    def __escapeBrackets(self, text):
        return text.replace("<", "&lt;").replace(">", "&gt;")

    def __deescapeBrackets(self, text):
        return text.replace("&lt;", "<").replace("&gt;", ">")

    def format(self, input, output, br):
        if os.path.exists(input):
            with open(input, "r") as f_in:
                with open(output, "w") as f_out:
                    if self.__formatPairs is None:
                        f_out.write(f_in.read())
                    else:
                        self.__tmpOut = self.__escapeBrackets(f_in.read())
                        formatted = self.__formatVersioned(self.__tmpOut, br)
                        formatted = self.__deescapeBrackets(formatted)
                        f_out.write(formatted)
        else:
            exit(1)
