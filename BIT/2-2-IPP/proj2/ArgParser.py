class ArgParser:
    __HELP_SEPARATOR = "<#$#>"

    memory = {}

    __allowed = ["help", "input", "output", "br", "format"]

    @staticmethod
    def __splitarg(arg):
        if arg in ArgParser.memory:
            return ArgParser.memory[arg]

        parts = arg[2:] \
            .replace("=", ArgParser.__HELP_SEPARATOR, 1) \
            .split(ArgParser.__HELP_SEPARATOR)


        ArgParser.memory[arg] = {
            "arg": parts[0],
            "val": parts[1] if len(parts) == 2 else ""
        }

        return ArgParser.memory[arg]

    @staticmethod
    def parseargs(args):
        return {
            ArgParser.__splitarg(a)["arg"]: ArgParser.__splitarg(a)["val"] for a in args[1:]
        }

    @staticmethod
    def isAllowed(key):
        return key in ArgParser.__allowed
