import re
import sys

class Tag:
    tags = {
        "bold": {"open": "b", "close": "/b"},
        "italic": {"open": "i", "close": "/i"},
        "underline": {"open": "u", "close": "/u"},
        "teletype": {"open": "tt", "close": "/tt"},
        "size": {"open": "font size=", "close": "/font"},
        "color": {"open": "font color=#", "close": "/font"},
        "": {"open": "", "close": ""}
    }

    def __init__(self, s):
        tag = s.strip().split(":", 1)

        if (tag[0] not in Tag.tags):
            exit(4)
        else:
            self.type = tag[0]
            if len(tag) == 1:
                self.val = None
            elif self.type == "color":
                if not re.match(r"^[a-fA-F0-9]{6}$", tag[1]):
                    exit(4)
                self.val = tag[1]
            elif self.type == "size":
                if not re.match(r"^\d$", tag[1]):
                    exit(4)
                self.val = int(tag[1], base=10)
                if not 1 <= self.val <= 7:
                    sys.exit(4)

    def __repr__(self):
        return self.type + ("" if self.val is None else "[" + str(self.val) + "]")

    def __iter__(self):
        return self

    @staticmethod
    def __wrap(text):
        return "<" + text + ">"

    def getOpenTag(self):
        if self.type == "":
            return ""

        return Tag.__wrap(Tag.tags[self.type]["open"] + (str(self.val) if self.val is not None else ""))

    def getCloseTag(self):
        if self.type == "":
            return ""

        return Tag.__wrap(Tag.tags[self.type]["close"])

class TagGroup:
    def __init__(self, tags):
        self.tags = tags

    def add(self, tag):
        self.tags.append(tag)

    def __repr__(self):
        return str(self.tags)

    def getBeforeTags(self):
        out = ""
        for s in [x.getOpenTag() for x in self.tags]:
            out += s
        return out

    def getAfterTags(self):
        out = ""
        for s in [x.getCloseTag() for x in self.tags[::-1]]:
            out += s
        return out