import sys
import os.path

from AppState import AppState
from ArgParser import ArgParser
from Formatter import Formatter

def leave(code):
    exit(code)


appState = AppState(sys.argv)

if appState.help != None:
    if len(appState) == 1:
        print("HELP MESSAGE") #TODO - add help message
    else:
        print("Zadejte --help jako jediny parametr")
        sys.exit(1)

for attr in appState.getAttr():
    if not ArgParser.isAllowed(attr):
        leave(1)

if appState.output is None:
    leave(1)

if appState.input is None:
    leave(1)

if not os.path.exists(appState.input):
    leave(2)

formatter = Formatter(appState.format)
formatter.format(appState.input, appState.output, appState.br)
