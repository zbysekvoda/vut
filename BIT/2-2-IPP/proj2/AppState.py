from ArgParser import ArgParser

import sys

class AppState:
    def __init__(self, args):
        self.__attributes = ArgParser.parseargs(args)

    def __getattr__(self, key):
        return self.__attributes[key] if key in self.__attributes else None

    def __len__(self):
        return len(self.__attributes)

    def __str__(self):
        return str(self.__attributes)

    def getAttr(self):
        return self.__attributes
