import re


class Expression:
    def __init__(self, s):
        def negation(r):
            openIndex = len(r) - 1
            closeIndex = 0

            while openIndex >= 0 and r[openIndex] != "(":
                if r[openIndex] == ")":
                    closeIndex = openIndex
                openIndex -= 1

            inner = r[openIndex + 1: closeIndex]
            inner = re.sub(r"!(.+)", r"(?!.*\1).*", inner)
            out = r[0:openIndex] + inner + r[closeIndex + 1: len(r)]

            return out

        def convertIPPRegexToPythonRegex(expr):
            expr = re.sub(r"(?=[^%])\.", "", expr)  # A.B -> AB
            expr = re.sub(r"%s", "[\\t\\n\\r\\f\\v]", expr)  # %s -> [\t\n\r\f\v]
            #expr = negation(expr)
            expr = re.sub(r"%a", ".", expr)  # %a -> .
            expr = re.sub(r"%d", "[0-9]", expr)  # %d -> [0-9]
            expr = re.sub(r"%l", "[a-z]", expr)  # %l -> [a-z]
            expr = re.sub(r"%L", "[A-Z]", expr)  # %L -> [A-Z]
            expr = re.sub(r"%w", "[a-zA-Z]", expr)  # %w -> [a-zA-Z]
            expr = re.sub(r"%W", "[0-9a-zA-Z]", expr)  # %W -> [0-9a-zA-Z]
            expr = re.sub(r"%t", "\\t", expr)  # %t -> \t
            expr = re.sub(r"%n", "\\n", expr)  # %n -> \n
            expr = re.sub(r"%([\.\|\*\+\(\)%])", r"\\\1", expr)  # special chars

            return "(" + expr + ")"

        self.__textual = convertIPPRegexToPythonRegex(s)

        self.expr = re.compile(self.__textual, re.DOTALL)

    def sub(self, repl, text):
        return self.expr.sub(repl, text)

    def __str__(self):
        return self.__textual
