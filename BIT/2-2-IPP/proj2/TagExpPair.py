import re
import sys

from Expression import Expression
from Tag import Tag, TagGroup

class TagExpPair:
    def __init__(self, text):
        def getTagsFromTagText(s):
            tags = list(filter(None, re.split(r",(\t*| *)*", s)))
            tags = [Tag(x) for x in tags]
            return TagGroup(tags)

        parts = re.split(r"\t+", text, 1)
        if len(parts) != 2:
            sys.exit(4)

        self.__reg = Expression(parts[0])
        self.__tags = getTagsFromTagText(parts[1])

    def apply(self, text):
        beforeTags = self.__tags.getBeforeTags()
        afterTags = self.__tags.getAfterTags()

        return self.__reg.sub(beforeTags + r"\1" + afterTags, text)

    def __repr__(self):
        return "R: " + str(self.__reg) + ",\n T: " + str(self.__tags)
