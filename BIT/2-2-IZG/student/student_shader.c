/*!
 * @file 
 * @brief This file contains implemenation of phong vertex and fragment shader.
 *
 * @author Tomáš Milet, imilet@fit.vutbr.cz
 */

#include<math.h>
#include<assert.h>

#include"student/student_shader.h"
#include"student/gpu.h"
#include"student/uniforms.h"
#include "vertexPuller.h"

#include <stdio.h>

/// \addtogroup shader_side Úkoly v shaderech
/// @{

void phong_vertexShader(
        GPUVertexShaderOutput *const output,
        GPUVertexShaderInput const *const input,
        GPU const gpu) {
    /// \todo Naimplementujte vertex shader, který transformuje vstupní vrcholy do clip-space.<br>
    /// <b>Vstupy:</b><br>
    /// Vstupní vrchol by měl v nultém atributu obsahovat pozici vrcholu ve world-space (vec3) a v prvním
    /// atributu obsahovat normálu vrcholu ve world-space (vec3).<br>
    /// <b>Výstupy:</b><br>
    /// Výstupní vrchol by měl v nultém atributu obsahovat pozici vrcholu (vec3) ve world-space a v prvním
    /// atributu obsahovat normálu vrcholu ve world-space (vec3).
    /// Výstupní vrchol obsahuje pozici a normálu vrcholu proto, že chceme počítat osvětlení ve world-space ve fragment shaderu.<br>
    /// <b>Uniformy:</b><br>
    /// Vertex shader by měl pro transformaci využít uniformní proměnné obsahující view a projekční matici.
    /// View matici čtěte z uniformní proměnné "viewMatrix" a projekční matici čtěte z uniformní proměnné "projectionMatrix".
    /// Zachovejte jména uniformních proměnných a pozice vstupních a výstupních atributů.
    /// Pokud tak neučiníte, akceptační testy selžou.<br>
    /// <br>
    /// Využijte vektorové a maticové funkce.
    /// Nepředávajte si data do shaderu pomocí globálních proměnných.
    /// Pro získání dat atributů použijte příslušné funkce vs_interpret* definované v souboru program.h.
    /// Pro získání dat uniformních proměnných použijte příslušné funkce shader_interpretUniform* definované v souboru program.h.
    /// Vrchol v clip-space by měl být zapsán do proměnné gl_Position ve výstupní struktuře.<br>
    /// <b>Seznam funkcí, které jistě použijete</b>:
    ///  - gpu_getUniformsHandle()
    ///  - getUniformLocation()
    ///  - shader_interpretUniformAsMat4()
    ///  - vs_interpretInputVertexAttributeAsVec3()
    ///  - vs_interpretOutputVertexAttributeAsVec3()

    Uniforms uf_handle = gpu_getUniformsHandle(gpu);
    UniformLocation uf_viewMatrix = getUniformLocation(gpu, "viewMatrix");
    UniformLocation uf_projectionMatrix = getUniformLocation(gpu, "projectionMatrix");

    Mat4 const *viewMatrix = shader_interpretUniformAsMat4(uf_handle, uf_viewMatrix);
    Mat4 const *projectionMatrix = shader_interpretUniformAsMat4(uf_handle, uf_projectionMatrix);

    Vec3 const *const vertexPosition = vs_interpretInputVertexAttributeAsVec3(gpu, input, 0);
    Vec3 const *const vertexNormal = vs_interpretInputVertexAttributeAsVec3(gpu, input, 1);

    Mat4 mvp;
    multiply_Mat4_Mat4(&mvp, projectionMatrix, viewMatrix);

    Vec4 vertex_pos4;
    copy_Vec3Float_To_Vec4(&vertex_pos4, vertexPosition, 1.0f);

    multiply_Mat4_Vec4(&output->gl_Position, &mvp, &vertex_pos4);

    Vec3 *const outputPosition = vs_interpretOutputVertexAttributeAsVec3(gpu, output, 0);
    Vec3 *const outputNormal = vs_interpretOutputVertexAttributeAsVec3(gpu, output, 1);

    init_Vec3(outputPosition, vertex_pos4.data[0], vertex_pos4.data[1], vertex_pos4.data[2]);
    init_Vec3(outputNormal, vertexNormal->data[0], vertexNormal->data[1], vertexNormal->data[2]);

}

void phong_fragmentShader(
        GPUFragmentShaderOutput *const output,
        GPUFragmentShaderInput const *const input,
        GPU const gpu) {
    /// \todo Naimplementujte fragment shader, který počítá phongův osvětlovací model s phongovým stínováním.<br>
    /// <b>Vstup:</b><br>
    /// Vstupní fragment by měl v nultém fragment atributu obsahovat interpolovanou pozici ve world-space a v prvním
    /// fragment atributu obsahovat interpolovanou normálu ve world-space.<br>
    /// <b>Výstup:</b><br>
    /// Barvu zapište do proměnné color ve výstupní struktuře.<br>
    /// <b>Uniformy:</b><br>
    /// Pozici kamery přečtěte z uniformní proměnné "cameraPosition" a pozici světla přečtěte z uniformní proměnné "lightPosition".
    /// Zachovejte jména uniformních proměnný.
    /// Pokud tak neučiníte, akceptační testy selžou.<br>
    /// <br>
    /// Dejte si pozor na velikost normálového vektoru, při lineární interpolaci v rasterizaci může dojít ke zkrácení.
    /// Zapište barvu do proměnné color ve výstupní struktuře.
    /// Shininess faktor nastavte na 40.f
    /// Difuzní barvu materiálu nastavte na čistou zelenou.
    /// Spekulární barvu materiálu nastavte na čistou bílou.
    /// Barvu světla nastavte na bílou.
    /// Nepoužívejte ambientní světlo.<br>
    /// <b>Seznam funkcí, které jistě využijete</b>:
    ///  - shader_interpretUniformAsVec3()
    ///  - fs_interpretInputAttributeAsVec3()

    Uniforms uf_handle = gpu_getUniformsHandle(gpu);
    UniformLocation uf_camera = getUniformLocation(gpu, "cameraPosition");
    UniformLocation uf_light = getUniformLocation(gpu, "lightPosition");

    Vec3 const *camera = shader_interpretUniformAsVec3(uf_handle, uf_camera);
    Vec3 const *light = shader_interpretUniformAsVec3(uf_handle, uf_light);
    Vec3 const *position = fs_interpretInputAttributeAsVec3(gpu, input, 0);
    Vec3 const *normal = fs_interpretInputAttributeAsVec3(gpu, input, 1);

    Vec3 hFromPointToCamera;
    Vec3 hFromPointToLight;
    Vec3 fromPointToCamera;
    Vec3 fromPointToLight;
    Vec3 normalizedNormal;

    sub_Vec3(&hFromPointToCamera, position, camera);
    sub_Vec3(&hFromPointToLight, light, position);

    normalize_Vec3(&fromPointToCamera, &hFromPointToCamera);
    normalize_Vec3(&fromPointToLight, &hFromPointToLight);
    normalize_Vec3(&normalizedNormal, normal);

    Vec3 reflection;
    reflect(&reflection, &fromPointToLight, &normalizedNormal);
    //normalize_Vec3(&reflection, &reflection);

    float x = powf((float)fmax(dot_Vec3(&fromPointToCamera, &reflection), 0), 40.f);
    float y = (float)fmax(dot_Vec3(&normalizedNormal, &fromPointToLight), 0);
    init_Vec4(&output->color, x, y + x, x, 1.0f);
}

/// @}
