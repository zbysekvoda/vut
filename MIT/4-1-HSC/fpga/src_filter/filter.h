#ifndef __FILTER__
#define __FILTER__

#include <ac_int.h>
#include "../../cpu/common.h"

#define MCU_SIZE        512

typedef ac_int<PIXEL_WIDTH,false> t_pixel;
typedef ac_int<32,false> t_mcu_data;

typedef ac_int<1, false> t_1b; // bool hodnoty
typedef ac_int<9, false> t_9b; // indexy pixel� - 0-319 < 2^9
typedef ac_int<2, false> t_2b; // 2b hodnota pozi�n�ch p��znak� pixelu
typedef ac_int<7, false> t_7b; // po�itadlo frames - sta�� 0-100

void filter(t_pixel &in_data, bool &in_data_vld, t_pixel &out_data, t_mcu_data mcu_data[MCU_SIZE]);

#endif
