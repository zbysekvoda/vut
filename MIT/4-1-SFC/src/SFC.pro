# SFC Project
# xvodaz01

QT       += core gui datavisualization widgets

TARGET = SFC
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

CONFIG += c++11

SOURCES += \
        main.cpp \
    simulation.cpp \
    printer.cpp \
    gui.cpp \
    overloadedops.cpp \
    numberformrow.cpp \
    numberinput.cpp

HEADERS += \
    simulation.h \
    types.h \
    printer.h \
    gui.h \
    numberformrow.h \
    numberinput.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
