/*
 * SFC Project
 * xvodaz01, Zbyšek Voda
 */

#include "numberformrow.h"

#include <QLabel>

// creates row consisting of label and numeric input
NumberFormRow::NumberFormRow(QFormLayout *fr, const char* labelText, const char *initial, double min, double max) {
    std::string s("");

    s += labelText;

    if(!isinf(min)) {
        s += "(min.: ";
        s += std::to_string(min);
        s += ")";
    }

    s += ":";

    QLabel *label = new QLabel(s.c_str());
    input = new NumberInput(labelText, initial, min, max);

    fr->addRow(label, input);
}

// returns input value
double NumberFormRow::getValue() {
    return input->getValue();
}

// sets output printer
void NumberFormRow::setPrinter(Printer *p) {
    input->setPrinter(p);
}

// sets input value
void NumberFormRow::setValue(std::string s) {
    input->setValue(s);
}
