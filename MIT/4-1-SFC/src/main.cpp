/*
 * SFC Project
 * xvodaz01, Zbyšek Voda
 */

#include <QApplication>
#include <math.h>

#include "gui.h"

static SimRun simulationRunning = NOT_RUNNING;
static GUI *gui;
static Simulation *simulation = new Simulation;

// checks if all used-inputed values are valid
bool simulationConfigIsOk(SimConfig *simConfig) {
    bool result = true;

    result = result && !isinf(simConfig->state.x);
    result = result && !isinf(simConfig->state.y);
    result = result && !isinf(simConfig->state.z);

    result = result && !isinf(simConfig->lc.sigma);
    result = result && !isinf(simConfig->lc.ro);
    result = result && !isinf(simConfig->lc.beta);

    result = result && !isinf(simConfig->tc.h);
    result = result && !isinf(simConfig->tc.maxT);

    result = result && !isinf(simConfig->eps);

    result = result && !isinf(simConfig->minPointPlotDistance);

    return result;
}

// main simulation run
void runSimulation() {
    SimConfig* simConfig = gui->getSimConfig();

    if(!simulationConfigIsOk(simConfig)) {
        // config is not valid -> error message is printed

        gui->printSimConfigError();
    }
    else {
        gui->setStartButtonText("STOP");

        simulation->init(simConfig);

        gui->initGraph();

        Vec3D lastPlottedPoint = {10000, 10000, 10000};

        bool finished = false;

        // solution points are computed one by one
        while(!finished) {
            RunResult r = simulation->getNextPoint();
            finished = r.finished;

            // plot point if it is far enough from last point
            double vecDiff = Simulation::diffVec3DSize(r.point, lastPlottedPoint);
            if(vecDiff > simConfig->minPointPlotDistance) {
                gui->addPlotPoint(r.point);
                lastPlottedPoint = r.point;
            }

            // after each round, GUI events are processed
            QCoreApplication::processEvents();

            if(simulationRunning == NOT_RUNNING) break;
        }

        simulation->stopSimulation();
        simulation->printSimStats();

        gui->setStartButtonText("START");
        simulationRunning = NOT_RUNNING;
    }
}

void startButtonHandler() {
    if(simulationRunning == RUNNING) {
        // smimulation is runnong -> set running flag to NOT_RUNNING in order to stop simulation
        simulationRunning = NOT_RUNNING;

        gui->setStartButtonText("START");
    }
    else if(simulationRunning == NOT_RUNNING){
        // begin simulation
        simulationRunning = RUNNING;
        runSimulation();
    }
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    // create GUI
    gui = new GUI(simulation);

    // set handler to handle button press
    gui->setStartButtonHandler(startButtonHandler);

    return app.exec();
}
