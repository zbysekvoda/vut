/*
 * SFC Project
 * xvodaz01, Zbyšek Voda
 */

#ifndef SIMULATION_H
#define SIMULATION_H

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

#include "types.h"
#include "printer.h"

class Simulation {
public:
    Simulation();
    void setPrinter(Printer *p);
    void init(SimConfig *Simulation);
    RunResult getNextPoint();
    void stopSimulation();
    void printSimStats();
    static double diffVec3DSize(Vec3D v1, Vec3D v2);

private:
    Printer *printer;
    SimConfig *simulation;
    clock_t startT, stopT;
    long long lastStatPrint;

    void printVec3D(Vec3D state);
    static Vec3D sumVec3D(Vec3D vectors[], uint8_t n);
    static Vec3D multiplyVec3DByConstant(Vec3D a, double c);
    static Vec3D addVec3D(Vec3D a, Vec3D b);
    static Vec3D diffVec3D(Vec3D v1, Vec3D v2);
    static double Vec3DSize(Vec3D v);
    static Vec3D lorenz(Vec3D state, LorenzConfig *lc);
    static Vec3D euler(Vec3D state, LorenzConfig *lc, double h);
    static Vec3D rk4(Vec3D yt, LorenzConfig *lc, double h);
    static Vec3D adamsBashforth(Vec3D yt, LorenzConfig *lc, double h);
    void printProgress();
    static ODESolverFunction getODESolverFunction(ODESolverEnum code);

friend Vec3D operator +(Vec3D a, Vec3D b);
friend Vec3D operator *(Vec3D a, const double c);
friend Vec3D operator *(const double c, Vec3D a);
friend Vec3D operator -(Vec3D a, Vec3D b);
friend Vec3D operator /(Vec3D a, const double c);
};

#endif // SIMULATION_H
