/*
 * SFC Project
 * xvodaz01, Zbyšek Voda
 */

#include "gui.h"

using namespace QtDataVisualization;

// creates all GUI elements
void GUI::init() {
    // input group for initial position
    QGroupBox *initialPositionGroupBox = new QGroupBox("Initial position");
    QFormLayout *initialPositionForm = new QFormLayout;
    initialPositionGroupBox->setLayout(initialPositionForm);

    initialXInput = new NumberFormRow(initialPositionForm, "x", "1.0", DOUBLE_NEG_INF, DOUBLE_POS_INF);
    initialYInput = new NumberFormRow(initialPositionForm, "y", "1.0", DOUBLE_NEG_INF, DOUBLE_POS_INF);
    initialZInput = new NumberFormRow(initialPositionForm, "z", "1.0", DOUBLE_NEG_INF, DOUBLE_POS_INF);

    // input group for setting lorenz paramaters
    QGroupBox *lorenzParametersGroupBox = new QGroupBox("Lorenz parameters");
    QFormLayout *lorenzParametersForm = new QFormLayout;
    lorenzParametersGroupBox->setLayout(lorenzParametersForm);

    roInput = new NumberFormRow(lorenzParametersForm, "ρ", "14.0", DOUBLE_NEG_INF, DOUBLE_POS_INF);
    sigmaInput = new NumberFormRow(lorenzParametersForm, "σ", "10.0", DOUBLE_NEG_INF, DOUBLE_POS_INF);
    betaInput = new NumberFormRow(lorenzParametersForm, "β", "8/3", DOUBLE_NEG_INF, DOUBLE_POS_INF);

    // input group for time control
    QGroupBox *timeGroupBox = new QGroupBox("Time");
    QFormLayout *timeForm = new QFormLayout;
    timeGroupBox->setLayout(timeForm);

    maxTimeInput = new NumberFormRow(timeForm, "Max", "10", DOUBLE_NEG_INF, DOUBLE_POS_INF);
    timeStepInput = new NumberFormRow(timeForm, "Step", "0.001", DOUBLE_NEG_INF, 0.01);

    // input group for solver parameters (method + epsilon)
    QGroupBox *solutionParametersGroupBox = new QGroupBox("Solution parameters");
    QFormLayout *solutionParametersForm = new QFormLayout;
    solutionParametersGroupBox->setLayout(solutionParametersForm);

    QLabel *solutionMethodLabel = new QLabel("Method:");
    solutionMethodInput = new QComboBox;
    solutionMethodInput->addItem("Euler");
    solutionMethodInput->addItem("4th order Runge-Kutta");
    solutionMethodInput->addItem("Adams-Bashforth method");
    solutionParametersForm->addRow(solutionMethodLabel, solutionMethodInput);

    epsInput = new NumberFormRow(solutionParametersForm, "ε", "10e-14", DOUBLE_NEG_INF, DOUBLE_POS_INF);

    // text area for text output
    terminalOutput = new QTextBrowser;

    // graph
    graph = new Q3DScatter();
    graph->setFlags(graph->flags() ^ Qt::FramelessWindowHint);
    graph->setShadowQuality(QAbstract3DGraph::ShadowQualityNone);

    QScatter3DSeries *series = new QScatter3DSeries;
    series->setItemSize(0.02f);
    series->setMesh(QAbstract3DSeries::MeshSphere);

    graph->addSeries(series);
    graphDataProxy = graph->seriesList().first()->dataProxy();

    graph->axisX()->setTitle("X");
    graph->axisY()->setTitle("Y");
    graph->axisZ()->setTitle("Z");

    graph->axisX()->setTitleVisible(true);
    graph->axisY()->setTitleVisible(true);
    graph->axisZ()->setTitleVisible(true);

    QLinearGradient gradient;
    gradient.setColorAt(0.0, Qt::black);
    gradient.setColorAt(0.33, Qt::blue);
    gradient.setColorAt(0.67, Qt::red);
    gradient.setColorAt(1.0, Qt::yellow);
    graph->seriesList().at(0)->setBaseGradient(gradient);
    graph->seriesList().at(0)->setColorStyle(Q3DTheme::ColorStyleRangeGradient);

    QWidget *graphWidget = QWidget::createWindowContainer(graph);
    graphWidget->setMinimumHeight(400);
    graphWidget->setMinimumWidth(400);

    // group box with START/STOP button
    QGroupBox *simulationControlGroupBox = new QGroupBox("Simulation control");
    QHBoxLayout *simulationControlLayout = new QHBoxLayout;
    simulationControlGroupBox->setLayout(simulationControlLayout);

    startButton = new QPushButton("START");
    simulationControlLayout->addWidget(startButton);

    connect(startButton, SIGNAL(released()), this, SLOT(on_startButton_released()));

    // group box for setting of minimal distance of points to plot
    QGroupBox *plotSettingsGroupBox = new QGroupBox("Plot settings");
    QFormLayout *plotSettingsForm = new QFormLayout;
    plotSettingsGroupBox->setLayout(plotSettingsForm);

    minPointDistanceInput = new NumberFormRow(plotSettingsForm, "Plot point distance", "0.5", DOUBLE_NEG_INF, DOUBLE_POS_INF);

    // group box with example buttons
    QGroupBox *examplesGroupBox = new QGroupBox("Examples");
    QHBoxLayout *examplesLayout = new QHBoxLayout();
    examplesGroupBox->setLayout(examplesLayout);

    QPushButton *ex1 = new QPushButton("Example 1");
    QPushButton *ex2 = new QPushButton("Example 2");
    QPushButton *ex3 = new QPushButton("Example 3");
    QPushButton *ex4 = new QPushButton("Example 4");

    examplesLayout->addWidget(ex1);
    examplesLayout->addWidget(ex2);
    examplesLayout->addWidget(ex3);
    examplesLayout->addWidget(ex4);

    connect(ex1, SIGNAL(released()), this, SLOT(loadExample1()));
    connect(ex2, SIGNAL(released()), this, SLOT(loadExample2()));
    connect(ex3, SIGNAL(released()), this, SLOT(loadExample3()));
    connect(ex4, SIGNAL(released()), this, SLOT(loadExample4()));


    // layout of right setting side
    QGridLayout *settingsGrid = new QGridLayout;
    settingsGrid->addWidget(initialPositionGroupBox, 0, 0);
    settingsGrid->addWidget(lorenzParametersGroupBox, 0, 1);
    settingsGrid->addWidget(timeGroupBox, 1, 0);
    settingsGrid->addWidget(solutionParametersGroupBox, 1, 1);
    settingsGrid->addWidget(simulationControlGroupBox, 2, 0);
    settingsGrid->addWidget(plotSettingsGroupBox, 2, 1);
    settingsGrid->addWidget(examplesGroupBox, 3, 0, 1, 2);

    // left side
    QVBoxLayout *leftSideLayout = new QVBoxLayout();
    leftSideLayout->addWidget(graphWidget, 7);

    // right side
    QVBoxLayout *rightSideLayout = new QVBoxLayout();
    rightSideLayout->addLayout(settingsGrid, 1);
    rightSideLayout->addWidget(terminalOutput, 2);

    // whole layout
    QHBoxLayout *mainLayout = new QHBoxLayout;
    mainLayout->addLayout(leftSideLayout, 2);
    mainLayout->addLayout(rightSideLayout, 1);

    // main window
    QWidget *window = new QWidget;
    window->setLayout(mainLayout);
    window->showMaximized();
}

// loads example 1
void GUI::loadExample1() {
    initialXInput->setValue("1");
    initialYInput->setValue("1");
    initialZInput->setValue("1");

    roInput->setValue("14");
    sigmaInput->setValue("10");
    betaInput->setValue("8/3");

    maxTimeInput->setValue("100");
    timeStepInput->setValue("0.001");

    epsInput->setValue("1e-14");
    minPointDistanceInput->setValue("0.1");
}

// loads example 2
void GUI::loadExample2() {
    initialXInput->setValue("1");
    initialYInput->setValue("1");
    initialZInput->setValue("1");

    roInput->setValue("13");
    sigmaInput->setValue("10");
    betaInput->setValue("8/3");

    maxTimeInput->setValue("10");
    timeStepInput->setValue("0.001");

    epsInput->setValue("1e-14");
    minPointDistanceInput->setValue("0.5");
}

// loads example 3
void GUI::loadExample3() {
    initialXInput->setValue("1");
    initialYInput->setValue("1");
    initialZInput->setValue("1");

    roInput->setValue("15");
    sigmaInput->setValue("10");
    betaInput->setValue("8/3");

    maxTimeInput->setValue("10");
    timeStepInput->setValue("0.001");

    epsInput->setValue("1e-14");
    minPointDistanceInput->setValue("0.5");
}

// loads example 4
void GUI::loadExample4() {
    initialXInput->setValue("1");
    initialYInput->setValue("1");
    initialZInput->setValue("1");

    roInput->setValue("28");
    sigmaInput->setValue("10");
    betaInput->setValue("8/3");

    maxTimeInput->setValue("1000");
    timeStepInput->setValue("0.001");

    epsInput->setValue("1e-14");
    minPointDistanceInput->setValue("0.5");
}

// sets START/STOP button click handler
void GUI::setStartButtonHandler(ButtonHandler handler) {
    startButtonHandler = handler;
}

// START/STOP button handler
void GUI::on_startButton_released() {
    if(startButtonHandler != nullptr) {
        startButtonHandler();
    }
}

// sets START/STOP button text
void GUI::setStartButtonText(const char * s) {
    startButton->setText(s);
}

// returns simulation config from GUI
SimConfig *GUI::getSimConfig() {
    SimConfig* simulation = static_cast<SimConfig*>(malloc(sizeof(SimConfig)));

    simulation->state.x = initialXInput->getValue();
    simulation->state.y = initialYInput->getValue();
    simulation->state.z = initialZInput->getValue();

    simulation->lc.sigma = sigmaInput->getValue();
    simulation->lc.ro = roInput->getValue();
    simulation->lc.beta = betaInput->getValue();

    simulation->tc.h = timeStepInput->getValue();
    simulation->tc.maxT = maxTimeInput->getValue();

    simulation->tc.t0 = 0.0;
    simulation->tc.t = 0.0;

    simulation->os = static_cast<ODESolverEnum>(solutionMethodInput->currentIndex());
    simulation->eps = epsInput->getValue();

    simulation->minPointPlotDistance = minPointDistanceInput->getValue();

    return simulation;
}

// adds plot point to graph
void GUI::addPlotPoint(Vec3D p) {
    float x = static_cast<float>(p.x);
    float y = static_cast<float>(p.y);
    float z = static_cast<float>(p.z);

    graphDataProxy->addItem(QVector3D(x, y, z));
}

// initializes graph
void GUI::initGraph() {
    if(graphDataProxy != nullptr) {
        int itemCount = graphDataProxy->itemCount();
        graphDataProxy->removeItems(0, itemCount);
    }
}

// prints input error
void GUI::printSimConfigError() {
    printer->print("ERROR: Check inputs. See text output for more details.\n", "configError");
}

// inits printer of all numeric input elements
void GUI::initPrinter(Printer *p) {
    initialXInput->setPrinter(p);
    initialYInput->setPrinter(p);
    initialZInput->setPrinter(p);
    roInput->setPrinter(p);
    sigmaInput->setPrinter(p);
    betaInput->setPrinter(p);
    maxTimeInput->setPrinter(p);
    timeStepInput->setPrinter(p);
    epsInput->setPrinter(p);
    minPointDistanceInput->setPrinter(p);
}

// GUI constructor
GUI::GUI(Simulation *simulation) : QObject (nullptr) {
    init();

    printer = new Printer();
    printer->setOutput(terminalOutput);
    initPrinter(printer);
    simulation->setPrinter(printer);
}

GUI::~GUI() {}
