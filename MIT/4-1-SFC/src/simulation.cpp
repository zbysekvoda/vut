/*
 * SFC Project
 * xvodaz01, Zbyšek Voda
 */

#include "simulation.h"
#include "overloadedops.cpp"

Simulation::Simulation() {}

// sets simulation output
void Simulation::setPrinter(Printer *p) {
    printer = p;
}

// prints Vec3D. For debug only
void Simulation::printVec3D(Vec3D state) {
    std::string x = std::to_string(state.x);
    std::string y = std::to_string(state.y);
    std::string z = std::to_string(state.z);

    std::string out = "(x: " + x + ", y: " + y + ", z: " + z + ")";

    printer->print(out, out.c_str());
}

// multiplies Vec3D by constant, eg. scales it
Vec3D Simulation::multiplyVec3DByConstant(Vec3D a, double c) {
    double x = a.x * c;
    double y = a.y * c;
    double z = a.z * c;

    return { x, y, z };
}

// adds two Vec3D
Vec3D Simulation::addVec3D(Vec3D a, Vec3D b) {
    double x = a.x + b.x;
    double y = a.y + b.y;
    double z = a.z + b.z;

    return { x, y, z };
}

// computes sum of Vec3D array
Vec3D Simulation::sumVec3D(Vec3D vectors[], uint8_t n) {
    Vec3D sum = {0, 0, 0};

    for (uint8_t i = 0; i < n; i++) {
        sum = addVec3D(sum, vectors[i]);
    }

    return sum;
}

// computes difference of Vec3D  component-wise
Vec3D Simulation::diffVec3D(Vec3D v1, Vec3D v2) {
    double x = v1.x - v2.x;
    double y = v1.y - v2.y;
    double z = v1.z - v2.z;

    return {x, y, z};
}

// computes length of Vec3D
double Simulation::Vec3DSize(Vec3D v) {
    double x = v.x;
    double y = v.y;
    double z = v.z;

    return sqrt(x*x + y*y + z*z);
}

// computes distance of two Vec3D
double Simulation::diffVec3DSize(Vec3D v1, Vec3D v2) {
    return Vec3DSize(diffVec3D(v1, v2));
}

// computes one step of Lorenz system
Vec3D Simulation::lorenz(Vec3D state, LorenzConfig *lc) {
    double x = lc->sigma * (state.y - state.x);
    double y = state.x * (lc->ro - state.z) - state.y;
    double z = state.x * state.y - lc->beta * state.z;

    return { x, y, z };
}

// implementation of Euler method for solving ordinary differential equations
Vec3D Simulation::euler(Vec3D state, LorenzConfig *lc, double h) {
    return state + lorenz(state, lc) * h;
}

// implementation of Runge-Kutta method for solving ordinary differential equations
Vec3D Simulation::rk4(Vec3D yt, LorenzConfig *lc, double h) {
    Vec3D k1 = h * lorenz(yt, lc);
    Vec3D k2 = h * lorenz(yt + k1/2, lc);
    Vec3D k3 = h * lorenz(yt + k2/2, lc);
    Vec3D k4 = h * lorenz(yt + k3, lc);

    return yt + k1/6 + k2/3 + k3/3 + k4/6;
}

// implementation of Adams-Bashforth method for solving ordinary differential equations
// https://en.wikipedia.org/wiki/Linear_multistep_method#Adams–Bashforth_methods
Vec3D Simulation::adamsBashforth(Vec3D yt, LorenzConfig *lc, double h) {
    static bool initialized = 0;
    static uint8_t step = 0;
    // {l_tpn0, l_tpn1, l_tpn2, l_tpn3}
    static Vec3D l_tnp[4];

    if(!initialized) {
        // first step
        if(step == 0) {
            Vec3D l0 = lorenz(yt, lc);

            l_tnp[0] = l0;
            step = 1;

            // y1
            return yt + h * l0;
        }

        // second step
        if(step == 1) {
            Vec3D l0 = l_tnp[0];
            Vec3D l1 = lorenz(yt, lc);

            l_tnp[1] = l1;
            step = 2;

            // y2
            return yt + h * ((3/2.0) * l1 - (1/2.0) * l0);
        }

        // third step
        if(step == 2) {
            Vec3D l0 = l_tnp[0];
            Vec3D l1 = l_tnp[1];
            Vec3D l2 = lorenz(yt, lc);

            l_tnp[2] = l2;
            step = 3;

            // y3
            return yt + h * ((23/12.0) * l2 - (4/3.0) * l1 + (5/12.0) * l0);
        }

        // fourth step
        if(step == 3) {
            Vec3D l0 = l_tnp[0];
            Vec3D l1 = l_tnp[1];
            Vec3D l2 = l_tnp[2];
            Vec3D l3 = lorenz(yt, lc);

            l_tnp[3] = l3;
            step = 0;

            // y4
            return yt + h * ((55/24.0) * l3 - (59/24.0) * l2 + (37/24.0) * l1 - (3/8.0) * l0);
        }

        initialized = true;
    }

    // any other step

    Vec3D l0 = l_tnp[0];
    Vec3D l1 = l_tnp[1];
    Vec3D l2 = l_tnp[2];
    Vec3D l3 = l_tnp[3];
    Vec3D l4 = lorenz(yt, lc);

    l_tnp[0] = l1;
    l_tnp[1] = l2;
    l_tnp[2] = l3;
    l_tnp[3] = l4;

    return yt + h * ((1901/720.0) * l4 - (1387/360.0) * l3 + (109/30.0) * l2 - (637/360) * l1 + (251/720.0) * l0);
}

// returns function implementing numerical solution method
ODESolverFunction Simulation::getODESolverFunction(ODESolverEnum code) {
    if(code == RK4) {
        return rk4;
    }

    if(code == ADAMS_BASHFORTH) {
        return adamsBashforth;
    }

    return euler;
}

// returns next point of solution
RunResult Simulation::getNextPoint() {
    ODESolverFunction odeSolver = getODESolverFunction(simulation->os);
    Vec3D y = simulation->state;
    LorenzConfig lc = simulation->lc;
    TimeConfig tc = simulation->tc;
    double eps = simulation->eps;

    // computes one point using selected solver
    Vec3D y_new = odeSolver(y, &lc, tc.h);
    double diff = diffVec3DSize(y, y_new);
    double newT = tc.t + tc.h;
    bool finished = eps >= diff || newT >= tc.maxT;

    simulation->state = y_new;
    simulation->tc.t = newT;

    printProgress();

    if(finished) {
        if(eps >= diff) {
            std::string epsString(16, '\0');
            int written = std::snprintf(&epsString[0], epsString.size(), "%g", eps);
            epsString.resize(static_cast<size_t>(written));

            printer->print("SOLUTION FOUND: Last two points distance is less than " + epsString, "satisfyingSolution");
        }

        printer->print("SIMULATION DONE", "done");
    }

    return { y, finished };
}

// prints simulation progress
void Simulation::printProgress() {
    TimeConfig tc = simulation->tc;

    long long actualPoints = static_cast<long long>(tc.t / tc.h);
    long long expectedPoints = static_cast<long long>((tc.maxT - tc.t0) / tc.h);
    long long percentPoint = expectedPoints / 100;

    if (actualPoints > lastStatPrint + 5 * percentPoint) {
        uint8_t progress = static_cast<uint8_t>(actualPoints / percentPoint);

        std::string out = "";

        out += std::to_string(progress);
        out += "%\t";
        out += progress < 10 ? "  " : " ";

        for (int8_t i = 0; i < 25; i++) {
            out += i < progress / 4 ? "|" : "  ";
        }

        printer->print(out, "progress");

        lastStatPrint = actualPoints;
    }
}

// prints simulation statistics
void Simulation::printSimStats() {
    ODESolverEnum odeSolver = simulation->os;
    LorenzConfig lc = simulation->lc;
    TimeConfig tc = simulation->tc;

    double ticks = static_cast<double>(stopT - startT);
    double time = ticks / CLOCKS_PER_SEC;

    long long actualPoints = static_cast<long long>(tc.t / tc.h);
    long long totalPoints = static_cast<long long>((tc.maxT - tc.t0) / tc.h);

    const char *odeSolverS = odeSolverNames[odeSolver];

    std::string out = "==================================================\n";
    out += "================== STATISTICS =======================\n";
    out += "= ODE Solver:\t ";
    out += odeSolverS;
    out += " method\n";
    out += "= Lorenz params:\n";
    out += "= \t sigma:\t " + std::to_string(lc.sigma) + "\n";
    out += "= \t beta:\t " + std::to_string(lc.beta) + "\n";
    out += "= \t ro:\t " + std::to_string(lc.ro) + "\n";
    out += "= Time settings: \n";
    out += "= \t h:\t " + std::to_string(tc.h) + "\n";
    out += "= \t t_max:\t " + std::to_string(tc.maxT) + "\n";
    out += "= Total points:\t " + std::to_string(actualPoints) + "/" + std::to_string(totalPoints) + "\n";
    out += "= Time elapsed:\t " + std::to_string(time) + " s\n";
    out += "==================================================\n";

    printer->print(out, "simulationOutput");
}

// initializes whole simulation
void Simulation::init(SimConfig *s) {
    simulation = s;
    startT = clock();
    lastStatPrint = 0;

    printer->clear();
    printer->print("SIMULATION INITIALIZED", "simulationInitialized");
}

void Simulation::stopSimulation() {
    stopT = clock();
}
