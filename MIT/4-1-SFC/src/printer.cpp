/*
 * SFC Project
 * xvodaz01, Zbyšek Voda
 */

#include "printer.h"

Printer::Printer() {}

// set printer to output to given textarea
void Printer::setOutput(QTextBrowser *tb) {
    textOutput = tb;
}

// prints string to textarea
void Printer::print(std::string s, std::string key) {
    mem[key] = s;
    printMem();
}

// check wheter outputs key has special meaning and should be placed differently in output
bool Printer::outputIsSpecial(std::string s) {
    for(uint8_t i = 0; i < SPEC_LEN; i++) {
        if(s == specialOutputs[i]) {
            return true;
        }
    }

    return false;
}

// prints content of messages memory to textarea
void Printer::printMem() {
    std::map<std::string, std::string>::iterator iterator = mem.begin();
    std::string output = "";
    std::string outputPostfix = "";

    // pick messages with special keys
    for(uint8_t i = 0; i < SPEC_LEN; i++) {
        std::map<std::string, std::string>::iterator iterator = mem.find(specialOutputs[i]);

        if(iterator != mem.end()) {
            outputPostfix += iterator->second;
            outputPostfix += "\n";
        }
    }

    // pick ordinary messages
    while (iterator != mem.end()) {
        std::string key = iterator->first;
        std::string val = iterator->second;

        if(!outputIsSpecial(key)) {
            output += val;
            output += "\n";
        }

        iterator++;
    }

    output += outputPostfix;

    textOutput->setText(QString(output.c_str()));

    QScrollBar* scrollBar = textOutput->verticalScrollBar();
    scrollBar->setValue(scrollBar->maximum());
}

// clear output
void Printer::clear() {
    mem.clear();
    printMem();
}

// hide message with given key
void Printer::hide(std::string key) {
    std::map<std::string, std::string>::iterator iterator = mem.begin();

    while (iterator != mem.end()) {
        std::string k = iterator->first;

        if(key == k){
            mem.erase(iterator);
            break;
        }

        iterator++;
    }

    printMem();
}
