/*
 * SFC Project
 * xvodaz01, Zbyšek Voda
 */

#ifndef MYNUMBERINPUT_H
#define MYNUMBERINPUT_H

#include <QLineEdit>
#include <QToolTip>

#include <limits>

#include "simulation.h"

class NumberInput : public QLineEdit {
    Q_OBJECT

public:
    NumberInput(const char* _name, const char* initial, double min, double max);
    double getValue();
    void setPrinter(Printer *p);
    void setValue(std::string s);

private slots:
    void inputChanged();

private:
    static double parseDoubleInput(QLineEdit* element, bool* result);
    double min;
    double max;
    bool valid;
    double value;
    Printer *printer = nullptr;
    std::string name;

};

#endif // MYNUMBERINPUT_H
