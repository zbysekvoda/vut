/*
 * SFC Project
 * xvodaz01, Zbyšek Voda
 */

#ifndef PRINTER_H
#define PRINTER_H

#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QScrollBar>

class Printer {
public:
    Printer();
    void setOutput(QTextBrowser *tb);
    void print(std::string s, std::string key);
    void hide(std::string key);
    void clear();

private:
    void printMem();
#define SPEC_LEN 6
    const std::string specialOutputs[SPEC_LEN] = {"configError", "simulationInitialized", "progress", "satisfyingSolution", "done", "simulationOutput"};
    bool outputIsSpecial(std::string s);

    QTextBrowser *textOutput;
    std::map<std::string, std::string> mem;
};

#endif // PRINTER_H
