/*
 * SFC Project
 * xvodaz01, Zbyšek Voda
 */

#include "simulation.h"

// overloads multiplication operator for Vec3D and constant
Vec3D operator *(Vec3D a, const double c) {
    return Simulation::multiplyVec3DByConstant(a, c);
}

// overloads multiplication operator for constant and Vec3D
Vec3D operator *(const double c, Vec3D a) {
    return Simulation::multiplyVec3DByConstant(a, c);
}

Vec3D operator /(Vec3D a, const double c) {
    return Simulation::multiplyVec3DByConstant(a, 1/c);
}

// overloads operator + to add two Vec3D
Vec3D operator +(Vec3D a, Vec3D b) {
    return Simulation::addVec3D(a, b);
}

// overloads operator - to subtract
Vec3D operator -(Vec3D a, Vec3D b) {
    return Simulation::diffVec3D(a, b);
}
