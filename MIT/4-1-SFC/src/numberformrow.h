/*
 * SFC Project
 * xvodaz01, Zbyšek Voda
 */

#ifndef FORMROW_H
#define FORMROW_H

#include <QFormLayout>
#include <math.h>

#include "numberinput.h"

class NumberFormRow {
private:
    NumberInput *input;

public:
    NumberFormRow(QFormLayout *fr, const char* label, const char *initial, double min, double max);
    double getValue();
    void setPrinter(Printer *p);
    void setValue(std::string s);
};

#endif // FORMROW_H
