/*
 * SFC Project
 * xvodaz01, Zbyšek Voda
 */

#ifndef GUI_H
#define GUI_H

#include <QWidget>
#include <QHBoxLayout>
#include <QGroupBox>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QComboBox>
#include <Q3DScatter>
#include <QPushButton>
#include <QMainWindow>

#include "printer.h"
#include "simulation.h"
#include "types.h"
#include "numberinput.h"
#include "numberformrow.h"

using namespace QtDataVisualization;

class GUI : public QObject {
    Q_OBJECT

private:
    NumberFormRow *initialXInput;
    NumberFormRow *initialYInput;
    NumberFormRow *initialZInput;
    NumberFormRow *roInput;
    NumberFormRow *sigmaInput;
    NumberFormRow *betaInput;
    NumberFormRow *maxTimeInput;
    NumberFormRow *timeStepInput;
    NumberFormRow *epsInput;
    NumberFormRow *minPointDistanceInput;

    QComboBox *solutionMethodInput;
    QTextBrowser *terminalOutput;
    Q3DScatter *graph;
    QPushButton *startButton;
    QScatterDataProxy *graphDataProxy;
    ButtonHandler startButtonHandler;
    QQuaternion xRotation;
    QQuaternion yRotation;
    QQuaternion zRotation;
    Printer *printer;

    void init();
    double parseDoubleInput(QLineEdit* element, bool* result);
    void initPrinter(Printer *p);

private slots:
    void on_startButton_released();
    void loadExample1();
    void loadExample2();
    void loadExample3();
    void loadExample4();

public:
    explicit GUI(Simulation *simulation);
    ~GUI();
    void setStartButtonHandler(ButtonHandler handler);
    void setStartButtonText(const char * s);
    SimConfig *getSimConfig();
    void addPlotPoint(Vec3D p);
    void initGraph();
    void printSimConfigError();
};

#endif // GUI_H
