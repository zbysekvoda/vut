/*
 * SFC Project
 * xvodaz01, Zbyšek Voda
 */

#include "numberinput.h"

// creates GUI element to input number in form of C++ number or fraction
// min and max can be set
NumberInput::NumberInput(const char* _name, const char* initial, double _min, double _max): QLineEdit(initial) {
    min = _min;
    max = _max;
    name = _name;

    inputChanged();

    connect(this, SIGNAL(textEdited(const QString &)), this, SLOT(inputChanged()));
}

// check if input is valid number otherwise colors input to black
void NumberInput::inputChanged() {
    bool result = true;

    double v = parseDoubleInput(this, &result);

    if(result && v >= min && v <= max) {
        // parsing was succesful

        value = v;
        this->setStyleSheet("QLineEdit {}");

        if(printer != nullptr) {
            printer->hide(name.c_str());
        }
    }
    else {
        std::string message = "ERROR: Value of \"";
        message += name;
        message += "\" ";

        if(result) {
            message += "is too ";

            if(v < min) {
                message += "small. Minimum is ";
                message += std::to_string(min);
            }
            else if(v > max) {
                message += "big. Maximum is ";
                message += std::to_string(max);
            }
        }
        else {
            message += "is not valid number. Insert C++ valid number or fraction (5/4 eg.)";
        }

        message += ".";

        // print error message
        if(printer != nullptr) {
            printer->print(message, name.c_str());
        }

        value = DOUBLE_POS_INF;
        this->setStyleSheet("QLineEdit {background: rgba(255, 0, 0, 50);}");
    }

    valid = result;
}

// returns actual input value
double NumberInput::getValue() {
    return value;
}

// parses input text to double
double NumberInput::parseDoubleInput(QLineEdit* element, bool* result) {
    bool res;

    double x = element->text().toDouble(&res);

    if(res) {
        *result = true;
        return x;
    }

    std::string content = element->text().toStdString();
    std::size_t length = content.length();

    std::size_t slashPos = content.find("/");
    if(slashPos == std::string::npos) {
        // string does not contain / char
        *result = false;
        return 0.0;
    }

    std::string a = content.substr(0, slashPos);
    std::string b = content.substr(slashPos + 1, length);

    QLineEdit A(a.c_str());
    QLineEdit B(b.c_str());

    // dividend
    double valA = A.text().toDouble(&res);
    if(!res) {
        *result = false;
        return 0.0;
    }

    // divisor
    double valB = B.text().toDouble(&res);
    if(!res) {
        *result = false;
        return 0.0;
    }

    return valA / valB;
}

// sets output printer
void NumberInput::setPrinter(Printer *p) {
    printer = p;
}

// sets input value
void NumberInput::setValue(std::string s) {
    this->setText(s.c_str());
    inputChanged();
}
