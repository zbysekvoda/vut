/*
 * SFC Project
 * xvodaz01, Zbyšek Voda
 */

#ifndef TYPES_H
#define TYPES_H

#define DOUBLE_POS_INF ( std::numeric_limits<double>::infinity())
#define DOUBLE_NEG_INF (-std::numeric_limits<double>::infinity())

typedef struct {
    double x;
    double y;
    double z;
} Vec3D;

typedef struct {
    double sigma;
    double ro;
    double beta;
} LorenzConfig;

typedef struct {
    double h;
    double t0;
    double t;
    double maxT;
} TimeConfig;

typedef enum {
    EULER,
    RK4,
    ADAMS_BASHFORTH
} ODESolverEnum;

typedef Vec3D (*ODESolverFunction)(Vec3D yt, LorenzConfig *lc, double h);

typedef void (*ButtonHandler)();

typedef struct {
    Vec3D state;
    LorenzConfig lc;
    TimeConfig tc;
    ODESolverEnum os;
    double eps;
    double minPointPlotDistance;
} SimConfig;

typedef struct {
    Vec3D point;
    bool finished;
} RunResult;

typedef enum {
    RUNNING,
    NOT_RUNNING
} SimRun;

static const char *odeSolverNames[3] = {"Euler", "4th order Runge-Kutta", "Adams Bashforth"};

#endif // TYPES_H
